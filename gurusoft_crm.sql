-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 24, 2024 at 02:19 PM
-- Server version: 10.4.32-MariaDB
-- PHP Version: 8.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gurusoft_crm`
--

-- --------------------------------------------------------

--
-- Table structure for table `activity_logs`
--

CREATE TABLE `activity_logs` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `action` enum('created','updated','deleted','bitbucket_notification_received','github_notification_received') NOT NULL,
  `log_type` varchar(30) NOT NULL,
  `log_type_title` mediumtext NOT NULL,
  `log_type_id` int(11) NOT NULL DEFAULT 0,
  `changes` mediumtext DEFAULT NULL,
  `log_for` varchar(30) NOT NULL DEFAULT '0',
  `log_for_id` int(11) NOT NULL DEFAULT 0,
  `log_for2` varchar(30) DEFAULT NULL,
  `log_for_id2` int(11) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `activity_logs`
--

INSERT INTO `activity_logs` (`id`, `created_at`, `created_by`, `action`, `log_type`, `log_type_title`, `log_type_id`, `changes`, `log_for`, `log_for_id`, `log_for2`, `log_for_id2`, `deleted`) VALUES
(1, '2022-12-22 15:34:01', 1, 'updated', '', '', 504, 'a:1:{s:25:\"MAIN TEST [:1,email,0,0:]\";a:2:{s:4:\"from\";s:0:\"\";s:2:\"to\";s:15:\"DCECF@GMAIL.COM\";}}', '', 0, NULL, NULL, 0),
(2, '2023-01-31 13:42:02', 1, 'created', 'task', 'Database Structure', 1, NULL, 'project', 2, '', 0, 0),
(3, '2023-01-31 13:57:04', 507, 'updated', 'task', 'Database Structure', 1, 'a:1:{s:9:\"status_id\";a:2:{s:4:\"from\";s:1:\"1\";s:2:\"to\";s:1:\"2\";}}', 'project', 2, '', 0, 0),
(4, '2023-01-31 14:08:42', 1, 'created', 'task_comment', 'Checked the Database Structure. Looks fine.', 1, NULL, 'project', 2, 'task', 1, 0),
(5, '2023-01-31 14:08:59', 1, 'updated', 'task', 'Database Structure', 1, 'a:2:{s:4:\"sort\";a:2:{s:4:\"from\";s:1:\"0\";s:2:\"to\";s:8:\"10000000\";}s:9:\"status_id\";a:2:{s:4:\"from\";s:1:\"2\";s:2:\"to\";s:1:\"6\";}}', 'project', 2, '', 0, 0),
(6, '2023-01-31 14:16:37', 507, 'updated', 'task', 'Database Structure', 1, 'a:1:{s:9:\"status_id\";a:2:{s:4:\"from\";s:1:\"6\";s:2:\"to\";s:1:\"7\";}}', 'project', 2, '', 0, 0),
(7, '2023-01-31 14:21:56', 506, 'updated', 'task', 'Database Structure', 1, 'a:1:{s:9:\"status_id\";a:2:{s:4:\"from\";s:1:\"7\";s:2:\"to\";s:1:\"9\";}}', 'project', 2, '', 0, 0),
(8, '2023-01-31 14:22:08', 506, 'updated', 'task', 'Database Structure', 1, 'a:1:{s:9:\"status_id\";a:2:{s:4:\"from\";s:1:\"9\";s:2:\"to\";s:2:\"10\";}}', 'project', 2, '', 0, 0),
(9, '2023-01-31 14:22:34', 506, 'updated', 'task', 'Database Structure', 1, 'a:1:{s:9:\"status_id\";a:2:{s:4:\"from\";s:2:\"10\";s:2:\"to\";s:1:\"3\";}}', 'project', 2, '', 0, 0),
(10, '2024-03-19 06:44:40', 1, 'created', 'task', 'skyi', 2, NULL, 'project', 4, '', 0, 0),
(11, '2024-03-19 06:44:56', 1, 'updated', 'task', 'skyi', 2, 'a:2:{s:4:\"sort\";a:2:{s:4:\"from\";s:1:\"0\";s:2:\"to\";s:8:\"10000000\";}s:9:\"status_id\";a:2:{s:4:\"from\";s:1:\"1\";s:2:\"to\";s:1:\"2\";}}', 'project', 4, '', 0, 0),
(12, '2024-03-19 06:45:04', 1, 'updated', 'task', 'skyi', 2, 'a:1:{s:9:\"status_id\";a:2:{s:4:\"from\";s:1:\"2\";s:2:\"to\";s:1:\"4\";}}', 'project', 4, '', 0, 0),
(13, '2024-03-19 06:46:05', 1, 'created', 'task_comment', 'file for proof', 2, NULL, 'project', 4, 'task', 2, 0),
(14, '2024-03-19 06:46:22', 1, 'updated', 'task', 'skyi', 2, 'a:1:{s:9:\"status_id\";a:2:{s:4:\"from\";s:1:\"4\";s:2:\"to\";s:1:\"7\";}}', 'project', 4, '', 0, 0),
(15, '2024-03-19 06:46:35', 1, 'updated', 'task', 'skyi', 2, 'a:1:{s:9:\"status_id\";a:2:{s:4:\"from\";s:1:\"7\";s:2:\"to\";s:1:\"9\";}}', 'project', 4, '', 0, 0),
(16, '2024-03-19 06:46:41', 1, 'updated', 'task', 'skyi', 2, 'a:1:{s:9:\"status_id\";a:2:{s:4:\"from\";s:1:\"9\";s:2:\"to\";s:2:\"10\";}}', 'project', 4, '', 0, 0),
(17, '2024-03-19 06:46:43', 1, 'updated', 'task', 'skyi', 2, 'a:1:{s:9:\"status_id\";a:2:{s:4:\"from\";s:2:\"10\";s:2:\"to\";s:1:\"3\";}}', 'project', 4, '', 0, 0),
(18, '2024-03-20 06:54:41', 1, 'updated', 'task', 'skyi', 2, 'a:1:{s:9:\"status_id\";a:2:{s:4:\"from\";s:1:\"3\";s:2:\"to\";s:2:\"10\";}}', 'project', 4, '', 0, 0),
(19, '2024-03-20 06:54:46', 1, 'updated', 'task', 'skyi', 2, 'a:1:{s:9:\"status_id\";a:2:{s:4:\"from\";s:2:\"10\";s:2:\"to\";s:1:\"7\";}}', 'project', 4, '', 0, 0),
(20, '2024-03-20 06:54:56', 1, 'updated', 'task', 'skyi', 2, 'a:1:{s:9:\"status_id\";a:2:{s:4:\"from\";s:1:\"7\";s:2:\"to\";s:1:\"4\";}}', 'project', 4, '', 0, 0),
(21, '2024-03-20 06:55:01', 1, 'updated', 'task', 'skyi', 2, 'a:1:{s:9:\"status_id\";a:2:{s:4:\"from\";s:1:\"4\";s:2:\"to\";s:1:\"1\";}}', 'project', 4, '', 0, 0),
(22, '2024-04-15 10:40:38', 1, 'created', 'task', 'ioWeb3 Website', 3, NULL, 'project', 4, '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `announcements`
--

CREATE TABLE `announcements` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `description` mediumtext NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `created_by` int(11) NOT NULL,
  `share_with` mediumtext DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `files` text NOT NULL,
  `read_by` mediumtext DEFAULT NULL,
  `deleted` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `attendance`
--

CREATE TABLE `attendance` (
  `id` int(11) NOT NULL,
  `status` enum('incomplete','pending','approved','rejected','deleted') NOT NULL DEFAULT 'incomplete',
  `user_id` int(11) NOT NULL,
  `in_time` datetime NOT NULL,
  `out_time` datetime DEFAULT NULL,
  `checked_by` int(11) DEFAULT NULL,
  `note` text DEFAULT NULL,
  `checked_at` datetime DEFAULT NULL,
  `reject_reason` text DEFAULT NULL,
  `deleted` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `attendance`
--

INSERT INTO `attendance` (`id`, `status`, `user_id`, `in_time`, `out_time`, `checked_by`, `note`, `checked_at`, `reject_reason`, `deleted`) VALUES
(1, 'pending', 1, '2022-02-26 09:55:23', '2022-02-26 09:55:27', NULL, '', NULL, NULL, 0),
(2, 'pending', 1, '2022-02-26 09:55:30', '2022-02-26 09:55:34', NULL, '', NULL, NULL, 0),
(3, 'pending', 1, '2022-03-11 18:27:45', NULL, NULL, NULL, NULL, NULL, 0),
(4, 'pending', 1, '2022-03-11 17:35:21', '2022-03-11 17:35:44', NULL, '', NULL, NULL, 0),
(5, 'pending', 502, '2022-03-11 18:50:48', NULL, NULL, NULL, NULL, NULL, 0),
(6, 'pending', 502, '2022-03-11 19:22:03', NULL, NULL, NULL, NULL, NULL, 0),
(7, 'pending', 502, '2022-03-16 18:42:51', NULL, NULL, NULL, NULL, NULL, 0),
(8, 'pending', 502, '2022-03-16 18:56:34', NULL, NULL, NULL, NULL, NULL, 0),
(9, 'pending', 502, '2022-03-16 18:59:16', NULL, NULL, NULL, NULL, NULL, 0),
(10, 'pending', 502, '2022-03-16 19:01:48', NULL, NULL, NULL, NULL, NULL, 0),
(11, 'pending', 502, '2022-03-16 19:02:26', NULL, NULL, NULL, NULL, NULL, 0),
(12, 'pending', 502, '2022-03-16 19:16:57', NULL, NULL, NULL, NULL, NULL, 0),
(13, 'pending', 502, '2022-03-16 19:17:31', NULL, NULL, NULL, NULL, NULL, 0),
(14, 'pending', 502, '2022-03-16 19:22:30', NULL, NULL, NULL, NULL, NULL, 0),
(15, 'pending', 502, '2022-03-16 19:24:44', NULL, NULL, NULL, NULL, NULL, 0),
(16, 'pending', 502, '2022-03-16 19:35:27', NULL, NULL, NULL, NULL, NULL, 0),
(17, 'pending', 502, '2022-03-16 19:36:26', NULL, NULL, NULL, NULL, NULL, 0),
(18, 'pending', 502, '2022-03-16 19:46:58', NULL, NULL, NULL, NULL, NULL, 0),
(19, 'pending', 502, '2022-03-16 19:53:49', NULL, NULL, NULL, NULL, NULL, 0),
(20, 'pending', 502, '2022-03-16 19:57:10', NULL, NULL, NULL, NULL, NULL, 0),
(21, 'pending', 502, '2022-03-16 20:04:58', NULL, NULL, NULL, NULL, NULL, 0),
(22, 'pending', 502, '2022-03-16 20:07:54', NULL, NULL, NULL, NULL, NULL, 0),
(23, 'pending', 1, '2022-08-04 07:49:53', '2022-08-04 13:40:07', NULL, 'done', NULL, NULL, 0),
(24, 'pending', 1, '2022-08-04 13:40:13', '2022-12-09 10:38:49', NULL, '', NULL, NULL, 0),
(25, 'pending', 1, '2022-12-09 10:53:27', '2023-01-31 14:25:09', NULL, 'Done', NULL, NULL, 0),
(26, 'incomplete', 507, '2023-01-31 14:15:24', NULL, NULL, NULL, NULL, NULL, 0),
(27, 'pending', 1, '2023-03-14 05:53:10', '2023-03-24 09:29:25', NULL, '', NULL, NULL, 0),
(28, 'incomplete', 508, '2023-03-24 09:38:48', NULL, NULL, NULL, NULL, NULL, 0),
(29, 'incomplete', 1, '2023-03-29 07:25:38', NULL, NULL, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `certification`
--

CREATE TABLE `certification` (
  `id` int(10) NOT NULL,
  `certificationNameificationName` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `checklist_items`
--

CREATE TABLE `checklist_items` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `is_checked` int(11) NOT NULL DEFAULT 0,
  `task_id` int(11) NOT NULL DEFAULT 0,
  `sort` int(11) NOT NULL DEFAULT 0,
  `deleted` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `checklist_items`
--

INSERT INTO `checklist_items` (`id`, `title`, `is_checked`, `task_id`, `sort`, `deleted`) VALUES
(1, 'input from ceo', 1, 2, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `checklist_template`
--

CREATE TABLE `checklist_template` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `id` varchar(128) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ci_sessions`
--

INSERT INTO `ci_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('52psqab2ma9cchson1ph9nror5gv9hbe', '::1', 1713949098, 0x5f5f63695f6c6173745f726567656e65726174657c693a313731333934393039383b5f63695f70726576696f75735f75726c7c733a3135303a22687474703a2f2f6c6f63616c686f73742f63726d2f63726d2e67757275736f6674776172652e696e2f6576656e74732f63616c656e6461725f6576656e74732f6576656e74732f303f73746172743d323032342d30332d3331543030253341303025334130302532423035253341333026656e643d323032342d30352d31325430302533413030253341303025324230352533413330223b757365725f69647c733a313a2231223b),
('1e67a0c6g6u12g57g2f27rpjhkobofnl', '::1', 1713950622, 0x5f5f63695f6c6173745f726567656e65726174657c693a313731333935303632323b5f63695f70726576696f75735f75726c7c733a35333a22687474703a2f2f6c6f63616c686f73742f63726d2f63726d2e67757275736f6674776172652e696e2f7465616d5f6d656d62657273223b757365725f69647c733a313a2231223b),
('lvua2knhh47frmm244dqva3n3qd4rkjs', '::1', 1713951215, 0x5f5f63695f6c6173745f726567656e65726174657c693a313731333935313231353b5f63695f70726576696f75735f75726c7c733a35333a22687474703a2f2f6c6f63616c686f73742f63726d2f63726d2e67757275736f6674776172652e696e2f7465616d5f6d656d62657273223b757365725f69647c733a313a2231223b),
('ljkkik1crbkk55oe9evf3gmecv2ms50f', '::1', 1713951645, 0x5f5f63695f6c6173745f726567656e65726174657c693a313731333935313634353b5f63695f70726576696f75735f75726c7c733a35333a22687474703a2f2f6c6f63616c686f73742f63726d2f63726d2e67757275736f6674776172652e696e2f7465616d5f6d656d62657273223b757365725f69647c733a313a2231223b),
('d8p3ipok66plqmck1v1ch4e0hsfpgo59', '::1', 1713952051, 0x5f5f63695f6c6173745f726567656e65726174657c693a313731333935323035313b5f63695f70726576696f75735f75726c7c733a3135303a22687474703a2f2f6c6f63616c686f73742f63726d2f63726d2e67757275736f6674776172652e696e2f6576656e74732f63616c656e6461725f6576656e74732f6576656e74732f303f73746172743d323032342d30332d3331543030253341303025334130302532423035253341333026656e643d323032342d30352d31325430302533413030253341303025324230352533413330223b757365725f69647c733a313a2231223b),
('q4s9i9g8kkoatgmciek9qgchui9jn84h', '::1', 1713953283, 0x5f5f63695f6c6173745f726567656e65726174657c693a313731333935333238333b5f63695f70726576696f75735f75726c7c733a3135303a22687474703a2f2f6c6f63616c686f73742f63726d2f63726d2e67757275736f6674776172652e696e2f6576656e74732f63616c656e6461725f6576656e74732f6576656e74732f303f73746172743d323032342d30332d3331543030253341303025334130302532423035253341333026656e643d323032342d30352d31325430302533413030253341303025324230352533413330223b757365725f69647c733a313a2231223b),
('ic9snj04d41a0uhsmmgne7mu178lbm5h', '::1', 1713953607, 0x5f5f63695f6c6173745f726567656e65726174657c693a313731333935333630373b5f63695f70726576696f75735f75726c7c733a3135303a22687474703a2f2f6c6f63616c686f73742f63726d2f63726d2e67757275736f6674776172652e696e2f6576656e74732f63616c656e6461725f6576656e74732f6576656e74732f303f73746172743d323032342d30332d3331543030253341303025334130302532423035253341333026656e643d323032342d30352d31325430302533413030253341303025324230352533413330223b757365725f69647c733a313a2231223b),
('1rn3uroe7hlf7j3gcbrtcnoe5mdr8udt', '::1', 1713954872, 0x5f5f63695f6c6173745f726567656e65726174657c693a313731333935343837323b5f63695f70726576696f75735f75726c7c733a3135303a22687474703a2f2f6c6f63616c686f73742f63726d2f63726d2e67757275736f6674776172652e696e2f6576656e74732f63616c656e6461725f6576656e74732f6576656e74732f303f73746172743d323032342d30332d3331543030253341303025334130302532423035253341333026656e643d323032342d30352d31325430302533413030253341303025324230352533413330223b757365725f69647c733a313a2231223b),
('im8iggu6o3useba23nrs6ltvlrpqfupl', '::1', 1713955918, 0x5f5f63695f6c6173745f726567656e65726174657c693a313731333935353931383b5f63695f70726576696f75735f75726c7c733a35333a22687474703a2f2f6c6f63616c686f73742f63726d2f63726d2e67757275736f6674776172652e696e2f7465616d5f6d656d62657273223b757365725f69647c733a313a2231223b),
('gagf3gcrjjbnlm7beqvvkdc2od39i6qc', '::1', 1713956545, 0x5f5f63695f6c6173745f726567656e65726174657c693a313731333935363534353b5f63695f70726576696f75735f75726c7c733a35333a22687474703a2f2f6c6f63616c686f73742f63726d2f63726d2e67757275736f6674776172652e696e2f7465616d5f6d656d62657273223b757365725f69647c733a313a2231223b),
('pesnrs5uql441mkrf2d3sn0jioa0avuk', '::1', 1713957424, 0x5f5f63695f6c6173745f726567656e65726174657c693a313731333935373432343b5f63695f70726576696f75735f75726c7c733a35333a22687474703a2f2f6c6f63616c686f73742f63726d2f63726d2e67757275736f6674776172652e696e2f7465616d5f6d656d62657273223b757365725f69647c733a313a2231223b),
('0a5di7j10qghi71elef4cpies1nm95tr', '::1', 1713957725, 0x5f5f63695f6c6173745f726567656e65726174657c693a313731333935373732353b5f63695f70726576696f75735f75726c7c733a35333a22687474703a2f2f6c6f63616c686f73742f63726d2f63726d2e67757275736f6674776172652e696e2f7465616d5f6d656d62657273223b757365725f69647c733a313a2231223b),
('ifs2njol59ueqm9sgotb7d5tespm42mo', '::1', 1713958027, 0x5f5f63695f6c6173745f726567656e65726174657c693a313731333935383032373b5f63695f70726576696f75735f75726c7c733a35333a22687474703a2f2f6c6f63616c686f73742f63726d2f63726d2e67757275736f6674776172652e696e2f7465616d5f6d656d62657273223b757365725f69647c733a313a2231223b),
('5qguqoeur3b5k6kv90dtm36outgo0ueb', '::1', 1713959661, 0x5f5f63695f6c6173745f726567656e65726174657c693a313731333935393636313b5f63695f70726576696f75735f75726c7c733a35333a22687474703a2f2f6c6f63616c686f73742f63726d2f63726d2e67757275736f6674776172652e696e2f7465616d5f6d656d62657273223b757365725f69647c733a313a2231223b),
('5md02s7c1v9a0v5nvgjolab4osqn3jjp', '::1', 1713960066, 0x5f5f63695f6c6173745f726567656e65726174657c693a313731333936303036363b5f63695f70726576696f75735f75726c7c733a35333a22687474703a2f2f6c6f63616c686f73742f63726d2f63726d2e67757275736f6674776172652e696e2f7465616d5f6d656d62657273223b757365725f69647c733a313a2231223b),
('sobib0rfdtgfgb7cqg5ht8mrrj39em74', '::1', 1713960543, 0x5f5f63695f6c6173745f726567656e65726174657c693a313731333936303534333b5f63695f70726576696f75735f75726c7c733a35333a22687474703a2f2f6c6f63616c686f73742f63726d2f63726d2e67757275736f6674776172652e696e2f7465616d5f6d656d62657273223b757365725f69647c733a313a2231223b),
('o726b5qivpjm7dvm7curf8jtfgl04dr2', '::1', 1713961144, 0x5f5f63695f6c6173745f726567656e65726174657c693a313731333936303534333b5f63695f70726576696f75735f75726c7c733a35333a22687474703a2f2f6c6f63616c686f73742f63726d2f63726d2e67757275736f6674776172652e696e2f7465616d5f6d656d62657273223b757365725f69647c733a313a2231223b);

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `id` int(11) NOT NULL,
  `company_name` varchar(150) NOT NULL,
  `address` text DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `state` varchar(50) DEFAULT NULL,
  `zip` varchar(50) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `created_date` date NOT NULL,
  `website` text DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `currency_symbol` varchar(20) DEFAULT NULL,
  `starred_by` mediumtext NOT NULL,
  `group_ids` text NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  `is_lead` tinyint(1) NOT NULL DEFAULT 0,
  `lead_status_id` int(11) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `sort` int(11) NOT NULL DEFAULT 0,
  `lead_source_id` int(11) NOT NULL,
  `last_lead_status` text NOT NULL,
  `client_migration_date` date NOT NULL,
  `vat_number` text DEFAULT NULL,
  `currency` varchar(3) DEFAULT NULL,
  `disable_online_payment` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`id`, `company_name`, `address`, `city`, `state`, `zip`, `country`, `created_date`, `website`, `phone`, `currency_symbol`, `starred_by`, `group_ids`, `deleted`, `is_lead`, `lead_status_id`, `owner_id`, `created_by`, `sort`, `lead_source_id`, `last_lead_status`, `client_migration_date`, `vat_number`, `currency`, `disable_online_payment`) VALUES
(0, 'Ioweb3 technologies', NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, NULL, NULL, '', '', 0, 0, 0, 0, 1, 0, 0, '', '0000-00-00', NULL, NULL, 0),
(1, 'Chandra Prakash', 'kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-13', 'Looking in Baner location. Visiting on Saturday for Mahlunge site<br />\nSame lead shared with sayali and Amruta', '9742200132', NULL, '', '', 0, 1, 2, 1, 0, 10100000, 6, '', '0000-00-00', NULL, NULL, 0),
(2, 'akanksha dhekane ', 'kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-13', NULL, '9552095977', NULL, '', '', 0, 1, 5, 1, 0, 10000000, 6, '', '0000-00-00', NULL, NULL, 0),
(3, 'Shankar', 'Ganga Platino Phase III, Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-13', NULL, '+91 9158342868', NULL, '', '', 0, 1, 6, 1, 0, 10000000, 7, '', '0000-00-00', NULL, NULL, 0),
(4, 'Janhavi Lokhande', 'Ganga Platino Phase III, Kharad', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-14', 'Not interested to see any of the project', '+91 8459344263', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(5, 'Pari', 'Goel Ganga Altus Wing D And E, Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-14', 'Already Book Ganga Altus', '+91 9834311099', NULL, '', '', 0, 1, 7, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(6, 'Dobriya Nurdin', 'Goel Ganga Altus Wing D And E, Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-14', 'Not conn', '+91 9960786716', NULL, '', '', 0, 1, 2, 1, 0, 10200000, 7, '', '0000-00-00', NULL, NULL, 0),
(7, 'Bilkis Pathan', 'Manav ildwoods Phase 1, Waghol', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-14', 'Booking done', '+91 8805858235', NULL, '', '', 0, 1, 5, 1, 0, 10000000, 7, '', '0000-00-00', NULL, NULL, 0),
(8, 'Priyanka Khurange', 'Amanor Gateway Tower 100, Hadapsar', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-14', '3bhk (2cr.) Specifically looking at Amanora Township.<br />_x000D_\nPlease do share with sachin sir.', '+91 9860337064', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(9, 'Akshay Singh', 'Majestique Rhythm County Phase 1, Handewadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-14', 'Not reachable', '+91 986768006', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(10, 'Jayshri L', 'Sukhwani Hermosa Casa, Mundhwa', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-14', 'Not receive', '+91 8007399739', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(11, 'S Surya', 'Manav Wildwoods Phase 1, Wagholi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-14', '2 bhk <br />_x000D_\n50 L <br />_x000D_\nSunday sit visit <br />_x000D_\nManav group ', '+91 9284870400', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(12, 'Ranjit', 'Zen Estate, Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-14', NULL, '+91 8149960317', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(13, 'Ankit Pandey', 'Bramha The Collection Vadgaon Sheri', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-14', 'Not Interested', '91-7040106106', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(14, 'Pint u munda ', 'Nyati Elysia Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-14', 'Not able to contact him.', '91-7854019864', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(15, 'Prashant', 'Zen Estate', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-14', NULL, '9175670114', NULL, '', '', 0, 1, 5, 1, 0, 10100000, 6, '', '0000-00-00', NULL, NULL, 0),
(16, 'akanksha dhekane', 'Zen Estate', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-14', 'Looking for 2 bhk in kharadi Location.<br />_x000D_\nSend her details. Weekend will be possible for visit<br />_x000D_\nSame lead is with Pavan also', '9552095977', NULL, '', '', 0, 1, 5, 1, 0, 10200000, 6, '', '0000-00-00', NULL, NULL, 0),
(17, 'Harshal Lodha', '29 Gold Coast', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-14', '3 bhk <br />_x000D_\n60L<br />_x000D_\nReady to move <br />_x000D_\nDhanori vishrantwadi', '9579678865', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(18, 'Pranav Bhatnagar ', 'Majestique Rhythm County,', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-14', '2 BHK in dhanori wagholi kharadi 50 lac ', '7350005892', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(19, 'Cathy carol ', 'Mantra Montana Dhanori', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-14', '2 bhk 40 L <br />_x000D_\nKothrud ', '91-9307737854', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(20, 'Kumar Matta', 'Township Codename Pegasus Kharadi, Pune', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-14', NULL, '9967827490', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(21, 'Amit Raj ', 'Kharadi & hadapsar', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-14', 'Want property 2bhk within 70L budget, call next week to visit', '7710002927', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(22, 'Christopher Meyers', 'Majestique Rhythm County, Handewadi, Pune', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-14', 'Looking for 3bhk, 1000+ carpet', '7507347571', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(23, 'Ritika', 'Venkatesh Graffiti Elan Phase 1, Mundhwa', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-14', 'Hold searching', '+91 8698766386', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(24, 'A K Tiwari', 'Undri', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-14', 'Today visit confirm undri location', '9326343700', NULL, '', '', 1, 1, 8, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(25, 'Rahul', 'hadapsar,shewalvadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-14', 'ready to move or near poss, send newton homes details', '7709899013', NULL, '', '', 0, 1, 9, 1, 0, 0, 9, '', '0000-00-00', NULL, NULL, 0),
(26, 'shrikant', 'hadapsar,dhanori,undri', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-14', 'Vc confirm Saturday ', '9623721851', NULL, '', '', 0, 1, 9, 1, 0, 0, 9, '', '0000-00-00', NULL, NULL, 0),
(27, 'Nurjahan', 'hadapsar,handevadi,yewalevadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-14', 'Client search flot undri side ', '7218786831', NULL, '', '', 0, 1, 9, 1, 0, 0, 9, '', '0000-00-00', NULL, NULL, 0),
(28, 'Amol', 'hadapsar,undri', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-14', 'ready to move or near by poss', '7387923843', NULL, '', '', 0, 1, 1, 1, 0, 0, 9, '', '0000-00-00', NULL, NULL, 0),
(29, 'Dinesh', 'Dhanori', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-14', 'Not looking for any property ', '9860053388', NULL, '', '', 0, 1, 9, 1, 0, 0, 9, '', '0000-00-00', NULL, NULL, 0),
(30, 'Nilesh CCIE', 'Venkatesh Graffiti Elan Phase 1, Mundhwa', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-14', 'phone is always switched off', '+91 9321632410', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(31, 'Minal', 'Majestique Towers East, Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-14', '2 Bhk 70 Lac<br />_x000D_\nDetail - Serio n M.T.<br />_x000D_\nZoom Meeting- Tom (Serio)<br />_x000D_\nVisit- Sat', '+91 9403253104', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(32, 'Ganesh Joshi ', '29 gold cost dhanori', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-14', 'Call not connect', '9689894867', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(33, 'abhishek', 'Kumar Prospera Hadapsar', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-14', 'Kumar prospera, serio, Naren bliss details share', '91-8099021081', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(34, 'sanket p', '29 gold coast', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-14', '2/3 bhk <br />_x000D_\n80L <br />_x000D_\nSaturday sit visit <br />_x000D_\n29 gol cost / Ganga New Town', '9763772424', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(35, 'Kuldeep', '29 gold coast', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-14', 'Call not connect', '9158411119', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(36, 'Mangal Alandikar ', '29 gold coast', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-14', 'All ready booked ', '9130486862', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(37, 'Tuli Nag', 'Majestique Rhythm County Phase 1, Handewadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-14', 'Call after 3 pm', '+91 9665367522', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(38, 'Vaibhav', 'Gera World of Joy Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-15', 'Rowhouse<br />_x000D_\nKharadi<br />_x000D_\n 80 Lac', '91-8805894486', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(39, 'Ankit', 'VTP Township Codename Pegasus Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-15', NULL, '91-9503460718', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(40, 'Rakesh', 'Riverdale heights', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-15', 'Looking in Kharadi Location. He is in mumbai. Send him details of projects. Looking for near possisson.', '91-9029392455', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(41, 'Priyanka', 'Mantra Montana, Dhanori', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-15', 'Not Interested ..', '91-7702377144', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(42, 'Megha', 'Kumar Prospera Hadapsar', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-15', '2 BHK Magarpatta 75 lac ready to move<br />_x000D_\n', '91-8600430002', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(43, 'Mayur Pawar', 'VTP Township Codename Pegasus Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-15', NULL, '91-9921587922', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(44, 'ankush', 'Kumar Prospera Hadapsar', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-15', '2bhk, magarpatta kharadi, all project visited,', '91-9985321330', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(45, 'Vijay Kumar Verma', 'Nyati Elysia Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-15', 'Not looking for any property...', '91-9336165027', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(46, 'Sumit Deswal', 'Ganga Platino Phase III, Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-15', NULL, '+91 8437119568', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(47, 'Chirag Karia', 'Naren Bliss, Hadapsar', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-15', '2bhk, ready or nearing possession in magarpatta kharadi', '+91 7405642013', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(48, 'User5400', 'Duville Riverdale Heights, Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-15', NULL, '+91 9011975400', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(49, 'Akshay', 'Rohan Abhilasha, Wagholi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-15', 'Looking after 3/4 month ', '+91 9405802883', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(50, 'Gitika Jain', 'Majestique Marbella Phase 1, Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-15', '3Bhk 85 Lac Ready to move<br />_x000D_\nVisit- Sat', '+91 7597076778', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(51, 'Minal', 'Majestique Marbella Phase 1, Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-15', 'Yesterday done calling by Ashwini... She is taking follow-up of this click', '9403253104', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(52, 'Nagesh Patil', 'Majestique Rhythm County Phase 1, Handewadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-15', 'Sunday visit confirm Rhythm county', '+91 7249101019', NULL, '', '', 0, 1, 10, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(53, 'Anupam Guru', 'Majestique Rhythm County Phase 1, Handewadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-15', '2 BHK in Hadapsar and undari 50 lac bud', '+91 8288030114', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(54, 'Ranjit Randive', 'Zen Estate, Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-15', NULL, '+91 8408855448', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(55, 'Rani Singh', 'Majestique Rhythm County,', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-15', NULL, '9096049362', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(56, 'Nitesh', 'Majestique Rhythm County Handewadi, Pune', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-15', NULL, '9767946463', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(57, 'Sikandar Pathan Aavas Financier Ltd', 'Mantra 29 Gold Coast, Dhanori', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-15', 'Fake lead-- <br />_x000D_\nHe is fainancer', '+91 9730747474', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(58, 'Jogendra Kumar', 'Shapoorji Joyville Hadapsar Annexe Shewalewadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-15', NULL, '91-9098516279', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(59, 'Amul kachhwaha ', 'Nyati Elysia Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-15', 'Not interested currently', '91-6261718072', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(60, 'NALINI PAWAR', 'Venkatesh Graffiti Elan Phase 1, Mundhwa', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-15', 'Already booked at Sahkarnagar', '+91 7350907272', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(61, 'Shubhangi Dongare', 'Unique Legacy Majestic, Mundhwa', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-15', 'raghvendra nagar -resale booked', '+91 9673909541', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(62, 'Sameer Bhate', 'Naren Bliss, Hadapsar', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-15', 'kharadi -done already', '+91 8975905700', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(63, 'Dattraya Bhausaheb Shelar', 'Sukhwani Hermosa Casa, Mundhwa', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-15', 'no response so long', '+91 9975928883', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(64, 'Sandeep ahluwalia', '29 Gold Coast', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-15', 'Not Interested<br />_x000D_\n', '9407982487', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(65, 'Manish ', 'Zen Estate', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-15', 'He seen most of the project. Call him @4pm for visit.<br />_x000D_\nVTP altire and Serio<br />_x000D_\n3bhk in kharadi Location. <br />_x000D_\n1cr budget', '9921619423', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(66, 'Manish Holey', 'Duville Riverdale Heights, Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-15', NULL, '+91 9611633443', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(67, 'Sumit Kumar', 'Nyati Elysia Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-15', NULL, '91-9536716731', NULL, '', '', 0, 1, 7, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(68, 'Vikash Kumar', 'Shapoorji Joyville Hadapsar Annexe Shewalewadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-15', 'Not interested', '91-7322950720', NULL, '', '', 0, 1, 7, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(69, 'Ramchandra', 'Nyati Elysia Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-15', NULL, '91-9116738668', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(70, 'nitinchaougule', 'Shapoorji Joyville Hadapsar Annexe Shewalewadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-15', 'Fake lead ', '91-9022686833', NULL, '', '', 0, 1, 7, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(71, 'Dinesh Mengu4', 'Naren Bliss, Hadapsar', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-15', '2 BHK in Magarpatta 65 lac ready to move', '+91 8333856006', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(72, 'sunil kori ', 'Nyati Elysia Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-15', NULL, '91-9545725002', NULL, '', '', 0, 1, 7, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(73, 'Yogesh Pardeshi', 'Kohinoor Zen Estate Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-15', 'Client looking for 1bhk. Interested hot customer', '91-9022005125', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(74, 'Deven Sabadra', 'Unique Legacy Majestic, Mundhwa', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-15', 'Client interested in vimannagar loaction ', '+91 8329711094', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(75, 'Rajani Konde', 'Nyati Evolve I, Mundhwa', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-15', 'not looking now', '+91 9763812662', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(76, 'NAVEENA SNAIL News And Quotes', 'Unique Legacy Majestic, Mundhwa', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-15', NULL, '+91 8095918778', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(77, 'Aashish', 'Unique Legacy Majestic, Mundhwa', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-15', 'Banker', '+91 8208802026', NULL, '', '', 0, 1, 7, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(78, 'Sumeet Chauhan', 'Mantra 29 Gold Coast, Dhanori', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-16', 'Plan Drop ..', '+91 7508197678', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(79, 'SAMTA SINHA', 'Rohan Abhilasha, Wagholi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-16', NULL, '+91 9168687716', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(80, 'Sangram Babar', 'VTP Township Codename Pegasus, Manjari Khurd', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-16', NULL, '+91 7350128743', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(81, 'Mukesh Gaba', 'Ganga Serio, Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-16', NULL, '+91 8390270787', NULL, '', '', 0, 1, 7, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(82, 'Deeps H', 'Goel Ganga Altus Wing D And E, Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-16', 'Client is in Nashik coming next month', '+91 9096296224', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(83, 'Ankit Pandey', 'K City Keshav Nagar', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-16', 'Not connected call, will call back again', '91-8554849405', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(84, 'Swalia', 'VTP Township Codename Pegasus Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-16', NULL, '91-7307571471', NULL, '', '', 0, 1, 7, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(85, 'User', 'VTP Township Codename Pegasus Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-16', 'Not received call', '91-7620141989', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(86, 'Sulendra ', 'Nyati Elysia Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-16', NULL, '91-8630625020', NULL, '', '', 0, 1, 7, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(87, 'Suhas Shete', 'Majestique Towers Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-16', NULL, '91-8600594219', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(88, 'Rohit Kadlag', 'Voski Emerald II, Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-16', NULL, '+91 9529374147', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(89, 'Suvarna Karale', 'Ganga Platino Phase III, Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-16', NULL, '+91 7507498666', NULL, '', '', 0, 1, 8, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(90, 'Abhishek Tamrakar', 'Nyati Elysia Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-16', 'He is already having a word with other. CP not interested to talk with any more', '91-9036315508', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(91, 'komal ', '29 Gold Coast', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-16', '2 bhk 60 L <br />_x000D_\nSaturday Sunday sit visit ', '9823608007', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(92, 'Vishal', 'Majestique Rhythm County Phase 1, Handewadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-16', '2 bhk in undari ready to move', '+91 9970193554', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(93, 'Sumaiya Khan', '29 Gold Coast', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-16', 'Not interested ', '9970196311', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(94, 'Narayan Pawale', 'Manav Wildwoods Phase 1, Wagholi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-16', NULL, '+91 9766711129', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(95, 'Pushpendra Gautam', 'Rohan Abhilasha, Wagholi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-16', '3 bhk <br />_x000D_\n80 L <br />_x000D_\nReady possession<br />_x000D_\nVtp and rohan abhilasha ', '+91 8770677862', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(96, 'Jyoti Pawar', 'Sukhwani Hermosa Casa, Mundhwa', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-16', '2 BHK, magarpatta, 55L', '+91 8459544701', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(97, 'Anita Jain', '29 Gold Coast', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-16', 'Call Not Received ', '9403248308', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(98, 'Amol', 'Shubh Shagun, Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-16', 'Call him after 1hr/ not received call', '+91 9049897866', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(99, 'Pvb', 'VTP Purvanchal, Kesnand', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-16', '1 bhk 26 L <br />_x000D_\nUandri ', '+91 9850603485', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(100, 'Madhuri Shah', 'Godrej Boulevard, Manjari Budruk', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-16', 'Location- Hadapsar/ Manjari<br />_x000D_\nUnit- 2bhk<br />_x000D_\nBud- 50 lc<br />_x000D_\nPlease do share with respected person', '+91 7387279941', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(101, 'Mohit', 'Nyati Elysia Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-16', NULL, '91-7415212085', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(102, 'Anita Jain ', '29 gold coast', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-16', NULL, '9403248308', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(103, 'Kishore', '29 Gold Coast', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-16', 'Charoli<br />_x000D_\n3 BHK 65 L<br />_x000D_\nAfter Aug-15 come pune ', '9970945486', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(104, 'Siddharth Patil', 'VTP Township Codename Pegasus Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-17', NULL, '91-9762466364', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(105, 'Amirsohail Bashir damte ', 'Shapoorji Joyville Hadapsar Annexe Shewalewadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-17', 'Already visited All property', '91-9923748748', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(106, 'Mahesh Jagtap ', 'Gera World of Joy Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-17', 'He already visited Vtp/ Riverdale/magistic tower.<br />_x000D_\nSend him details of Godrej/ Zen Estate', '91-8446188569', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(107, 'Abhijit Ghorpade ', 'VTP Township Codename Pegasus Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-17', NULL, '91-9689473028', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(108, 'rohit', 'Nyati Elysia Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-17', NULL, '91-9920975974', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(109, 'Amit D', 'Gera World of Joy Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-17', 'Interested in only Gera world of Joy. ', '91-7030014642', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(110, 'yojna ', 'Gera World of Joy Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-17', NULL, '91-8446116170', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(111, 'Vishal Kale', 'Manav Wildwoods Phase 1, Wagholi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-17', 'Call Not Received', '+91 9823063625', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(112, 'Anindita Mukherjee', 'Venkatesh Graffiti Elan Phase 1, Mundhwa', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-17', 'call disconnected', '+91 9811150918', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(113, 'Sanjay Agarwal', 'Majestique Marbella Phase 1, Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-17', NULL, '+91 8180993568', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(114, 'Vaishnavi', 'Duville Riverdale Heights, Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-17', 'Not searching for property', '+91 7665436155', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(115, 'Girish Dilip Patil', 'Majestique Marbella Phase 1, Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-17', NULL, '+91 9579389435', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(116, 'CID ', 'Voski Emerald II, Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-17', NULL, '+91 8668650784', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(117, 'Priya', 'Manav Wildwoods Phase 1, Wagholi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-17', '3 Pm call beak ', '+91 8806792999', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(118, 'Swapnil', 'Venkatesh Graffiti Elan Phase 1, Mundhwa', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-17', NULL, '+91 9890322998', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(119, 'sayali', 'Shapoorji Joyville Hadapsar Annexe', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-17', 'Call not receive ', '91-8600577467', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(120, 'Digvijay Kulkarni', 'Rohan Abhilasha Wagholi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-17', '2 BHK Nearing Possession<br />_x000D_\n50 L<br />_x000D_\nSangali ', '91-9422159570', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(121, 'sameer r bhate', 'Majestique Towers Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-17', 'In kharadi most of the project he already seen.looking for near possisson project', '91-9367712101', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(122, 'Priyanka Sherkhane', 'Mantra Insignia Phase 2, Mundhwa', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-17', 'Near viman Nagar, 2bhk, ready or U.C, send parsun sarvam details, currently out of Pune', '+91 8369066768', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(123, 'UMESH NIMBALKAR', 'Rohan Abhilasha, Wagholi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-17', 'Not interested', '+91 9689211588', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(124, 'Rahul Kataria ', 'Ganga Platino Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-17', NULL, '91-9158003703', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(125, 'Mangesh ', 'K City Keshav Nagar', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-17', 'already visited with us kcity, mantra, unique legacy. plan changed for now about property', '91-9975294648', NULL, '', '', 0, 1, 8, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(126, 'Ajay Moorthy', 'Kumar Prospera Hadapsar', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-17', '2 bhk Magarpatta ready to move 65 lac bud', '91-8861155366', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(127, 'Arabinda Ghosh', 'Unique Legacy Majestic, Mundhwa', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-17', 'Not interested for property', '+91 8378987429', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(128, 'Amol Chaudhari', 'Majestique Towers East, Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-17', NULL, '+91 9823362982', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(129, 'Anil', 'Kumar Prospera Hadapsar', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-17', 'Not interested ', '91-9834699795', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(130, 'Puneet', 'Duville Riverdale Heights Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-17', NULL, '91-9335409404', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(131, 'Housing.com', 'NULL', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-17', 'NULL', 'NULL', NULL, '', '', 0, 1, 1, 1, 0, 0, 10, '', '0000-00-00', NULL, NULL, 0),
(132, 'Shreyash Naikwadi', '29 Gold Coast', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-17', '3 bhk 1. Cr <br />_x000D_\nDhanori', '7249496749', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(133, 'Supriya', '29 Gold Coast', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-17', NULL, '9049257772', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(134, 'DINKAR KHANDE ', 'Gera World of Joy Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-17', 'Not interested', '91-9158414001', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(135, 'Sp Momin', 'VTP Purvanchal, Kesnand', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-18', NULL, '+91 9999415298', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(136, 'Waseem Khan', 'Geras World Of Joy S, Wagholi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-18', NULL, '+91 9112066601', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(137, 'Shashank Kamath', 'Ganga Platino Phase III, Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-18', NULL, '+91 9900246996', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(138, 'SUNNY BAVISKAR', 'Voski Emerald II, Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-18', NULL, '+91 9561511484', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(139, 'Khedkar', 'VTP Purvanchal, Kesnand', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-18', 'Call not connect', '+91 9420167043', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(140, 'Prashant Prakash Pathak', 'VTP Purvanchal, Kesnand', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-18', NULL, '+91 9860210195', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(141, 'Amit', 'Sukhwani Hermosa Casa, Mundhwa', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-18', 'Plan hold<br />_x000D_\n', '+91 8888846429', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(142, 'Yaswanth Jayakumar', 'BramhaCorp The Collection, Wadgaon Sheri', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-18', 'call at tomm', '+91 9884077502', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(143, 'Ankita Chandekar', 'Shubh Shagun, Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-18', 'Send her a detail od Ganga Newtown. She already seen 29 gold coast.', '+91 7387414338', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(144, 'jainraj', 'Unique Legacy Majestic, Mundhwa', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-18', 'Not interested', '+91 9970430973', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(145, 'Nidhi', 'Goel Ganga Altus Wing D And E, Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-18', NULL, '+91 9958491279', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(146, 'Amit Purty', 'Riverdale Residences I, Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-18', 'He is quite interested 4.5 bhk... Send him the details.. Arrange VC. He Live in Hyderabad', '+91 9106335643', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(147, 'R Naik', 'Ganga Serio, Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-18', NULL, '+91 9970320163', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(148, 'Priyanka Rai', 'Mantra Montana Dhanori', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-18', NULL, '91-8888606644', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(149, 'Piyush Chowdhary ', 'Kumar Prospera Hadapsar', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-18', 'Cl searching rentel property ', '91-9830270601', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(150, 'Maya', 'Satyam Niranjani,Hadapsar', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-18', 'CL searching property hadapsar manjri 1bhk 450√ó500 carpet buget 25lac ready to move', '91-9604397323', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(151, 'Akhilesh Singh', 'Kumar Prospera Hadapsar', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-18', 'Hadpasar, 2bhk, 70L', '91-8554842644', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(152, 'Kiran Lokhande', 'Majestique Towers Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-18', NULL, '91-9860204324', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(153, 'Saurav', 'Ganga Serio Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-18', NULL, '91-9834879400', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(154, 'Manish singla', 'Gera World of Joy Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-18', 'Looking for Plot in Dhanori/ wagholi/lohegav location.<br />_x000D_\n', '91-9552186200', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(155, 'Abhishek Sahu', 'VTP Township Codename Pegasus Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-18', NULL, '91-8793544616', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(156, 'Vjp', 'VTP Township Codename Pegasus Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-18', 'Post pond his place', '91-9545228745', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(157, 'Shweta Sharma ', 'Gera World of Joy Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-18', 'Looking for Row house in 50lc budget. Location comfort with any like kharadi/wagholi....', '91-8850650287', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(158, 'Mahesh kote', 'Rohan Abhilasha Wagholi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-18', NULL, '91-9975274984', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(159, 'kv rao', '29 Gold Coast', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-18', NULL, '9966009850', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(160, 'santosh patil', 'Mantra Montana Dhanori', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-18', '1bhk <br />_x000D_\n30 L <br />_x000D_\nDhanori <br />_x000D_\nNearing possession ', '91-9767102764', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(161, 'Mitul', 'Mantra 29 Gold Coast Dhanori', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-18', NULL, '91-8377973676', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(162, 'Neeraj Haval', 'Naren Bliss Hadapsar', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-18', 'want to close the deal at naren bliss but funds issue so plan kept on hold', '91-7507302049', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(163, 'Ashish Bedre', 'Godrej Boulevard, Manjari Budruk', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-18', 'Send him the details of VtP godrej. Call him on Wednesday<br />_x000D_\nVisit done in Godrej/VTP/ mantra wonderland/anutham/laxmi ', '+91 8446050800', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(164, 'ASHOK', 'Mantra Insignia Phase 2, Mundhwa', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-18', 'disconnecting call', '+91 9604508243', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(165, 'Aditya Thakare', 'Kohinoor Zen Estate Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-18', NULL, '91-9604254607', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(166, 'Amol Hegana', 'Shubh Shagun, Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-18', NULL, '+91 9766378149', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(167, 'Swapnil D', 'Sukhwani Hermosa Casa, Mundhwa', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-18', 'booked somewhere else', '+91 9922990617', NULL, '', '', 0, 1, 8, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(168, 'Rahul Lawrence', 'Naren Bliss,Hadapsar, Pune', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-18', NULL, '9762542057', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(169, 'Harsha ', 'Zen Estate, Kharadi, Pune', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-18', NULL, '8669075379', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(170, 'trupti ', 'Ganga Platino Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-18', NULL, '91-9923523838', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(171, 'Swapnil Kamarikar ', 'Majestique Rhythm County, Handewadi, Pune', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-18', 'no answer', '9561918381', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(172, 'Hussain Abbas', 'Nyati Elysia Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-18', NULL, '91-9950882985', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(173, 'Deepa', 'Naren Bliss Hadapsar', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-18', 'Not receive call ', '91-8169951599', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(174, 'Mrs jamal', 'Kohinoor Zen Estate Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-18', NULL, '91-7875276216', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(175, 'Pooja', '29 Gold Coast,', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-18', 'Call not canect', '7028417479', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(176, 'Amit Kalinge', 'Naren Bliss, Hadapsar', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-18', NULL, '+91 9112145912', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(177, 'Saba', 'Sukhwani Hermosa Casa, Mundhwa', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-19', 'She is broker', '+91 9503329186', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(178, ' chaudhari ', 'Prasun Sarvam Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-19', NULL, '91-8275313404', NULL, '', '', 1, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(179, 'Santosh Gujar', 'Gada Anutham Hadapsar', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-19', 'Not answer', '91-7057936531', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(180, 'Krishnakant', 'Kumar Prospera Hadapsar', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-19', 'Call back 5pm', '91-7776919003', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(181, 'User', 'Gera World of Joy Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-19', NULL, '91-8279477342', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(182, 'Anupam', 'Kumar Prospera Hadapsar', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-19', 'Not interested', '91-9620208416', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(183, 'valkal bairagi', 'Rohan Abhilasha Wagholi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-19', '3 bhk <br />_x000D_\n70L <br />_x000D_\nWagholi/dhanori ', '91-9713694609', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(184, 'Priyanka Kumari', 'Mantra Insignia, Keshav Nagar, Pune', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-19', 'no response', '7263033767', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(185, 'Juhi', 'Mantra Montana Dhanori', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-19', 'Ashvini led pass ', '91-8698059350', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(186, 'Sai Raj', 'VTP Township Codename Pegasus Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-19', 'Switch off', '91-7719040204', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(187, 'Abhijit', 'Kolte Patil Ivy Nia Wagholi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-19', NULL, '91-7875690452', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(188, 'Vinod Parashar', 'Nyati Evolve I, Mundhwa', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-19', 'Looking in baner only', '+91 7976023867', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(189, 'Abhijeet', 'Mantra Montana Dhanori', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-19', NULL, '91-9834125267', NULL, '', '', 0, 1, 7, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(190, 'Bhushan Parate', 'Goel Ganga Altus Wing D And E, Kharad', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-19', NULL, '+91 9021146604', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(191, 'Mahesh', 'Ganga Serio, Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-19', 'He already booked in Riverdale Highest project', '+91 9561725908', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(192, 'Vijay Lanke', 'VTP Purvanchal, Kesnand', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-19', '1 bhk <br />_x000D_\n30L wagholi ', '+91 9767490380', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(193, 'Vikas Joshi', 'Nyati Evolve I, Mundhwa', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-19', '3bhk in kharadi reputed builder bud 1 cr.<br />_x000D_\n<br />_x000D_\nBujte:- 1.+ Cr<br />_x000D_\n<br />_x000D_\n<br />_x000D_\nCarpet:- 1100+<br />_x000D_\n<br />_x000D_\nPassion :- nearing.<br />_x000D_\n<br />_x000D_\n<br />_x000D_\nVisit plan:- August:- 1week.come Pune<br />_x000D_\n', '+91 9518537246', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(194, 'shital patil ', 'Mantra Montana Dhanori', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-19', NULL, '91-9673750127', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(195, 'Nilesh Giri Goswami', 'Majestique Marbella Phase 1, Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-19', 'Client is interested only in Magistic Marbella project.looking forward to new launch of marbella', '+91 8698587700', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(196, 'Mahadev', NULL, 'Pune', 'Maharashtra', '411014', 'India', '2021-07-20', NULL, NULL, NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(197, 'User8436', 'Duville Riverdale Heights, Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-20', NULL, '+91 9373988436', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(198, 'Ravi', 'VTP Purvanchal, Kesnand', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-20', '2 BHK Ready To Move ', '+91 9764006881', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(199, 'Vardhaman', 'VTP Purvanchal, Kesnand', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-20', '2&3 BHK Required , Ready & Nearing passion<br />_x000D_\n<br />_x000D_\nBujte:- 55lac <br />_x000D_\nArea :- KD, wagoli <br />_x000D_\n<br />_x000D_\nDitels send.<br />_x000D_\n<br />_x000D_\nVisit plan Only Sunday.<br />_x000D_\n<br />_x000D_\n', '+91 9225543015', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(200, 'Nilima ', 'Rhythm County', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-20', NULL, '8857815688', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(201, 'Ritesh', 'Naren Bliss, Hadapsar, Pune', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-20', 'Already final in dhankavadi', '7775813688', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(202, 'Amrin memon', 'Kumar Prospera Hadapsar', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-20', 'Call not receive ', '91-7057509348', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(203, 'Dillip Meher ', 'Kohinoor Zen Estate Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-20', NULL, '91-9823330408', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(204, 'ridhika ', 'VTP Township Codename Pegasus Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-20', NULL, '91-9403131623', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(205, 'atish ', 'Duville Riverdale Residences Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-20', NULL, '91-7709335171', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(206, 'Anupama Shukla', 'Rohan Abhilasha Wagholi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-20', 'CP', '91-8446413579', NULL, '', '', 0, 1, 7, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(207, 'Hemali', 'Naren Bliss, Hadapsar', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-20', NULL, '+91 9503057839', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(208, 'Aishwarya Bhandari', 'Kumar Prospera Hadapsar', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-20', 'Cp Keshavnagar', '91-9511692381', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(209, 'Rushikesh', 'Kohinoor Zen Estate Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-20', NULL, '91-8796803313', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(210, 'Chavan', 'VTP Purvanchal, Kesnand', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-20', '1 BHK Ready to move near possession<br />_x000D_\nWagholi 20L<br />_x000D_\n', '+91 8208474293', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(211, 'yogesh ', 'Ganga Platino Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-20', NULL, '91-7095595747', NULL, '', '', 0, 1, 7, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(212, 'Mohamediqbal Umarkhan Got', 'Shapoorji Joyville Hadapsar Annexe Shewalewadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-20', NULL, '91-8177868605', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(213, 'Ashutosh chauhan', 'Majestique Towers Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-20', NULL, '91-7746882140', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(214, 'Nilima', 'Shubh Shagun, Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-20', NULL, '+91 9860125154', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(215, 'Monali ', 'Shapoorji Joyville Hadapsar Annexe Shewalewadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-20', '2 bhk Hadapsar 55lac', '91-8830737561', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(216, 'Atul Gadekar', 'Venkatesh Graffiti Elan Phase 1, Mundhwa', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-20', 'Gold towers fimalized', '+91 9922614488', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(217, 'Sayali Sheth', 'VTP Purvanchal, Kesnand', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-20', NULL, '+91 7020800963', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(218, 'Rohidas Bhapkar', 'VTP Purvanchal, Kesnand', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-20', NULL, '+91 9764849842', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(219, 'Triveni', 'Mantra Montana Dhanori', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-20', '1 BHK Danori<br />_x000D_\n25- 30 L<br />_x000D_\nunder construction<br />_x000D_\n', '91-9588603429', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(220, 'Dattatray', 'Rhythm County', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-21', 'Not interested not looking for any property', '9021185246', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(221, 'Valmik Jagdale', 'Township Codename Pegasus, Kharadi, Pune', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-21', NULL, '8806756703', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(222, 'Santosh Shinde', 'Gera World of Joy Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-21', NULL, '91-9096176999', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0);
INSERT INTO `clients` (`id`, `company_name`, `address`, `city`, `state`, `zip`, `country`, `created_date`, `website`, `phone`, `currency_symbol`, `starred_by`, `group_ids`, `deleted`, `is_lead`, `lead_status_id`, `owner_id`, `created_by`, `sort`, `lead_source_id`, `last_lead_status`, `client_migration_date`, `vat_number`, `currency`, `disable_online_payment`) VALUES
(223, 'Sudeep ', 'Ganga Platino Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-21', NULL, '91-9172839999', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(224, 'Salunke Santosh Ramdas', 'Kolte Patil Ivy Nia Wagholi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-21', '1bhk<br />_x000D_\nWagholi<br />_x000D_\nMonday sit visit', '91-9892726409', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(225, 'navkar navkar', 'Gera World of Joy Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-21', NULL, '91-8793175181', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(226, 'Akshay mhalaskar', 'Kolte Patil Ivy Nia Wagholi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-21', 'Call not connect', '91-7756045251', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(227, 'Keshabdatt Joshi', 'Kumar Prospera Hadapsar', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-21', 'Not interested', '91-7972158435', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(228, 'Kailash', 'VTP Township Codename Pegasus, Manjari', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-21', NULL, '+91 9588265935', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(229, 'Sunny Nagdevi', 'Duville Riverdale Heights, Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-21', NULL, '+91 9220557435', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(230, 'Anurag Shukla', 'Rohan Abhilasha, Wagholi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-21', 'Call not connect', '+91 7875654156', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(231, 'Bharat Bhanushali', 'Manav Wildwoods Phase 1, Wagholi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-21', '2BHK Requirement.<br />_x000D_\n<br />_x000D_\n<br />_x000D_\n50lac.. <br />_x000D_\nArea:- KD , wagoli <br />_x000D_\n<br />_x000D_\n<br />_x000D_\nProject Ditels send in <br />_x000D_\nAll.<br />_x000D_\n<br />_x000D_\n<br />_x000D_\nDotter ke liye search in property.<br />_x000D_\nClient staying Mumbai..<br />_x000D_\n<br />_x000D_\n<br />_x000D_\nFollow Continue......<br />_x000D_\n<br />_x000D_\nClient : -. Hot...', '+91 9922553769', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(232, 'Pravinkumar Baburao Jadhav', 'Venkatesh Graffiti Elan Phase 1, Mundhwa', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-21', 'Keshavnagar 2 bhk 55 lac', '+91 9372142365', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(233, 'Kiran', 'Majestique Rhythm County Phase 1,', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-21', '2bhk, katraj kondhwa road,', '+91 8855996867', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(234, 'Purva Thakur', 'Majestique Rhythm County Phase 1,', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-21', NULL, '+91 8087380426', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(235, 'Shahanawaj Shaikh', 'Majestique Towers Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-21', 'Looking for 2bhk (70lc) pune solapur road. Please do share the lead .', '91-8379007979', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(236, 'Muskan', 'Majestique Rhythm County, Handewadi, Pune', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-21', NULL, '7620470025', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(237, 'Anil trakroo', 'Mantra Insignia', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-21', NULL, '9422316290', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(238, 'Rakesh Sahu', 'BramhaCorp The Collection, Wadgaon Sheri', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-21', 'He is already seen the project from othe CP. Don\'t want to see from our end', '+91 9007899339', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(239, 'Sharad Rastogi', 'Manav Wildwoods Wagholi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-21', 'Phone sweech  office..<br />_x000D_\n<br />_x000D_\nCall not canting.', '91-7083565012', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(240, 'Tyagraj Keer', 'Gera World of Joy Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-21', NULL, '91-8019130241', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(241, 'Rachita Pillai', 'Majestique Towers East Wing B, Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-21', NULL, '+91 9168024646', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(242, 'User6703', 'Godrej Boulevard, Manjari Budruk', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-21', 'Not received call/ switch off/not received call', '+91 8806756703', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(243, 'Abhay Kolapkar', 'Majestique Towers Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-21', NULL, '91-7387664951', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(244, 'Ramesh', 'VTP Purvanchal, Kesnand', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-21', '1bhk <br />_x000D_\n25L <br />_x000D_\n sit visit done oxy Desire', '+91 9403659323', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(245, 'Deepika', 'Geras World Of Joy S, Wagholi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-21', NULL, '+91 7568501651', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(246, 'Hrishikesh Dange', 'Rohan Abhilasha Wagholi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-21', 'Friends Regarding search project.<br />_x000D_\n& Future planning.<br />_x000D_\n<br />_x000D_\nClient:-  Cool...  <br />_x000D_\n', '91-9764350530', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(247, 'nitin ', 'Kumar Prospera Hadapsar', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-22', '3bhk, magarpatta, 1cr, ready possession', '91-9096133311', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(248, 'Ganesh', 'Venkatesh Graffiti Elan Phase 1, Mundhwa', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-22', ' then newplan, resale ealier now', '+91 7720094552', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(249, 'Sanket', 'Unique Legacy Majestic, Mundhwa', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-22', 'Already booked at amnora in resale', '+91 9850665757', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(250, 'Sudhir Jena', 'Majestique Marbella Phase 1, Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-22', 'Visited with me ivy nia 1 bhk 2 units want to visit gulmohar also', '+91 7774016383', NULL, '', '', 0, 1, 8, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(251, 'Rajendra Bhavsar', 'Majestique Towers East, Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-22', 'Looking for ready to move property in Viman Nagar.<br />_x000D_\nSpecifically in Rohan Mithila.(A to I) bud- 1.15 cr', '+91 9422751999', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(252, 'Nitu', 'Duville Riverdale Residences Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-22', 'Looking in Kharadi Location..<br />_x000D_\n3bhk.... Send her the details', '91-9819303320', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(253, 'Sayyed Shafi', 'Ganga Serio Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-22', 'CP (flying Real estate)', '91-9967773524', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(254, 'Prerana Kothawade', 'Majestique Towers East Wing B, Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-22', 'Not interested', '+91 8411008213', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(255, 'Amit Kunar', 'Goel Ganga Altus Wing D And E, Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-22', 'Client is mostly looking for ready to move property. All most all projects he seen in kharadi Location. Bud-70-75 lc', '+91 9689870080', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(256, 'Mayur', 'Township Codename Pegasus,', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-22', 'Not interested', '8669919936', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(257, 'Aalia Raashid', 'Manav Wildwoods Phase 1, Wagholi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-22', 'All ready final', '+91 8830632127', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(258, 'Soniya Chabukswar ', 'Township Codename Pegasus', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-22', 'Looking in Keshav nagar/ Mundva / Hadapsar<br />_x000D_\n1bhk- 30lc ready to move in property', '8149622512', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(259, 'Ritu ', '29 Gold Coast', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-22', NULL, '9963775528', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(260, 'Dipali Thakar', 'Sukhwani Hermosa Casa, Mundhwa', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-22', 'Want 3bhk ready to move in only within 1CR in magarpatta area', '+91 9032306649', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(261, 'Manoj Sonawane', 'Riverdale Residences I, Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-22', 'Not looking for any property', '+91 9923656565', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(262, 'Mayur Baliram Dalvi', 'VTP Purvanchal, Kesnand', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-22', 'Aurangabad surch in property', '+91 7666598752', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(263, 'Anyata Singh Jamwal', 'Riverdale Residences I, Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-22', 'Mostly looking in Trinity tower and Gera song of joy..<br />_x000D_\n3bhk ', '+91 9545085566', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(264, 'Preetam Samtani', 'Rhythm County', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-22', '2bhk, ready possession, undri, 40 to 45 lac budget, out of Pune, call September', '9960611765', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(265, 'Srinivas', 'Township Codename Pegasus', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-22', 'Looking property for Hyderabad City', '8978888423', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(266, 'divya', 'Naren Bliss', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-22', '3bhk, ready possession, budget 1.3cr', '7620638436', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(267, 'Sanchay Kumar', 'Majestique Rhythm County', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-22', 'Not interested', '6200322889', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(268, 'Savita chavan ', 'Township Codename Pegasus', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-22', 'Cut the call while taking not interested', '8007087008', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(269, 'Aadesh Ganesh Uttekar', 'Township Codename Pegasus', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-22', 'Looking for 2bhk (38lc) in lohagav and Dhanori location', '9623913641', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(270, 'joy ', 'Majestique Rhythm County', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-22', '2bhk, undri, share arv royal details', '7276119960', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(271, 'Anurudh R', 'Majestique Rhythm County', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-22', '2bhk, budget 40L, Lula Nagar, wanwadi, handewadi road', '9029750193', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(272, 'Harsimran', 'Mantra Insignia', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-22', 'booked resale at dhanori', '9781390714', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(273, 'Saiee Kulkarni', 'Mantra Insignia', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-22', NULL, '9156564011', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(274, 'Vishal Gavali ', 'Majestique Rhythm County', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-22', '2bhk , manjri road , suggest shiv zen world', '9975701148', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(275, 'Ajinkya patil', 'Mantra Insignia', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-22', 'due to job , change in plan, postponed for year', '8796573039', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(276, 'Ranjana Tamhane', 'Township Codename Pegasus', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-22', 'Mundva / Magarpatta  looking for 1bhk/ studio appartment. Ready to move property needed', '8828015650', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(277, 'Mukta', 'Mantra Insignia', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-22', 'always plan cancelling for visiting site', '9822812863', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(278, 'Meghe adsul', 'Township Codename Pegasus', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-22', 'She poste pond her plann for 1 yr', '9373341039', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(279, 'Namrata Barethiya', 'Ganga Serio, Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-22', 'Location- Kharadi<br />_x000D_\nUnit-3bhk<br />_x000D_\nBud-90lc<br />_x000D_\nLooking for ready to move property. Already visited Serio', '+91 8007846877', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(280, 'Rahul', 'Shubh Shagun, Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-22', 'Not interested...', '+91 8928647764', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(281, 'Neeta', '29 Gold Coast', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-22', NULL, '9699621471', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(282, 'A r kawde', 'Naren Bliss', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-22', NULL, '9225511025', NULL, '', '', 1, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(283, 'Nidhi dubey', 'Ganga Platino Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-22', 'Location-kharadi<br />_x000D_\nUnit-3.5/4.5 bhk<br />_x000D_\nBud- 1.60cr<br />_x000D_\nMostly looking for ready to move property', '91-8888679836', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(284, 'Aadesh', 'Nyati Evolve I, Mundhwa', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-22', 'Looking for ready to move property in Kalyani Nagar<br />_x000D_\n3bhk- 1cr budget', '+91 9881477225', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(285, 'Vinod Pokharna', 'Nyati Evolve I, Mundhwa', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-22', 'Client is looking for ready to move property in Kalyani Nagar. 3 bhk budget around 1cr', '+91 9822258333', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(286, 'avi ', 'Mantra Insignia', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-23', '2bhk, Keshav Nagar, 50L', '9960313636', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(287, 'Aditya Panadare', 'Zen Estate,', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-23', 'NA', '8149516548', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(288, 'Priya Daundmath', 'Zen Estate', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-23', 'Looking for ready to move property. <br />_x000D_\nIn Kharadi Location.<br />_x000D_\n3bhk - 1cr bud.', '8879886181', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(289, 'Robbin Joseph', 'Geras World Of Joy S, Wagholi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-23', 'NA', '+91 9623436111', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(290, 'Akshay Randhir', 'VTP Purvanchal, Kesnand', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-23', '1 bhk <br />_x000D_\nNearing Poss<br />_x000D_\n22 L<br />_x000D_\nUndri ', '+91 9921843481', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(291, 'Ashwini Shirbhate', 'Rohan Abhilasha, Wagholi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-23', '2bhk <br />_x000D_\n50L <br />_x000D_\nSaturday sit visit', '+91 8830853113', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(292, 'Akshay Somani', 'Godrej Boulevard, Manjari Budruk', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-23', 'Send him the details. And call him on Saturday for site visit', '+91 9975223114', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(293, 'Jittu', 'Naren Bliss, Hadapsar', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-23', 'The day from lead received, not respond call.<br />_x000D_\nno response', '+91 8007486789', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(294, 'Rohan Kakade', 'Geras World Of Joy S, Wagholi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-23', '3/3.5 BHK<br />_x000D_\n1.50 cr<br />_x000D_\nDetail Share- GWJ/Serio/Platino', '+91 7709909674', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(295, 'shahid sayyad', 'Gera World of Joy Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-23', 'already follow up', '91-9657863783', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(296, 'Tushar Dhokchaule', 'Rohan Abhilasha Wagholi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-23', NULL, '91-9518747032', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(297, 'Swapnil ', 'Rohan Abhilasha Wagholi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-23', 'Plot search wagholi ', '91-8208986522', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(298, 'Ram', 'VTP Township Codename Pegasus Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-23', 'Not interested', '91-8291121814', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(299, 'Someshwar ghungase', 'VTP Township Codename Pegasus Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-23', 'NA', '91-8788290694', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(300, 'User', 'VTP Township Codename Pegasus Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-23', 'He is not looking for any property. Only his number used for searching online', '91-7023962273', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(301, 'Firoza', 'Majestique Towers Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-23', 'Not call receive...', '91-9149554486', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(302, 'shubhangi', 'Zen Estate', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-23', 'Trying to contact by whatsapp. It\'s international no.<br />_x000D_\n', '-403973173', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(303, 'Preeti ', 'Naren Bliss', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-23', 'Call not receive ', '9881716876', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(304, 'Ajinkya Kalkutaki', 'Zen Estate', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-23', '2 bhk 55 lac Wagholi<br />_x000D_\nReady to move', '9226178704', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(305, 'Jyoti Gawade', 'Godrej Boulevard, Manjari Budruk', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-23', 'He postponed his plann for property search', '+91 9527975278', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(306, 'mudra shah', 'Gera World of Joy Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-23', 'Not call receive..', '91-7567116935', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(307, 'sumit ', 'Naren Bliss', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-23', NULL, '8237313599', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(308, 'Praphul pawar', 'Zen Estate', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-23', '2 bhk 65 lac<br />_x000D_\ndetail vtp god gera<br />_x000D_\nsite visit sunday 10 am', '9921223003', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(309, 'Aditya ', 'Majestique Rhythm County,', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-23', NULL, '9373429883', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(310, 'Prasad', '29 Gold Coast', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-23', '2 bhk<br />_x000D_\nViman Nagar<br />_x000D_\nDhanori<br />_x000D_\nDetails share', '9595609058', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(311, 'manali', 'Naren Bliss,', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-23', 'Hold searching property', '9767103570', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(312, 'Kshitij Gupta ', '29 Gold Coast', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-23', 'CP', '8585019197', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(313, 'Vivek Krishna', 'VTP Township Codename Pegasus, Manjari', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-23', '3 bhk rowhouse<br />_x000D_\ndetail share- Gera,VTP Altair<br />_x000D_\nSV- Sun 10 Am', '+91 9765303344', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(314, 'Rahul ', 'Manav Wildwoods Wagholi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-23', 'Call not connect', '91-8237255979', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(315, 'User', 'VTP Township Codename Pegasus Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-23', '1bhk. Kharadi/ Manjari location<br />_x000D_\nReady to move property looking for.', '91-9810586244', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(316, 'Monu ram', 'Sukhwani Hermosa Casa Hadapsar', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-24', NULL, '91-8102140041', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(317, 'Abhilasha Alone', 'Ganga Platino Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-24', 'call at evening', '91-9766878959', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(318, 'Mohini Kurekar', 'Mantra Montana Dhanori', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-24', '1BHK .<br />_x000D_\n25lac..<br />_x000D_\n<br />_x000D_\nNearing passion.<br />_x000D_\n<br />_x000D_\nDhanori, lohegav.<br />_x000D_\n', '91-9579535435', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(319, 'Harsh ', 'VTP Township Codename Pegasus Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-24', 'Not interested', '91-7249186624', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(320, 'Vikas Pathak', 'Kumar Prospera Hadapsar', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-24', NULL, '91-9011857800', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(321, 'KISHOR KANASE', 'Shapoorji Pallonji Joyville Hadapsar Annexe Phase 1, Shewalewadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-24', NULL, '+91 7507054125', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(322, 'Sumit', 'Duville Riverdale Heights, Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-24', '2 bhk <br />_x000D_\n85 lac<br />_x000D_\nResale<br />_x000D_\nBeryl,Langston', '+91 7030634340', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(323, 'Rajgauri Kanchan', 'Naren Bliss, Hadapsar', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-24', 'Network problem ', '+91 7741038711', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(324, 'Amita Karadkhedkar', 'Ganga Serio, Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-24', 'NA', '+91 9890279595', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(325, 'Rahul Jadhav', 'Majestique Rhythm County Phase 1, Handewadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-24', NULL, '+91 9975156890', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(326, 'Nitin', '29 Gold Coast,', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-24', 'Fake Lead', '9833577688', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(327, 'Sachin Agrawal Sir Jio', 'Mantra 29 Gold Coast, Dhanori', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-24', NULL, '+91 9762199110', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(328, 'Prashant Battellu', 'Mantra 29 Gold Coast, Dhanori', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-24', 'Call not connect', '+91 9822699829', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(329, 'Swapna Kulkarni', 'Majestique Towers East, Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-24', 'Location- comfortable with East pune<br />_x000D_\nUnit-2/3 bhk<br />_x000D_\nBud-80/-<br />_x000D_\nSend her a detaile', '+91 9766317336', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(330, 'Avinash', 'Shubh Shagun, Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-24', NULL, '+91 9762615986', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(331, 'Reena Haldankar', 'Mantra Insignia Phase 2, Mundhwa', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-24', 'Transferred lead to akash due to looking in wagholi', '+91 7057722898', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(332, 'fashion dreams', 'VTP Township Codename Pegasus Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-24', 'She is looking for 2bhk @ 42-45lc in kharadi Location', '91-7972273877', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(333, 'Diwakar mandal', 'Shapoorji Joyville Hadapsar Annexe Shewalewadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-24', 'Not interested ', '91-9098191178', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(334, 'swapnil waghmode', 'Duville Riverdale Heights Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-24', NULL, '91-8766707693', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(335, 'Uma', '47 East', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-24', '-She was busy, call her by 6.30pm<br />_x000D_\n-looking 6months later ,as of now at hometown<br />_x000D_\ncall not able to connect', '8602217218', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(336, 'Rajiv Wadhwa', 'Rohan Abhilasha Wagholi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-24', NULL, '91-9818302538', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(337, 'Ashwini Musale', 'Gera World of Joy Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-24', NULL, '91-9022573009', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(338, 'Yogesh', 'Shapoorji Joyville Hadapsar Annexe Shewalewadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-24', NULL, '91-9666261498', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(339, 'NITIN PAWAR ', 'Kohinoor Zen Estate Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-24', 'Location- kharadi<br />_x000D_\nUnit-1 bhk<br />_x000D_\nSend him the details', '91-7741803015', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(340, 'Gawade D R', 'Manav Wildwoods Phase 1, Wagholi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-24', '2 bhk <br />_x000D_\nDetails share<br />_x000D_\nWagholi', '+91 9730325221', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(341, 'Sanjay Bobate', 'Shubh Shagun, Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-24', NULL, '+91 8698052573', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(342, 'Kiran K', 'Gera World of Joy Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-24', 'Location- kharadi<br />_x000D_\nUnit-2bhk<br />_x000D_\nSend him the details', '91-9591979269', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(343, 'Sahil Pardeshi', 'Bramha Corp The Collection, Wadgaon Sheri', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-24', NULL, '+91 8605616677', NULL, '', '', 0, 1, 7, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(344, 'User', 'VTP Township Codename Pegasus Kharad', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-24', 'Not interested', '91-8423303993', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(345, 'VIKAS THOMBE ', 'Mantra Montana Dhanori', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-24', '1 bhk <br />_x000D_\nMorning sit visit <br />_x000D_\nWagholi', '91-9309553836', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(346, 'Brinda Gurbuxani', 'VTP Purvanchal, Kesnand', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-24', NULL, '+91 9833443634', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(347, 'Pradeep Dhumal', 'Naren Bliss Hadapsar', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-25', NULL, '91-9820076220', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(348, 'User', 'Gera World of Joy Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-25', 'Bharat Sir call', '91-9764997170', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(349, 'Roland', '2BHK Apartment Duville Riverdale Residences Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-25', '2 bhk- 20 lac<br />_x000D_\nkeshavnagar<br />_x000D_\nLow Budget<br />_x000D_\npass to sayali', '91-9762771836', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(350, 'SANJEEV Jagtap', 'Duville Riverdale Heights Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-25', 'Location- kharadi<br />_x000D_\nUnit-1 bhk<br />_x000D_\nBud-40lc<br />_x000D_\nSend him the details', '91-7204888168', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(351, 'User', '2BHK Apartment Prasun Sarvam Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-25', 'already book', '91-9890009236', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(352, 'User', 'Ganga Serio Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-25', 'not interested', '91-9632968760', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(353, 'prashant trivedi', 'Prasun Sarvam Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-25', NULL, '91-7042783410', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(354, 'Ashok Bhojne', 'Kolte Patil Ivy Nia Wagholi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-25', '2 bhk <br />_x000D_\n30 L <br />_x000D_\nWagholi <br />_x000D_\n', '91-9850199777', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(355, 'Roshan', 'Kohinoor Zen Estate Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-25', 'Not interested', '91-8982274252', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(356, 'Shubham Kulkarni', 'VTP Township Codename Pegasus Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-25', NULL, '91-7083116773', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(357, 'Nilesh', 'Manav Wildwoods Phase 1, Wagholi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-25', NULL, '+91 7820936737', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(358, 'Sunny Kunjir', 'Sukhwani Hermosa Casa, Mundhwa', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-25', 'not answering call', '+91 9923250606', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(359, 'Nilesh Devgirikar', 'Venkatesh Graffiti Elan Phase 1, Mundhwa', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-25', 'Looking 2bhk in 50-55lacs nearby keshvnagar, suggested kcity as well as venktesh elan, coming to visit on saturday', '+91 8999345621', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(360, 'Ajitpalsing Rajput', 'Venkatesh Graffiti Elan Phase 1, Mundhwa', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-25', NULL, '+91 9028409381', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(361, 'Debashis Sarkar', 'Mantra 29 Gold Coast, Dhanori', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-25', 'All ready booked', '+91 9820530250', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(362, 'Swapnil', 'Unique Legacy Majestic, Mundhwa', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-25', NULL, '+91 7709982987', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(363, 'Animesh Bhat', 'Mantra Insignia Phase 2, Mundhwa', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-25', 'Not answered call<br />_x000D_\nwas looking for rent in kharadi, now done.', '+91 8109692095', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(364, 'Ujwala', 'Manav Wildwoods Phase 1, Wagholi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-25', NULL, '+91 8830483991', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(365, 'Nilesh Lande', 'Unique Legacy Majestic, Mundhwa', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-25', NULL, '+91 7719999919', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(366, 'Nikhil', 'Manta insignia', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-25', '2bhk, ready possession, 65L budget, Keshav Nagar', '9823128806', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(367, 'Deepak joshi', 'mantra Insignia', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-25', 'Currently in Rajasthan, will be back by 7-8 days then will come to visit.<br />_x000D_\n<br />_x000D_\nnot answering call', '7507209123', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(368, 'Shital', 'Rhytham County', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-25', NULL, '9657682788', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(369, 'Pravin Patil ', 'Satyam Niranjani,Hadapsar', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-25', NULL, '91-8668278174', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(370, 'Priyasha', 'Kohinoor Zen Estate Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-25', 'Not response', '91-9028001501', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(371, 'prashant', '2BHK Apartment Manav Wildwoods Wagholi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-25', NULL, '91-9665719609', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(372, 'ankit', 'Gera World of Joy Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-25', 'NA', '91-9049900720', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(373, 'pawankumar', 'Shapoorji Joyville Hadapsar Annexe Shewalewadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-25', '1bhk, hadpsar, 30L', '91-9767610067', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(374, 'Shambhu ', 'Kolte Patil Ivy Nia Wagholi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-25', NULL, '91-9822813730', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(375, 'Pravaja Bhilpawar', 'Riverdale Residences I, Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-25', 'Hold project search ', '+91 9665044511', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(376, 'Javed Khan', 'Shubh Shagun, Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-25', NULL, '+91 9765114786', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(377, 'Datta', '29 Gold Coast', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-25', '2 bhk <br />_x000D_\n60L<br />_x000D_\nDhanori <br />_x000D_\nNext week Saturday visit', '9552934569', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(378, 'Aniket D', 'Mantra 29 Gold Coast, Dhanori', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-25', '2 bhk <br />_x000D_\n60 L <br />_x000D_\nDhanori<br />_x000D_\nReady possession', '+91 9082931078', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(379, 'Priyanka Sumant Naik', 'Shapoorji Pallonji Joyville Hadapsar Annexe Phase 1, Shewalewadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-25', NULL, '+91 9960010502', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(380, 'Urvi Rabadiya', 'VTP Township Codename Pegasus Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-25', 'Not looking for any property', '91-8141870379', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(381, 'milind sable', 'Kolte Patil Ivy Nia Wagholi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-25', NULL, '91-7767965533', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(382, 'Swapnil ', 'Majestique Towers Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-25', 'cp', '91-7030534747', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(383, 'Rahul Mehrotra', 'Gera World of Joy Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-25', NULL, '65-86999278', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(384, 'User', 'Ganga Platino Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-25', '2/3 bhk<br />_x000D_\n1.20 cr<br />_x000D_\nready to move <br />_x000D_\nresale', '91-7022903466', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(385, 'yashashree', 'Mantra Montana Dhanori', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-25', '2 bhk <br />_x000D_\n60 L <br />_x000D_\nDhanori <br />_x000D_\nSit visit in Saturday Sunday ', '91-9209440757', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(386, 'A S', 'Shapoorji Pallonji Joyville Hadapsar Annexe Phase 1, Shewalewadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-25', 'Call not receive ', '+91 8087035084', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(387, 'Narendra Ramse', 'Bramha Corp The Collection, Wadgaon Sheri', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-26', 'He is not interested ', '+91 9823719123', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(388, 'Niranjan', 'VTP Township Codename Pegasus, Manjari Khurd', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-26', '2/3 bhk<br />_x000D_\n60 lac<br />_x000D_\nwagholi <br />_x000D_\nNG rathi,NG Arban<br />_x000D_\npass to akash', '+91 9764758885', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(389, 'Shinde Navnath Dnyanoba', 'Ganga Serio, Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-26', 'NA', '+91 8329866679', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(390, 'Ramkrishna ', 'Sukhwani Hermosa Casa Hadapsar', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-26', 'plan cancelled for a year', '91-7737837999', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(391, 'Nitin kale', 'Duville Riverdale Heights Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-26', 'already lead', '91-9970736427', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(392, 'Karan gadve', 'Duville Riverdale Heights Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-26', 'Not received call<br />_x000D_\n', '91-9699896637', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(393, 'User', 'Duville Riverdale Heights Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-26', NULL, '91-7028028362', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(394, 'Sandeep Jaiswal', 'Gera World of Joy Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-26', 'Not interested', '91-9151299108', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(395, 'Roopali Sawant', 'Rohan Abhilasha Wagholi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-26', '3 bhk <br />_x000D_\n1 cr <br />_x000D_\nKhradi and keshavnagar ( bhart sir )', '91-9860613159', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(396, 'Hardik Bhatt', 'Duville Riverdale Heights Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-26', 'Location- Kharadi<br />_x000D_\nUnit-1 bhk<br />_x000D_\nSpecifically looking in Riverdale Highest project', '91-9819837774', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(397, 'skbadirul', 'Kolte Patil Ivy Nia Wagholi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-26', 'Not Interested', '91-8768312561', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(398, 'Kende p b', 'Nyati Elysia Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-26', '3 bhk 1 cr', '91-8805418876', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(399, 'Guddu Raj', 'Kolte Patil Ivy Nia Wagholi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-26', NULL, '91-9835804200', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(400, 'Sanjay Kumar', 'Bramha Corp The Collection, Wadgaon Sheri', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-26', '3bhk 90 lac<br />_x000D_\nReady to move<br />_x000D_\nNear pune camp 5 km', '+91 9045020080', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(401, 'ROSHAN KINAGE', 'Mantra Insignia, Keshav Nagar, Pune', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-26', 'Required property in a range of 50-55L within one year possession.<br />_x000D_\nAlready booked at anandtara as visited before', '8983498081', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(402, 'GAUTAM', 'Kumar Prospera Hadapsar', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-26', NULL, '91-9176998928', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(403, 'Rahul Kumar', 'Rohan Abhilasha Wagholi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-26', 'Coll not canect ', '91-9570164975', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(404, 'Prashant', 'Duville Riverdale Heights Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-26', 'Switch Off', '91-8097215429', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(405, 'Amardeep Behera', 'Geras World Of Joy S, Wagholi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-26', NULL, '+91 9503030833', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(406, 'Sandip Gugaliya', 'Rohan Abhilasha Wagholi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-26', NULL, '91-9405707525', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(407, 'Rajiv', 'Majestique Towers East, Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-28', '2/3 bhk<br />_x000D_\n1 cr<br />_x000D_\nposs-dec22<br />_x000D_\nalready seen all projects ', '+91 8904406760', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(408, 'Manthan Surana', 'Majestique Marbella Phase 1, Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-28', NULL, '+91 9067878989', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(409, 'Kumardip Chakraborty', 'Godrej Boulevard, Manjari Budruk', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-28', 'VC done for Godrej Rivergreen.<br />_x000D_\nHe is not interested to go through CP. Want to talk directly to Builder lobby', '+91 9925471499', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(410, 'Ritesh Kumar Singh', 'Naren Bliss, Hadapsar', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-28', '2 BHK Magarpatta', '+91 9561316789', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(411, 'Poonamwaghmare', 'Naren Bliss, Hadapsar', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-28', 'Cp', '+91 8208247702', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(412, 'Harsh Bansal', 'Zen Estate, Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-28', 'NA', '+91 6263917887', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(413, 'Rahul Wagh', 'Sukhwani Hermosa Casa, Mundhwa', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-28', 'call disconnecting always', '+91 9860580720', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(414, 'Siddhi Ninawe', 'Unique Legacy Majestic, Mundhwa', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-28', 'Already booked at kharadi resale property. ', '+91 7032917031', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(415, 'Shubham', 'Mantra Montana Dhanori', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-28', NULL, '91-7058736450', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(416, 'Rupesh Mallade ', 'VTP Township Codename Pegasus Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-28', 'call at 5pm', '91-9511256832', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(417, 'Abhijeet Controllu ', 'Mantra Marigold,Yewalewadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-28', NULL, '91-9619761222', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(418, 'Chinmay', 'Gera World of Joy Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-28', 'Location- Kharadi<br />_x000D_\nUnit-3bhk<br />_x000D_\nBud- comfortable with the price<br />_x000D_\nSend him the details', '91-7767896960', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(419, 'Deepali', 'Mantra Montana Dhanori', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-28', 'Not interested', '91-9561555408', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(420, 'Kedar Sapre', 'Duville Riverdale Residences Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-28', NULL, '91-9819167889', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(421, 'Amikesh', 'Mantra 29 Gold Coast Dhanori', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-28', '2BHK  Ready to move.<br />_x000D_\nDhanori, Vishrant wadi.<br />_x000D_\nResale <br />_x000D_\n<br />_x000D_\nBujte:- 40lac..', '91-8285481804', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(422, 'Shahwaiz Sheikh', 'Majestique Rhythm County', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-28', 'Not interested', '-3032111847', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(423, 'Pratik', 'Mantra Montana Dhanori', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-28', NULL, '91-6006200479', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(424, 'Mrunal', 'Shapoorji Joyville Hadapsar Annexe Shewalewadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-28', NULL, '91-9930045445', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(425, 'Amit', 'City Amanora Ascent Towers,Hadapsar', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-28', NULL, '91-8983138188', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(426, 'Shardul ', 'Mantra Insignia', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-28', 'looking at dhanori 3bhk-70 L budget, discussed with jyoti also, budget concern not getting in budget', '8882006007', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(427, 'Vishwas', 'Township Codename Pegasus, Kharadi, Pune', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-28', 'CP', '7666612651', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(428, 'Mithen Kadam', 'Riverdale Residences I, Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-28', 'Location- Kharadi<br />_x000D_\nUnit-2/3bhk<br />_x000D_\nBud- 75-80lc<br />_x000D_\nSend him the details<br />_x000D_\n8/8/21- He post pond his plann for 1yr', '+91 9833958866', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(429, 'nishita', 'Godrej Rejuve Keshav Nagar', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-28', 'call not answered', '91-9511621596', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(430, 'Mvvv', 'Gera World of Joy Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-28', 'cp', '91-8484965802', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(431, 'Sonia', 'Nyati Evolve I, Mundhwa', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-28', 'Broker', '+91 9595261155', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(432, 'Priyanka Patil mahesh', 'Township Codename Pegasus', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-28', '1 bhk<br />_x000D_\n30 lac<br />_x000D_\nready to move<br />_x000D_\nHadapsar<br />_x000D_\npass to vivek', '9529220489', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(433, 'Amol ', 'Majestique Rhythm County', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-28', NULL, '9881542134', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0);
INSERT INTO `clients` (`id`, `company_name`, `address`, `city`, `state`, `zip`, `country`, `created_date`, `website`, `phone`, `currency_symbol`, `starred_by`, `group_ids`, `deleted`, `is_lead`, `lead_status_id`, `owner_id`, `created_by`, `sort`, `lead_source_id`, `last_lead_status`, `client_migration_date`, `vat_number`, `currency`, `disable_online_payment`) VALUES
(434, 'Praful Turate', 'Zen Estate', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-28', '2BHK <br />_x000D_\nCarpet 800+ Requ....<br />_x000D_\n<br />_x000D_\nBujte :- 60lac.<br />_x000D_\n<br />_x000D_\n<br />_x000D_\n<br />_x000D_\nVtp visit..<br />_x000D_\n<br />_x000D_\n<br />_x000D_\n<br />_x000D_\nRe-visit Ganpati visarjan ke bad.....<br />_x000D_\n<br />_x000D_\n<br />_x000D_\n', '9503418565', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(435, 'Nilesh', 'Zen Estate', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-28', NULL, '9850720243', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(436, 'Manoj Kadam', 'Majestique Rhythm County', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-28', 'Cl serching hadapsar loaction 2bhk', '9420463046', NULL, '', '', 0, 1, 8, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(437, 'Rajendra shelar', 'Township Codename Pegasus', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-28', 'NA', '7498646427', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(438, 'pratiksha Bhavan', 'Township Codename Pegasus', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-28', 'Location- wagholi<br />_x000D_\nUnit-1 bhk<br />_x000D_\nBud-25-28 lc<br />_x000D_\n share the lead to Akash', '8788310573', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(439, 'Ankit Mishra', 'Mantra Insignia', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-28', 'Transfer record to sachin sir, as looking at phursungi, hadapsar<br />_x000D_\n plan cancelled', '8174817843', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(440, 'Rameshwar Lilakar', 'Township Codename Pegasus', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-28', 'Not interested', '7798031455', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(441, 'Kiran kadam', 'Township Codename Pegasus', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-28', '2 bhk 45 lac<br />_x000D_\ndhanori,Tingare nagar<br />_x000D_\nlead pass to Akash', '9011227905', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(442, 'Tripti Bhardwaj', '29 Gold Coast', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-28', NULL, '7760219646', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(443, 'Manoj Mulye', 'Township Codename Pegasus', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-28', NULL, '9405957279', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(444, 'A S Rao', 'Mantra Insignia', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-28', 'postponed for 6month', '7999472795', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(445, 'arun Chaudhari', 'Naren Bliss', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-28', '3bhk, Roystonea, resale, budget 1.3cr', '7588010145', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(446, 'mm', 'Township Codename Pegasus', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-28', 'NA', '7620262995', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(447, 'Tabassum Qureshi', 'Township Codename Pegasus', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-28', 'Call her on Saturday she is busy now', '8308609984', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(448, 'Jyoti', 'Zen Estate', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-28', '3 bhk-1.10<br />_x000D_\ncarpet -1400<br />_x000D_\nvtp altair already seen<br />_x000D_\ndetail share-serio', '9823296843', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(449, 'Ajeem Shaikh', 'Zen Estate', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-28', NULL, '8237563137', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(450, 'Ashutosh Rokade', 'Majestique Rhythm County', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-28', 'Call not receive', '8208443098', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(451, 'Ruchika Sanghavi ', '47 East', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-28', 'Transferred to sachin sir as looking in mohammadwadi and undri location.<br />_x000D_\nplan cancelled ', '9850450497', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(452, 'Tomsy', 'Mantra Insignia', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-28', 'no answer so long', '9960717503', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(453, 'Lakshmi R ', 'Naren Bliss', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-28', 'Not interested', '8762992651', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(454, 'Manish Sharma', 'Naren Bliss, Hadapsar', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-28', 'Cl searching property magarapatta 3bhk <br />_x000D_\nBudget 95lac', '+91 9588908048', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(455, 'Bharat shinde', 'Majestique Rhythm County', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-28', NULL, '9018207271', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(456, 'Nishad Deshmukh', 'Zen Estate', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-28', '2/3 bhk<br />_x000D_\ncarpet 1000 to 1400<br />_x000D_\n95 lac<br />_x000D_\ndetail - vtp altair,serio,platino<br />_x000D_\nSV- sat', '7038481878', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(457, 'Arpita S', 'Majestique Rhythm County', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-28', 'Cl call you back ', '8999057482', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(458, 'Kiran kumar', 'Mantra Insignia', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-28', 'not answered call', '9356577268', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(459, 'Vinayak', 'Mantra Insignia', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-28', 'call disconnecting, already shared details', '9881400819', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(460, 'Atul Deshpande', '47 East', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-28', 'plan postponed for next 3-6 months due to funds issue', '7972246542', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(461, 'Nagraj baraki ', 'Naren Bliss', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-28', 'Call not receive ', '9860443217', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(462, 'Avinash Sharma', 'Zen Estate', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-28', '2BHK<br />_x000D_\n60lac..<br />_x000D_\n<br />_x000D_\nNearing passion..<br />_x000D_\n<br />_x000D_\n<br />_x000D_\nSunday visit plan.12pm..<br />_x000D_\n<br />_x000D_\nTeacher client...', '8087421735', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(463, 'Sunil Tapdiya', 'Mantra Insignia', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-28', 'visited earlier at unique legacy, liked that , shown other projects like vrindavan heights, newton homes, kcity, mantra wonderland.', '9421487036', NULL, '', '', 0, 1, 8, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(464, 'Priyanka', 'Kohinoor Zen Estate Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-28', 'Not received', '91-8265031214', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(465, 'Suraj ', 'Majestique Rhythm County', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-29', NULL, '9923646449', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(466, 'Nandita', 'Mantra Insignia', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-29', 'Given to amruta, looking in wagholi location, max budget is 40 lakh<br />_x000D_\npostponed till january as not in pune', '8999943064', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(467, 'Aabasaheb ', 'Mantra Insignia', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-29', 'Given to akash as required in budget 75 lakh max for 3bhk with ready to move in', '9561756963', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(468, 'Kartik Dhumal', '29 Gold Coast', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-29', NULL, '8888882348', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(469, 'Mukesh Gairola', 'Majestique Rhythm County', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-29', '2bhk, handewadi, budget 45 L', '7906342447', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(470, 'Ganesh Nanaware', 'VTP Township Codename Pegasus Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-29', 'already discuss other cp', '91-9730001719', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(471, 'B Kumar', 'Gera World of Joy Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-29', 'He is looking in balevadi and Baner location', '91-9869655934', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(472, 'Dr.Shantanurao Pachkore', 'Shapoorji Joyville Hadapsar Annexe Shewalewadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-29', '3bhk, hadpsar, Solapur highway, 80L', '91-9404236063', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(473, 'KUNAL VIKAMSEY', 'Gera World of Joy Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-29', NULL, '91-9821021571', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(474, 'Varun', 'Gera World of Joy Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-29', 'call after sometime', '91-8806661948', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(475, 'Rahul GOsavi', 'Rohan Abhilasha Wagholi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-29', 'Not interested', '91-9823359927', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(476, 'Pranav Datar', 'Godrej Rejuve Keshav Nagar', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-29', 'He is channel partner', '9834645394', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(477, 'Vijay Suradkar', 'Shubh Shagun, Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-29', 'NA', '+91 8788442538', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(478, 'Preeti Kadam', 'Mantra 29 Gold Coast, Dhanori', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-29', NULL, '+91 9689833193', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(479, 'Amrit', 'Nyati Evolve I, Mundhwa', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-29', 'Transferred to supriya, looking only in kharadi location.<br />_x000D_\nresale out first property, then will plan', '+91 7044097102', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(480, 'Fahim Mohammed', 'Majestique Towers East Wing B, Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-29', 'Call him in the evening', '+91 9822764680', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(481, 'Ranjit Wale', 'Rohan Abhilasha, Wagholi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-29', 'Evening call back 6:30 pm ', '+91 8700773540', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(482, 'Shubhra', 'Panchshil Towers', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-29', NULL, '7507136000', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(483, 'Gurpreet Parekh', 'Panchshil Towers Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-29', NULL, '91-7838000874', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(484, 'Umesh', 'Duville Riverdale Heights, Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-29', '2 bhk 70 lac<br />_x000D_\nvisit-sat', '+91 8510042867', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(485, 'Shahnawaz Alam', 'Naren Bliss, Hadapsar', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-29', 'Call not receive ', '+91 9097579626', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(486, 'Bhuvanesh Gomase', 'Gera World of Joy Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-29', 'He is looking for individual houses. In Keshav nagar and manjri location. 60lc budget', '91-9175285755', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(487, 'Ramesh More', 'Majestique Towers East Wing B, Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-29', NULL, '+91 9890372182', NULL, '', '', 0, 1, 1, 1, 0, 0, 7, '', '0000-00-00', NULL, NULL, 0),
(488, 'Abhilash Pachupate', 'Kolte Patil Ivy Nia Wagholi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-29', 'Call not connect', '91-9765940134', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(489, 'shubham', 'VTP Township Codename Pegasus Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-29', 'Not conn', '91-9172715269', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(490, 'User', 'VTP Township Codename Pegasus Kharadi', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-29', 'She already finished the project', '91-7875565206', NULL, '', '', 0, 1, 1, 1, 0, 0, 8, '', '0000-00-00', NULL, NULL, 0),
(491, 'Arpita ', '29 Gold Coast', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-29', NULL, '9689713912', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(492, 'Kartik', 'Mantra Insignia', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-29', 'call not answering, busy.', '9579035590', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(493, 'Shivraj Bandwal', 'Majestique Rhythm County', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-29', '1bhk, handewadi, 25L, under construction', '9527666199', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(494, 'Rizwan Shaikh', 'Majestique Rhythm County', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-29', NULL, '9503473033', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(495, 'Ashish Khanna ', 'Naren Bliss', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-29', '2.5 BHK magarpatta, hadpasar, ready possession or nearing possession, 1.1cr', '9096399963', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(496, 'Bajirao Patil', 'Majestique Rhythm County', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-29', NULL, '9665990312', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(497, 'Rahul', 'Majestique Rhythm County', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-29', NULL, '9922244414', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(498, 'asha swami', 'Naren Bliss', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-29', 'Cp', '9028118953', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(499, 'adesh dabhade', 'Naren Bliss', 'Pune', 'Maharashtra', '411014', 'India', '2021-07-29', NULL, '8788087158', NULL, '', '', 0, 1, 1, 1, 0, 0, 6, '', '0000-00-00', NULL, NULL, 0),
(500, 'Softflame', 'Pune', 'Pune', 'maharashtra', '412307', 'india', '2022-07-04', 'softflame.in', '1123456789', 'AED', '', '', 1, 0, 0, 0, 1, 0, 0, '', '0000-00-00', 'GHTSS123456', '', 0),
(501, 'ismili center', 'dubai', 'dubai', '', '', 'UAE', '2022-07-12', '', '0522513210', 'AED', '', '', 1, 0, 0, 0, 1, 0, 0, '', '0000-00-00', '', 'AED', 0),
(502, 'Gurusoftwares', '9, new ajay apartament, sb road, pune', 'Pune', 'Maharshtra', '411016', 'India', '2022-08-03', 'gurusoftwares.in', '8446425690', '', '', '', 0, 0, 0, 0, 1, 0, 0, '', '0000-00-00', '', '', 0),
(503, 'Reldyn Tech Sdn. Bhd', 'B-13-02, Kiaramas Danai, \r\nJalan Desa Kiara, Mont Kiara, \r\n50480 Kuala Lumpur, Malaysia.', 'Mont Kiara', 'Kuala Lumpur', '50480', 'Malaysia', '2022-08-04', 'https://reldyn.co/', '', '$', '', '', 0, 0, 0, 0, 1, 0, 0, '', '0000-00-00', '', '', 0),
(504, 'IOWEB3 TECHNOLOGES', '', '', '', '', '', '2022-12-22', '', '', '', '', '', 0, 1, 1, 2, 0, 0, 10, '', '0000-00-00', '', '', 0),
(505, 'Demandifymedia', 'Pune\r\nMaharshtra\r\n411006', '66 W Flagler Street STE 900', 'Miami, FL 33130', '33130', 'United States', '2023-01-09', 'https://demandifymedia.com/', '', '₹', '', '', 0, 0, 0, 0, 1, 0, 0, '', '0000-00-00', '27AAJCD3765F1ZI', 'INR', 0),
(506, 'Skyi', 'Pune', 'Pune', 'Maharashtra', '411045', 'India', '2023-01-31', 'www.website.com', '9999999999', '₹', '', '', 0, 0, 0, 0, 1, 0, 0, '', '0000-00-00', '', 'INR', 0),
(507, 'Kedar Naik', 'Pune', 'Pune', 'Maharshtra', '', 'India', '2023-02-09', 'http://www.kdraclimited.com/', '+23057211074', '₹', '', '', 0, 0, 0, 0, 507, 0, 0, '', '0000-00-00', '', 'INR', 0),
(508, 'Mahimna Construction Private Limited', '18-19, 3rd Floor, Bhosale-Shinde Arcade,\r\nJangali Maharaj Road, Deccan Gymkhana, Pune-411004', 'Pune', 'Maharshtra', '411004', 'India', '2023-02-09', 'https://mahimna.com/', '020 25513400', '₹', '', '', 0, 0, 0, 0, 1, 0, 0, '', '0000-00-00', '27AAQCM0198Q1ZI', 'INR', 0),
(509, 'Anthem Tech Solutions Inc', '7600 Chevy Chase Drive\r\nSuite 300 Austin \r\nTx 78752', 'Baltimore', 'Maryland', '', 'USA ', '2023-02-11', 'Anthemtechsol.com', '+1(443)8524-640', '₹', '', '', 0, 0, 0, 0, 1, 0, 0, '', '0000-00-00', '', 'INR', 0),
(510, 'Homeseva', 'Office No 11, 4th Floor, Rokade Heights, Paud Road, Kothrud Pune, Pune, Maharashtra 411038', 'Pune', 'Maharshtra', '411038', 'India', '2023-03-09', 'homeseva.co.in', '9090636363', '₹', '', '', 0, 0, 0, 0, 1, 0, 0, '', '0000-00-00', '27AAJFH4134K2Z7', 'INR', 0),
(511, 'Curiotory', 'Office Number- 16B, 6th Floor, \r\nDowntown City Vista.', 'Pune', 'Maharshtra', '411014', 'India', '2023-03-15', 'https://curiotory.com/', '8793004686', '₹', '', '', 0, 0, 0, 0, 1, 0, 0, '', '0000-00-00', '27AAICC8767D1ZC', 'INR', 0),
(512, 'Baitalanaqah', 'Dubai, UAE', 'Dubai', 'Dubai', '', 'UAE', '2023-05-05', 'https://www.baitalanaqah.com/', '+971 509777763', '₹', '', '', 0, 0, 0, 0, 1, 0, 0, '', '0000-00-00', '', 'INR', 0),
(513, 'PROPTIMISE (INDIA) PRIVATE LIMITED', '#39, NGEF Lane, 2nd Floor,\r\nSuite No.1103\r\nIndiranagar, Bangalore - 560038\r\nKarnataka, India', 'Bangalore', 'Karnataka', '560038', 'India', '2023-05-19', '', '', '₹', '', '', 0, 0, 0, 0, 1, 0, 0, '', '0000-00-00', '', 'INR', 0),
(514, 'DSTUDIO90 SOLUTIONS PVT LTD', 'FL B/7, SN 16/1/4, Shree Datta Palace, Sun City Road, Anand Nagar, Sinhgad Road, Pune, Maharashtra, 411051', 'Pune', 'Maharashtra', '411051', 'India', '2023-06-06', 'https://www.dstudio90.com/', '', '₹', '', '', 0, 0, 0, 0, 1, 0, 0, '', '0000-00-00', '27AAJCD0150C1Z7', 'INR', 0),
(515, 'Rasi international ', 'Rasi International LLC Horizon Towers, Tower D,21st Floor Office No. - 2101, Al Rashidiya 1, Ajman - U.A.E', 'Ajman', 'UAE', '', 'UAE', '2023-07-10', 'rasi.ae', '+971-65265365', '₹', '', '', 0, 0, 0, 0, 1, 0, 0, '', '0000-00-00', '', 'INR', 0),
(516, 'Softflame Solutions Private Limited', 'Flat No. 3, Building 1, New Ajay Society, Senapati Bapat Rd, beside Domino\'s Pizza, Shivaji Co operative Housing Society, Gokhalenagar', 'Pune', 'Maharshtra', '411016', 'India', '2023-08-14', 'softflame.in', '8408898845', '₹', '', '', 0, 0, 0, 0, 1, 0, 0, '', '0000-00-00', '27ABBCS3317F1ZL', 'INR', 0),
(517, 'Housemate Hotels', 'Housemate Hotels, Besides Chandan Marble, Next to Indian Oil Petrol Pump, Kharadi', 'Pune', 'Maharshtra', '411014', 'India', '2023-08-14', 'housematehotesl.com', '8380097228', '₹', '', '', 0, 0, 0, 0, 1, 0, 0, '', '0000-00-00', '27AASFN1264F1Z3', 'INR', 0),
(518, 'Bangloreseva', '45 746, begur village mico layout, hongasandra, bangalore,\r\nBengaluru (Bangalore) Urban, Karnataka, 560068', 'Bangalore', 'Karnataka', '560068', 'India', '2023-08-21', '', '9538477108', '₹', '', '', 0, 0, 0, 0, 1, 0, 0, '', '0000-00-00', '29CIRPS1485A1ZU', 'INR', 0),
(519, 'Neelkanth Modular Kitchen', 'Tower Kad Chowk, The Hub, Shop No. 3, Geeta Complex, Katraj - Undri - Hadapsar Bypass Rd, opposite MARVEL ISOLA, Mohammed Wadi, Pune, Maharashtra 411028', 'Pune', 'Maharshtra', '411028', 'India', '2023-08-29', 'www.nilkanthmodularkitchen.com', '9724301658', '₹', '', '', 0, 0, 0, 0, 1, 0, 0, '', '0000-00-00', '', 'INR', 0),
(520, 'Retro Forest Resort', '', 'Lonavla', 'Maharshtra', '410405', 'India', '2023-10-19', 'www.retroforestresortlonavala.in', '080 4803 7480', '₹', '', '', 0, 0, 0, 0, 1, 0, 0, '', '0000-00-00', '', 'INR', 0),
(521, 'Uni Klinger Ltd.', 'SC1, 5th Floor, Kohinoor estate, Wakadewadi, Shivaji Nagar', 'Pune', 'Maharshtra', '411003', 'India', '2023-12-20', 'uniklinger.com/', '020 4102 3000', '₹', '', '', 0, 0, 0, 0, 1, 0, 0, '', '0000-00-00', '27AAACU0771B1ZQ', 'INR', 0),
(522, 'HISATI TECHNOLOGIES PRIVATE LIMITED', 'FLAT NO. 104, HARSHRAJ APPARTMENT, ,ROAD\r\nNO.11, PATEL NAGAR,P.S-SHASTRI NAGAR,', 'Patna', 'Bihar', '800023', 'India', '2023-12-20', 'http://www.hisati.com', '9871032239', '₹ ', '', '', 0, 0, 0, 0, 1, 0, 0, '', '0000-00-00', '10AADCH1618R1ZM', 'INR', 0),
(523, 'VediFarm Resort', ' Wai, Mahabaleshvar Satara highway Highway, cross Khambatki Ghat, Soloshi ', 'Wai', 'Maharshtra', '415525', 'India', '2024-01-08', 'http://vedifarm.com/', '080 4803 3968', '₹', '', '', 0, 0, 0, 0, 1, 0, 0, '', '0000-00-00', '', 'INR', 0),
(524, 'BestAgro Projects Private Limited', 'Solapur, Maharashtra', 'Solapur', 'Maharshtra', '413221', 'India', '2024-01-16', 'bestagro.in', '9763944669', '₹', '', '', 0, 0, 0, 0, 1, 0, 0, '', '0000-00-00', '27AAGCB6782G1ZB', 'INR', 0),
(525, 'Mentebit Softac Service Private Limited', '125, Gera\'s Imperium Rise,\r\nPhase-II, Hinjewadi, Pune - 411057', 'Pune', 'Maharshtra', '411057', 'India', '2024-02-13', '', '7276500002', '₹', '', '', 0, 0, 0, 0, 1, 0, 0, '', '0000-00-00', '', 'INR', 0),
(526, 'Ashwini R Katakdhond ', 'Solapur, India', 'Solapur', 'Maharshtra', '413221', 'India', '2024-03-20', '', '', '₹', '', '', 0, 0, 0, 0, 1, 0, 0, '', '0000-00-00', '', 'INR', 0);

-- --------------------------------------------------------

--
-- Table structure for table `client_groups`
--

CREATE TABLE `client_groups` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `custom_fields`
--

CREATE TABLE `custom_fields` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `placeholder` text NOT NULL,
  `example_variable_name` text DEFAULT NULL,
  `options` mediumtext NOT NULL,
  `field_type` varchar(50) NOT NULL,
  `related_to` varchar(50) NOT NULL,
  `sort` int(11) NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT 0,
  `add_filter` tinyint(1) NOT NULL DEFAULT 0,
  `show_in_table` tinyint(1) NOT NULL DEFAULT 0,
  `show_in_invoice` tinyint(1) NOT NULL DEFAULT 0,
  `show_in_estimate` tinyint(1) NOT NULL DEFAULT 0,
  `show_in_order` tinyint(1) NOT NULL DEFAULT 0,
  `show_in_proposal` tinyint(1) NOT NULL DEFAULT 0,
  `visible_to_admins_only` tinyint(1) NOT NULL DEFAULT 0,
  `hide_from_clients` tinyint(1) NOT NULL DEFAULT 0,
  `disable_editing_by_clients` tinyint(1) NOT NULL DEFAULT 0,
  `show_on_kanban_card` tinyint(1) NOT NULL DEFAULT 0,
  `deleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `custom_fields`
--

INSERT INTO `custom_fields` (`id`, `title`, `placeholder`, `example_variable_name`, `options`, `field_type`, `related_to`, `sort`, `required`, `add_filter`, `show_in_table`, `show_in_invoice`, `show_in_estimate`, `show_in_order`, `show_in_proposal`, `visible_to_admins_only`, `hide_from_clients`, `disable_editing_by_clients`, `show_on_kanban_card`, `deleted`) VALUES
(1, 'MAIN TEST ', '', '', '', 'email', 'leads', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `custom_field_values`
--

CREATE TABLE `custom_field_values` (
  `id` int(11) NOT NULL,
  `related_to_type` varchar(50) NOT NULL,
  `related_to_id` int(11) NOT NULL,
  `custom_field_id` int(11) NOT NULL,
  `value` longtext NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `custom_field_values`
--

INSERT INTO `custom_field_values` (`id`, `related_to_type`, `related_to_id`, `custom_field_id`, `value`, `deleted`) VALUES
(1, 'leads', 504, 1, 'DCECF@GMAIL.COM', 0);

-- --------------------------------------------------------

--
-- Table structure for table `custom_widgets`
--

CREATE TABLE `custom_widgets` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `title` text DEFAULT NULL,
  `content` text DEFAULT NULL,
  `show_title` tinyint(1) NOT NULL DEFAULT 0,
  `show_border` tinyint(1) NOT NULL DEFAULT 0,
  `deleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `dashboards`
--

CREATE TABLE `dashboards` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `title` text DEFAULT NULL,
  `data` text DEFAULT NULL,
  `color` varchar(15) NOT NULL,
  `sort` int(11) NOT NULL DEFAULT 0,
  `deleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `dashboards`
--

INSERT INTO `dashboards` (`id`, `user_id`, `title`, `data`, `color`, `sort`, `deleted`) VALUES
(1, 1, 'leads', 'a:0:{}', '#29c2c2', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `email_templates`
--

CREATE TABLE `email_templates` (
  `id` int(11) NOT NULL,
  `template_name` varchar(50) NOT NULL,
  `email_subject` text NOT NULL,
  `default_message` mediumtext NOT NULL,
  `custom_message` mediumtext DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `email_templates`
--

INSERT INTO `email_templates` (`id`, `template_name`, `email_subject`, `default_message`, `custom_message`, `deleted`) VALUES
(1, 'login_info', 'Login details', '<div style=\"background-color: #eeeeef; padding: 50px 0; \"><div style=\"max-width:640px; margin:0 auto; \"> <div style=\"color: #fff; text-align: center; background-color:#33333e; padding: 30px; border-top-left-radius: 3px; border-top-right-radius: 3px; margin: 0;\">  <h1>Login Details</h1></div><div style=\"padding: 20px; background-color: rgb(255, 255, 255);\">            <p style=\"color: rgb(85, 85, 85); font-size: 14px;\"> Hello {USER_FIRST_NAME} {USER_LAST_NAME},<br><br>An account has been created for you.</p>            <p style=\"color: rgb(85, 85, 85); font-size: 14px;\"> Please use the following info to login your dashboard:</p>            <hr>            <p style=\"color: rgb(85, 85, 85); font-size: 14px;\">Dashboard URL:&nbsp;<a href=\"{DASHBOARD_URL}\" target=\"_blank\">{DASHBOARD_URL}</a></p>            <p style=\"color: rgb(85, 85, 85); font-size: 14px;\"></p>            <p style=\"\"><span style=\"color: rgb(85, 85, 85); font-size: 14px; line-height: 20px;\">Email: {USER_LOGIN_EMAIL}</span><br></p>            <p style=\"\"><span style=\"color: rgb(85, 85, 85); font-size: 14px; line-height: 20px;\">Password:&nbsp;{USER_LOGIN_PASSWORD}</span></p>            <p style=\"color: rgb(85, 85, 85);\"><br></p>            <p style=\"color: rgb(85, 85, 85); font-size: 14px;\">{SIGNATURE}</p>        </div>    </div></div>', '', 0),
(2, 'reset_password', 'Reset password', '<div style=\"background-color: #eeeeef; padding: 50px 0; \"><div style=\"max-width:640px; margin:0 auto; \"><div style=\"color: #fff; text-align: center; background-color:#33333e; padding: 30px; border-top-left-radius: 3px; border-top-right-radius: 3px; margin: 0;\"><h1>Reset Password</h1>\n </div>\n <div style=\"padding: 20px; background-color: rgb(255, 255, 255); color:#555;\">                    <p style=\"font-size: 14px;\"> Hello {ACCOUNT_HOLDER_NAME},<br><br>A password reset request has been created for your account.&nbsp;</p>\n                    <p style=\"font-size: 14px;\"> To initiate the password reset process, please click on the following link:</p>\n                    <p style=\"font-size: 14px;\"><a href=\"{RESET_PASSWORD_URL}\" target=\"_blank\">Reset Password</a></p>\n                    <p style=\"font-size: 14px;\"></p>\n                    <p style=\"\"><span style=\"font-size: 14px; line-height: 20px;\"><br></span></p>\n<p style=\"\"><span style=\"font-size: 14px; line-height: 20px;\">If you\'ve received this mail in error, it\'s likely that another user entered your email address by mistake while trying to reset a password.</span><br></p>\n<p style=\"\"><span style=\"font-size: 14px; line-height: 20px;\">If you didn\'t initiate the request, you don\'t need to take any further action and can safely disregard this email.</span><br></p>\n<p style=\"font-size: 14px;\"><br></p>\n<p style=\"font-size: 14px;\">{SIGNATURE}</p>\n                </div>\n            </div>\n        </div>', '', 0),
(3, 'team_member_invitation', 'You are invited', '<div style=\"background-color: #eeeeef; padding: 50px 0; \"><div style=\"max-width:640px; margin:0 auto; \"> <div style=\"color: #fff; text-align: center; background-color:#33333e; padding: 30px; border-top-left-radius: 3px; border-top-right-radius: 3px; margin: 0;\"><h1>Account Invitation</h1>   </div>  <div style=\"padding: 20px; background-color: rgb(255, 255, 255);\">            <p style=\"\"><span style=\"color: rgb(85, 85, 85); font-size: 14px; line-height: 20px;\">Hello,</span><span style=\"color: rgb(85, 85, 85); font-size: 14px; line-height: 20px;\"><span style=\"font-weight: bold;\"><br></span></span></p>            <p style=\"\"><span style=\"color: rgb(85, 85, 85); font-size: 14px; line-height: 20px;\"><span style=\"font-weight: bold;\">{INVITATION_SENT_BY}</span> has sent you an invitation to join with a team.</span></p><p style=\"\"><span style=\"color: rgb(85, 85, 85); font-size: 14px; line-height: 20px;\"><br></span></p>            <p style=\"\"><span style=\"color: rgb(85, 85, 85); font-size: 14px; line-height: 20px;\"><a style=\"background-color: #00b393; padding: 10px 15px; color: #ffffff;\" href=\"{INVITATION_URL}\" target=\"_blank\">Accept this Invitation</a></span></p>            <p style=\"\"><span style=\"color: rgb(85, 85, 85); font-size: 14px; line-height: 20px;\"><br></span></p><p style=\"\"><span style=\"color: rgb(85, 85, 85); font-size: 14px; line-height: 20px;\">If you don\'t want to accept this invitation, simply ignore this email.</span><br><br></p>            <p style=\"color: rgb(85, 85, 85); font-size: 14px;\">{SIGNATURE}</p>        </div>    </div></div>', '', 0),
(4, 'send_invoice', 'New invoice', '<div style=\"background-color: #eeeeef; padding: 50px 0; \"> <div style=\"max-width:640px; margin:0 auto; \"> <div style=\"color: #fff; text-align: center; background-color:#33333e; padding: 30px; border-top-left-radius: 3px; border-top-right-radius: 3px; margin: 0;\"><h1>INVOICE #{INVOICE_ID}</h1></div> <div style=\"padding: 20px; background-color: rgb(255, 255, 255);\">  <p style=\"\"><span style=\"color: rgb(85, 85, 85); font-size: 14px; line-height: 20px;\">Hello {CONTACT_FIRST_NAME},</span><br></p><p style=\"\"><span style=\"font-size: 14px; line-height: 20px;\">Thank you for your business cooperation.</span><br></p><p style=\"\"><span style=\"color: rgb(85, 85, 85); font-size: 14px; line-height: 20px;\">Your invoice for the project {PROJECT_TITLE} has been generated and is attached here.</span></p><p style=\"\"><br></p><p style=\"\"><span style=\"color: rgb(85, 85, 85); font-size: 14px; line-height: 20px;\"><a style=\"background-color: #00b393; padding: 10px 15px; color: #ffffff;\" href=\"{INVOICE_URL}\" target=\"_blank\">Show Invoice</a></span></p><p style=\"\"><span style=\"font-size: 14px; line-height: 20px;\"><br></span></p><p style=\"\"><span style=\"font-size: 14px; line-height: 20px;\">Invoice balance due is {BALANCE_DUE}</span><br></p><p style=\"\"><span style=\"color: rgb(85, 85, 85); font-size: 14px; line-height: 20px;\">Please pay this invoice within {DUE_DATE}.&nbsp;</span></p><p style=\"\"><span style=\"color: rgb(85, 85, 85); font-size: 14px; line-height: 20px;\"><br></span></p><p style=\"color: rgb(85, 85, 85); font-size: 14px;\">{SIGNATURE}</p>  </div> </div></div>', '', 0),
(5, 'signature', 'Signature', 'Powered By: <a href=\"http://fairsketch.com/\" target=\"_blank\">fairsketch </a>', '', 0),
(6, 'client_contact_invitation', 'You are invited', '<div style=\"background-color: #eeeeef; padding: 50px 0; \">    <div style=\"max-width:640px; margin:0 auto; \">  <div style=\"color: #fff; text-align: center; background-color:#33333e; padding: 30px; border-top-left-radius: 3px; border-top-right-radius: 3px; margin: 0;\"><h1>Account Invitation</h1> </div> <div style=\"padding: 20px; background-color: rgb(255, 255, 255);\">            <p style=\"\"><span style=\"color: rgb(85, 85, 85); font-size: 14px; line-height: 20px;\">Hello,</span><span style=\"color: rgb(85, 85, 85); font-size: 14px; line-height: 20px;\"><span style=\"font-weight: bold;\"><br></span></span></p>            <p style=\"\"><span style=\"color: rgb(85, 85, 85); font-size: 14px; line-height: 20px;\"><span style=\"font-weight: bold;\">{INVITATION_SENT_BY}</span> has sent you an invitation to a client portal.</span></p><p style=\"\"><span style=\"color: rgb(85, 85, 85); font-size: 14px; line-height: 20px;\"><br></span></p>            <p style=\"\"><span style=\"color: rgb(85, 85, 85); font-size: 14px; line-height: 20px;\"><a style=\"background-color: #00b393; padding: 10px 15px; color: #ffffff;\" href=\"{INVITATION_URL}\" target=\"_blank\">Accept this Invitation</a></span></p>            <p style=\"\"><span style=\"color: rgb(85, 85, 85); font-size: 14px; line-height: 20px;\"><br></span></p><p style=\"\"><span style=\"color: rgb(85, 85, 85); font-size: 14px; line-height: 20px;\">If you don\'t want to accept this invitation, simply ignore this email.</span><br><br></p>            <p style=\"color: rgb(85, 85, 85); font-size: 14px;\">{SIGNATURE}</p>        </div>    </div></div>', '', 0),
(7, 'ticket_created', 'Ticket  #{TICKET_ID} - {TICKET_TITLE}', '<div style=\"background-color: #eeeeef; padding: 50px 0; \"> <div style=\"max-width:640px; margin:0 auto; \"> <div style=\"color: #fff; text-align: center; background-color:#33333e; padding: 30px; border-top-left-radius: 3px; border-top-right-radius: 3px; margin: 0;\"><h1>Ticket #{TICKET_ID} Opened</h1></div><div style=\"padding: 20px; background-color: rgb(255, 255, 255);\"><p style=\"\"><span style=\"line-height: 18.5714px; font-weight: bold;\">Title: {TICKET_TITLE}</span><span style=\"line-height: 18.5714px;\"><br></span></p><p style=\"\"><span style=\"line-height: 18.5714px;\">{TICKET_CONTENT}</span><br></p> <p style=\"\"><br></p> <p style=\"\"><span style=\"color: rgb(85, 85, 85); font-size: 14px; line-height: 20px;\"><a style=\"background-color: #00b393; padding: 10px 15px; color: #ffffff;\" href=\"{TICKET_URL}\" target=\"_blank\">Show Ticket</a></span></p> <p style=\"\"><br></p><p style=\"\">Regards,</p><p style=\"\"><span style=\"line-height: 18.5714px;\">{USER_NAME}</span><br></p>   </div>  </div> </div>', '', 0),
(8, 'ticket_commented', 'Ticket  #{TICKET_ID} - {TICKET_TITLE}', '<div style=\"background-color: #eeeeef; padding: 50px 0; \"> <div style=\"max-width:640px; margin:0 auto; \"> <div style=\"color: #fff; text-align: center; background-color:#33333e; padding: 30px; border-top-left-radius: 3px; border-top-right-radius: 3px; margin: 0;\"><h1>Ticket #{TICKET_ID} Replies</h1></div><div style=\"padding: 20px; background-color: rgb(255, 255, 255);\"><p style=\"\"><span style=\"line-height: 18.5714px; font-weight: bold;\">Title: {TICKET_TITLE}</span><span style=\"line-height: 18.5714px;\"><br></span></p><p style=\"\"><span style=\"line-height: 18.5714px;\">{TICKET_CONTENT}</span></p><p style=\"\"><span style=\"line-height: 18.5714px;\"><br></span></p><p style=\"\"><span style=\"color: rgb(85, 85, 85); font-size: 14px; line-height: 20px;\"><a style=\"background-color: #00b393; padding: 10px 15px; color: #ffffff;\" href=\"{TICKET_URL}\" target=\"_blank\">Show Ticket</a></span></p></div></div></div>', '', 0),
(9, 'ticket_closed', 'Ticket  #{TICKET_ID} - {TICKET_TITLE}', '<div style=\"background-color: #eeeeef; padding: 50px 0; \"> <div style=\"max-width:640px; margin:0 auto; \"> <div style=\"color: #fff; text-align: center; background-color:#33333e; padding: 30px; border-top-left-radius: 3px; border-top-right-radius: 3px; margin: 0;\"><h1>Ticket #{TICKET_ID}</h1></div><div style=\"padding: 20px; background-color: rgb(255, 255, 255);\"><p style=\"\"><span style=\"line-height: 18.5714px;\">The Ticket #{TICKET_ID} has been closed by&nbsp;</span><span style=\"line-height: 18.5714px;\">{USER_NAME}</span></p> <p style=\"\"><br></p> <p style=\"\"><span style=\"color: rgb(85, 85, 85); font-size: 14px; line-height: 20px;\"><a style=\"background-color: #00b393; padding: 10px 15px; color: #ffffff;\" href=\"{TICKET_URL}\" target=\"_blank\">Show Ticket</a></span></p>   </div>  </div> </div>', '', 0),
(10, 'ticket_reopened', 'Ticket  #{TICKET_ID} - {TICKET_TITLE}', '<div style=\"background-color: #eeeeef; padding: 50px 0; \"> <div style=\"max-width:640px; margin:0 auto; \"> <div style=\"color: #fff; text-align: center; background-color:#33333e; padding: 30px; border-top-left-radius: 3px; border-top-right-radius: 3px; margin: 0;\"><h1>Ticket #{TICKET_ID}</h1></div><div style=\"padding: 20px; background-color: rgb(255, 255, 255);\"><p style=\"\"><span style=\"line-height: 18.5714px;\">The Ticket #{TICKET_ID} has been reopened by&nbsp;</span><span style=\"line-height: 18.5714px;\">{USER_NAME}</span></p><p style=\"\"><br></p><p style=\"\"><span style=\"color: rgb(85, 85, 85); font-size: 14px; line-height: 20px;\"><a style=\"background-color: #00b393; padding: 10px 15px; color: #ffffff;\" href=\"{TICKET_URL}\" target=\"_blank\">Show Ticket</a></span></p>  </div> </div></div>', '', 0),
(11, 'general_notification', '{EVENT_TITLE}', '<div style=\"background-color: #eeeeef; padding: 50px 0; \"> <div style=\"max-width:640px; margin:0 auto; \"> <div style=\"color: #fff; text-align: center; background-color:#33333e; padding: 30px; border-top-left-radius: 3px; border-top-right-radius: 3px; margin: 0;\"><h1>{APP_TITLE}</h1></div><div style=\"padding: 20px; background-color: rgb(255, 255, 255);\"><p style=\"\"><span style=\"line-height: 18.5714px;\">{EVENT_TITLE}</span></p><p style=\"\"><span style=\"line-height: 18.5714px;\">{EVENT_DETAILS}</span></p><p style=\"\"><span style=\"line-height: 18.5714px;\"><br></span></p><p style=\"\"><span style=\"line-height: 18.5714px;\"></span></p><p style=\"\"><span style=\"color: rgb(85, 85, 85); font-size: 14px; line-height: 20px;\"><a style=\"background-color: #00b393; padding: 10px 15px; color: #ffffff;\" href=\"{NOTIFICATION_URL}\" target=\"_blank\">View Details</a></span></p>  </div> </div></div>', '', 0),
(12, 'invoice_payment_confirmation', 'Payment received', '<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #EEEEEE;border-top: 0;border-bottom: 0;\">\r\n <tbody><tr>\r\n <td align=\"center\" valign=\"top\" style=\"padding-top: 30px;padding-right: 10px;padding-bottom: 30px;padding-left: 10px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\">\r\n <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\" style=\"border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\">\r\n <tbody><tr>\r\n <td align=\"center\" valign=\"top\" style=\"mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\">\r\n <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #FFFFFF;\">\r\n                                        <tbody><tr>\r\n                                                <td valign=\"top\" style=\"mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\">\r\n                                                    <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\">\r\n                                                        <tbody>\r\n                                                            <tr>\r\n                                                                <td valign=\"top\" style=\"mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\">\r\n                                                                    <table align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"background-color: #33333e; max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\" width=\"100%\">\r\n                                                                        <tbody><tr>\r\n                                                                                <td valign=\"top\" style=\"padding-top: 40px;padding-right: 18px;padding-bottom: 40px;padding-left: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #606060;font-family: Arial;font-size: 15px;line-height: 150%;text-align: left;\">\r\n                                                                                    <h2 style=\"display: block;margin: 0;padding: 0;font-family: Arial;font-size: 30px;font-style: normal;font-weight: bold;line-height: 100%;letter-spacing: -1px;text-align: center;color: #ffffff !important;\">Payment Confirmation</h2>\r\n                                                                                </td>\r\n                                                                            </tr>\r\n                                                                        </tbody>\r\n                                                                    </table>\r\n                                                                </td>\r\n                                                            </tr>\r\n                                                        </tbody>\r\n                                                    </table>\r\n                                                    <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\">\r\n                                                        <tbody>\r\n                                                            <tr>\r\n                                                                <td valign=\"top\" style=\"mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\">\r\n\r\n                                                                    <table align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\" width=\"100%\">\r\n                                                                        <tbody><tr>\r\n                                                                                <td valign=\"top\" style=\"padding-top: 20px;padding-right: 18px;padding-bottom: 0;padding-left: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #606060;font-family: Arial;font-size: 15px;line-height: 150%;text-align: left;\">\r\n                                                                                    Hello,<br>\r\n                                                                                    We have received your payment of {PAYMENT_AMOUNT} for {INVOICE_ID} <br>\r\n                                                                                    Thank you for your business cooperation.\r\n                                                                                </td>\r\n                                                                            </tr>\r\n                                                                            <tr>\r\n                                                                                <td valign=\"top\" style=\"padding-top: 10px;padding-right: 18px;padding-bottom: 10px;padding-left: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #606060;font-family: Arial;font-size: 15px;line-height: 150%;text-align: left;\">\r\n                                                                                    <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\">\r\n                                                                                        <tbody>\r\n                                                                                            <tr>\r\n                                                                                                <td style=\"padding-top: 15px;padding-right: 0x;padding-bottom: 15px;padding-left: 0px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\">\r\n                                                                                                    <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse: separate !important;border-radius: 2px;background-color: #00b393;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\">\r\n                                                                                                        <tbody>\r\n                                                                                                            <tr>\r\n                                                                                                                <td align=\"center\" valign=\"middle\" style=\"font-family: Arial;font-size: 16px;padding: 10px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\">\r\n                                                                                                                    <a href=\"{INVOICE_URL}\" target=\"_blank\" style=\"font-weight: bold;letter-spacing: normal;line-height: 100%;text-align: center;text-decoration: none;color: #FFFFFF;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;display: block;\">View Invoice</a>\r\n                                                                                                                </td>\r\n                                                                                                            </tr>\r\n                                                                                                        </tbody>\r\n                                                                                                    </table>\r\n                                                                                                </td>\r\n                                                                                            </tr>\r\n                                                                                        </tbody>\r\n                                                                                    </table>\r\n                                                                                </td>\r\n                                                                            </tr>\r\n                                                                            <tr>\r\n                                                                                <td valign=\"top\" style=\"padding-top: 0px;padding-right: 18px;padding-bottom: 10px;padding-left: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #606060;font-family: Arial;font-size: 15px;line-height: 150%;text-align: left;\"> \r\n                                                                                    \r\n                                                                                </td>\r\n                                                                            </tr>\r\n                                                                            <tr>\r\n                                                                                <td valign=\"top\" style=\"padding-top: 0px;padding-right: 18px;padding-bottom: 20px;padding-left: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #606060;font-family: Arial;font-size: 15px;line-height: 150%;text-align: left;\"> \r\n  {SIGNATURE}\r\n  </td>\r\n </tr>\r\n </tbody>\r\n  </table>\r\n  </td>\r\n  </tr>\r\n  </tbody>\r\n </table>\r\n  </td>\r\n </tr>\r\n  </tbody>\r\n  </table>\r\n  </td>\r\n </tr>\r\n </tbody>\r\n </table>\r\n </td>\r\n </tr>\r\n </tbody>\r\n  </table>', NULL, 0),
(13, 'message_received', '{SUBJECT}', '<meta content=\"text/html; charset=utf-8\" http-equiv=\"Content-Type\"> <meta content=\"width=device-width, initial-scale=1.0\" name=\"viewport\"> <style type=\"text/css\"> #message-container p {margin: 10px 0;} #message-container h1, #message-container h2, #message-container h3, #message-container h4, #message-container h5, #message-container h6 { padding:10px; margin:0; } #message-container table td {border-collapse: collapse;} #message-container table { border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; } #message-container a span{padding:10px 15px !important;} </style> <table id=\"message-container\" align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"background:#eee; margin:0; padding:0; width:100% !important; line-height: 100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0; font-family:Helvetica,Arial,sans-serif; color: #555;\"> <tbody> <tr> <td valign=\"top\"> <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"> <tbody> <tr> <td height=\"50\" width=\"600\">&nbsp;</td> </tr> <tr> <td style=\"background-color:#33333e; padding:25px 15px 30px 15px; font-weight:bold; \" width=\"600\"><h2 style=\"color:#fff; text-align:center;\">{USER_NAME} sent you a message</h2></td> </tr> <tr> <td bgcolor=\"whitesmoke\" style=\"background:#fff; font-family:Helvetica,Arial,sans-serif\" valign=\"top\" width=\"600\"> <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"> <tbody> <tr> <td height=\"10\" width=\"560\">&nbsp;</td> </tr> <tr> <td width=\"560\"><p><span style=\"background-color: transparent;\">{MESSAGE_CONTENT}</span></p> <p style=\"display:inline-block; padding: 10px 15px; background-color: #00b393;\"><a href=\"{MESSAGE_URL}\" style=\"text-decoration: none; color:#fff;\" target=\"_blank\">Reply Message</a></p> </td> </tr> <tr> <td height=\"10\" width=\"560\">&nbsp;</td> </tr> </tbody> </table> </td> </tr> <tr> <td height=\"60\" width=\"600\">&nbsp;</td> </tr> </tbody> </table> </td> </tr> </tbody> </table>', '', 0),
(14, 'invoice_due_reminder_before_due_date', 'Invoice due reminder', '<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #EEEEEE;border-top: 0;border-bottom: 0;\"> <tbody><tr> <td align=\"center\" valign=\"top\" style=\"padding-top: 30px;padding-right: 10px;padding-bottom: 30px;padding-left: 10px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\"> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\" style=\"border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\"> <tbody><tr> <td align=\"center\" valign=\"top\" style=\"mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\"> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #FFFFFF;\"> <tbody><tr> <td valign=\"top\" style=\"mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\"> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\"> <tbody> <tr> <td valign=\"top\" style=\"mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\"> <table align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"background-color: #33333e; max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\" width=\"100%\"> <tbody><tr> <td valign=\"top\" style=\"padding-top: 40px;padding-right: 18px;padding-bottom: 40px;padding-left: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #606060;font-family: Arial;font-size: 15px;line-height: 150%;text-align: left;\"> <h2 style=\"display: block;margin: 0;padding: 0;font-family: Arial;font-size: 30px;font-style: normal;font-weight: bold;line-height: 100%;letter-spacing: -1px;text-align: center;color: #ffffff !important;\">Invoice Due Reminder</h2></td></tr></tbody></table></td></tr></tbody></table> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\"> <tbody> <tr> <td valign=\"top\" style=\"mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\"> <table align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\" width=\"100%\"> <tbody><tr> <td valign=\"top\" style=\"padding-top: 20px;padding-right: 18px;padding-bottom: 0;padding-left: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #606060;font-family: Arial;font-size: 15px;line-height: 150%;text-align: left;\"><p> Hello,<br>We would like to remind you that invoice {INVOICE_ID} is due on {DUE_DATE}. Please pay the invoice within due date.&nbsp;</p><p></p></td></tr><tr><td valign=\"top\" style=\"padding-top: 10px;padding-right: 18px;padding-bottom: 10px;padding-left: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #606060;font-family: Arial;font-size: 15px;line-height: 150%;text-align: left;\"><p>If you have already submitted the payment, please ignore this email.</p><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\"><tbody><tr><td style=\"padding-top: 15px;padding-right: 0x;padding-bottom: 15px;padding-left: 0px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\"><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse: separate !important;border-radius: 2px;background-color: #00b393;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\"><tbody><tr><td align=\"center\" valign=\"middle\" style=\"font-family: Arial;font-size: 16px;padding: 10px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\"><a href=\"{INVOICE_URL}\" target=\"_blank\" style=\"font-weight: bold;letter-spacing: normal;line-height: 100%;text-align: center;text-decoration: none;color: #FFFFFF;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;display: block;\">View Invoice</a> </td> </tr> </tbody> </table> </td> </tr> </tbody> </table> <p></p></td> </tr> <tr> <td valign=\"top\" style=\"padding-top: 0px;padding-right: 18px;padding-bottom: 20px;padding-left: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #606060;font-family: Arial;font-size: 15px;line-height: 150%;text-align: left;\"> {SIGNATURE} </td> </tr> </tbody> </table> </td> </tr> </tbody> </table> </td> </tr> </tbody> </table> </td> </tr> </tbody> </table> </td> </tr> </tbody> </table>', '', 0),
(15, 'invoice_overdue_reminder', 'Invoice overdue reminder', '<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #EEEEEE;border-top: 0;border-bottom: 0;\"> <tbody><tr> <td align=\"center\" valign=\"top\" style=\"padding-top: 30px;padding-right: 10px;padding-bottom: 30px;padding-left: 10px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\"> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\" style=\"border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\"> <tbody><tr> <td align=\"center\" valign=\"top\" style=\"mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\"> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #FFFFFF;\"> <tbody><tr> <td valign=\"top\" style=\"mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\"> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\"> <tbody> <tr> <td valign=\"top\" style=\"mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\"> <table align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"background-color: #33333e; max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\" width=\"100%\"> <tbody><tr> <td valign=\"top\" style=\"padding-top: 40px;padding-right: 18px;padding-bottom: 40px;padding-left: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #606060;font-family: Arial;font-size: 15px;line-height: 150%;text-align: left;\"> <h2 style=\"display: block;margin: 0;padding: 0;font-family: Arial;font-size: 30px;font-style: normal;font-weight: bold;line-height: 100%;letter-spacing: -1px;text-align: center;color: #ffffff !important;\">Invoice Overdue Reminder</h2></td></tr></tbody></table></td></tr></tbody></table> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\"> <tbody> <tr> <td valign=\"top\" style=\"mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\"> <table align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\" width=\"100%\"> <tbody><tr> <td valign=\"top\" style=\"padding-top: 20px;padding-right: 18px;padding-bottom: 0;padding-left: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #606060;font-family: Arial;font-size: 15px;line-height: 150%;text-align: left;\"><p> Hello,<br>We would like to remind you that you have an unpaid invoice {INVOICE_ID}. We kindly request you to pay the invoice as soon as possible.&nbsp;</p><p></p></td></tr><tr><td valign=\"top\" style=\"padding-top: 10px;padding-right: 18px;padding-bottom: 10px;padding-left: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #606060;font-family: Arial;font-size: 15px;line-height: 150%;text-align: left;\"><p>If you have already submitted the payment, please ignore this email.</p><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\"><tbody><tr><td style=\"padding-top: 15px;padding-right: 0x;padding-bottom: 15px;padding-left: 0px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\"><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse: separate !important;border-radius: 2px;background-color: #00b393;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\"><tbody><tr><td align=\"center\" valign=\"middle\" style=\"font-family: Arial;font-size: 16px;padding: 10px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\"><a href=\"{INVOICE_URL}\" target=\"_blank\" style=\"font-weight: bold;letter-spacing: normal;line-height: 100%;text-align: center;text-decoration: none;color: #FFFFFF;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;display: block;\">View Invoice</a> </td> </tr> </tbody> </table> </td> </tr> </tbody> </table> <p></p></td> </tr> <tr> <td valign=\"top\" style=\"padding-top: 0px;padding-right: 18px;padding-bottom: 20px;padding-left: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #606060;font-family: Arial;font-size: 15px;line-height: 150%;text-align: left;\"> {SIGNATURE} </td> </tr> </tbody> </table> </td> </tr> </tbody> </table> </td> </tr> </tbody> </table> </td> </tr> </tbody> </table> </td> </tr> </tbody> </table>', '', 0),
(16, 'recurring_invoice_creation_reminder', 'Recurring invoice creation reminder', '<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #EEEEEE;border-top: 0;border-bottom: 0;\"> <tbody><tr> <td align=\"center\" valign=\"top\" style=\"padding-top: 30px;padding-right: 10px;padding-bottom: 30px;padding-left: 10px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\"> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\" style=\"border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\"> <tbody><tr> <td align=\"center\" valign=\"top\" style=\"mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\"> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #FFFFFF;\"> <tbody><tr> <td valign=\"top\" style=\"mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\"> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\"> <tbody> <tr> <td valign=\"top\" style=\"mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\"> <table align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"background-color: #33333e; max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\" width=\"100%\"> <tbody><tr> <td valign=\"top\" style=\"padding-top: 40px;padding-right: 18px;padding-bottom: 40px;padding-left: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #606060;font-family: Arial;font-size: 15px;line-height: 150%;text-align: left;\"> <h2 style=\"display: block;margin: 0;padding: 0;font-family: Arial;font-size: 30px;font-style: normal;font-weight: bold;line-height: 100%;letter-spacing: -1px;text-align: center;color: #ffffff !important;\">Invoice Cration Reminder</h2></td></tr></tbody></table></td></tr></tbody></table> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\"> <tbody> <tr> <td valign=\"top\" style=\"mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\"> <table align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\" width=\"100%\"> <tbody><tr> <td valign=\"top\" style=\"padding-top: 20px;padding-right: 18px;padding-bottom: 0;padding-left: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #606060;font-family: Arial;font-size: 15px;line-height: 150%;text-align: left;\"><p> Hello,<br>We would like to remind you that a recurring invoice will be created on {NEXT_RECURRING_DATE}.</p><p></p></td></tr><tr><td valign=\"top\" style=\"padding-top: 10px;padding-right: 18px;padding-bottom: 10px;padding-left: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #606060;font-family: Arial;font-size: 15px;line-height: 150%;text-align: left;\"><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"min-width: 100%; text-size-adjust: 100%;\"><tbody><tr><td style=\"padding-top: 15px; padding-bottom: 15px; text-size-adjust: 100%;\"><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse: separate !important;border-radius: 2px;background-color: #00b393;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\"><tbody><tr><td align=\"center\" valign=\"middle\" style=\"font-size: 16px; padding: 10px; text-size-adjust: 100%;\"><a href=\"{INVOICE_URL}\" target=\"_blank\" style=\"font-weight: bold; line-height: 100%; color: rgb(255, 255, 255); text-size-adjust: 100%; display: block;\">View Invoice</a></td></tr></tbody></table></td></tr></tbody></table> <p></p></td> </tr> <tr> <td valign=\"top\" style=\"padding-top: 0px;padding-right: 18px;padding-bottom: 20px;padding-left: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #606060;font-family: Arial;font-size: 15px;line-height: 150%;text-align: left;\"> {SIGNATURE} </td> </tr> </tbody> </table> </td> </tr> </tbody> </table> </td> </tr> </tbody> </table> </td> </tr> </tbody> </table> </td> </tr> </tbody> </table>', '', 0),
(17, 'project_task_deadline_reminder', 'Project task deadline reminder', '<div style=\"background-color: #eeeeef; padding: 50px 0; \"> <div style=\"max-width:640px; margin:0 auto; \"> <div style=\"color: #fff; text-align: center; background-color:#33333e; padding: 30px; border-top-left-radius: 3px; border-top-right-radius: 3px; margin: 0;\"><h1>{APP_TITLE}</h1></div> <div style=\"padding: 20px; background-color: rgb(255, 255, 255);\">  <p style=\"\"><span style=\"color: rgb(85, 85, 85); font-size: 14px; line-height: 20px;\">Hello,</span></p><p style=\"\"><span style=\"color: rgb(85, 85, 85); font-size: 14px; line-height: 20px;\">This is to remind you that there are some tasks which deadline is {DEADLINE}.</span></p><p style=\"\"><span style=\"color: rgb(85, 85, 85); font-size: 14px; line-height: 20px;\">{TASKS_LIST}</span></p><p style=\"\"><span style=\"color: rgb(85, 85, 85); font-size: 14px; line-height: 20px;\"><br></span></p><p style=\"color: rgb(85, 85, 85); font-size: 14px;\">{SIGNATURE}</p>  </div> </div></div>', '', 0),
(18, 'estimate_sent', 'New estimate', '<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #EEEEEE;border-top: 0;border-bottom: 0;\"> <tbody><tr> <td align=\"center\" valign=\"top\" style=\"padding-top: 30px;padding-right: 10px;padding-bottom: 30px;padding-left: 10px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\"> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\" style=\"border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\"> <tbody><tr> <td align=\"center\" valign=\"top\" style=\"mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\"> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #FFFFFF;\"> <tbody><tr> <td valign=\"top\" style=\"mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\"> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\"> <tbody> <tr> <td valign=\"top\" style=\"mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\"> <table align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"background-color: #33333e; max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\" width=\"100%\"> <tbody><tr> <td valign=\"top\" style=\"padding: 40px 18px; text-size-adjust: 100%; word-break: break-word; line-height: 150%; text-align: left;\"> <h2 style=\"display: block; margin: 0px; padding: 0px; line-height: 100%; text-align: center;\"><font color=\"#ffffff\" face=\"Arial\"><span style=\"letter-spacing: -1px;\"><b>ESTIMATE #{ESTIMATE_ID}</b></span></font><br></h2></td></tr></tbody></table></td></tr></tbody></table> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\"> <tbody> <tr> <td valign=\"top\" style=\"mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\"> <table align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\" width=\"100%\"> <tbody><tr> <td valign=\"top\" style=\"padding-top: 20px;padding-right: 18px;padding-bottom: 0;padding-left: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #606060;font-family: Arial;font-size: 15px;line-height: 150%;text-align: left;\"><p> Hello {CONTACT_FIRST_NAME},<br></p><p>Here is the estimate. Please check the attachment.</p><p></p></td></tr><tr><td valign=\"top\" style=\"padding-top: 10px;padding-right: 18px;padding-bottom: 10px;padding-left: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #606060;font-family: Arial;font-size: 15px;line-height: 150%;text-align: left;\"><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"min-width: 100%; text-size-adjust: 100%;\"><tbody><tr><td style=\"padding-top: 15px; padding-bottom: 15px; text-size-adjust: 100%;\"><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse: separate !important;border-radius: 2px;background-color: #00b393;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\"><tbody><tr><td align=\"center\" valign=\"middle\" style=\"font-size: 16px; padding: 10px; text-size-adjust: 100%;\"><a href=\"{ESTIMATE_URL}\" target=\"_blank\" style=\"font-weight: bold; line-height: 100%; color: rgb(255, 255, 255); text-size-adjust: 100%; display: block;\">Show Estimate</a></td></tr></tbody></table></td></tr></tbody></table> <p></p></td> </tr> <tr> <td valign=\"top\" style=\"padding-top: 0px;padding-right: 18px;padding-bottom: 20px;padding-left: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #606060;font-family: Arial;font-size: 15px;line-height: 150%;text-align: left;\"> {SIGNATURE} </td> </tr> </tbody> </table> </td> </tr> </tbody> </table> </td> </tr> </tbody> </table> </td> </tr> </tbody> </table> </td> </tr> </tbody> </table>', '', 0);
INSERT INTO `email_templates` (`id`, `template_name`, `email_subject`, `default_message`, `custom_message`, `deleted`) VALUES
(19, 'estimate_request_received', 'Estimate request received', '<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #EEEEEE;border-top: 0;border-bottom: 0;\"> <tbody><tr> <td align=\"center\" valign=\"top\" style=\"padding-top: 30px;padding-right: 10px;padding-bottom: 30px;padding-left: 10px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\"> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\" style=\"border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\"> <tbody><tr> <td align=\"center\" valign=\"top\" style=\"mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\"> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #FFFFFF;\"> <tbody><tr> <td valign=\"top\" style=\"mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\"> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\"> <tbody> <tr> <td valign=\"top\" style=\"mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\"> <table align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"background-color: #33333e; max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\" width=\"100%\"> <tbody><tr> <td valign=\"top\" style=\"padding: 40px 18px; text-size-adjust: 100%; word-break: break-word; line-height: 150%; text-align: left;\"> <h2 style=\"display: block; margin: 0px; padding: 0px; line-height: 100%; text-align: center;\"><font color=\"#ffffff\" face=\"Arial\"><span style=\"letter-spacing: -1px;\"><b>ESTIMATE REQUEST #{ESTIMATE_REQUEST_ID}</b></span></font><br></h2></td></tr></tbody></table></td></tr></tbody></table> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\"> <tbody> <tr> <td valign=\"top\" style=\"mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\"> <table align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\" width=\"100%\"> <tbody><tr> <td valign=\"top\" style=\"padding: 20px 18px 0px; text-size-adjust: 100%; word-break: break-word; line-height: 150%; text-align: left;\"><p style=\"color: rgb(96, 96, 96); font-family: Arial; font-size: 15px;\"><span style=\"background-color: transparent;\">A new estimate request has been received from {CONTACT_FIRST_NAME}.</span><br></p><p style=\"color: rgb(96, 96, 96); font-family: Arial; font-size: 15px;\"></p></td></tr><tr><td valign=\"top\" style=\"padding-top: 10px;padding-right: 18px;padding-bottom: 10px;padding-left: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #606060;font-family: Arial;font-size: 15px;line-height: 150%;text-align: left;\"><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"min-width: 100%; text-size-adjust: 100%;\"><tbody><tr><td style=\"padding-top: 15px; padding-bottom: 15px; text-size-adjust: 100%;\"><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse: separate !important;border-radius: 2px;background-color: #00b393;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\"><tbody><tr><td align=\"center\" valign=\"middle\" style=\"font-size: 16px; padding: 10px; text-size-adjust: 100%;\"><a href=\"{ESTIMATE_REQUEST_URL}\" target=\"_blank\" style=\"font-weight: bold; line-height: 100%; color: rgb(255, 255, 255); text-size-adjust: 100%; display: block;\">Show Estimate Request</a></td></tr></tbody></table></td></tr></tbody></table> <p></p></td> </tr> <tr> <td valign=\"top\" style=\"padding-top: 0px;padding-right: 18px;padding-bottom: 20px;padding-left: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #606060;font-family: Arial;font-size: 15px;line-height: 150%;text-align: left;\"> {SIGNATURE} </td> </tr> </tbody> </table> </td> </tr> </tbody> </table> </td> </tr> </tbody> </table> </td> </tr> </tbody> </table> </td> </tr> </tbody> </table>', '', 0),
(20, 'estimate_rejected', 'Estimate rejected', '<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #EEEEEE;border-top: 0;border-bottom: 0;\"> <tbody><tr> <td align=\"center\" valign=\"top\" style=\"padding-top: 30px;padding-right: 10px;padding-bottom: 30px;padding-left: 10px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\"> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\" style=\"border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\"> <tbody><tr> <td align=\"center\" valign=\"top\" style=\"mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\"> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #FFFFFF;\"> <tbody><tr> <td valign=\"top\" style=\"mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\"> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\"> <tbody> <tr> <td valign=\"top\" style=\"mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\"> <table align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"background-color: #33333e; max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\" width=\"100%\"> <tbody><tr> <td valign=\"top\" style=\"padding: 40px 18px; text-size-adjust: 100%; word-break: break-word; line-height: 150%; text-align: left;\"> <h2 style=\"display: block; margin: 0px; padding: 0px; line-height: 100%; text-align: center;\"><font color=\"#ffffff\" face=\"Arial\"><span style=\"letter-spacing: -1px;\"><b>ESTIMATE #{ESTIMATE_ID}</b></span></font><br></h2></td></tr></tbody></table></td></tr></tbody></table> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\"> <tbody> <tr> <td valign=\"top\" style=\"mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\"> <table align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\" width=\"100%\"> <tbody><tr> <td valign=\"top\" style=\"padding: 20px 18px 0px; text-size-adjust: 100%; word-break: break-word; line-height: 150%; text-align: left;\"><p style=\"\"><font color=\"#606060\" face=\"Arial\"><span style=\"font-size: 15px;\">The estimate #{ESTIMATE_ID} has been rejected.</span></font><br></p><p style=\"color: rgb(96, 96, 96); font-family: Arial; font-size: 15px;\"></p></td></tr><tr><td valign=\"top\" style=\"padding-top: 10px;padding-right: 18px;padding-bottom: 10px;padding-left: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #606060;font-family: Arial;font-size: 15px;line-height: 150%;text-align: left;\"><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"min-width: 100%; text-size-adjust: 100%;\"><tbody><tr><td style=\"padding-top: 15px; padding-bottom: 15px; text-size-adjust: 100%;\"><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse: separate !important;border-radius: 2px;background-color: #00b393;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\"><tbody><tr><td align=\"center\" valign=\"middle\" style=\"font-size: 16px; padding: 10px; text-size-adjust: 100%;\"><a href=\"{ESTIMATE_URL}\" target=\"_blank\" style=\"font-weight: bold; line-height: 100%; color: rgb(255, 255, 255); text-size-adjust: 100%; display: block;\">Show Estimate</a></td></tr></tbody></table></td></tr></tbody></table> <p></p></td> </tr> <tr> <td valign=\"top\" style=\"padding-top: 0px;padding-right: 18px;padding-bottom: 20px;padding-left: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #606060;font-family: Arial;font-size: 15px;line-height: 150%;text-align: left;\"> {SIGNATURE} </td> </tr> </tbody> </table> </td> </tr> </tbody> </table> </td> </tr> </tbody> </table> </td> </tr> </tbody> </table> </td> </tr> </tbody> </table>', '', 0),
(21, 'estimate_accepted', 'Estimate accepted', '<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #EEEEEE;border-top: 0;border-bottom: 0;\"> <tbody><tr> <td align=\"center\" valign=\"top\" style=\"padding-top: 30px;padding-right: 10px;padding-bottom: 30px;padding-left: 10px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\"> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\" style=\"border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\"> <tbody><tr> <td align=\"center\" valign=\"top\" style=\"mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\"> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #FFFFFF;\"> <tbody><tr> <td valign=\"top\" style=\"mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\"> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\"> <tbody> <tr> <td valign=\"top\" style=\"mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\"> <table align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"background-color: #33333e; max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\" width=\"100%\"> <tbody><tr> <td valign=\"top\" style=\"padding: 40px 18px; text-size-adjust: 100%; word-break: break-word; line-height: 150%; text-align: left;\"> <h2 style=\"display: block; margin: 0px; padding: 0px; line-height: 100%; text-align: center;\"><font color=\"#ffffff\" face=\"Arial\"><span style=\"letter-spacing: -1px;\"><b>ESTIMATE #{ESTIMATE_ID}</b></span></font><br></h2></td></tr></tbody></table></td></tr></tbody></table> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\"> <tbody> <tr> <td valign=\"top\" style=\"mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\"> <table align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\" width=\"100%\"> <tbody><tr> <td valign=\"top\" style=\"padding: 20px 18px 0px; text-size-adjust: 100%; word-break: break-word; line-height: 150%; text-align: left;\"><p style=\"\"><font color=\"#606060\" face=\"Arial\"><span style=\"font-size: 15px;\">The estimate #{ESTIMATE_ID} has been accepted.</span></font><br></p><p style=\"color: rgb(96, 96, 96); font-family: Arial; font-size: 15px;\"></p></td></tr><tr><td valign=\"top\" style=\"padding-top: 10px;padding-right: 18px;padding-bottom: 10px;padding-left: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #606060;font-family: Arial;font-size: 15px;line-height: 150%;text-align: left;\"><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"min-width: 100%; text-size-adjust: 100%;\"><tbody><tr><td style=\"padding-top: 15px; padding-bottom: 15px; text-size-adjust: 100%;\"><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse: separate !important;border-radius: 2px;background-color: #00b393;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\"><tbody><tr><td align=\"center\" valign=\"middle\" style=\"font-size: 16px; padding: 10px; text-size-adjust: 100%;\"><a href=\"{ESTIMATE_URL}\" target=\"_blank\" style=\"font-weight: bold; line-height: 100%; color: rgb(255, 255, 255); text-size-adjust: 100%; display: block;\">Show Estimate</a></td></tr></tbody></table></td></tr></tbody></table> <p></p></td> </tr> <tr> <td valign=\"top\" style=\"padding-top: 0px;padding-right: 18px;padding-bottom: 20px;padding-left: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #606060;font-family: Arial;font-size: 15px;line-height: 150%;text-align: left;\"> {SIGNATURE} </td> </tr> </tbody> </table> </td> </tr> </tbody> </table> </td> </tr> </tbody> </table> </td> </tr> </tbody> </table> </td> </tr> </tbody> </table>', '', 0),
(22, 'new_client_greetings', 'Welcome!', '<div style=\"background-color: #eeeeef; padding: 50px 0; \">    <div style=\"max-width:640px; margin:0 auto; \">  <div style=\"color: #fff; text-align: center; background-color:#33333e; padding: 30px; border-top-left-radius: 3px; border-top-right-radius: 3px; margin: 0;\"><h1>Welcome to {COMPANY_NAME}</h1> </div> <div style=\"padding: 20px; background-color: rgb(255, 255, 255);\">            <p><span style=\"color: rgb(85, 85, 85); font-size: 14px; line-height: 20px;\">Hello {CONTACT_FIRST_NAME},</span></p><p><span style=\"color: rgb(85, 85, 85); font-size: 14px; line-height: 20px;\">Thank you for creating your account. </span></p><p><span style=\"color: rgb(85, 85, 85); font-size: 14px; line-height: 20px;\">We are happy to see you here.<br></span></p><hr><p style=\"color: rgb(85, 85, 85); font-size: 14px;\">Dashboard URL:&nbsp;<a href=\"{DASHBOARD_URL}\" target=\"_blank\">{DASHBOARD_URL}</a></p><p style=\"color: rgb(85, 85, 85); font-size: 14px;\"></p><p><span style=\"color: rgb(85, 85, 85); font-size: 14px; line-height: 20px;\">Email: {CONTACT_LOGIN_EMAIL}</span><br></p><p><span style=\"color: rgb(85, 85, 85); font-size: 14px; line-height: 20px;\">Password:&nbsp;{CONTACT_LOGIN_PASSWORD}</span></p><p style=\"color: rgb(85, 85, 85);\"><br></p><p style=\"color: rgb(85, 85, 85); font-size: 14px;\">{SIGNATURE}</p>        </div>    </div></div>', '', 0),
(23, 'verify_email', 'Please verify your email', '<div style=\"background-color: #eeeeef; padding: 50px 0; \"><div style=\"max-width:640px; margin:0 auto; \"><div style=\"color: #fff; text-align: center; background-color:#33333e; padding: 30px; border-top-left-radius: 3px; border-top-right-radius: 3px; margin: 0;\"><h1>Account verification</h1></div><div style=\"padding: 20px; background-color: rgb(255, 255, 255); color:#555;\"><p style=\"font-size: 14px;\">To initiate the signup process, please click on the following link:<br></p><p style=\"font-size: 14px;\"><br></p><p style=\"\"><span style=\"color: rgb(85, 85, 85); font-size: 14px; line-height: 20px;\"><a style=\"background-color: #00b393; padding: 10px 15px; color: #ffffff;\" href=\"{VERIFY_EMAIL_URL}\" target=\"_blank\">Verify Email</a></span></p>  <p style=\"font-size: 14px;\"><br></p><p style=\"\"><span style=\"font-size: 14px;\">If you did not initiate the request, you do not need to take any further action and can safely disregard this email.</span></p><p style=\"\"><span style=\"font-size: 14px;\"><br></span></p><p style=\"font-size: 14px;\">{SIGNATURE}</p></div></div></div>', '', 0),
(24, 'new_order_received', 'New order received', '<div style=\"background-color: #eeeeef; padding: 50px 0; \"> <div style=\"max-width:640px; margin:0 auto; \"> <div style=\"color: #fff; text-align: center; background-color:#33333e; padding: 30px; border-top-left-radius: 3px; border-top-right-radius: 3px; margin: 0;\"><h1>ORDER #{ORDER_ID}</h1></div> <div style=\"padding: 20px; background-color: rgb(255, 255, 255);\">  <p style=\"\"><span style=\"color: rgb(85, 85, 85); font-size: 14px;\">A new order has been received from&nbsp;</span><span style=\"color: rgb(85, 85, 85); font-size: 14px;\">{CONTACT_FIRST_NAME} and is attached here.</span><br></p><p style=\"\"><br></p><p style=\"\"><span style=\"color: rgb(85, 85, 85); font-size: 14px; line-height: 20px;\"><a style=\"background-color: #00b393; padding: 10px 15px; color: #ffffff;\" href=\"{ORDER_URL}\" target=\"_blank\">Show Order</a></span></p><p style=\"\"><br></p>  </div> </div></div>', '', 0),
(25, 'order_status_updated', 'Order status updated', '<div style=\"background-color: #eeeeef; padding: 50px 0; \"> <div style=\"max-width:640px; margin:0 auto; \"> <div style=\"color: #fff; text-align: center; background-color:#33333e; padding: 30px; border-top-left-radius: 3px; border-top-right-radius: 3px; margin: 0;\"><h1>ORDER #{ORDER_ID}</h1></div> <div style=\"padding: 20px; background-color: rgb(255, 255, 255);\">  <p><span style=\"color: rgb(85, 85, 85); font-size: 14px; line-height: 20px;\">Hello {CONTACT_FIRST_NAME},</span><br></p><p><span style=\"font-size: 14px; line-height: 20px;\">Thank you for your business cooperation.</span><br></p><p><span style=\"color: rgb(85, 85, 85); font-size: 14px; line-height: 20px;\">Your order&nbsp;</span><font color=\"#555555\"><span style=\"font-size: 14px;\">has been updated&nbsp;</span></font><span style=\"color: rgb(85, 85, 85); font-size: 14px;\">and is attached here.</span></p><p style=\"\"><br></p><p style=\"\"><span style=\"color: rgb(85, 85, 85); font-size: 14px; line-height: 20px;\"><a style=\"background-color: #00b393; padding: 10px 15px; color: #ffffff;\" href=\"{ORDER_URL}\" target=\"_blank\">Show Order</a></span></p><p style=\"\"><br></p>  </div> </div></div>', '', 0),
(26, 'proposal_sent', 'Proposal sent', '<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #EEEEEE;border-top: 0;border-bottom: 0;\"> <tbody><tr> <td align=\"center\" valign=\"top\" style=\"padding-top: 30px;padding-right: 10px;padding-bottom: 30px;padding-left: 10px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\"> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\" style=\"border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\"> <tbody><tr> <td align=\"center\" valign=\"top\" style=\"mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\"> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #FFFFFF;\"> <tbody><tr> <td valign=\"top\" style=\"mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\"> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\"> <tbody> <tr> <td valign=\"top\" style=\"mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\"> <table align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"background-color: #33333e; max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\" width=\"100%\"> <tbody><tr> <td valign=\"top\" style=\"padding: 40px 18px; text-size-adjust: 100%; word-break: break-word; line-height: 150%; text-align: left;\"> <h2 style=\"display: block; margin: 0px; padding: 0px; line-height: 100%; text-align: center;\"><font color=\"#ffffff\" face=\"Arial\"><span style=\"letter-spacing: -1px;\"><b>PROPOSAL #{PROPOSAL_ID}</b></span></font><br></h2></td></tr></tbody></table></td></tr></tbody></table> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\"> <tbody> <tr> <td valign=\"top\" style=\"mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\"> <table align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\" width=\"100%\"> <tbody><tr> <td valign=\"top\" style=\"padding-top: 20px;padding-right: 18px;padding-bottom: 0;padding-left: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #606060;font-family: Arial;font-size: 15px;line-height: 150%;text-align: left;\"><p> Hello {CONTACT_FIRST_NAME},<br></p><p>Here is a proposal for you.</p><p></p></td></tr><tr><td valign=\"top\" style=\"padding-top: 10px;padding-right: 18px;padding-bottom: 10px;padding-left: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #606060;font-family: Arial;font-size: 15px;line-height: 150%;text-align: left;\"><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"min-width: 100%; text-size-adjust: 100%;\"><tbody><tr><td style=\"padding-top: 15px; padding-bottom: 15px; text-size-adjust: 100%;\"><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse: separate !important;border-radius: 2px;background-color: #00b393;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\"><tbody><tr><td align=\"center\" valign=\"middle\" style=\"font-size: 16px; padding: 10px; text-size-adjust: 100%;\"><a href=\"{PROPOSAL_URL}\" target=\"_blank\" style=\"font-weight: bold; line-height: 100%; color: rgb(255, 255, 255); text-size-adjust: 100%; display: block;\">Show Proposal</a></td></tr></tbody></table></td></tr></tbody></table> <p></p></td> </tr> <tr> <td valign=\"top\" style=\"padding-top: 0px;padding-right: 18px;padding-bottom: 20px;padding-left: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #606060;font-family: Arial;font-size: 15px;line-height: 150%;text-align: left;\"><p> </p><p>Public URL: {PUBLIC_PROPOSAL_URL}</p><p><br></p><p>{SIGNATURE} </p></td> </tr> </tbody> </table> </td> </tr> </tbody> </table> </td> </tr> </tbody> </table> </td> </tr> </tbody> </table> </td> </tr> </tbody> </table>', NULL, 0),
(27, 'project_completed', 'Project completed', '<div style=\"background-color: #eeeeef; padding: 50px 0; \"> <div style=\"max-width:640px; margin:0 auto; \"> <div style=\"color: #fff; text-align: center; background-color:#33333e; padding: 30px; border-top-left-radius: 3px; border-top-right-radius: 3px; margin: 0;\"><h1>Project #{PROJECT_ID}</h1></div><div style=\"padding: 20px; background-color: rgb(255, 255, 255);\"><p style=\"\"><span style=\"line-height: 18.5714px;\">The Project #{PROJECT_ID}&nbsp;has been closed by&nbsp;</span><span style=\"line-height: 18.5714px;\">{USER_NAME}</span></p><p style=\"\"><span style=\"line-height: 18.5714px;\">Title:&nbsp;</span>{PROJECT_TITLE}</p> <p style=\"\"><br></p> <p style=\"\"><span style=\"color: rgb(85, 85, 85); font-size: 14px; line-height: 20px;\"><a style=\"background-color: #00b393; padding: 10px 15px; color: #ffffff;\" href=\"{PROJECT_URL}\" target=\"_blank\">Show Project</a></span></p><p style=\"\"><span style=\"color: rgb(85, 85, 85); font-size: 14px; line-height: 20px;\"><br></span></p><p style=\"\"><span style=\"color: rgb(85, 85, 85); font-size: 14px; line-height: 20px;\"><span style=\"color: rgb(78, 94, 106); font-size: 13.5px;\">{SIGNATURE}</span><br></span></p>   </div>  </div> </div>', '', 0),
(28, 'proposal_accepted', 'Proposal accepted', '<div style=\"background-color: #eeeeef; padding: 50px 0; \"> <div style=\"max-width:640px; margin:0 auto; \"> <div style=\"color: #fff; text-align: center; background-color:#33333e; padding: 30px; border-top-left-radius: 3px; border-top-right-radius: 3px; margin: 0;\"><h1>PROPOSAL #{PROPOSAL_ID}</h1></div> <div style=\"padding: 20px; background-color: rgb(255, 255, 255);\">  <p style=\"\"><span style=\"color: rgb(85, 85, 85); font-size: 14px;\">The proposal #{PROPOSAL_ID} has been accepted.</span><br></p><p style=\"\"><br></p><p style=\"\"><span style=\"color: rgb(85, 85, 85); font-size: 14px; line-height: 20px;\"><a style=\"background-color: #00b393; padding: 10px 15px; color: #ffffff;\" href=\"{PROPOSAL_URL}\" target=\"_blank\">Show Proposal</a></span></p><p style=\"\"><br></p>  </div> </div></div>', '', 0),
(29, 'proposal_rejected', 'Proposal rejected', '<div style=\"background-color: #eeeeef; padding: 50px 0; \"> <div style=\"max-width:640px; margin:0 auto; \"> <div style=\"color: #fff; text-align: center; background-color:#33333e; padding: 30px; border-top-left-radius: 3px; border-top-right-radius: 3px; margin: 0;\"><h1>PROPOSAL #{PROPOSAL_ID}</h1></div> <div style=\"padding: 20px; background-color: rgb(255, 255, 255);\">  <p style=\"\"><span style=\"color: rgb(85, 85, 85); font-size: 14px;\">The proposal #{PROPOSAL_ID} has been rejected.</span><br></p><p style=\"\"><br></p><p style=\"\"><span style=\"color: rgb(85, 85, 85); font-size: 14px; line-height: 20px;\"><a style=\"background-color: #00b393; padding: 10px 15px; color: #ffffff;\" href=\"{PROPOSAL_URL}\" target=\"_blank\">Show Proposal</a></span></p><p style=\"\"><br></p>  </div> </div></div>', '', 0),
(30, 'estimate_commented', 'Estimate  #{ESTIMATE_ID}', '<div style=\"background-color: #eeeeef; padding: 50px 0; \"> <div style=\"max-width:640px; margin:0 auto; \"> <div style=\"color: #fff; text-align: center; background-color:#33333e; padding: 30px; border-top-left-radius: 3px; border-top-right-radius: 3px; margin: 0;\"><h1>Estimate #{ESTIMATE_ID} Replies</h1></div><div style=\"padding: 20px; background-color: rgb(255, 255, 255);\"><p style=\"\"><span style=\"line-height: 18.5714px;\">{COMMENT_CONTENT}</span></p><p style=\"\"><span style=\"line-height: 18.5714px;\"><br></span></p><p style=\"\"><span style=\"color: rgb(85, 85, 85); font-size: 14px; line-height: 20px;\"><a style=\"background-color: #00b393; padding: 10px 15px; color: #ffffff;\" href=\"{ESTIMATE_URL}\" target=\"_blank\">Show Estimate</a></span></p></div></div></div>', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `estimates`
--

CREATE TABLE `estimates` (
  `id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `estimate_request_id` int(11) NOT NULL DEFAULT 0,
  `estimate_date` date NOT NULL,
  `valid_until` date NOT NULL,
  `note` mediumtext DEFAULT NULL,
  `last_email_sent_date` date DEFAULT NULL,
  `status` enum('draft','sent','accepted','declined') NOT NULL DEFAULT 'draft',
  `tax_id` int(11) NOT NULL DEFAULT 0,
  `tax_id2` int(11) NOT NULL DEFAULT 0,
  `discount_type` enum('before_tax','after_tax') NOT NULL,
  `discount_amount` double NOT NULL,
  `discount_amount_type` enum('percentage','fixed_amount') NOT NULL,
  `project_id` int(11) NOT NULL DEFAULT 0,
  `accepted_by` int(11) NOT NULL DEFAULT 0,
  `created_by` int(11) NOT NULL,
  `signature` text NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `estimates`
--

INSERT INTO `estimates` (`id`, `client_id`, `estimate_request_id`, `estimate_date`, `valid_until`, `note`, `last_email_sent_date`, `status`, `tax_id`, `tax_id2`, `discount_type`, `discount_amount`, `discount_amount_type`, `project_id`, `accepted_by`, `created_by`, `signature`, `deleted`) VALUES
(1, 501, 0, '2022-07-11', '2022-07-15', '', '2022-07-13', 'sent', 0, 0, 'before_tax', 0, 'percentage', 0, 0, 1, '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `estimate_comments`
--

CREATE TABLE `estimate_comments` (
  `id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `description` mediumtext NOT NULL,
  `estimate_id` int(11) NOT NULL DEFAULT 0,
  `files` longtext DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `estimate_forms`
--

CREATE TABLE `estimate_forms` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `description` longtext NOT NULL,
  `status` enum('active','inactive') NOT NULL,
  `assigned_to` int(11) NOT NULL,
  `public` tinyint(1) NOT NULL DEFAULT 0,
  `enable_attachment` tinyint(4) NOT NULL DEFAULT 0,
  `deleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `estimate_items`
--

CREATE TABLE `estimate_items` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `description` text DEFAULT NULL,
  `quantity` double NOT NULL,
  `unit_type` varchar(20) NOT NULL DEFAULT '',
  `rate` double NOT NULL,
  `total` double NOT NULL,
  `sort` int(11) NOT NULL DEFAULT 0,
  `estimate_id` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  `unit_sale` double NOT NULL,
  `total_unit_sale` double NOT NULL,
  `margin` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `estimate_items`
--

INSERT INTO `estimate_items` (`id`, `title`, `description`, `quantity`, `unit_type`, `rate`, `total`, `sort`, `estimate_id`, `deleted`, `unit_sale`, `total_unit_sale`, `margin`) VALUES
(1, 'lenovo Think Pad', 'E15 Gen/intel 11gen/core 15/1135G7/16GB ', 1, '1', 250, 250, 0, 1, 0, 0, 0, 0),
(2, 'lenovo Think Pad', 'E15 Gen/intel 11gen/core 15/1135G7/16GB ', 2, '1', 250, 500, 0, 1, 0, 500, 1000, 0),
(3, 'lenovo Think Pad', 'E15 Gen/intel 11gen/core 15/1135G7/16GB ', 2, '1', 250, 500, 0, 1, 0, 500, 1000, 500),
(4, 'Phone', 'Lenovo', 2, 'NA', 20000, 40000, 0, 1, 0, 50000, 100000, 60000);

-- --------------------------------------------------------

--
-- Table structure for table `estimate_requests`
--

CREATE TABLE `estimate_requests` (
  `id` int(11) NOT NULL,
  `estimate_form_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `client_id` int(11) NOT NULL,
  `lead_id` int(11) NOT NULL,
  `assigned_to` int(11) NOT NULL,
  `status` enum('new','processing','hold','canceled','estimated') NOT NULL DEFAULT 'new',
  `files` mediumtext NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `description` mediumtext NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date DEFAULT NULL,
  `start_time` time DEFAULT NULL,
  `end_time` time DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `location` mediumtext DEFAULT NULL,
  `client_id` int(11) NOT NULL DEFAULT 0,
  `labels` text NOT NULL,
  `share_with` mediumtext DEFAULT NULL,
  `editable_google_event` tinyint(1) NOT NULL DEFAULT 0,
  `google_event_id` text NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT 0,
  `color` varchar(15) NOT NULL,
  `recurring` int(1) NOT NULL DEFAULT 0,
  `repeat_every` int(11) NOT NULL DEFAULT 0,
  `repeat_type` enum('days','weeks','months','years') DEFAULT NULL,
  `no_of_cycles` int(11) NOT NULL DEFAULT 0,
  `last_start_date` date DEFAULT NULL,
  `recurring_dates` longtext NOT NULL,
  `confirmed_by` text NOT NULL,
  `rejected_by` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `expenses`
--

CREATE TABLE `expenses` (
  `id` int(11) NOT NULL,
  `expense_date` date NOT NULL,
  `category_id` int(11) NOT NULL,
  `description` mediumtext DEFAULT NULL,
  `amount` double NOT NULL,
  `files` mediumtext NOT NULL,
  `title` text NOT NULL,
  `project_id` int(11) NOT NULL DEFAULT 0,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `tax_id` int(11) NOT NULL DEFAULT 0,
  `tax_id2` int(11) NOT NULL DEFAULT 0,
  `client_id` int(11) NOT NULL DEFAULT 0,
  `recurring` tinyint(4) NOT NULL DEFAULT 0,
  `recurring_expense_id` tinyint(4) NOT NULL DEFAULT 0,
  `repeat_every` int(11) NOT NULL DEFAULT 0,
  `repeat_type` enum('days','weeks','months','years') DEFAULT NULL,
  `no_of_cycles` int(11) NOT NULL DEFAULT 0,
  `next_recurring_date` date DEFAULT NULL,
  `no_of_cycles_completed` int(11) NOT NULL DEFAULT 0,
  `deleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `expense_categories`
--

CREATE TABLE `expense_categories` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `expense_categories`
--

INSERT INTO `expense_categories` (`id`, `title`, `deleted`) VALUES
(1, 'Miscellaneous expense', 0);

-- --------------------------------------------------------

--
-- Table structure for table `file_category`
--

CREATE TABLE `file_category` (
  `id` int(11) NOT NULL,
  `name` text DEFAULT NULL,
  `type` enum('project') NOT NULL DEFAULT 'project',
  `deleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `general_files`
--

CREATE TABLE `general_files` (
  `id` int(11) NOT NULL,
  `file_name` text NOT NULL,
  `file_id` text DEFAULT NULL,
  `service_type` varchar(20) DEFAULT NULL,
  `description` mediumtext DEFAULT NULL,
  `file_size` double NOT NULL,
  `created_at` datetime NOT NULL,
  `client_id` int(11) NOT NULL DEFAULT 0,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `uploaded_by` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `help_articles`
--

CREATE TABLE `help_articles` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `description` longtext NOT NULL,
  `category_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `files` text NOT NULL,
  `total_views` int(11) NOT NULL DEFAULT 0,
  `sort` int(11) NOT NULL DEFAULT 0,
  `deleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `help_categories`
--

CREATE TABLE `help_categories` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `description` text NOT NULL,
  `type` enum('help','knowledge_base') NOT NULL,
  `sort` int(11) NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `deleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `invoices`
--

CREATE TABLE `invoices` (
  `id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL DEFAULT 0,
  `bill_date` date NOT NULL,
  `due_date` date NOT NULL,
  `note` mediumtext DEFAULT NULL,
  `labels` text DEFAULT NULL,
  `last_email_sent_date` date DEFAULT NULL,
  `status` enum('draft','not_paid','cancelled') NOT NULL DEFAULT 'draft',
  `tax_id` int(11) NOT NULL DEFAULT 0,
  `tax_id2` int(11) NOT NULL DEFAULT 0,
  `tax_id3` int(11) NOT NULL DEFAULT 0,
  `recurring` tinyint(4) NOT NULL DEFAULT 0,
  `recurring_invoice_id` int(11) NOT NULL DEFAULT 0,
  `repeat_every` int(11) NOT NULL DEFAULT 0,
  `repeat_type` enum('days','weeks','months','years') DEFAULT NULL,
  `no_of_cycles` int(11) NOT NULL DEFAULT 0,
  `next_recurring_date` date DEFAULT NULL,
  `no_of_cycles_completed` int(11) NOT NULL DEFAULT 0,
  `due_reminder_date` date DEFAULT NULL,
  `recurring_reminder_date` date DEFAULT NULL,
  `discount_amount` double NOT NULL,
  `discount_amount_type` enum('percentage','fixed_amount') NOT NULL,
  `discount_type` enum('before_tax','after_tax') NOT NULL,
  `cancelled_at` datetime DEFAULT NULL,
  `cancelled_by` int(11) NOT NULL,
  `files` mediumtext NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `invoices`
--

INSERT INTO `invoices` (`id`, `client_id`, `project_id`, `bill_date`, `due_date`, `note`, `labels`, `last_email_sent_date`, `status`, `tax_id`, `tax_id2`, `tax_id3`, `recurring`, `recurring_invoice_id`, `repeat_every`, `repeat_type`, `no_of_cycles`, `next_recurring_date`, `no_of_cycles_completed`, `due_reminder_date`, `recurring_reminder_date`, `discount_amount`, `discount_amount_type`, `discount_type`, `cancelled_at`, `cancelled_by`, `files`, `deleted`) VALUES
(1, 502, 1, '2022-08-04', '2022-08-07', 'This is test invoice for testing crm only ', '', NULL, 'draft', 0, 0, 0, 0, 0, 1, 'months', 0, NULL, 0, NULL, NULL, 0, 'percentage', 'before_tax', NULL, 0, 'a:0:{}', 0),
(2, 502, 1, '2022-08-04', '2022-08-06', '', '', NULL, 'draft', 0, 0, 0, 0, 0, 1, 'months', 0, NULL, 0, NULL, NULL, 0, 'percentage', 'before_tax', NULL, 0, 'a:0:{}', 0),
(11, 503, 0, '2022-07-28', '2022-08-28', 'Total Invoice Amount in $1,533.189 @ ₹79.20/$ = ₹ 1,21,428.571\r\n\r\nBank Account Details:\r\nBank Name -: IDFC FIRST Bank\r\nAccount Holder Name- IOWEB3 Technologies Private Limited\r\nAccount Number- : 10089435901\r\nBranch - Kalyani Nagar, Pune- 411006\r\nSwift Code- IDFBINBBMUM\r\nIFSC - IDFB0041358\r\n\r\nRemark- Invoice For Month Of July 2022', '', NULL, 'draft', 0, 0, 0, 0, 0, 1, 'months', 0, NULL, 0, NULL, NULL, 0, 'percentage', 'before_tax', NULL, 0, 'a:0:{}', 0),
(12, 503, 0, '2022-08-04', '2022-08-10', '', '', NULL, 'draft', 0, 0, 0, 0, 0, 1, 'months', 0, NULL, 0, NULL, NULL, 0, 'percentage', 'before_tax', NULL, 0, 'a:0:{}', 0),
(13, 503, 0, '2022-12-05', '2022-12-30', 'Total Invoice Amount in $1,847 @ ₹81.25/$ = ₹ 1,50,068.75\r\nBank Account Details:\r\nBank Name: IDFC FIRST Bank\r\nBank Branch: Kalyani Nagar Branch\r\nBank Address: S No 206, Unit No 1, Ground Floor, Plot No 88, Ramwadi, Kalyani Nagar, Near Bishop School RTGS/NEFT IFSC Code: IDFB0041358\r\nAccount No: 10089435901\r\nPIN/Zone Code: 411006\r\nRemark- Invoice For Month Of November 2022', '', NULL, 'draft', 0, 0, 0, 0, 0, 1, 'months', 0, NULL, 0, NULL, NULL, 0, 'percentage', 'before_tax', NULL, 0, 'a:0:{}', 0),
(23, 503, 0, '2022-12-09', '2022-12-30', '\r\nSub Total 	$ 0.00 	\r\nBalance Due 	$ 0.00 	\r\n\r\nTotal Invoice Amount in $1,847 @ ₹81.25/$ = ₹ 1,50,068.75\r\nBank Account Details:\r\nBank Name: IDFC FIRST Bank\r\nBank Branch: Kalyani Nagar Branch\r\nBank Address: S No 206, Unit No 1, Ground Floor, Plot No 88, Ramwadi, Kalyani Nagar, Near Bishop School RTGS/NEFT IFSC Code: IDFB0041358\r\nAccount No: 10089435901\r\nPIN/Zone Code: 411006\r\nRemark- Invoice For Month Of November 2022', '', NULL, 'draft', 0, 0, 0, 0, 0, 1, 'months', 0, NULL, 0, NULL, NULL, 0, 'percentage', 'before_tax', NULL, 0, 'a:0:{}', 0),
(24, 503, 0, '2022-12-05', '2022-12-30', 'Total Invoice Amount in $1,847 @ ₹81.25/$ = ₹ 1,50,068.75\r\nBank Account Details:\r\nBank Name: IDFC FIRST Bank\r\nBank Branch: Kalyani Nagar Branch\r\nBank Address: S No 206, Unit No 1, Ground Floor, Plot No 88, Ramwadi, Kalyani Nagar, Near Bishop School RTGS/NEFT IFSC Code: IDFB0041358\r\nAccount No: 10089435901\r\nPIN/Zone Code: 411006\r\nRemark- Invoice For Month Of November 2022', '', NULL, 'draft', 0, 0, 0, 0, 0, 1, 'months', 0, NULL, 0, NULL, NULL, 0, 'percentage', 'before_tax', NULL, 0, 'a:0:{}', 0),
(26, 503, 0, '2022-12-05', '2022-12-30', 'Total Invoice Amount in $1,847 @ ₹81.25/$ = ₹ 1,50,068.75\r\nBank Account Details:\r\nBank Name: IDFC FIRST Bank\r\nBank Branch: Kalyani Nagar Branch\r\nBank Address: S No 206, Unit No 1, Ground Floor, Plot No 88, Ramwadi, Kalyani Nagar, Near Bishop School RTGS/NEFT IFSC Code: IDFB0041358\r\nAccount No: 10089435901\r\nPIN/Zone Code: 411006\r\nRemark- Invoice For Month Of November 2022', '', NULL, 'draft', 0, 0, 0, 0, 0, 1, 'months', 0, NULL, 0, NULL, NULL, 0, 'percentage', 'before_tax', NULL, 0, 'a:0:{}', 0),
(27, 503, 0, '2022-12-05', '2022-12-30', 'Total Invoice Amount in $1,847 @ ₹81.25/$ = ₹ 1,50,068.75\r\nBank Account Details:\r\nBank Name: IDFC FIRST Bank\r\nBank Branch: Kalyani Nagar Branch\r\nBank Address: S No 206, Unit No 1, Ground Floor, Plot No 88, Ramwadi, Kalyani Nagar, Near Bishop School RTGS/NEFT IFSC Code: IDFB0041358\r\nAccount No: 10089435901\r\nPIN/Zone Code: 411006\r\nRemark- Invoice For Month Of November 2022', '', NULL, 'draft', 0, 0, 0, 0, 0, 1, 'months', 0, NULL, 0, NULL, NULL, 0, 'percentage', 'before_tax', NULL, 0, 'a:0:{}', 0),
(28, 505, 0, '2023-01-09', '2023-01-16', '', '', NULL, 'draft', 2, 0, 1, 0, 0, 1, 'months', 0, NULL, 0, NULL, NULL, 0, 'percentage', 'before_tax', NULL, 0, 'a:0:{}', 0),
(29, 503, 0, '2023-01-14', '2023-01-31', 'Total Invoice Amount in $1729.81 @ ₹81.12/$ = ₹ 1,40,322.58 Bank Account Details:\r\nBank Name: IDFC FIRST Bank\r\nBank Branch: Kalyani Nagar Branch\r\nBank Address: S No 206, Unit No 1, Ground Floor, Plot No 88, Ramwadi, Kalyani Nagar, Near Bishop School RTGS/NEFT IFSC Code: IDFB0041358\r\nAccount No: 10089435901\r\nPIN/Zone Code: 411006\r\nRemark- Invoice For Month Of December 2022', '', NULL, 'draft', 0, 0, 0, 0, 0, 1, 'months', 0, NULL, 0, NULL, NULL, 0, 'percentage', 'before_tax', NULL, 0, 'a:0:{}', 0),
(30, 507, 0, '2023-02-01', '2023-02-10', '', '', NULL, 'draft', 2, 0, 0, 0, 0, 1, 'months', 0, NULL, 0, NULL, NULL, 0, 'percentage', 'before_tax', NULL, 0, 'a:0:{}', 0),
(31, 508, 0, '2023-02-09', '2023-02-10', 'Bank name-: IDFC First Bank\r\nAccount no-: 10089435901\r\nIFSC-: IDFB0041358\r\nName-: IOWEB3 TECHNOLOGIES PRIVATE LIMITED\r\nBranch-: KALYANI NAGAR, PUNE- 411006\r\nSwift code- IDFBINBBMUM', '', NULL, 'draft', 2, 0, 0, 0, 0, 1, 'months', 0, NULL, 0, NULL, NULL, 0, 'percentage', 'before_tax', NULL, 0, 'a:0:{}', 0),
(32, 505, 0, '2023-02-10', '2023-02-16', '', '', NULL, 'draft', 2, 0, 1, 0, 0, 1, 'months', 0, NULL, 0, NULL, NULL, 0, 'percentage', 'before_tax', NULL, 0, 'a:0:{}', 0),
(33, 509, 0, '2023-02-11', '2023-02-15', 'Account no-: 10089435901\r\nIFSC-: IDFB0041358\r\nName-: IOWEB3 TECHNOLOGIES PRIVATE LIMITED\r\nBranch-: KALYANI NAGAR, PUNE- 411006\r\nSwift code- IDFBINBBMUM', '', NULL, 'draft', 0, 0, 0, 0, 0, 1, 'months', 0, NULL, 0, NULL, NULL, 0, 'percentage', 'before_tax', NULL, 0, 'a:0:{}', 0),
(34, 503, 0, '2023-02-20', '2023-02-28', 'Total Invoice Amount in $1809.04@ ₹81.68/$ = ₹ 1,47,762.38 Bank Account Details:\r\nBank Name: IDFC FIRST Bank\r\nBank Branch: Kalyani Nagar Branch\r\nBank Address: S No 206, Unit No 1, Ground Floor, Plot No 88, Ramwadi, Kalyani Nagar, Near Bishop School RTGS/NEFT IFSC Code: IDFB0041358\r\nAccount No: 10089435901\r\nPIN/Zone Code: 411006\r\nRemark- Invoice For Month Of January 2022', '', NULL, 'draft', 0, 0, 0, 0, 0, 1, 'months', 0, NULL, 0, NULL, NULL, 0, 'percentage', 'before_tax', NULL, 0, 'a:0:{}', 0),
(35, 510, 0, '2023-03-09', '2023-03-10', 'Bank name-: IDFC First Bank\r\nAccount no-: 10089435901\r\nIFSC-: IDFB0041358\r\nName-: IOWEB3 TECHNOLOGIES PRIVATE LIMITED\r\nBranch-: KALYANI NAGAR, PUNE- 411006\r\nSwift code- IDFBINBBMUM', '', NULL, 'draft', 2, 0, 0, 0, 0, 1, 'months', 0, NULL, 0, NULL, NULL, 0, 'percentage', 'before_tax', NULL, 0, 'a:0:{}', 0),
(36, 503, 0, '2023-02-20', '2023-02-28', 'Total Invoice Amount in $1607.60@ ₹81.64/$ = ₹ 1,31,250 Bank Account Details:\r\nBank Name: IDFC FIRST Bank\r\nBank Branch: Kalyani Nagar Branch\r\nBank Address: S No 206, Unit No 1, Ground Floor, Plot No 88, Ramwadi, Kalyani Nagar, Near Bishop School RTGS/NEFT IFSC Code: IDFB0041358\r\nAccount No: 10089435901\r\nPIN/Zone Code: 411006\r\nRemark- Invoice For Month Of February 2023', '', NULL, 'draft', 0, 0, 0, 0, 0, 1, 'months', 0, NULL, 0, NULL, NULL, 0, 'percentage', 'before_tax', NULL, 0, 'a:0:{}', 0),
(37, 511, 3, '2023-03-16', '2023-03-25', 'Bank name-: IDFC First Bank\r\nAccount no-: 10089435901\r\nIFSC-: IDFB0041358\r\nName-: IOWEB3 TECHNOLOGIES PRIVATE LIMITED\r\nBranch-: KALYANI NAGAR, PUNE- 411006\r\nSwift code- IDFBINBBMUM', '', NULL, 'draft', 2, 0, 0, 0, 0, 1, 'months', 0, NULL, 0, NULL, NULL, 0, 'percentage', 'before_tax', NULL, 0, 'a:0:{}', 0),
(38, 508, 0, '2023-03-22', '2023-03-22', '', '', NULL, 'draft', 2, 0, 0, 0, 0, 1, 'months', 0, NULL, 0, NULL, NULL, 0, 'percentage', 'before_tax', NULL, 0, 'a:0:{}', 0),
(39, 510, 0, '2023-03-29', '2023-03-31', 'Payment method \r\nAccount number 1010101\r\nidfc- idfb0000', '', NULL, 'draft', 3, 4, 0, 0, 0, 1, 'months', 0, NULL, 0, NULL, NULL, 0, 'percentage', 'before_tax', NULL, 0, 'a:0:{}', 0),
(40, 510, 0, '2023-04-17', '2023-04-20', 'Bank Account Details:\r\nBank Name: IDFC FIRST Bank\r\nBank Branch: Kalyani Nagar Branch\r\nBank Address: S No 206, Unit No 1, Ground Floor, Plot No 88, Ramwadi, Kalyani Nagar, Near Bishop School RTGS/NEFT IFSC Code: IDFB0041358\r\nAccount No: 10089435901\r\nPIN/Zone Code: 411006', '', NULL, 'draft', 2, 0, 0, 0, 0, 1, 'months', 0, NULL, 0, NULL, NULL, 0, 'percentage', 'before_tax', NULL, 0, 'a:0:{}', 0),
(41, 503, 0, '2023-04-18', '2023-04-20', 'Total Invoice Amount in $1748.43@ ₹81.64/$ = ₹ 1,42,742 Bank Account Details:\r\nBank Name: IDFC FIRST Bank\r\nBank Branch: Kalyani Nagar Branch\r\nBank Address: S No 206, Unit No 1, Ground Floor, Plot No 88, Ramwadi, Kalyani Nagar, Near Bishop School RTGS/NEFT IFSC Code: IDFB0041358\r\nAccount No: 10089435901\r\nPIN/Zone Code: 411006\r\nRemark- Invoice For Month Of March 2023(Considering 1.5 Leave)', '', NULL, 'draft', 0, 0, 0, 0, 0, 1, 'months', 0, NULL, 0, NULL, NULL, 0, 'percentage', 'before_tax', NULL, 0, 'a:0:{}', 0),
(42, 503, 0, '2023-04-21', '2023-04-24', 'Total Invoice Amount in $1607.60@ ₹81.64/$ = ₹ 1,31,250 Bank Account Details:\r\nMissed Invoice Amount - $32.80@ ₹81.64/$ = ₹ 2678.00\r\nBank Name: IDFC FIRST Bank\r\nBank Branch: Kalyani Nagar Branch\r\nBank Address: S No 206, Unit No 1, Ground Floor, Plot No 88, Ramwadi, Kalyani Nagar, Near Bishop School RTGS/NEFT IFSC Code: IDFB0041358\r\nAccount No: 10089435901\r\nPIN/Zone Code: 411006\r\nRemark- Invoice For Month Of February 2023\r\nBank Address: S No 206, Unit No 1, Ground Floor, Plot No 88, Ramwadi, Kalyani Nagar, Near Bishop School RTGS/NEFT IFSC Code: IDFB0041358\r\nAccount No: 10089435901\r\nPIN/Zone Code: 411006\r\nRemark- Invoice For Month Of February 2023 (Because of miscalculations)', '', NULL, 'draft', 0, 0, 0, 0, 0, 1, 'months', 0, NULL, 0, NULL, NULL, 0, 'percentage', 'before_tax', NULL, 0, 'a:0:{}', 0),
(43, 511, 0, '2023-04-18', '2023-04-20', ' Bank Account Details:\r\nBank Name: IDFC FIRST Bank\r\nBank Branch: Kalyani Nagar Branch\r\nBank Address: S No 206, Unit No 1, Ground Floor, Plot No 88, Ramwadi, Kalyani Nagar, Near Bishop School\r\nRTGS/NEFT IFSC Code: IDFB0041358\r\nAccount No: 10089435901\r\nPIN/Zone Code: 411006\r\nRemark- Invoice For Month Of 15 March 2023 to 15 April 2023', '', NULL, 'draft', 2, 0, 0, 0, 0, 1, 'months', 0, NULL, 0, NULL, NULL, 0, 'percentage', 'before_tax', NULL, 0, 'a:0:{}', 0),
(44, 512, 0, '2023-05-03', '2023-05-04', '', '', NULL, 'cancelled', 0, 0, 0, 0, 0, 1, 'months', 0, NULL, 0, NULL, NULL, 0, 'percentage', 'before_tax', '2023-05-09 04:56:47', 1, 'a:0:{}', 0),
(45, 505, 0, '2023-05-11', '2023-05-15', 'Bill For 11 April to 30 April\r\nBank Account Details:\r\nBank Name: IDFC FIRST Bank\r\nBank Branch: Kalyani Nagar Branch\r\nBank Address: S No 206, Unit No 1, Ground Floor,\r\n Plot No 88, Ramwadi, Kalyani Nagar, Near Bishop School RTGS/NEFT IFSC Code: IDFB0041358\r\nAccount No: 10089435901\r\nPIN/Zone Code: 411006', '', NULL, 'draft', 2, 0, 0, 0, 0, 1, 'months', 0, NULL, 0, NULL, NULL, 0, 'percentage', 'before_tax', NULL, 0, 'a:0:{}', 0),
(46, 511, 0, '2023-05-15', '2023-05-30', ' Bank Account Details:\r\nBank Name: IDFC FIRST Bank\r\nBank Branch: Kalyani Nagar Branch\r\nBank Address: S No 206, Unit No 1, Ground Floor, Plot No 88, Ramwadi, Kalyani Nagar, Near Bishop School\r\nRTGS/NEFT IFSC Code: IDFB0041358\r\nAccount No: 10089435901\r\nPIN/Zone Code: 411006\r\nRemark- Invoice For Month Of 15 April 2023 to 15 May 2023', '', NULL, 'draft', 2, 0, 0, 0, 0, 1, 'months', 0, NULL, 0, NULL, NULL, 0, 'percentage', 'before_tax', NULL, 0, 'a:0:{}', 0),
(47, 513, 0, '2023-05-19', '2023-05-20', 'Bank Account Details:\r\nBank Name: IDFC FIRST Bank\r\nBank Branch: Kalyani Nagar Branch\r\nBank Address: S No 206, Unit No 1, Ground Floor, Plot No 88, Ramwadi, Kalyani Nagar, Near Bishop School RTGS/NEFT IFSC Code: IDFB0041358\r\nAccount No: 10089435901\r\nPIN/Zone Code: 411006\r\nRemark- Invoice For Advance Website Payment', '', NULL, 'draft', 2, 0, 0, 0, 0, 1, 'months', 0, NULL, 0, NULL, NULL, 0, 'percentage', 'before_tax', NULL, 0, 'a:0:{}', 0),
(48, 514, 0, '2023-06-06', '2023-06-10', 'Bank name-: IDFC First Bank\r\nAccount no-: 10089435901\r\nIFSC-: IDFB0041358\r\nName-: IOWEB3 TECHNOLOGIES PRIVATE LIMITED\r\nBranch-: KALYANI NAGAR, PUNE- 411006\r\nSwift code- IDFBINBBMUM\r\nBill For Ajay For May 2023', '', NULL, 'draft', 2, 0, 0, 0, 0, 1, 'months', 0, NULL, 0, NULL, NULL, 0, 'percentage', 'before_tax', NULL, 0, 'a:0:{}', 0),
(49, 505, 0, '2023-06-06', '2023-06-07', 'Bank name-: IDFC First Bank\r\nAccount no-: 10089435901\r\nIFSC-: IDFB0041358\r\nName-: IOWEB3 TECHNOLOGIES PRIVATE LIMITED\r\nBranch-: KALYANI NAGAR, PUNE- 411006\r\nSwift code- IDFBINBBMUM\r\nBill For PHP Developer For May Month', '', NULL, 'draft', 2, 0, 1, 0, 0, 1, 'months', 0, NULL, 0, NULL, NULL, 0, 'percentage', 'before_tax', NULL, 0, 'a:0:{}', 0),
(50, 512, 0, '2023-06-06', '2023-06-10', 'Bank name-: IDFC First Bank\r\nAccount no-: 10089435901\r\nIFSC-: IDFB0041358\r\nName-: IOWEB3 TECHNOLOGIES PRIVATE LIMITED\r\nBranch-: KALYANI NAGAR, PUNE- 411006\r\nSwift code- IDFBINBBMUM\r\n', '', NULL, 'draft', 0, 0, 0, 0, 0, 1, 'months', 0, NULL, 0, NULL, NULL, 0, 'percentage', 'before_tax', NULL, 0, 'a:0:{}', 0),
(51, 511, 0, '2023-06-11', '2023-06-18', 'Bank Account Details:\r\nBank Name: IDFC FIRST Bank\r\nBank Branch: Kalyani Nagar Branch\r\nBank Address: S No 206, Unit No 1, Ground Floor, Plot No 88, Ramwadi, Kalyani Nagar, Near Bishop School\r\nRTGS/NEFT IFSC Code: IDFB0041358\r\nAccount No: 10089435901\r\nPIN/Zone Code: 411006\r\nRemark- Invoice For Month Of 16 May 2023 to 15 June 2023', '', NULL, 'draft', 2, 0, 1, 0, 0, 1, 'months', 0, NULL, 0, NULL, NULL, 0, 'percentage', 'before_tax', NULL, 0, 'a:0:{}', 1),
(52, 511, 0, '2023-06-20', '2023-06-25', 'Bank Account Details:\r\nBank Name: IDFC FIRST Bank\r\nAccount Name: IOWEB3 technologies Private limited \r\nBank Branch: Kalyani Nagar Branch\r\nBank Address: S No 206, Unit No 1, Ground Floor, Plot No 88, Ramwadi, Kalyani Nagar, Near Bishop School\r\nRTGS/NEFT IFSC Code: IDFB0041358\r\nAccount No: 10089435901\r\nPIN/Zone Code: 411006\r\nRemark- Invoice For Social Media 15 May To 15 June', '', NULL, 'draft', 2, 0, 1, 0, 0, 1, 'months', 0, NULL, 0, NULL, NULL, 0, 'percentage', 'before_tax', NULL, 0, 'a:0:{}', 0),
(53, 505, 0, '2023-07-06', '2023-07-10', 'Bank Account Details:\r\nBank Name: IDFC FIRST Bank\r\nAccount Name: IOWEB3 technologies Private limited \r\nBank Branch: Kalyani Nagar Branch\r\nBank Address: S No 206, Unit No 1, Ground Floor, Plot No 88, Ramwadi, Kalyani Nagar, Near Bishop School\r\nRTGS/NEFT IFSC Code: IDFB0041358\r\nAccount No: 10089435901\r\nPIN/Zone Code: 411006\r\nRemark- Invoice For Month Of June 2023', '', NULL, 'draft', 2, 0, 1, 0, 0, 1, 'months', 0, NULL, 0, NULL, NULL, 0, 'percentage', 'before_tax', NULL, 0, 'a:0:{}', 0),
(54, 514, 0, '2023-07-07', '2023-07-10', 'Bank name-: IDFC First Bank\r\nAccount no-: 10089435901\r\nIFSC-: IDFB0041358\r\nName-: IOWEB3 TECHNOLOGIES PRIVATE LIMITED\r\nBranch-: KALYANI NAGAR, PUNE- 411006\r\nSwift code- IDFBINBBMUM\r\nBill For Ajay For June 2023', '', NULL, 'draft', 2, 0, 1, 0, 0, 1, 'months', 0, NULL, 0, NULL, NULL, 0, 'percentage', 'before_tax', NULL, 0, 'a:0:{}', 0),
(55, 515, 0, '2023-07-10', '2023-07-11', '8000 Advance paid on 04-07-2023\r\n', '', NULL, 'draft', 0, 0, 0, 0, 0, 1, 'months', 0, NULL, 0, NULL, NULL, 0, 'percentage', 'before_tax', NULL, 0, 'a:0:{}', 0),
(56, 511, 0, '2023-07-24', '2023-07-28', 'Bank name-: IDFC First Bank\r\nAccount no-: 10089435901\r\nIFSC-: IDFB0041358\r\nName-: IOWEB3 TECHNOLOGIES PRIVATE LIMITED Branch-: KALYANI NAGAR, PUNE- 411006\r\nSwift code- IDFBINBBMUM \r\nBill For Social Media For 15 June to 15 July 2023', '', NULL, 'draft', 2, 0, 0, 0, 0, 1, 'months', 0, NULL, 0, NULL, NULL, 0, 'percentage', 'before_tax', NULL, 0, 'a:0:{}', 0),
(57, 514, 0, '2023-08-11', '2023-08-12', 'Bank name-: IDFC First Bank\r\nAccount no-: 10089435901\r\nIFSC-: IDFB0041358\r\nName-: IOWEB3 TECHNOLOGIES PRIVATE LIMITED\r\nBranch-: KALYANI NAGAR, PUNE- 411006\r\nSwift code- IDFBINBBMUM\r\nBill For Ajay For July 2023', '', NULL, 'draft', 2, 0, 1, 0, 0, 1, 'months', 0, NULL, 0, NULL, NULL, 0, 'percentage', 'before_tax', NULL, 0, 'a:0:{}', 0),
(58, 516, 0, '2023-08-14', '2023-08-15', 'Bank name-: IDFC First Bank\r\nAccount no-: 10089435901\r\nIFSC-: IDFB0041358\r\nName-: IOWEB3 TECHNOLOGIES PRIVATE LIMITED Branch-: KALYANI NAGAR, PUNE- 411006\r\nSwift code- IDFBINBBMUM Bill For July 2023', '', NULL, 'draft', 2, 0, 5, 0, 0, 1, 'months', 0, NULL, 0, NULL, NULL, 0, 'percentage', 'before_tax', NULL, 0, 'a:0:{}', 0),
(59, 516, 0, '2023-08-14', '2023-08-15', 'Bank name-: IDFC First Bank\r\nAccount no-: 10089435901\r\nIFSC-: IDFB0041358\r\nName-: IOWEB3 TECHNOLOGIES PRIVATE LIMITED Branch-: KALYANI NAGAR, PUNE- 411006\r\nSwift code- IDFBINBBMUM Bill For July 2023', '', NULL, 'draft', 2, 0, 5, 0, 0, 1, 'months', 0, NULL, 0, NULL, NULL, 0, 'percentage', 'before_tax', NULL, 0, 'a:0:{}', 0),
(60, 516, 0, '2023-08-14', '2023-08-15', 'Bank name-: IDFC First Bank\r\nAccount no-: 10089435901\r\nIFSC-: IDFB0041358\r\nName-: IOWEB3 TECHNOLOGIES PRIVATE LIMITED Branch-: KALYANI NAGAR, PUNE- 411006\r\nSwift code- IDFBINBBMUM Bill For July 2023', '', NULL, 'draft', 2, 0, 5, 0, 0, 1, 'months', 0, NULL, 0, NULL, NULL, 0, 'percentage', 'before_tax', NULL, 0, 'a:0:{}', 0),
(61, 517, 0, '2023-08-14', '2023-08-15', 'Bank name-: IDFC First Bank\r\nAccount no-: 10089435901\r\nIFSC-: IDFB0041358\r\nName-: IOWEB3 TECHNOLOGIES PRIVATE LIMITED Branch-: KALYANI NAGAR, PUNE- 411006 Swift code- IDFBINBBMUM ', '', NULL, 'draft', 2, 0, 0, 0, 0, 1, 'months', 0, NULL, 0, NULL, NULL, 0, 'percentage', 'before_tax', NULL, 0, 'a:0:{}', 0),
(62, 511, 0, '2023-08-19', '2023-08-25', 'Bank name-: IDFC First Bank\r\nAccount no-: 10089435901\r\nIFSC-: IDFB0041358\r\nName-: IOWEB3 TECHNOLOGIES PRIVATE LIMITED Branch-: KALYANI NAGAR, PUNE- 411006\r\nSwift code- IDFBINBBMUM \r\nBill For Social Media For 15 July to 15 August 2023', '', NULL, 'draft', 2, 0, 0, 0, 0, 1, 'months', 0, NULL, 0, NULL, NULL, 0, 'percentage', 'before_tax', NULL, 0, 'a:0:{}', 0),
(63, 518, 0, '2023-08-21', '2023-08-24', 'Bank name-: IDFC First Bank\r\nAccount no-: 10089435901\r\nIFSC-: IDFB0041358\r\nName-: IOWEB3 TECHNOLOGIES PRIVATE LIMITED Branch-: KALYANI NAGAR, PUNE- 411006 Swift code- IDFBINBBMUM Bill For July 2023', '', NULL, 'draft', 2, 0, 0, 0, 0, 1, 'months', 0, NULL, 0, NULL, NULL, 0, 'percentage', 'before_tax', NULL, 0, 'a:0:{}', 0),
(64, 519, 0, '2023-08-13', '2023-08-13', '', '', NULL, 'draft', 0, 0, 0, 0, 0, 1, 'months', 0, NULL, 0, NULL, NULL, 0, 'percentage', 'before_tax', NULL, 0, 'a:0:{}', 0),
(65, 511, 0, '2023-08-30', '2023-09-15', 'Bank name-: IDFC First Bank\r\nAccount no-: 10089435901\r\nIFSC-: IDFB0041358\r\nName-: IOWEB3 TECHNOLOGIES PRIVATE LIMITED Branch-: KALYANI NAGAR, PUNE- 411006\r\nSwift code- IDFBINBBMUM', '', NULL, 'draft', 2, 0, 0, 0, 0, 1, 'months', 0, NULL, 0, NULL, NULL, 0, 'percentage', 'before_tax', NULL, 0, 'a:0:{}', 0),
(66, 505, 0, '2023-08-30', '2023-09-05', 'Bank name-: IDFC First Bank\r\nAccount no-: 10089435901\r\nIFSC-: IDFB0041358\r\nName-: IOWEB3 TECHNOLOGIES PRIVATE LIMITED Branch-: KALYANI NAGAR, PUNE- 411006 Swift code- IDFBINBBMUM\r\nNote- Bill for August 2023', '', NULL, 'draft', 2, 0, 1, 0, 0, 1, 'months', 0, NULL, 0, NULL, NULL, 0, 'percentage', 'before_tax', NULL, 0, 'a:0:{}', 0),
(67, 516, 0, '2023-09-11', '2023-09-15', 'Bank name-: IDFC First Bank\r\nAccount no-: 10089435901\r\nIFSC-: IDFB0041358\r\nName-: IOWEB3 TECHNOLOGIES PRIVATE LIMITED Branch-: KALYANI NAGAR, PUNE- 411006 Swift code- IDFBINBBMUM Bill For August 2023', '', NULL, 'draft', 2, 0, 5, 0, 0, 1, 'months', 0, NULL, 0, NULL, NULL, 0, 'percentage', 'before_tax', NULL, 0, 'a:0:{}', 0),
(68, 516, 0, '2023-09-11', '2023-09-15', 'Bank name-: IDFC First Bank\r\nAccount no-: 10089435901\r\nIFSC-: IDFB0041358\r\nName-: IOWEB3 TECHNOLOGIES PRIVATE LIMITED Branch-: KALYANI NAGAR, PUNE- 411006 Swift code- IDFBINBBMUM Bill For August 2023', '', NULL, 'draft', 2, 0, 5, 0, 0, 1, 'months', 0, NULL, 0, NULL, NULL, 0, 'percentage', 'before_tax', NULL, 0, 'a:0:{}', 0),
(69, 516, 0, '2023-09-11', '2023-09-15', 'Bank name-: IDFC First Bank\r\nAccount no-: 10089435901\r\nIFSC-: IDFB0041358\r\nName-: IOWEB3 TECHNOLOGIES PRIVATE LIMITED Branch-: KALYANI NAGAR, PUNE- 411006 Swift code- IDFBINBBMUM Bill For August 2023', '', NULL, 'draft', 2, 0, 5, 0, 0, 1, 'months', 0, NULL, 0, NULL, NULL, 0, 'percentage', 'before_tax', NULL, 0, 'a:0:{}', 0),
(70, 505, 0, '2023-09-11', '2023-09-15', 'Bank name-: IDFC First Bank\r\nAccount no-: 10089435901\r\nIFSC-: IDFB0041358\r\nName-: IOWEB3 TECHNOLOGIES PRIVATE LIMITED \r\nBranch-: KALYANI NAGAR, PUNE- 411006 \r\nSwift code- IDFBINBBMUM\r\nAdvance 30% Payment for  B2B (Business-to-Business) account intelligence platform.', '', NULL, 'draft', 2, 0, 0, 0, 0, 1, 'months', 0, NULL, 0, NULL, NULL, 0, 'percentage', 'before_tax', NULL, 0, 'a:0:{}', 0),
(71, 514, 0, '2023-09-12', '2023-09-13', 'Bank name-: IDFC First Bank\r\nAccount no-: 10089435901\r\nIFSC-: IDFB0041358\r\nName-: IOWEB3 TECHNOLOGIES PRIVATE LIMITED\r\nBranch-: KALYANI NAGAR, PUNE- 411006\r\nSwift code- IDFBINBBMUM\r\nBill For Ajay For August 2023', '', NULL, 'draft', 2, 0, 1, 0, 0, 1, 'months', 0, NULL, 0, NULL, NULL, 0, 'percentage', 'before_tax', NULL, 0, 'a:0:{}', 0),
(72, 515, 0, '2023-10-19', '2023-10-19', 'Pending amount 7000 + 10000 for extra work\r\n', '', NULL, 'draft', 0, 0, 0, 0, 0, 1, 'months', 0, NULL, 0, NULL, NULL, 0, 'percentage', 'before_tax', NULL, 0, 'a:0:{}', 0),
(73, 517, 0, '2023-10-19', '2023-10-25', 'Monthly Estimated Reach- \r\n56760 to 164000 Targeted Audience monthly in your area.\r\nFor Three Months -\r\n1,50,000 to 4,80,000\r\n\r\n\r\nSub Total 	₹ 7,500.00 	\r\nDiscount 	₹ 0.00 	   \r\nGST (18%) 	₹ 1,350.00 	\r\nBalance Due 	₹ 8,850.00 	\r\n\r\nBank name-: IDFC First Bank\r\nAccount no-: 10089435901\r\nIFSC-: IDFB0041358\r\nName-: IOWEB3 TECHNOLOGIES PRIVATE LIMITED Branch-: KALYANI NAGAR, PUNE- 411006 Swift code- IDFBINBBMUM \r\n\r\nNote- Payment Will be InAdvance', '', NULL, 'draft', 2, 0, 0, 0, 0, 1, 'months', 0, NULL, 0, NULL, NULL, 0, 'percentage', 'before_tax', NULL, 0, 'a:0:{}', 0),
(74, 517, 0, '2023-10-19', '2023-10-25', 'Bank name-: IDFC First Bank\r\nAccount no-: 10089435901\r\nIFSC-: IDFB0041358\r\nName-: IOWEB3 TECHNOLOGIES PRIVATE LIMITED Branch-: KALYANI NAGAR, PUNE- 411006 Swift code- IDFBINBBMUM ', '', NULL, 'draft', 2, 0, 0, 0, 0, 1, 'months', 0, NULL, 0, NULL, NULL, 0, 'percentage', 'before_tax', NULL, 0, 'a:0:{}', 0),
(75, 520, 0, '2023-10-19', '2023-10-20', 'Bank name-: IDFC First Bank\r\nAccount no-: 10089435901\r\nIFSC-: IDFB0041358\r\nName-: IOWEB3 TECHNOLOGIES PRIVATE LIMITED Branch-: KALYANI NAGAR, PUNE- 411006 Swift code- IDFBINBBMUM', '', NULL, 'draft', 0, 0, 0, 0, 0, 1, 'months', 0, NULL, 0, NULL, NULL, 0, 'percentage', 'before_tax', NULL, 0, 'a:0:{}', 0),
(76, 516, 0, '2023-10-20', '2023-10-25', '', '', NULL, 'draft', 2, 0, 5, 0, 0, 1, 'months', 0, NULL, 0, NULL, NULL, 0, 'percentage', 'before_tax', NULL, 0, 'a:0:{}', 0),
(77, 521, 0, '2023-12-20', '2023-12-22', 'Bank name-: IDFC First Bank\r\nAccount no-: 10089435901\r\nIFSC-: IDFB0041358\r\nName-: IOWEB3 TECHNOLOGIES PRIVATE LIMITED\r\nBranch-: KALYANI NAGAR, PUNE- 411006 \r\nSwift code- IDFBINBBMUM\r\nBill Against PO 23005601', '', NULL, 'draft', 2, 0, 0, 0, 0, 1, 'months', 0, NULL, 0, NULL, NULL, 0, 'percentage', 'before_tax', NULL, 0, 'a:0:{}', 0),
(78, 522, 0, '2023-12-20', '2023-12-31', 'Bank name-: IDFC First Bank\r\nAccount no-: 10089435901\r\nIFSC-: IDFB0041358\r\nName-: IOWEB3 TECHNOLOGIES PRIVATE LIMITED\r\nBranch-: KALYANI NAGAR, PUNE- 411006 \r\nSwift code- IDFBINBBMUM', '', NULL, 'draft', 2, 0, 5, 0, 0, 1, 'months', 0, NULL, 0, NULL, NULL, 0, 'percentage', 'before_tax', NULL, 0, 'a:0:{}', 0),
(79, 524, 0, '2024-01-16', '2024-02-28', 'Bank Account Details:\r\nBank Name: IDFC FIRST Bank\r\nAccount Name: IOWEB3 technologies Private limited \r\nBank Branch: Kalyani Nagar Branch\r\nBank Address: S No 206, Unit No 1, Ground Floor, Plot No 88, Ramwadi, Kalyani Nagar, Near Bishop School\r\nRTGS/NEFT IFSC Code: IDFB0041358\r\nAccount No: 10089435901\r\nPIN/Zone Code: 411006', '', NULL, 'draft', 2, 0, 0, 0, 0, 1, 'months', 0, NULL, 0, NULL, NULL, 0, 'percentage', 'before_tax', NULL, 0, 'a:0:{}', 0),
(80, 523, 0, '2024-01-17', '2024-01-18', 'Bank Account Details:\r\nBank Name: IDFC FIRST Bank\r\nAccount Name: IOWEB3 technologies Private limited \r\nBank Branch: Kalyani Nagar Branch\r\nBank Address: S No 206, Unit No 1, Ground Floor, Plot No 88, Ramwadi, Kalyani Nagar, Near Bishop School\r\nRTGS/NEFT IFSC Code: IDFB0041358\r\nAccount No: 10089435901\r\nPIN/Zone Code: 411006', '', NULL, 'draft', 2, 0, 0, 0, 0, 1, 'months', 0, NULL, 0, NULL, NULL, 0, 'percentage', 'before_tax', NULL, 0, 'a:0:{}', 0),
(81, 516, 0, '2024-01-30', '2024-01-31', 'December billing', '', NULL, 'draft', 2, 0, 5, 0, 0, 1, 'months', 0, NULL, 0, NULL, NULL, 0, 'percentage', 'before_tax', NULL, 0, 'a:0:{}', 0),
(82, 525, 0, '2024-02-13', '2024-02-17', 'Bank Account Details:\r\nBank Name: IDFC FIRST Bank\r\nAccount Name: IOWEB3 technologies Private limited \r\nBank Branch: Kalyani Nagar Branch\r\nBank Address: S No 206, Unit No 1, Ground Floor, Plot No 88, Ramwadi, Kalyani Nagar, Near Bishop School\r\nRTGS/NEFT IFSC Code: IDFB0041358\r\nAccount No: 10089435901\r\nPIN/Zone Code: 411006', '', NULL, 'draft', 0, 0, 0, 0, 0, 1, 'months', 0, NULL, 0, NULL, NULL, 0, 'percentage', 'before_tax', NULL, 0, 'a:0:{}', 0),
(83, 517, 0, '2024-02-13', '2024-02-16', '', '', NULL, 'draft', 2, 0, 0, 0, 0, 1, 'months', 0, NULL, 0, NULL, NULL, 0, 'percentage', 'before_tax', NULL, 0, 'a:0:{}', 0),
(84, 507, 0, '2024-02-15', '2024-02-20', '', '', NULL, 'draft', 2, 0, 0, 0, 0, 1, 'months', 0, NULL, 0, NULL, NULL, 0, 'percentage', 'before_tax', NULL, 0, 'a:0:{}', 0),
(85, 523, 0, '2024-02-29', '2024-02-29', '', '', NULL, 'draft', 2, 0, 0, 0, 0, 1, 'months', 0, NULL, 0, NULL, NULL, 0, 'percentage', 'before_tax', NULL, 0, 'a:0:{}', 0),
(86, 516, 0, '2024-02-29', '2024-02-29', 'January billing', '', NULL, 'not_paid', 2, 0, 5, 0, 0, 1, 'months', 0, NULL, 0, NULL, NULL, 0, 'percentage', 'before_tax', NULL, 0, 'a:0:{}', 0),
(87, 525, 0, '2024-03-04', '2024-03-05', 'Bank Account Details:\r\nBank Name: IDFC FIRST Bank\r\nAccount Name: IOWEB3 technologies Private limited \r\nBank Branch: Kalyani Nagar Branch\r\nBank Address: S No 206, Unit No 1, Ground Floor, Plot No 88, Ramwadi, Kalyani Nagar, Near Bishop School\r\nRTGS/NEFT IFSC Code: IDFB0041358\r\nAccount No: 10089435901\r\nPIN/Zone Code: 411006', '', NULL, 'draft', 2, 0, 0, 0, 0, 1, 'months', 0, NULL, 0, NULL, NULL, 0, 'percentage', 'before_tax', NULL, 0, 'a:0:{}', 0),
(88, 514, 0, '2024-03-05', '2024-03-05', '', '', NULL, 'draft', 2, 0, 0, 0, 0, 1, 'months', 0, NULL, 0, NULL, NULL, 0, 'percentage', 'before_tax', NULL, 0, 'a:0:{}', 0),
(89, 522, 0, '2024-03-06', '2024-03-10', '', '', NULL, 'draft', 2, 0, 0, 0, 0, 1, 'months', 0, NULL, 0, NULL, NULL, 0, 'percentage', 'before_tax', NULL, 0, 'a:0:{}', 0),
(90, 517, 0, '2024-03-14', '2024-03-16', '', '', NULL, 'draft', 2, 0, 0, 0, 0, 1, 'months', 0, NULL, 0, NULL, NULL, 0, 'percentage', 'before_tax', NULL, 0, 'a:0:{}', 0),
(91, 509, 0, '2024-03-19', '2024-03-21', 'IDFC0000123\r\n76216782345\r\nkharadi pune', '', NULL, 'draft', 2, 0, 1, 1, 0, 1, 'years', 5, '2025-03-19', 0, NULL, NULL, 10, 'percentage', 'before_tax', NULL, 0, 'a:0:{}', 0),
(92, 526, 0, '2024-03-20', '2024-03-31', 'Bank Account Details:\r\nBank Name: IDFC FIRST Bank\r\nAccount Name: IOWEB3 technologies Private limited \r\nBank Branch: Kalyani Nagar Branch\r\nBank Address: S No 206, Unit No 1, Ground Floor, Plot No 88, Ramwadi, Kalyani Nagar, Near Bishop School\r\nRTGS/NEFT IFSC Code: IDFB0041358\r\nAccount No: 10089435901\r\nPIN/Zone Code: 411006', '', NULL, 'draft', 6, 0, 0, 0, 0, 1, 'months', 0, NULL, 0, NULL, NULL, 0, 'percentage', 'before_tax', NULL, 0, 'a:0:{}', 0),
(93, 516, 0, '2024-03-29', '2024-03-31', 'February billing', '', NULL, 'draft', 2, 0, 5, 0, 0, 1, 'months', 0, NULL, 0, NULL, NULL, 0, 'percentage', 'before_tax', NULL, 0, 'a:0:{}', 0),
(94, 525, 0, '2024-04-03', '2024-04-09', 'March billing\r\nBank Account Details:\r\nBank Name: IDFC FIRST Bank\r\nAccount Name: IOWEB3 technologies Private limited\r\nBank Branch: Kalyani Nagar Branch\r\nBank Address: S No 206, Unit No 1, Ground Floor, Plot No 88, Ramwadi, Kalyani Nagar, Near  RTGS/NEFT IFSC Code: IDFB0041358\r\nAccount No: 10089435901\r\nPIN/Zone Code: 411006', '', NULL, 'draft', 2, 0, 0, 0, 0, 1, 'months', 0, NULL, 0, NULL, NULL, 0, 'percentage', 'before_tax', NULL, 0, 'a:0:{}', 0),
(95, 517, 0, '2024-04-03', '2024-04-05', 'Bank Account Details:\r\nBank Name: IDFC FIRST Bank\r\nAccount Name: IOWEB3 technologies Private limited\r\nBank Branch: Kalyani Nagar Branch\r\nBank Address: S No 206, Unit No 1, Ground Floor, Plot No 88, Ramwadi, Kalyani Nagar, RTGS/NEFT IFSC Code: IDFB0041358\r\nAccount No: 10089435901\r\nPIN/Zone Code: 411006', '', NULL, 'draft', 2, 0, 0, 0, 0, 1, 'months', 0, NULL, 0, NULL, NULL, 0, 'percentage', 'before_tax', NULL, 0, 'a:0:{}', 0),
(96, 510, 0, '2024-04-04', '2024-04-04', 'Powered by TCPDF (www.tcpdf.org)\r\nBank Account Details:\r\nBank Name: IDFC FIRST Bank\r\nAccount Name: IOWEB3 technologies Private limited\r\nBank Branch: Kalyani Nagar Branch\r\nBank Address: S No 206, Unit No 1, Ground Floor, Plot No 88, Ramwadi, RTGS/NEFT IFSC Code: IDFB0041358\r\nAccount No: 10089435901\r\nPIN/Zone Code: 411006', '', NULL, 'draft', 2, 0, 0, 0, 0, 1, 'months', 0, NULL, 0, NULL, NULL, 0, 'percentage', 'before_tax', NULL, 0, 'a:0:{}', 0),
(97, 516, 0, '2024-04-06', '2024-04-15', 'March Billing', '', NULL, 'draft', 2, 0, 5, 0, 0, 1, 'months', 0, NULL, 0, NULL, NULL, 0, 'percentage', 'before_tax', NULL, 0, 'a:0:{}', 0);

-- --------------------------------------------------------

--
-- Table structure for table `invoice_items`
--

CREATE TABLE `invoice_items` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `description` text DEFAULT NULL,
  `quantity` double NOT NULL,
  `unit_type` varchar(20) NOT NULL DEFAULT '',
  `rate` double NOT NULL,
  `total` double NOT NULL,
  `sort` int(11) NOT NULL DEFAULT 0,
  `invoice_id` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `invoice_items`
--

INSERT INTO `invoice_items` (`id`, `title`, `description`, `quantity`, `unit_type`, `rate`, `total`, `sort`, `invoice_id`, `deleted`) VALUES
(1, 'website development', 'complete website', 1, '', 6000, 6000, 0, 1, 0),
(2, 'Digital Insights Toolkit Part # 4', '', 1, '', 1533.189, 1533.189, 0, 11, 0),
(3, 'Software Professional Billing', 'Software Professional Name- Mr. Deepak', 1, '', 1895, 1895, 0, 12, 1),
(4, 'Digital Insights Toolkit Part # 4', '', 1, '', 1895, 1895, 0, 12, 0),
(5, 'Digital Insights Toolkit Part # 4', '', 1, '', 1847, 1847, 0, 24, 0),
(6, 'Digital Insights Toolkit Part # 4', '', 1, '', 1847, 1847, 0, 26, 0),
(7, 'Digital Insights Toolkit Part # 4', '', 1, '', 1847, 1847, 0, 27, 0),
(8, 'Backend API', '', 25, 'hrs', 1000, 25000, 0, 28, 0),
(9, 'Digital Insights Toolkit Part # 4', '', 1, '', 1729.81, 1729.81, 0, 29, 0),
(10, 'Website Hosting', 'Website Hosting Renewal for 1 yr (Next Renewal Date - 30th Nov. 2023)', 1, '', 5050, 5050, 0, 30, 0),
(11, 'Profile Designing', '', 1, '', 5000, 5000, 0, 31, 0),
(12, 'Backend API', '', 16, 'hours', 400, 6400, 0, 32, 0),
(13, 'Web  Portal Development', 'Advance ', 1, '', 350000, 350000, 0, 33, 0),
(14, 'Digital Insights Toolkit Part # 4', '', 1, '', 1809.04, 1809.04, 0, 34, 0),
(15, 'G-Suite', '', 20, '', 125, 2500, 0, 35, 1),
(16, 'G-Suite Mail id\'s for Homeseva.co.in', '125rs/month', 20, '', 1500, 30000, 0, 35, 0),
(17, 'Digital Insights Toolkit Part # 4', '', 1, '', 1607.6, 1607.6, 0, 36, 0),
(18, 'Curiotory Application Development ', '20% Second installment ', 1, '', 130000, 130000, 0, 37, 0),
(19, 'Live Video Integration with whiteboard', '50% Advance (Work Complete)', 1, '', 67500, 67500, 0, 37, 0),
(20, 'Web application for end users', '50% Advance (work complete)', 1, '', 90000, 90000, 0, 37, 0),
(21, 'Website Hosting', 'Website Hosting Server Charges for 1 year ', 1, '', 3000, 3000, 0, 38, 0),
(22, 'SSL certificate', 'SSL certificate godaddy', 1, '', 2250, 2250, 0, 38, 0),
(23, 'Domain Name', '', 1, '', 900, 900, 0, 38, 0),
(24, 'yellow fabric roll', 'kjcj lkjsdlk', 200, 'kg', 10, 2000, 0, 39, 1),
(25, 'yellow fabric roll', 'kjcj lkjsdlk', 200, 'kg', 10, 2000, 0, 39, 0),
(26, 'Website Hosting', 'Website Hosting Server Charges for 1 year', 1, '', 2800, 2800, 0, 40, 0),
(27, 'SSL certificate', 'SSL certificate godaddy', 1, '', 2250, 2250, 0, 40, 0),
(28, 'Digital Insights Toolkit Part # 4', '', 1, '', 32.8, 32.8, 0, 42, 0),
(29, 'Digital Insights Toolkit Part # 4', '', 1, '', 1748.43, 1748.43, 0, 41, 0),
(30, 'Social Media Marketing', 'Social Pro Plus Plan', 1, '', 28000, 28000, 0, 43, 0),
(31, 'Google Ads', '', 1, '', 15000, 15000, 0, 43, 0),
(32, 'eCommerce API back-end development', '', 1, '', 25000, 25000, 0, 44, 0),
(33, 'Website Backup ', '', 1, '', 1800, 1800, 0, 40, 0),
(34, 'Developer Billing', 'April Billing', 1, '', 20000, 20000, 0, 45, 0),
(35, 'Social Media Marketing', 'Social Pro Plus Plan', 1, '', 28000, 28000, 0, 46, 0),
(36, 'Google Ads', '', 1, '', 15000, 15000, 0, 46, 0),
(37, 'website development', 'Advance Payment', 1, '', 25000, 25000, 0, 47, 0),
(38, 'Ajay Golang Billing', '', 1, '', 167097, 167097, 0, 48, 0),
(39, 'PHP Developer For May Month', '', 1, '', 30000, 30000, 0, 49, 0),
(40, 'eCommerce API back-end development', '', 1, '', 25000, 25000, 0, 50, 0),
(41, 'Social Media Marketing', 'Social Pro Plan', 1, '', 28000, 28000, 0, 51, 0),
(42, 'Social Media Marketing', 'Social Pro Plan', 1, '', 28000, 28000, 0, 52, 0),
(43, 'Hosting Monthly Plan', 'Digital Ocean for last two months', 2, '', 4900, 9800, 0, 52, 0),
(44, 'June Monthly Maintenance ', '', 1, '', 30000, 30000, 0, 53, 0),
(45, 'Ajay Golang Billing', '', 1, '', 185000, 185000, 0, 54, 0),
(46, 'website development Advance', '', 1, '', 8000, 8000, 0, 55, 0),
(47, 'Social Media Marketing', 'Social Pro Plan', 1, '', 28000, 28000, 0, 56, 0),
(48, 'Curiotory Application Hosting', 'Digital Ocean For july', 1, '', 4800, 4800, 0, 56, 0),
(49, 'Ajay Golang Billing', '', 1, '', 185000, 185000, 0, 57, 0),
(50, 'Technical Services for month of July 2023 (DG) ', '', 1, '', 82258, 82258, 0, 58, 0),
(51, 'Technical Services for month of July 2023 (GOP) ', '', 1, '', 110322.58, 110322.58, 0, 59, 0),
(52, 'Technical Services for month of July 2023 (DIV) ', '', 1, '', 79838.7, 79838.7, 0, 60, 0),
(53, 'Laptop Rent', '', 1, '', 1500, 1500, 0, 60, 0),
(54, 'Laptop Rent', '', 1, '', 1500, 1500, 0, 59, 0),
(55, 'Laptop Rent', '', 1, '', 1500, 1500, 0, 58, 0),
(56, 'Creatives Monthly 15+5', 'Creatives For Social Media and Social Media Handling ', 1, '', 7500, 7500, 0, 61, 0),
(57, 'Social Media Marketing', 'Social Pro Plan', 1, '', 28000, 28000, 0, 62, 0),
(58, 'Curiotory Application Hosting', 'Digital Ocean For july', 1, '', 4800, 4800, 0, 62, 0),
(59, 'Bangloreseva Application Development', '', 3, 'Resources', 45000, 135000, 0, 63, 0),
(60, 'Digital Marketing SEO yearly Plan', 'Tool - Nowfloats', 12, '', 2370.9166, 28450.9992, 0, 64, 0),
(61, 'Curiotory Application Development ', '50% Final installment ', 1, '', 325000, 325000, 0, 65, 0),
(62, 'Live Video Integration with whiteboard', '50% Final Intallment ', 1, '', 67500, 67500, 0, 65, 0),
(63, 'Web application for end users', '50% Final installment', 1, '', 90000, 90000, 0, 65, 0),
(64, 'Software Development Monthly billing', '', 3, '', 50000, 150000, 0, 66, 0),
(65, 'Technical Services For month of August (DG)', '', 1, '', 159032, 159032, 0, 67, 0),
(66, 'Development of B2B (Business-to-Business) account intelligence platform', 'Advance Payment-30%', 1, '', 450000, 450000, 0, 70, 0),
(67, 'Ajay Golang Billing', '', 1, '', 185000, 185000, 0, 71, 0),
(68, 'Technical Services For month of August (DIV)', '', 1, '', 37258.06, 37258.06, 0, 68, 0),
(69, 'Technical Services for month of August 2023 (GOPI)', '', 1, '', 162580, 162580, 0, 69, 0),
(70, 'Laptop Rent', '', 1, '', 1500, 1500, 0, 69, 0),
(71, 'Laptop Rent', '', 1, '', 1500, 1500, 0, 67, 0),
(72, 'Laptop Rent', '', 1, '', 1500, 1500, 0, 68, 0),
(73, 'website development Pendning Amount', '', 1, '', 7000, 7000, 0, 72, 0),
(74, 'website development', 'Extra Work', 1, '', 10000, 10000, 0, 72, 0),
(75, 'Facebook and Instagram Budget', 'For Banquet, Rooftop Restaurant, Hotel ', 3, '', 12000, 36000, 0, 73, 0),
(76, 'Social Media Handling Charges ', 'Social Media Page Handling', 3, '', 3000, 9000, 0, 73, 0),
(77, 'Post Design for Social Media', 'Social Media Design Plan', 3, '', 7500, 22500, 0, 73, 1),
(78, 'Creatives Monthly 15+5', 'Creatives For Social Media and Social Media Handling ', 3, '', 7500, 22500, 0, 74, 0),
(79, 'Creatives Monthly 15+5', 'Creatives For Social Media and Social Media Handling ', 3, '', 6000, 18000, 0, 75, 0),
(80, 'Technical service for month of September ', '2.5 days extra work', 1, '', 184167, 184167, 0, 76, 0),
(81, 'Laptop Rent', '', 1, '', 3000, 3000, 0, 76, 0),
(82, 'SWAI Revamp', 'Advance Payment 30% Against PO 23005601', 1, '', 37500, 37500, 0, 77, 0),
(83, 'Ashok ', 'Senior Developer (ISHIR)', 9, 'Day', 5250, 47250, 0, 78, 0),
(84, 'Amol', 'Senior Developer (PWC) Nov.', 11, 'Day', 8250, 90750, 0, 78, 0),
(85, 'Design and Develop Mobile Application', 'Best Agro E-commerce App', 1, '', 450000, 450000, 0, 79, 0),
(86, 'Design and Develop Mobile Application', 'Best Agro Krushi Services', 1, '', 750000, 750000, 0, 79, 0),
(87, 'Post Design for Social Media', 'Social Media Design Plan (2 months recived)', 2, '', 6000, 12000, 0, 80, 1),
(88, 'Ashok Billing', 'December Working Days 20', 20, '', 5250, 105000, 0, 81, 0),
(89, 'Amol Billing', 'December Working Days 20', 19, '', 8250, 156750, 0, 81, 0),
(90, 'Harikrishna Billing', 'December Working Days 20', 15, '', 6000, 90000, 0, 81, 0),
(91, 'E-learning Platform Source code + Deployment ', '50% Advance Total Cost 300000/-', 1, '', 150000, 150000, 0, 82, 0),
(92, 'ReactJS Developer Billing ', 'ADV Monthly Billing', 2, '', 85000, 170000, 0, 82, 1),
(93, 'Senior Developer (NodeJs)', 'Monthly Billing', 40, 'hours', 1500, 60000, 0, 82, 1),
(94, 'Project Manager (Full stack developer)', 'Monthly Billing', 1, '', 100000, 100000, 0, 82, 1),
(95, 'Backend (Node JS)', 'ADV Monthly', 1, '', 65000, 65000, 0, 82, 1),
(96, 'Creatives Monthly 15+5', 'Creatives For Social Media and Social Media Handling ', 1, '', 7500, 7500, 0, 83, 0),
(97, 'Website Hosting', 'Website Hosting Renewal for 1 yr (Next Renewal Date - 30th Nov. 2023)', 1, '', 5250, 5250, 0, 84, 0),
(98, 'Facebook and Instagram Budget', 'Daily 500 from 26 feb ', 20, '', 500, 10000, 0, 85, 0),
(99, 'Social Media Handling Charges', '', 1, '', 2000, 2000, 0, 85, 0),
(100, 'Ashok Billing', 'December Working Days 20', 20, '', 5000, 100000, 0, 86, 0),
(101, 'Amol Billing', 'December Working Days 20', 21, '', 7857.143, 165000.003, 0, 86, 0),
(102, 'Harikrishna Billing', 'December Working Days 20', 21, '', 5714.29, 120000.09, 0, 86, 0),
(103, 'Chandrapratap Billing', '13 days out of 21', 13, '', 5952.38, 77380.94, 0, 86, 0),
(104, 'E-learning Platform Source code + Deployment ', '50% on code transfer ', 1, '', 150000, 150000, 0, 87, 1),
(105, 'Developer Billing', 'Ishwari, Huzaif, Tejas Feb month Billing', 3, '', 29250, 87750, 0, 87, 0),
(106, 'Advance payment pending', 'in 50% advance ioweb3 receive 150k inclusive gst', 1, '', 27000, 27000, 0, 87, 1),
(107, 'Developer Billing', 'Java 2022', 1, '', 25000, 25000, 0, 88, 0),
(108, 'website development', 'complete website', 1, '', 5000, 5000, 0, 89, 0),
(109, 'Creatives Monthly 15+5', 'Creatives For Social Media and Social Media Handling ', 1, '', 7500, 7500, 0, 90, 0),
(110, 'BIS certificate', 'ISI mark product certificate', 1, '', 30000, 30000, 0, 91, 0),
(111, 'Java October bill', 'Java dadasaheb', 1, '', 164516, 164516, 0, 86, 0),
(112, 'DGCA Certied Agriculture Drone ', '3 Set of Battery Included', 2, '', 895238, 1790476, 0, 92, 0),
(113, 'Ashok Billing', 'February Working Days 20', 21, '', 5000, 105000, 0, 93, 0),
(114, 'Amol Billing', 'February Working Days 20', 20, '', 8250, 165000, 0, 93, 0),
(115, 'Harikrishna Billing', 'February Working Days 20', 9, '', 5714.29, 51428.61, 0, 93, 1),
(116, 'Chandrapratap Billing', 'February Working Days 20', 20, '', 6250, 125000, 0, 93, 0),
(117, 'Java October bill', 'Java dadasaheb', 1, '', 164516, 164516, 0, 93, 1),
(118, 'Praveen Billing', 'February Working Days 20', 20, '', 6000, 120000, 0, 93, 0),
(119, 'Backend (Node JS)', 'Tejas March Billing', 1, '', 45000, 45000, 0, 94, 0),
(120, 'Front End (React Js development)', 'Ishwari and Huzaif March billing', 2, '', 45000, 90000, 0, 94, 0),
(121, 'Senior Developer (Angular Js)', 'Shubham Billing For SYU', 141.5, 'hours', 600, 84900, 0, 94, 0),
(122, 'Junior Front End (Angular Developer)', 'Sanket Billing For SYU', 167.5, 'Hours', 400, 67000, 0, 94, 0),
(123, 'Creatives Monthly 15+5', 'Creatives For Social Media and Social Media Handling (March)', 1, '', 7500, 7500, 0, 95, 0),
(124, 'Gsuite Renewal', 'April 2024 to April 2025', 10, '', 2250, 22500, 0, 96, 0),
(125, 'Ashok Billing', 'February Working Days 20', 7, '', 5000, 35000, 0, 97, 0),
(126, 'Amol Billing', 'March Working Days 21', 21, '', 7857.2, 165001.2, 0, 97, 0),
(127, 'Chandrapratap Billing', 'February Working Days 20', 21, '', 5953, 125013, 0, 97, 0),
(128, 'Praveen Billing', 'February Working Days 20', 18, '', 5715, 102870, 0, 97, 0),
(129, 'Post Design for Social Media', 'Social Media Design Plan (10 Feb to 10 April)', 2, '', 6000, 12000, 0, 80, 0);

-- --------------------------------------------------------

--
-- Table structure for table `invoice_payments`
--

CREATE TABLE `invoice_payments` (
  `id` int(11) NOT NULL,
  `amount` double NOT NULL,
  `payment_date` date NOT NULL,
  `payment_method_id` int(11) NOT NULL,
  `note` text DEFAULT NULL,
  `invoice_id` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  `transaction_id` tinytext DEFAULT NULL,
  `created_by` int(11) DEFAULT 1,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `invoice_payments`
--

INSERT INTO `invoice_payments` (`id`, `amount`, `payment_date`, `payment_method_id`, `note`, `invoice_id`, `deleted`, `transaction_id`, `created_by`, `created_at`) VALUES
(1, 536000, '2024-03-18', 1, '', 86, 0, NULL, 1, '2024-03-19 07:00:19');

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `description` text DEFAULT NULL,
  `unit_type` varchar(20) NOT NULL DEFAULT '',
  `rate` double NOT NULL,
  `files` mediumtext NOT NULL,
  `show_in_client_portal` tinyint(1) NOT NULL DEFAULT 0,
  `category_id` int(11) NOT NULL,
  `sort` int(11) NOT NULL DEFAULT 0,
  `deleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `title`, `description`, `unit_type`, `rate`, `files`, `show_in_client_portal`, `category_id`, `sort`, `deleted`) VALUES
(6, 'Phone', 'Lenovo', 'NA', 20000, '', 0, 0, 0, 0),
(7, 'lenovo Think Pad', 'E15 Gen/intel 11gen/core 15/1135G7/16GB ', '1', 250, '', 0, 0, 0, 0),
(8, 'website development', 'complete website', '', 6000, '', 0, 0, 0, 0),
(9, 'Deepak Billing', 'Contractual', '', 1898, '', 0, 0, 0, 0),
(10, 'Software Professional Billing', 'Software Professional Name- Mr. Deepak', '', 1895, '', 0, 0, 0, 0),
(11, 'Digital Insights Toolkit Part # 4', '', '', 1895, '', 0, 0, 0, 0),
(12, 'Backend API', '', '30 hrs', 25000, '', 0, 0, 0, 0),
(13, 'Website Hosting', 'Website Hosting Server Charges for 1 yesr', '', 5050, '', 0, 0, 0, 0),
(14, 'Profile Designing', '', '', 5000, '', 0, 0, 0, 0),
(15, 'Backend API', '', 'hours', 400, '', 0, 0, 0, 0),
(16, 'Web  Portal Development', 'Advance 30%', '', 105000, '', 0, 0, 0, 0),
(17, 'G-Suite', '', '', 125, '', 0, 0, 0, 0),
(18, 'G-Suite', '125rs/month', '', 1500, '', 0, 0, 0, 0),
(19, 'Curiotory Application Development ', '20% Second installment ', '', 130000, '', 0, 0, 0, 0),
(20, 'Live Video Integration with whiteboard', '50% Advance ', '', 67500, '', 0, 0, 0, 0),
(21, 'Web application for end users', '50% Advance (work complete)', '', 90000, '', 0, 0, 0, 0),
(22, 'SSL certificate', 'SSL certificate godaddy', '', 2250, '', 0, 0, 0, 0),
(23, 'Domain Name', '', '', 900, '', 0, 0, 0, 0),
(24, 'yellow fabric roll', 'kjcj lkjsdlk', 'kg', 100, '', 0, 0, 0, 0),
(25, 'Social Media Marketing', 'Social Pro Plus Plan', '', 28000, '', 0, 0, 0, 0),
(26, 'Google Ads', '', '', 15000, '', 0, 0, 0, 0),
(27, 'eCommerce API back-end development', '', '', 25000, '', 0, 0, 0, 0),
(28, 'Website Backup ', '', '', 1800, '', 0, 0, 0, 0),
(29, 'Developer Billing', 'April Billing', '', 20000, '', 0, 0, 0, 0),
(30, 'Ajay Golang Billing', '', '', 167097, '', 0, 0, 0, 0),
(31, 'PHP Developer For May Month', '', '', 30000, '', 0, 0, 0, 0),
(32, 'eCommerce API back-end development', '', '', 25000, '', 0, 0, 0, 0),
(33, 'Social Media Marketing', 'Social Pro Plan', '', 28000, '', 0, 0, 0, 0),
(34, 'Hosting Monthly Plan', 'Digital Ocean', '', 4900, '', 0, 0, 0, 0),
(35, 'Monthly Maintenance ', '', '', 30000, '', 0, 0, 0, 0),
(36, 'Curiotory Application Hosting', 'For july', '', 4800, '', 0, 0, 0, 0),
(37, 'Technical Services for month of July 2023 (DG) ', '', '', 82258, '', 0, 0, 0, 0),
(38, 'Laptop Rent', '', '', 3000, '', 0, 0, 0, 0),
(39, 'Creatives Monthly 15+5', 'Creatives For Social Media and Social Media Handling ', '', 7500, '', 0, 0, 0, 0),
(40, 'Bangloreseva Application Development', '', 'Resources', 45000, '', 0, 0, 0, 0),
(41, 'Digital Marketing SEO yearly Plan', 'Tool - Nowfloats', '', 2370.9166, '', 0, 0, 0, 0),
(42, 'Software Development Monthly billing', '', '', 50000, '', 0, 0, 0, 0),
(43, 'Technical Services For month of August (DG)', '', '', 170000, '', 0, 0, 0, 0),
(44, 'Development of B2B (Business-to-Business) account intelligence platform', '', '', 450000, '', 0, 0, 0, 0),
(45, 'Technical Services For month of August (DIV)', '', '', 69193.5483, '', 0, 0, 0, 0),
(46, 'Technical Services for month of August 2023 (DG)', '', '', 159032, '', 0, 0, 0, 0),
(47, 'Facebook and Instagram Budget', 'For Banquet, Rooftop Restaurant, Hotel ', '', 12000, '', 0, 0, 0, 0),
(48, 'Social Media Handling Charges ', 'Social Media Page Handling', '', 3000, '', 0, 0, 0, 0),
(49, 'Post Design for Social Media', 'Social Media Design Plan', '', 7500, '', 0, 0, 0, 0),
(50, 'Technical service for month of September ', '2.5 days extra work', '', 184167, '', 0, 0, 0, 0),
(51, 'SWAI Revamp', '', '', 37500, '', 0, 0, 0, 0),
(52, 'Ashok ', 'Senior Developer (ISHIR)', 'Day', 3500, '', 0, 0, 0, 0),
(53, 'Amol', 'Senior Developer (PWC) Nov.', 'Day', 5500, '', 0, 0, 0, 0),
(54, 'Design and Develop Mobile Application', 'Best Agro E-commerce App', '', 450000, '', 0, 0, 0, 0),
(55, 'Ashok Billing', 'December ', '', 5000, '', 0, 0, 0, 0),
(56, 'Amol Billing', 'December', '', 8000, '', 0, 0, 0, 0),
(57, 'Harikrishna Billing', 'December', '', 6000, '', 0, 0, 0, 0),
(58, 'E-learning Platform Source code + Deployment ', '85% Advance Total Cost 300000/-', '', 255000, '', 0, 0, 0, 0),
(59, 'AngularJs Developer Billing ', '', '', 85000, '', 0, 0, 0, 0),
(60, 'Senior Developer (NodeJs)', '', '', 120000, '', 0, 0, 0, 0),
(61, 'Project Manager', '', '', 100000, '', 0, 0, 0, 0),
(62, 'Backend (Node JS)', 'ADV Monthly', '', 65000, '', 0, 0, 0, 0),
(63, 'Facebook and Instagram Budget', 'Daily 500 from 26 feb ', '', 500, '', 0, 0, 0, 0),
(64, 'Social Media Handling Charges', '', '', 2000, '', 0, 0, 0, 0),
(65, 'Chandrapratap Billing', '13 days out of 21', '', 6190.48, '', 0, 0, 0, 0),
(66, 'Advance payment pending', 'in 50% advance ioweb3 receive 150k inclusive gst', '', 27000, '', 0, 0, 0, 0),
(67, 'BIS certificate', 'ISI mark product certificate', '', 30000, '', 0, 0, 0, 0),
(68, 'Java October bill', 'Java dadasaheb', '', 164516, '', 0, 0, 0, 0),
(69, 'DGCA Certied Agriculture Drone ', '3 Set of Battery Included', '', 895238, '', 0, 0, 0, 0),
(70, 'Praveen Billing', 'February Working Days 20', '', 6000, '', 0, 0, 0, 0),
(71, 'Backend (Node JS)', 'Tejas March Billing', '', 45000, '', 0, 0, 0, 0),
(72, 'Front End (React Js development)', 'Ishwari and Huzaif March billing', '', 45000, '', 0, 0, 0, 0),
(73, 'Senior Developer (Angular Js)', 'Shubham Billing ', 'hours', 600, '', 0, 0, 0, 0),
(74, 'Junior Front End (Angular Developer)', 'Sanket Billing For SYU', 'Hours', 400, '', 0, 0, 0, 0),
(75, 'Gsuite Renewal', 'April 2024 to April 2025', '', 2250, '', 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `item_categories`
--

CREATE TABLE `item_categories` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `item_categories`
--

INSERT INTO `item_categories` (`id`, `title`, `deleted`) VALUES
(1, 'General item', 0);

-- --------------------------------------------------------

--
-- Table structure for table `labels`
--

CREATE TABLE `labels` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `color` varchar(15) NOT NULL,
  `context` enum('event','invoice','note','project','task','ticket','to_do') DEFAULT NULL,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `deleted` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `labels`
--

INSERT INTO `labels` (`id`, `title`, `color`, `context`, `user_id`, `deleted`) VALUES
(1, 'Web (Front-end)', '#83c340', 'task', 0, 0),
(2, 'Web (Back-end)', '#83c340', 'task', 0, 0),
(3, 'APIs', '#83c340', 'task', 0, 0),
(4, 'Mobile App (Front-end)', '#83c340', 'task', 0, 0),
(5, 'Mobile App (API Integration)', '#83c340', 'task', 0, 0),
(6, 'Prod Deployment', '#83c340', 'task', 0, 0),
(7, 'Web (Testing)', '#83c340', 'task', 0, 0),
(8, 'Mobile App (Testing)', '#83c340', 'task', 0, 0),
(9, 'iOS Build', '#83c340', 'task', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `leads`
--

CREATE TABLE `leads` (
  `id` int(11) NOT NULL,
  `company_name` varchar(150) NOT NULL,
  `first_name` varchar(150) DEFAULT NULL,
  `last_name` varchar(150) DEFAULT NULL,
  `email` varchar(150) NOT NULL,
  `address` text DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `state` varchar(50) DEFAULT NULL,
  `zip` varchar(50) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `created_date` date NOT NULL,
  `website` text DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `lead_source`
--

CREATE TABLE `lead_source` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `sort` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `lead_source`
--

INSERT INTO `lead_source` (`id`, `title`, `sort`, `deleted`) VALUES
(1, 'Google', 1, 0),
(2, 'Facebook', 2, 0),
(3, 'Twitter', 3, 0),
(4, 'Youtube', 4, 0),
(5, 'Elsewhere', 5, 0),
(6, 'Housing.com', 6, 0),
(7, 'Reference', 7, 0),
(8, 'Magic Bricks', 8, 0),
(9, 'Email', 9, 0),
(10, 'Website form', 10, 0);

-- --------------------------------------------------------

--
-- Table structure for table `lead_status`
--

CREATE TABLE `lead_status` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `color` varchar(7) NOT NULL,
  `sort` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `lead_status`
--

INSERT INTO `lead_status` (`id`, `title`, `color`, `sort`, `deleted`) VALUES
(1, 'New', '#f1c40f', 0, 0),
(2, 'Qualified', '#2d9cdb', 1, 0),
(3, 'Discussion', '#29c2c2', 2, 0),
(4, 'Negotiation', '#2d9cdb', 3, 0),
(5, 'Won', '#83c340', 4, 0),
(6, 'Lost', '#e74c3c', 5, 0),
(7, 'Dead', '#f1c40f', 6, 0),
(8, 'Site Visit Done', '#f1c40f', 7, 0),
(9, 'Old', '#f1c40f', 8, 0),
(10, 'Booking Done', '#f1c40f', 9, 0),
(11, 'MAIN ERA', '#83c340', 10, 0);

-- --------------------------------------------------------

--
-- Table structure for table `leave_applications`
--

CREATE TABLE `leave_applications` (
  `id` int(11) NOT NULL,
  `leave_type_id` int(11) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `total_hours` decimal(7,2) NOT NULL,
  `total_days` decimal(5,2) NOT NULL,
  `applicant_id` int(11) NOT NULL,
  `reason` mediumtext NOT NULL,
  `status` enum('pending','approved','rejected','canceled') NOT NULL DEFAULT 'pending',
  `created_at` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `checked_at` datetime DEFAULT NULL,
  `checked_by` int(11) NOT NULL DEFAULT 0,
  `deleted` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `leave_applications`
--

INSERT INTO `leave_applications` (`id`, `leave_type_id`, `start_date`, `end_date`, `total_hours`, `total_days`, `applicant_id`, `reason`, `status`, `created_at`, `created_by`, `checked_at`, `checked_by`, `deleted`) VALUES
(1, 1, '2023-02-01', '2023-02-01', 8.00, 1.00, 507, 'Casual Leave.', 'approved', '2023-01-31 14:13:54', 0, '2023-02-09 11:19:58', 1, 0),
(2, 1, '2023-02-01', '2023-02-02', 16.00, 2.00, 1, 'test', 'approved', '2023-02-09 11:19:23', 0, '2023-02-09 11:19:51', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `leave_types`
--

CREATE TABLE `leave_types` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `color` varchar(7) NOT NULL,
  `description` text DEFAULT NULL,
  `deleted` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `leave_types`
--

INSERT INTO `leave_types` (`id`, `title`, `status`, `color`, `description`, `deleted`) VALUES
(1, 'Casual Leave', 'active', '#83c340', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `likes`
--

CREATE TABLE `likes` (
  `id` int(11) NOT NULL,
  `project_comment_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `likes`
--

INSERT INTO `likes` (`id`, `project_comment_id`, `created_by`, `created_at`, `deleted`) VALUES
(1, 1, 506, '2023-01-31 14:23:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int(11) NOT NULL,
  `subject` varchar(255) NOT NULL DEFAULT 'Untitled',
  `message` mediumtext NOT NULL,
  `created_at` datetime NOT NULL,
  `from_user_id` int(11) NOT NULL,
  `to_user_id` int(11) NOT NULL,
  `status` enum('unread','read') NOT NULL DEFAULT 'unread',
  `message_id` int(11) NOT NULL DEFAULT 0,
  `deleted` int(1) NOT NULL DEFAULT 0,
  `files` longtext NOT NULL,
  `deleted_by_users` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `milestones`
--

CREATE TABLE `milestones` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `project_id` int(11) NOT NULL,
  `due_date` date NOT NULL,
  `description` text NOT NULL,
  `deleted` tinyint(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `notes`
--

CREATE TABLE `notes` (
  `id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `title` text NOT NULL,
  `description` mediumtext DEFAULT NULL,
  `project_id` int(11) NOT NULL DEFAULT 0,
  `client_id` int(11) NOT NULL DEFAULT 0,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `labels` text DEFAULT NULL,
  `files` mediumtext NOT NULL,
  `is_public` tinyint(1) NOT NULL DEFAULT 0,
  `deleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `description` longtext NOT NULL,
  `created_at` datetime NOT NULL,
  `notify_to` mediumtext NOT NULL,
  `read_by` mediumtext NOT NULL,
  `event` varchar(250) NOT NULL,
  `project_id` int(11) NOT NULL,
  `task_id` int(11) NOT NULL,
  `project_comment_id` int(11) NOT NULL,
  `ticket_id` int(11) NOT NULL,
  `ticket_comment_id` int(11) NOT NULL,
  `project_file_id` int(11) NOT NULL,
  `leave_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `to_user_id` int(11) NOT NULL,
  `activity_log_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `lead_id` int(11) NOT NULL,
  `invoice_payment_id` int(11) NOT NULL,
  `invoice_id` int(11) NOT NULL,
  `estimate_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `estimate_request_id` int(11) NOT NULL,
  `actual_message_id` int(11) NOT NULL,
  `parent_message_id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `announcement_id` int(11) NOT NULL,
  `proposal_id` int(11) NOT NULL,
  `estimate_comment_id` int(11) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `user_id`, `description`, `created_at`, `notify_to`, `read_by`, `event`, `project_id`, `task_id`, `project_comment_id`, `ticket_id`, `ticket_comment_id`, `project_file_id`, `leave_id`, `post_id`, `to_user_id`, `activity_log_id`, `client_id`, `lead_id`, `invoice_payment_id`, `invoice_id`, `estimate_id`, `order_id`, `estimate_request_id`, `actual_message_id`, `parent_message_id`, `event_id`, `announcement_id`, `proposal_id`, `estimate_comment_id`, `deleted`) VALUES
(1, 1, '', '2023-01-31 13:36:40', '507', '', 'project_member_added', 2, 0, 0, 0, 0, 0, 0, 0, 507, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(2, 1, '', '2023-01-31 13:36:40', '506,507', '', 'project_member_added', 2, 0, 0, 0, 0, 0, 0, 0, 506, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(3, 1, '', '2023-01-31 13:42:02', '506,507', '', 'project_task_created', 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(4, 1, '', '2023-01-31 14:08:42', '506', ',507,506', 'project_task_commented', 2, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(5, 1, '', '2023-01-31 14:08:59', '506', '', 'project_task_updated', 2, 1, 0, 0, 0, 0, 0, 0, 0, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(6, 1, '', '2023-02-09 11:19:51', '', '', 'leave_approved', 0, 0, 0, 0, 0, 0, 2, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(7, 1, '', '2023-02-09 11:19:58', '507', '', 'leave_approved', 0, 0, 0, 0, 0, 0, 1, 0, 507, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(8, 1, '', '2024-03-19 06:44:40', '', '', 'project_task_created', 4, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(9, 1, '', '2024-03-19 06:44:56', '', '', 'project_task_updated', 4, 2, 0, 0, 0, 0, 0, 0, 0, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(10, 1, '', '2024-03-19 06:46:05', '', '', 'project_task_commented', 4, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(11, 1, '', '2024-04-15 10:40:39', '', '', 'project_task_created', 4, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `notification_settings`
--

CREATE TABLE `notification_settings` (
  `id` int(11) NOT NULL,
  `event` varchar(250) NOT NULL,
  `category` varchar(50) NOT NULL,
  `enable_email` int(1) NOT NULL DEFAULT 0,
  `enable_web` int(1) NOT NULL DEFAULT 0,
  `enable_slack` int(1) NOT NULL DEFAULT 0,
  `notify_to_team` text NOT NULL,
  `notify_to_team_members` text NOT NULL,
  `notify_to_terms` text NOT NULL,
  `sort` int(11) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `notification_settings`
--

INSERT INTO `notification_settings` (`id`, `event`, `category`, `enable_email`, `enable_web`, `enable_slack`, `notify_to_team`, `notify_to_team_members`, `notify_to_terms`, `sort`, `deleted`) VALUES
(1, 'project_created', 'project', 0, 0, 0, '', '', '', 1, 0),
(2, 'project_deleted', 'project', 0, 0, 0, '', '', '', 2, 0),
(3, 'project_task_created', 'project', 0, 1, 0, '', '', 'project_members,task_assignee', 3, 0),
(4, 'project_task_updated', 'project', 0, 1, 0, '', '', 'task_assignee', 4, 0),
(5, 'project_task_assigned', 'project', 0, 1, 0, '', '', 'task_assignee', 5, 0),
(7, 'project_task_started', 'project', 0, 0, 0, '', '', '', 7, 0),
(8, 'project_task_finished', 'project', 0, 0, 0, '', '', '', 8, 0),
(9, 'project_task_reopened', 'project', 0, 0, 0, '', '', '', 9, 0),
(10, 'project_task_deleted', 'project', 0, 1, 0, '', '', 'task_assignee', 10, 0),
(11, 'project_task_commented', 'project', 0, 1, 0, '', '', 'task_assignee', 11, 0),
(12, 'project_member_added', 'project', 0, 1, 0, '', '', 'project_members', 12, 0),
(13, 'project_member_deleted', 'project', 0, 1, 0, '', '', 'project_members', 13, 0),
(14, 'project_file_added', 'project', 0, 1, 0, '', '', 'project_members', 14, 0),
(15, 'project_file_deleted', 'project', 0, 1, 0, '', '', 'project_members', 15, 0),
(16, 'project_file_commented', 'project', 0, 1, 0, '', '', 'project_members', 16, 0),
(17, 'project_comment_added', 'project', 0, 1, 0, '', '', 'project_members', 17, 0),
(18, 'project_comment_replied', 'project', 0, 1, 0, '', '', 'project_members,comment_creator', 18, 0),
(19, 'project_customer_feedback_added', 'project', 0, 1, 0, '', '', 'project_members', 19, 0),
(20, 'project_customer_feedback_replied', 'project', 0, 1, 0, '', '', 'project_members,comment_creator', 20, 0),
(21, 'client_signup', 'client', 0, 0, 0, '', '', '', 21, 0),
(22, 'invoice_online_payment_received', 'invoice', 0, 0, 0, '', '', '', 22, 0),
(23, 'leave_application_submitted', 'leave', 0, 0, 0, '', '', '', 23, 0),
(24, 'leave_approved', 'leave', 0, 1, 0, '', '', 'leave_applicant', 24, 0),
(25, 'leave_assigned', 'leave', 0, 1, 0, '', '', 'leave_applicant', 25, 0),
(26, 'leave_rejected', 'leave', 0, 1, 0, '', '', 'leave_applicant', 26, 0),
(27, 'leave_canceled', 'leave', 0, 0, 0, '', '', '', 27, 0),
(28, 'ticket_created', 'ticket', 0, 0, 0, '', '', '', 28, 0),
(29, 'ticket_commented', 'ticket', 0, 1, 0, '', '', 'client_primary_contact,ticket_creator', 29, 0),
(30, 'ticket_closed', 'ticket', 0, 1, 0, '', '', 'client_primary_contact,ticket_creator', 30, 0),
(31, 'ticket_reopened', 'ticket', 0, 1, 0, '', '', 'client_primary_contact,ticket_creator', 31, 0),
(32, 'estimate_request_received', 'estimate', 0, 0, 0, '', '', '', 32, 0),
(34, 'estimate_accepted', 'estimate', 0, 0, 0, '', '', '', 34, 0),
(35, 'estimate_rejected', 'estimate', 0, 0, 0, '', '', '', 35, 0),
(36, 'new_message_sent', 'message', 0, 0, 0, '', '', '', 36, 0),
(37, 'message_reply_sent', 'message', 0, 0, 0, '', '', '', 37, 0),
(38, 'invoice_payment_confirmation', 'invoice', 0, 0, 0, '', '', '', 22, 0),
(39, 'new_event_added_in_calendar', 'event', 0, 0, 0, '', '', '', 39, 0),
(40, 'recurring_invoice_created_vai_cron_job', 'invoice', 0, 0, 0, '', '', '', 22, 0),
(41, 'new_announcement_created', 'announcement', 0, 0, 0, '', '', 'recipient', 41, 0),
(42, 'invoice_due_reminder_before_due_date', 'invoice', 0, 0, 0, '', '', '', 22, 0),
(43, 'invoice_overdue_reminder', 'invoice', 0, 0, 0, '', '', '', 22, 0),
(44, 'recurring_invoice_creation_reminder', 'invoice', 0, 0, 0, '', '', '', 22, 0),
(45, 'project_completed', 'project', 0, 0, 0, '', '', '', 2, 0),
(46, 'lead_created', 'lead', 0, 0, 0, '', '', '', 21, 0),
(47, 'client_created_from_lead', 'lead', 0, 0, 0, '', '', '', 21, 0),
(48, 'project_task_deadline_pre_reminder', 'project', 0, 1, 0, '', '', 'task_assignee', 20, 0),
(49, 'project_task_reminder_on_the_day_of_deadline', 'project', 0, 1, 0, '', '', 'task_assignee', 20, 0),
(50, 'project_task_deadline_overdue_reminder', 'project', 0, 1, 0, '', '', 'task_assignee', 20, 0),
(51, 'recurring_task_created_via_cron_job', 'project', 0, 1, 0, '', '', 'project_members,task_assignee', 20, 0),
(52, 'calendar_event_modified', 'event', 0, 0, 0, '', '', '', 39, 0),
(53, 'client_contact_requested_account_removal', 'client', 0, 0, 0, '', '', '', 21, 0),
(54, 'bitbucket_push_received', 'project', 0, 1, 0, '', '', '', 45, 0),
(55, 'github_push_received', 'project', 0, 1, 0, '', '', '', 45, 0),
(56, 'invited_client_contact_signed_up', 'client', 0, 0, 0, '', '', '', 21, 0),
(57, 'created_a_new_post', 'timeline', 0, 0, 0, '', '', '', 52, 0),
(58, 'timeline_post_commented', 'timeline', 0, 0, 0, '', '', '', 52, 0),
(59, 'ticket_assigned', 'ticket', 0, 0, 0, '', '', 'ticket_assignee', 31, 0),
(60, 'new_order_received', 'order', 0, 0, 0, '', '', '', 1, 0),
(61, 'order_status_updated', 'order', 0, 0, 0, '', '', '', 2, 0),
(62, 'proposal_accepted', 'proposal', 0, 0, 0, '', '', '', 34, 0),
(63, 'proposal_rejected', 'proposal', 0, 0, 0, '', '', '', 35, 0),
(64, 'estimate_commented', 'estimate', 0, 0, 0, '', '', '', 35, 0),
(65, 'invoice_manual_payment_added', 'invoice', 0, 0, 0, '', '', '', 22, 0);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `order_date` date NOT NULL,
  `note` mediumtext DEFAULT NULL,
  `status_id` int(11) NOT NULL,
  `tax_id` int(11) NOT NULL DEFAULT 0,
  `tax_id2` int(11) NOT NULL DEFAULT 0,
  `discount_amount` double NOT NULL,
  `discount_amount_type` enum('percentage','fixed_amount') NOT NULL,
  `discount_type` enum('before_tax','after_tax') NOT NULL,
  `created_by` int(11) NOT NULL DEFAULT 0,
  `project_id` int(11) NOT NULL DEFAULT 0,
  `deleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `order_items`
--

CREATE TABLE `order_items` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `description` text DEFAULT NULL,
  `quantity` double NOT NULL,
  `unit_type` varchar(20) NOT NULL DEFAULT '',
  `rate` double NOT NULL,
  `total` double NOT NULL,
  `order_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `item_id` int(11) NOT NULL DEFAULT 0,
  `sort` int(11) NOT NULL DEFAULT 0,
  `deleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `order_items`
--

INSERT INTO `order_items` (`id`, `title`, `description`, `quantity`, `unit_type`, `rate`, `total`, `order_id`, `created_by`, `item_id`, `sort`, `deleted`) VALUES
(1, 'Ajay Golang Billing', NULL, 1, '', 167097, 167097, 0, 1, 30, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `order_status`
--

CREATE TABLE `order_status` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `color` varchar(7) NOT NULL,
  `sort` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `order_status`
--

INSERT INTO `order_status` (`id`, `title`, `color`, `sort`, `deleted`) VALUES
(1, 'New', '#f1c40f', 0, 0),
(2, 'Processing', '#29c2c2', 1, 0),
(3, 'Confirmed', '#83c340', 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(11) NOT NULL,
  `title` text DEFAULT NULL,
  `content` text DEFAULT NULL,
  `slug` text DEFAULT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `internal_use_only` tinyint(1) NOT NULL DEFAULT 0,
  `visible_to_team_members_only` tinyint(1) NOT NULL DEFAULT 0,
  `visible_to_clients_only` tinyint(1) NOT NULL DEFAULT 0,
  `deleted` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payment_methods`
--

CREATE TABLE `payment_methods` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `type` varchar(100) NOT NULL DEFAULT 'custom',
  `description` text NOT NULL,
  `online_payable` tinyint(1) NOT NULL DEFAULT 0,
  `available_on_invoice` tinyint(1) NOT NULL DEFAULT 0,
  `minimum_payment_amount` double NOT NULL DEFAULT 0,
  `settings` longtext NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `payment_methods`
--

INSERT INTO `payment_methods` (`id`, `title`, `type`, `description`, `online_payable`, `available_on_invoice`, `minimum_payment_amount`, `settings`, `deleted`) VALUES
(1, 'Cash', 'custom', 'Cash payments', 0, 0, 0, '', 0),
(2, 'Stripe', 'stripe', 'Stripe online payments', 1, 0, 0, 'a:3:{s:15:\"pay_button_text\";s:6:\"Stripe\";s:10:\"secret_key\";s:6:\"\";s:15:\"publishable_key\";s:6:\"\";}', 0),
(3, 'PayPal Payments Standard', 'paypal_payments_standard', 'PayPal Payments Standard Online Payments', 1, 0, 0, 'a:4:{s:15:\"pay_button_text\";s:6:\"PayPal\";s:5:\"email\";s:4:\"\";s:11:\"paypal_live\";s:1:\"0\";s:5:\"debug\";s:1:\"0\";}', 0),
(4, 'Paytm', 'paytm', 'Paytm online payments', 1, 0, 0, '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `paypal_ipn`
--

CREATE TABLE `paypal_ipn` (
  `id` int(11) NOT NULL,
  `transaction_id` tinytext DEFAULT NULL,
  `ipn_hash` longtext NOT NULL,
  `ipn_data` longtext NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pin_comments`
--

CREATE TABLE `pin_comments` (
  `id` int(11) NOT NULL,
  `project_comment_id` int(11) NOT NULL,
  `pinned_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `description` mediumtext NOT NULL,
  `post_id` int(11) NOT NULL,
  `share_with` text DEFAULT NULL,
  `files` longtext DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `description` mediumtext DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `deadline` date DEFAULT NULL,
  `client_id` int(11) NOT NULL,
  `created_date` date DEFAULT NULL,
  `created_by` int(11) NOT NULL DEFAULT 0,
  `status` enum('open','completed','hold','canceled') NOT NULL DEFAULT 'open',
  `labels` text DEFAULT NULL,
  `price` double NOT NULL DEFAULT 0,
  `starred_by` mediumtext NOT NULL,
  `estimate_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`id`, `title`, `description`, `start_date`, `deadline`, `client_id`, `created_date`, `created_by`, `status`, `labels`, `price`, `starred_by`, `estimate_id`, `order_id`, `deleted`) VALUES
(1, 'Website Development', 'Website for company', '2022-06-01', '2022-07-30', 502, '2022-08-03', 1, 'completed', '', 6000, '', 0, 0, 0),
(2, 'Skyi - Mobile App', 'Skyi - Mobile App Development', '2022-10-01', '2023-02-28', 506, '2023-01-31', 1, 'completed', '', 150000, '', 0, 0, 0),
(3, 'Curiotory Web and Application Development', 'Application Web and Mobile for curiotory with live classroom for 10 Language learning LMS system ', '2022-11-15', '2023-03-30', 511, '2023-03-15', 1, 'completed', '', 0, '', 0, 0, 0),
(4, 'certificate iso', 'iso 9001 certificate ', '2024-03-19', '2024-03-21', 506, '2024-03-19', 1, 'open', '', 3000, '', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `project_comments`
--

CREATE TABLE `project_comments` (
  `id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `description` mediumtext NOT NULL,
  `project_id` int(11) NOT NULL DEFAULT 0,
  `comment_id` int(11) NOT NULL DEFAULT 0,
  `task_id` int(11) NOT NULL DEFAULT 0,
  `file_id` int(11) NOT NULL DEFAULT 0,
  `customer_feedback_id` int(11) NOT NULL DEFAULT 0,
  `files` longtext DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `project_comments`
--

INSERT INTO `project_comments` (`id`, `created_by`, `created_at`, `description`, `project_id`, `comment_id`, `task_id`, `file_id`, `customer_feedback_id`, `files`, `deleted`) VALUES
(1, 1, '2023-01-31 14:08:42', 'Checked the Database Structure. Looks fine.', 2, 0, 1, 0, 0, 'a:0:{}', 0),
(2, 1, '2024-03-19 06:46:05', 'file for proof', 4, 0, 2, 0, 0, 'a:1:{i:0;a:4:{s:9:\"file_name\";s:48:\"project_comment_file65f934adc30ad-LOGO_BLACK.png\";s:9:\"file_size\";s:5:\"84384\";s:7:\"file_id\";N;s:12:\"service_type\";N;}}', 0);

-- --------------------------------------------------------

--
-- Table structure for table `project_files`
--

CREATE TABLE `project_files` (
  `id` int(11) NOT NULL,
  `file_name` text NOT NULL,
  `file_id` text DEFAULT NULL,
  `service_type` varchar(20) DEFAULT NULL,
  `description` mediumtext DEFAULT NULL,
  `file_size` double NOT NULL,
  `created_at` datetime NOT NULL,
  `project_id` int(11) NOT NULL,
  `uploaded_by` int(11) NOT NULL,
  `category_id` int(11) NOT NULL DEFAULT 0,
  `deleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `project_members`
--

CREATE TABLE `project_members` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `is_leader` tinyint(1) DEFAULT 0,
  `deleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `project_members`
--

INSERT INTO `project_members` (`id`, `user_id`, `project_id`, `is_leader`, `deleted`) VALUES
(1, 1, 1, 1, 0),
(2, 1, 2, 1, 0),
(3, 507, 2, 0, 0),
(4, 506, 2, 0, 0),
(5, 1, 3, 1, 0),
(6, 1, 4, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `project_settings`
--

CREATE TABLE `project_settings` (
  `project_id` int(11) NOT NULL,
  `setting_name` varchar(100) NOT NULL,
  `setting_value` mediumtext NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `project_time`
--

CREATE TABLE `project_time` (
  `id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `start_time` datetime NOT NULL,
  `end_time` datetime DEFAULT NULL,
  `hours` float NOT NULL,
  `status` enum('open','logged','approved') NOT NULL DEFAULT 'logged',
  `note` text DEFAULT NULL,
  `task_id` int(11) NOT NULL DEFAULT 0,
  `deleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `project_time`
--

INSERT INTO `project_time` (`id`, `project_id`, `user_id`, `start_time`, `end_time`, `hours`, `status`, `note`, `task_id`, `deleted`) VALUES
(1, 2, 506, '2023-01-31 13:43:16', '2023-01-31 13:55:44', 0, 'logged', 'Tried to create the Database Structure', 1, 0),
(2, 2, 507, '2023-01-31 13:56:36', '2023-01-31 13:59:07', 0, 'logged', 'Done with the Database Designing. Awaiting for Approval.', 1, 0),
(3, 2, 507, '2023-01-31 14:12:04', '2023-01-31 14:16:33', 0, 'logged', 'Tested Database Structure. QA Passed.', 1, 0),
(4, 2, 506, '2023-01-31 14:22:02', '2023-01-31 14:22:30', 0, 'logged', 'Test Passed on Production.', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `proposals`
--

CREATE TABLE `proposals` (
  `id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `proposal_date` date NOT NULL,
  `valid_until` date NOT NULL,
  `note` mediumtext DEFAULT NULL,
  `last_email_sent_date` date DEFAULT NULL,
  `status` enum('draft','sent','accepted','declined') NOT NULL DEFAULT 'draft',
  `tax_id` int(11) NOT NULL DEFAULT 0,
  `tax_id2` int(11) NOT NULL DEFAULT 0,
  `discount_type` enum('before_tax','after_tax') NOT NULL,
  `discount_amount` double NOT NULL,
  `discount_amount_type` enum('percentage','fixed_amount') NOT NULL,
  `content` text NOT NULL,
  `public_key` varchar(10) NOT NULL,
  `accepted_by` int(11) NOT NULL DEFAULT 0,
  `meta_data` text NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `proposals`
--

INSERT INTO `proposals` (`id`, `client_id`, `proposal_date`, `valid_until`, `note`, `last_email_sent_date`, `status`, `tax_id`, `tax_id2`, `discount_type`, `discount_amount`, `discount_amount_type`, `content`, `public_key`, `accepted_by`, `meta_data`, `deleted`) VALUES
(1, 500, '2022-07-04', '2022-07-10', 'Due on 14', NULL, 'draft', 1, 1, 'before_tax', 0, 'percentage', '<div style=\"display: flex; flex-direction: column;\">\n\n <table style=\"margin-top: 0; margin-bottom: 40px\">\n <tbody>\n <tr>\n <td style=\"padding: 0;\">\n <div style=\"background-size: cover; background-position: center top; background-image: url(\'https://fairsketch.com/media/proposal/bg-2.png\'); padding: 140px 10px 100px;\">\n <div style=\"color:#FFFFFF;font-size:50px;line-height:62px !important;font-weight:600;letter-spacing:0px; text-align: center;\">App Development<br>Proposal</div>\n <div style=\"color: #FFFFFF;\n font-size: 15px;\n text-align: center;\n  padding-top: 30px;\">We develop amazing apps for your business. <br>Here is our best offer for you.</div>\n     </div>\n </td>\n  </tr>\n        </tbody>\n    </table>We are excited to get to work on your new mobile app, and we want to make sure you are satisfied with our proposal and have a full understanding of what to expect in this lengthy process. Creating a mobile app is exciting and our expert team is fully capable of giving you something unique that will help grow your business.<table style=\"margin-top: 0; margin-bottom: 40px\"><tbody>\n        </tbody>\n    </table>\n\n    <table style=\"margin-top: 0; margin-bottom: 40px\">\n        <tbody>\n  <tr>\n <td style=\"padding: 0;\">\n     <div style=\"text-align: center\">\n         <div style=\"font-size: 30px\">{PROPOSAL_ID}</div>\n         <div style=\"height: 3px;\n  width: 80px;\n  background-color: #2AA384;\n  margin: 25px auto 20px;\"></div>\n         <div style=\"margin-bottom: 20px;\"><div style=\"text-align: center;\">Proposal Date:&nbsp;<span style=\"text-align: start;\">{PROPOSAL_DATE}&nbsp;</span></div><div style=\"text-align: center;\"><span style=\"text-align: start;\">Expiry Date:&nbsp;&nbsp;{PROPOSAL_EXPIRY_DATE}</span></div><div style=\"text-align: left;\"><br></div></div>\n     </div>\n     <div style=\"clear: both;\">\n         <div style=\"width: 50%; float: left;  padding-right: 10px\">\n <img src=\"https://fairsketch.com/media/proposal/proposal-to.png\" style=\"width: 100%; margin-bottom: 15px\">\n <p><i>Proposal For</i></p>\n <br>\n {PROPOSAL_TO_INFO}\n         </div>\n\n         <div style=\"width: 50%; float: left; padding-left: 10px\">\n <img src=\"https://fairsketch.com/media/proposal/proposal-from.png\" style=\"width: 100%; margin-bottom: 15px\">\n <p><i>Proposal From</i></p>\n <br>\n {COMPANY_INFO}\n         </div>\n     </div>\n </td>\n  </tr>\n        </tbody>\n    </table>\n\n    <table style=\"margin-top: 0; margin-bottom: 40px\">\n        <tbody>\n  <tr>\n <td style=\"padding: 0;\">\n     <div style=\"margin-top: 20px\">\n         <div style=\"text-align: center\">\n <div style=\"font-size: 30px\">Our Best Offer</div>\n <div style=\"height: 3px; width: 80px; background-color: #2AA384; margin: 25px auto 20px;\"></div>\n <div style=\"margin-bottom: 20px;\">We would like to develop a mobile app for your business. <br>Please feel free to contact us if you have any questions.</div>\n         </div>\n\n         <p>\n {PROPOSAL_ITEMS}\n         </p>\n     </div>\n </td>\n  </tr>\n        </tbody>\n    </table>\n\n    <table style=\"margin-top: 0; margin-bottom: 60px\">\n        <tbody>\n  <tr>\n <td style=\"padding: 0;\">\n\n     <div style=\"\">\n         <div style=\"text-align: center\">\n <div style=\"font-size: 30px\">Our Objective</div>\n <div style=\"height: 3px;\n   width: 80px;\n   background-color: #2AA384;\n   margin: 25px auto 20px;\"></div>\n <div style=\"margin-bottom: 20px;\">We build professional apps for business. Customer satisfaction is our main goal.&nbsp;</div><div style=\"margin-bottom: 20px;\"><br></div>\n         </div>\n\n         <div style=\"clear: both;\">\n <div style=\"width: 50%; float: left;  padding-right: 10px\">\n  <img src=\"https://fairsketch.com/media/proposal/phone-picture.png\" style=\"    display: block; border: 0;\n       width: 100%;\n       max-width: 290px;\">\n </div>\n\n <div style=\"width: 50%; float: left; padding-left: 10px\">\n  <div style=\"font-size:18px;line-height:28px;font-weight:600;letter-spacing:0px;padding:0;padding-bottom:20px; padding-top: 80px;\">\n      We Provide High Quality and Cost Effective Services.\n  </div>\n  <div style=\"font-size:15px;line-height:28px;letter-spacing:0px;padding:0;padding-bottom:30px;\">Our app development process contains different levels of testing. We can confirm you the largest amount of device support guaranty.</div>\n  <div>Work with us.</div>\n  <div style=\"font-size: 15px; color:  #2AA384; padding-top: 5px;\">Be a part of our happy customers.</div><div style=\"font-size: 15px; color:  #2AA384; padding-top: 5px;\"><br></div><div style=\"font-size: 15px; color:  #2AA384; padding-top: 5px;\"><br></div><br></div>\n         </div>\n     </div>\n\n </td>\n  </tr>\n        </tbody>\n    </table>\n\n    <table style=\"margin-top: 0; margin-bottom: 40px\">\n        <tbody>\n  <tr>\n <td style=\"padding: 0;\">\n\n     <div style=\"\">\n         <div style=\"text-align: center\">\n <div style=\"font-size: 30px\">Our Portfolio</div>\n <div style=\"height: 3px;\n   width: 80px;\n   background-color: #2AA384;\n   margin: 25px auto 20px;\"></div>\n <div style=\"margin-bottom: 20px;\">Some of our previous work here</div>\n         </div>\n\n         <div style=\"clear: both; margin: 0 -10px;\">\n <div style=\"width: 33.33%; float: left;  padding: 0 10px; margin-bottom: 20px\">\n  <img src=\"https://fairsketch.com/media/proposal/portfolio-1.png\" style=\"    display: block; border: 0;\n       width: 100%;\n       max-width: 290px;\">\n </div>\n <div style=\"width: 33.33%; float: left; padding: 0 10px; margin-bottom: 20px\">\n  <img src=\"https://fairsketch.com/media/proposal/portfolio-2.png\" style=\"    display: block; border: 0;\n       width: 100%;\n       max-width: 290px;\">\n </div>\n <div style=\"width: 33.33%; float: left;  padding: 0 10px; margin-bottom: 20px\">\n  <img src=\"https://fairsketch.com/media/proposal/portfolio-3.png\" style=\"    display: block; border: 0;\n       width: 100%;\n       max-width: 290px;\">\n </div>\n         </div>\n         <div style=\"clear: both; margin: 0 -10px;\">\n <div style=\"width: 33.33%; float: left;  padding: 0 10px; margin-bottom: 20px\">\n  <img src=\"https://fairsketch.com/media/proposal/portfolio-4.png\" style=\"    display: block; border: 0;\n       width: 100%;\n       max-width: 290px;\">\n </div>\n <div style=\"width: 33.33%; float: left; padding: 0 10px; margin-bottom: 20px\">\n  <img src=\"https://fairsketch.com/media/proposal/portfolio-5.png\" style=\"    display: block; border: 0;\n       width: 100%;\n       max-width: 290px;\">\n </div>\n <div style=\"width: 33.33%; float: left;  padding: 0 10px; margin-bottom: 20px\">\n  <img src=\"https://fairsketch.com/media/proposal/portfolio-6.png\" style=\"    display: block; border: 0;\n       width: 100%;\n       max-width: 290px;\">\n </div>\n         </div>\n\n     </div>\n\n </td>\n  </tr>\n        </tbody>\n    </table>\n\n    <table style=\"margin-top: 0; margin-bottom: 40px\">\n        <tbody>\n  <tr>\n <td style=\"padding: 0;\">\n\n     <div style=\"\">\n         <div style=\"text-align: center\">\n <div style=\"font-size: 30px\">Contact Us</div>\n <div style=\"height: 3px;\n   width: 80px;\n   background-color: #2AA384;\n   margin: 25px auto 20px;\"></div>\n <div style=\"margin-bottom: 20px;\"></div>\n         </div>\n\n         <p style=\"text-align: center; margin-bottom: 20px;\">We are looking forward working with you. Please feel free to contact us.&nbsp;</p><p style=\"text-align: center; margin-bottom: 20px;\"><br></p>\n         <div style=\"text-align: center;\"><img src=\"https://fairsketch.com/media/proposal/bg-1.png\" style=\"width: 100%;\"></div></div>\n\n </td>\n  </tr>\n        </tbody>\n    </table>\n\n    {PROPOSAL_NOTE}\n\n</div>', 'wtsidztyLm', 0, '', 1),
(2, 500, '2022-07-14', '2022-07-28', 'NA', '2022-07-10', 'sent', 0, 0, 'before_tax', 0, 'percentage', '<div style=\"display: flex; flex-direction: column;\">\n\n <table style=\"margin-top: 0; margin-bottom: 40px\">\n <tbody>\n <tr>\n <td style=\"padding: 0;\">\n <div style=\"background-size: cover; background-position: center top; background-image: url(\'https://fairsketch.com/media/proposal/bg-2.png\'); padding: 140px 10px 100px;\">\n <div style=\"color:#FFFFFF;font-size:50px;line-height:62px !important;font-weight:600;letter-spacing:0px; text-align: center;\">App Development<br>Proposal</div>\n <div style=\"color: #FFFFFF;\n font-size: 15px;\n text-align: center;\n  padding-top: 30px;\">We develop amazing apps for your business. <br>Here is our best offer for you.</div>\n     </div>\n </td>\n  </tr>\n        </tbody>\n    </table>We are excited to get to work on your new mobile app, and we want to make sure you are satisfied with our proposal and have a full understanding of what to expect in this lengthy process. Creating a mobile app is exciting and our expert team is fully capable of giving you something unique that will help grow your business.<table style=\"margin-top: 0; margin-bottom: 40px\"><tbody>\n        </tbody>\n    </table>\n\n    <table style=\"margin-top: 0; margin-bottom: 40px\">\n        <tbody>\n  <tr>\n <td style=\"padding: 0;\">\n     <div style=\"text-align: center\">\n         <div style=\"font-size: 30px\">{PROPOSAL_ID}</div>\n         <div style=\"height: 3px;\n  width: 80px;\n  background-color: #2AA384;\n  margin: 25px auto 20px;\"></div>\n         <div style=\"margin-bottom: 20px;\"><div style=\"text-align: center;\">Proposal Date:&nbsp;<span style=\"text-align: start;\">{PROPOSAL_DATE}&nbsp;</span></div><div style=\"text-align: center;\"><span style=\"text-align: start;\">Expiry Date:&nbsp;&nbsp;{PROPOSAL_EXPIRY_DATE}</span></div><div style=\"text-align: left;\"><br></div></div>\n     </div>\n     <div style=\"clear: both;\">\n         <div style=\"width: 50%; float: left;  padding-right: 10px\">\n <img src=\"https://fairsketch.com/media/proposal/proposal-to.png\" style=\"width: 100%; margin-bottom: 15px\">\n <p><i>Proposal For</i></p>\n <br>\n {PROPOSAL_TO_INFO}\n         </div>\n\n         <div style=\"width: 50%; float: left; padding-left: 10px\">\n <img src=\"https://fairsketch.com/media/proposal/proposal-from.png\" style=\"width: 100%; margin-bottom: 15px\">\n <p><i>Proposal From</i></p>\n <br>\n {COMPANY_INFO}\n         </div>\n     </div>\n </td>\n  </tr>\n        </tbody>\n    </table>\n\n    <table style=\"margin-top: 0; margin-bottom: 40px\">\n        <tbody>\n  <tr>\n <td style=\"padding: 0;\">\n     <div style=\"margin-top: 20px\">\n         <div style=\"text-align: center\">\n <div style=\"font-size: 30px\">Our Best Offer</div>\n <div style=\"height: 3px; width: 80px; background-color: #2AA384; margin: 25px auto 20px;\"></div>\n <div style=\"margin-bottom: 20px;\">We would like to develop a mobile app for your business. <br>Please feel free to contact us if you have any questions.</div>\n         </div>\n\n         <p>\n {PROPOSAL_ITEMS}\n         </p>\n     </div>\n </td>\n  </tr>\n        </tbody>\n    </table>\n\n    <table style=\"margin-top: 0; margin-bottom: 60px\">\n        <tbody>\n  <tr>\n <td style=\"padding: 0;\">\n\n     <div style=\"\">\n         <div style=\"text-align: center\">\n <div style=\"font-size: 30px\">Our Objective</div>\n <div style=\"height: 3px;\n   width: 80px;\n   background-color: #2AA384;\n   margin: 25px auto 20px;\"></div>\n <div style=\"margin-bottom: 20px;\">We build professional apps for business. Customer satisfaction is our main goal.&nbsp;</div><div style=\"margin-bottom: 20px;\"><br></div>\n         </div>\n\n         <div style=\"clear: both;\">\n <div style=\"width: 50%; float: left;  padding-right: 10px\">\n  <img src=\"https://fairsketch.com/media/proposal/phone-picture.png\" style=\"    display: block; border: 0;\n       width: 100%;\n       max-width: 290px;\">\n </div>\n\n <div style=\"width: 50%; float: left; padding-left: 10px\">\n  <div style=\"font-size:18px;line-height:28px;font-weight:600;letter-spacing:0px;padding:0;padding-bottom:20px; padding-top: 80px;\">\n      We Provide High Quality and Cost Effective Services.\n  </div>\n  <div style=\"font-size:15px;line-height:28px;letter-spacing:0px;padding:0;padding-bottom:30px;\">Our app development process contains different levels of testing. We can confirm you the largest amount of device support guaranty.</div>\n  <div>Work with us.</div>\n  <div style=\"font-size: 15px; color:  #2AA384; padding-top: 5px;\">Be a part of our happy customers.</div><div style=\"font-size: 15px; color:  #2AA384; padding-top: 5px;\"><br></div><div style=\"font-size: 15px; color:  #2AA384; padding-top: 5px;\"><br></div><br></div>\n         </div>\n     </div>\n\n </td>\n  </tr>\n        </tbody>\n    </table>\n\n    <table style=\"margin-top: 0; margin-bottom: 40px\">\n        <tbody>\n  <tr>\n <td style=\"padding: 0;\">\n\n     <div style=\"\">\n         <div style=\"text-align: center\">\n <div style=\"font-size: 30px\">Our Portfolio</div>\n <div style=\"height: 3px;\n   width: 80px;\n   background-color: #2AA384;\n   margin: 25px auto 20px;\"></div>\n <div style=\"margin-bottom: 20px;\">Some of our previous work here</div>\n         </div>\n\n         <div style=\"clear: both; margin: 0 -10px;\">\n <div style=\"width: 33.33%; float: left;  padding: 0 10px; margin-bottom: 20px\">\n  <img src=\"https://fairsketch.com/media/proposal/portfolio-1.png\" style=\"    display: block; border: 0;\n       width: 100%;\n       max-width: 290px;\">\n </div>\n <div style=\"width: 33.33%; float: left; padding: 0 10px; margin-bottom: 20px\">\n  <img src=\"https://fairsketch.com/media/proposal/portfolio-2.png\" style=\"    display: block; border: 0;\n       width: 100%;\n       max-width: 290px;\">\n </div>\n <div style=\"width: 33.33%; float: left;  padding: 0 10px; margin-bottom: 20px\">\n  <img src=\"https://fairsketch.com/media/proposal/portfolio-3.png\" style=\"    display: block; border: 0;\n       width: 100%;\n       max-width: 290px;\">\n </div>\n         </div>\n         <div style=\"clear: both; margin: 0 -10px;\">\n <div style=\"width: 33.33%; float: left;  padding: 0 10px; margin-bottom: 20px\">\n  <img src=\"https://fairsketch.com/media/proposal/portfolio-4.png\" style=\"    display: block; border: 0;\n       width: 100%;\n       max-width: 290px;\">\n </div>\n <div style=\"width: 33.33%; float: left; padding: 0 10px; margin-bottom: 20px\">\n  <img src=\"https://fairsketch.com/media/proposal/portfolio-5.png\" style=\"    display: block; border: 0;\n       width: 100%;\n       max-width: 290px;\">\n </div>\n <div style=\"width: 33.33%; float: left;  padding: 0 10px; margin-bottom: 20px\">\n  <img src=\"https://fairsketch.com/media/proposal/portfolio-6.png\" style=\"    display: block; border: 0;\n       width: 100%;\n       max-width: 290px;\">\n </div>\n         </div>\n\n     </div>\n\n </td>\n  </tr>\n        </tbody>\n    </table>\n\n    <table style=\"margin-top: 0; margin-bottom: 40px\">\n        <tbody>\n  <tr>\n <td style=\"padding: 0;\">\n\n     <div style=\"\">\n         <div style=\"text-align: center\">\n <div style=\"font-size: 30px\">Contact Us</div>\n <div style=\"height: 3px;\n   width: 80px;\n   background-color: #2AA384;\n   margin: 25px auto 20px;\"></div>\n <div style=\"margin-bottom: 20px;\"></div>\n         </div>\n\n         <p style=\"text-align: center; margin-bottom: 20px;\">We are looking forward working with you. Please feel free to contact us.&nbsp;</p><p style=\"text-align: center; margin-bottom: 20px;\"><br></p>\n         <div style=\"text-align: center;\"><img src=\"https://fairsketch.com/media/proposal/bg-1.png\" style=\"width: 100%;\"></div></div>\n\n </td>\n  </tr>\n        </tbody>\n    </table>\n\n    {PROPOSAL_NOTE}\n\n</div>', 'DWiOGSpaei', 0, '', 0),
(3, 517, '2023-10-19', '2023-10-21', 'Monthly Traffic-\r\n56760 to 164070 ', NULL, 'draft', 2, 0, 'before_tax', 0, 'percentage', '<div style=\"display: flex; flex-direction: column;\">\n\n <table style=\"margin-top: 0; margin-bottom: 40px\">\n <tbody>\n <tr>\n <td style=\"padding: 0;\">\n <div style=\"background-size: cover; background-position: center top; background-image: url(\'https://fairsketch.com/media/proposal/bg-2.png\'); padding: 140px 10px 100px;\">\n <div style=\"color:#FFFFFF;font-size:50px;line-height:62px !important;font-weight:600;letter-spacing:0px; text-align: center;\">App Development<br>Proposal</div>\n <div style=\"color: #FFFFFF;\n font-size: 15px;\n text-align: center;\n  padding-top: 30px;\">We develop amazing apps for your business. <br>Here is our best offer for you.</div>\n     </div>\n </td>\n  </tr>\n        </tbody>\n    </table>We are excited to get to work on your new mobile app, and we want to make sure you are satisfied with our proposal and have a full understanding of what to expect in this lengthy process. Creating a mobile app is exciting and our expert team is fully capable of giving you something unique that will help grow your business.<table style=\"margin-top: 0; margin-bottom: 40px\"><tbody>\n        </tbody>\n    </table>\n\n    <table style=\"margin-top: 0; margin-bottom: 40px\">\n        <tbody>\n  <tr>\n <td style=\"padding: 0;\">\n     <div style=\"text-align: center\">\n         <div style=\"font-size: 30px\">{PROPOSAL_ID}</div>\n         <div style=\"height: 3px;\n  width: 80px;\n  background-color: #2AA384;\n  margin: 25px auto 20px;\"></div>\n         <div style=\"margin-bottom: 20px;\"><div style=\"text-align: center;\">Proposal Date:&nbsp;<span style=\"text-align: start;\">{PROPOSAL_DATE}&nbsp;</span></div><div style=\"text-align: center;\"><span style=\"text-align: start;\">Expiry Date:&nbsp;&nbsp;{PROPOSAL_EXPIRY_DATE}</span></div><div style=\"text-align: left;\"><br></div></div>\n     </div>\n     <div style=\"clear: both;\">\n         <div style=\"width: 50%; float: left;  padding-right: 10px\">\n <img src=\"https://fairsketch.com/media/proposal/proposal-to.png\" style=\"width: 100%; margin-bottom: 15px\">\n <p><i>Proposal For</i></p>\n <br>\n {PROPOSAL_TO_INFO}\n         </div>\n\n         <div style=\"width: 50%; float: left; padding-left: 10px\">\n <img src=\"https://fairsketch.com/media/proposal/proposal-from.png\" style=\"width: 100%; margin-bottom: 15px\">\n <p><i>Proposal From</i></p>\n <br>\n {COMPANY_INFO}\n         </div>\n     </div>\n </td>\n  </tr>\n        </tbody>\n    </table>\n\n    <table style=\"margin-top: 0; margin-bottom: 40px\">\n        <tbody>\n  <tr>\n <td style=\"padding: 0;\">\n     <div style=\"margin-top: 20px\">\n         <div style=\"text-align: center\">\n <div style=\"font-size: 30px\">Our Best Offer</div>\n <div style=\"height: 3px; width: 80px; background-color: #2AA384; margin: 25px auto 20px;\"></div>\n <div style=\"margin-bottom: 20px;\">We would like to develop a mobile app for your business. <br>Please feel free to contact us if you have any questions.</div>\n         </div>\n\n         <p>\n {PROPOSAL_ITEMS}\n         </p>\n     </div>\n </td>\n  </tr>\n        </tbody>\n    </table>\n\n    <table style=\"margin-top: 0; margin-bottom: 60px\">\n        <tbody>\n  <tr>\n <td style=\"padding: 0;\">\n\n     <div style=\"\">\n         <div style=\"text-align: center\">\n <div style=\"font-size: 30px\">Our Objective</div>\n <div style=\"height: 3px;\n   width: 80px;\n   background-color: #2AA384;\n   margin: 25px auto 20px;\"></div>\n <div style=\"margin-bottom: 20px;\">We build professional apps for business. Customer satisfaction is our main goal.&nbsp;</div><div style=\"margin-bottom: 20px;\"><br></div>\n         </div>\n\n         <div style=\"clear: both;\">\n <div style=\"width: 50%; float: left;  padding-right: 10px\">\n  <img src=\"https://fairsketch.com/media/proposal/phone-picture.png\" style=\"    display: block; border: 0;\n       width: 100%;\n       max-width: 290px;\">\n </div>\n\n <div style=\"width: 50%; float: left; padding-left: 10px\">\n  <div style=\"font-size:18px;line-height:28px;font-weight:600;letter-spacing:0px;padding:0;padding-bottom:20px; padding-top: 80px;\">\n      We Provide High Quality and Cost Effective Services.\n  </div>\n  <div style=\"font-size:15px;line-height:28px;letter-spacing:0px;padding:0;padding-bottom:30px;\">Our app development process contains different levels of testing. We can confirm you the largest amount of device support guaranty.</div>\n  <div>Work with us.</div>\n  <div style=\"font-size: 15px; color:  #2AA384; padding-top: 5px;\">Be a part of our happy customers.</div><div style=\"font-size: 15px; color:  #2AA384; padding-top: 5px;\"><br></div><div style=\"font-size: 15px; color:  #2AA384; padding-top: 5px;\"><br></div><br></div>\n         </div>\n     </div>\n\n </td>\n  </tr>\n        </tbody>\n    </table>\n\n    <table style=\"margin-top: 0; margin-bottom: 40px\">\n        <tbody>\n  <tr>\n <td style=\"padding: 0;\">\n\n     <div style=\"\">\n         <div style=\"text-align: center\">\n <div style=\"font-size: 30px\">Our Portfolio</div>\n <div style=\"height: 3px;\n   width: 80px;\n   background-color: #2AA384;\n   margin: 25px auto 20px;\"></div>\n <div style=\"margin-bottom: 20px;\">Some of our previous work here</div>\n         </div>\n\n         <div style=\"clear: both; margin: 0 -10px;\">\n <div style=\"width: 33.33%; float: left;  padding: 0 10px; margin-bottom: 20px\">\n  <img src=\"https://fairsketch.com/media/proposal/portfolio-1.png\" style=\"    display: block; border: 0;\n       width: 100%;\n       max-width: 290px;\">\n </div>\n <div style=\"width: 33.33%; float: left; padding: 0 10px; margin-bottom: 20px\">\n  <img src=\"https://fairsketch.com/media/proposal/portfolio-2.png\" style=\"    display: block; border: 0;\n       width: 100%;\n       max-width: 290px;\">\n </div>\n <div style=\"width: 33.33%; float: left;  padding: 0 10px; margin-bottom: 20px\">\n  <img src=\"https://fairsketch.com/media/proposal/portfolio-3.png\" style=\"    display: block; border: 0;\n       width: 100%;\n       max-width: 290px;\">\n </div>\n         </div>\n         <div style=\"clear: both; margin: 0 -10px;\">\n <div style=\"width: 33.33%; float: left;  padding: 0 10px; margin-bottom: 20px\">\n  <img src=\"https://fairsketch.com/media/proposal/portfolio-4.png\" style=\"    display: block; border: 0;\n       width: 100%;\n       max-width: 290px;\">\n </div>\n <div style=\"width: 33.33%; float: left; padding: 0 10px; margin-bottom: 20px\">\n  <img src=\"https://fairsketch.com/media/proposal/portfolio-5.png\" style=\"    display: block; border: 0;\n       width: 100%;\n       max-width: 290px;\">\n </div>\n <div style=\"width: 33.33%; float: left;  padding: 0 10px; margin-bottom: 20px\">\n  <img src=\"https://fairsketch.com/media/proposal/portfolio-6.png\" style=\"    display: block; border: 0;\n       width: 100%;\n       max-width: 290px;\">\n </div>\n         </div>\n\n     </div>\n\n </td>\n  </tr>\n        </tbody>\n    </table>\n\n    <table style=\"margin-top: 0; margin-bottom: 40px\">\n        <tbody>\n  <tr>\n <td style=\"padding: 0;\">\n\n     <div style=\"\">\n         <div style=\"text-align: center\">\n <div style=\"font-size: 30px\">Contact Us</div>\n <div style=\"height: 3px;\n   width: 80px;\n   background-color: #2AA384;\n   margin: 25px auto 20px;\"></div>\n <div style=\"margin-bottom: 20px;\"></div>\n         </div>\n\n         <p style=\"text-align: center; margin-bottom: 20px;\">We are looking forward working with you. Please feel free to contact us.&nbsp;</p><p style=\"text-align: center; margin-bottom: 20px;\"><br></p>\n         <div style=\"text-align: center;\"><img src=\"https://fairsketch.com/media/proposal/bg-1.png\" style=\"width: 100%;\"></div></div>\n\n </td>\n  </tr>\n        </tbody>\n    </table>\n\n    {PROPOSAL_NOTE}\n\n</div>', 'fPYbRqTbdM', 0, '', 0),
(4, 511, '2024-03-19', '2024-03-22', '', NULL, 'draft', 2, 0, 'before_tax', 0, 'percentage', '<div style=\"display: flex; flex-direction: column;\">\n\n <table style=\"margin-top: 0; margin-bottom: 40px\">\n <tbody>\n <tr>\n <td style=\"padding: 0;\">\n <div style=\"background-size: cover; background-position: center top; background-image: url(\'https://fairsketch.com/media/proposal/bg-2.png\'); padding: 140px 10px 100px;\">\n <div style=\"color:#FFFFFF;font-size:50px;line-height:62px !important;font-weight:600;letter-spacing:0px; text-align: center;\">Certification Proposal<br></div>\n <div style=\"color: #FFFFFF;\n font-size: 15px;\n text-align: center;\n  padding-top: 30px;\">We develop amazing apps for your business. <br>Here is our best offer for you.</div>\n     </div>\n </td>\n  </tr>\n        </tbody>\n    </table>We are excited to get to work on your new mobile app, and we want to make sure you are satisfied with our proposal and have a full understanding of what to expect in this lengthy process. Creating a mobile app is exciting and our expert team is fully capable of giving you something unique that will help grow your business.<table style=\"margin-top: 0; margin-bottom: 40px\"><tbody>\n        </tbody>\n    </table>\n\n    <table style=\"margin-top: 0; margin-bottom: 40px\">\n        <tbody>\n  <tr>\n <td style=\"padding: 0;\">\n     <div style=\"text-align: center\">\n         <div style=\"font-size: 30px\">{PROPOSAL_ID}</div>\n         <div style=\"height: 3px;\n  width: 80px;\n  background-color: #2AA384;\n  margin: 25px auto 20px;\"></div>\n         <div style=\"margin-bottom: 20px;\"><div style=\"text-align: center;\">Proposal Date:&nbsp;<span style=\"text-align: start;\">{PROPOSAL_DATE}&nbsp;</span></div><div style=\"text-align: center;\"><span style=\"text-align: start;\">Expiry Date:&nbsp;&nbsp;{PROPOSAL_EXPIRY_DATE}</span></div><div style=\"text-align: left;\"><br></div></div>\n     </div>\n     <div style=\"clear: both;\">\n         <div style=\"width: 50%; float: left;  padding-right: 10px\">\n <img src=\"https://fairsketch.com/media/proposal/proposal-to.png\" style=\"width: 100%; margin-bottom: 15px\">\n <p><i>Proposal For</i></p>\n <br>\n {PROPOSAL_TO_INFO}\n         </div>\n\n         <div style=\"width: 50%; float: left; padding-left: 10px\">\n <img src=\"https://fairsketch.com/media/proposal/proposal-from.png\" style=\"width: 100%; margin-bottom: 15px\">\n <p><i>Proposal From</i></p>\n <br>\n {COMPANY_INFO}\n         </div>\n     </div>\n </td>\n  </tr>\n        </tbody>\n    </table>\n\n    <table style=\"margin-top: 0; margin-bottom: 40px\">\n        <tbody>\n  <tr>\n <td style=\"padding: 0;\">\n     <div style=\"margin-top: 20px\">\n         <div style=\"text-align: center\">\n <div style=\"font-size: 30px\">Our Best Offer</div>\n <div style=\"height: 3px; width: 80px; background-color: #2AA384; margin: 25px auto 20px;\"></div>\n <div style=\"margin-bottom: 20px;\">We would like to develop a mobile app for your business. <br>Please feel free to contact us if you have any questions.</div>\n         </div>\n\n         <p>\n {PROPOSAL_ITEMS}\n         </p>\n     </div>\n </td>\n  </tr>\n        </tbody>\n    </table>\n\n    <table style=\"margin-top: 0; margin-bottom: 60px\">\n        <tbody>\n  <tr>\n <td style=\"padding: 0;\">\n\n     <div style=\"\">\n         <div style=\"text-align: center\">\n <div style=\"font-size: 30px\">Our Objective</div>\n <div style=\"height: 3px;\n   width: 80px;\n   background-color: #2AA384;\n   margin: 25px auto 20px;\"></div>\n <div style=\"margin-bottom: 20px;\">We build professional apps for business. Customer satisfaction is our main goal.&nbsp;</div><div style=\"margin-bottom: 20px;\"><br></div>\n         </div>\n\n         <div style=\"clear: both;\">\n <div style=\"width: 50%; float: left;  padding-right: 10px\">\n  <img src=\"https://fairsketch.com/media/proposal/phone-picture.png\" style=\"    display: block; border: 0;\n       width: 100%;\n       max-width: 290px;\">\n </div>\n\n <div style=\"width: 50%; float: left; padding-left: 10px\">\n  <div style=\"font-size:18px;line-height:28px;font-weight:600;letter-spacing:0px;padding:0;padding-bottom:20px; padding-top: 80px;\">\n      We Provide High Quality and Cost Effective Services.\n  </div>\n  <div style=\"font-size:15px;line-height:28px;letter-spacing:0px;padding:0;padding-bottom:30px;\">Our app development process contains different levels of testing. We can confirm you the largest amount of device support guaranty.</div>\n  <div>Work with us.</div>\n  <div style=\"font-size: 15px; color:  #2AA384; padding-top: 5px;\">Be a part of our happy customers.</div><div style=\"font-size: 15px; color:  #2AA384; padding-top: 5px;\"><br></div><div style=\"font-size: 15px; color:  #2AA384; padding-top: 5px;\"><br></div><br></div>\n         </div>\n     </div>\n\n </td>\n  </tr>\n        </tbody>\n    </table>\n\n    <table style=\"margin-top: 0; margin-bottom: 40px\">\n        <tbody>\n  <tr>\n <td style=\"padding: 0;\">\n\n     <div style=\"\">\n         <div style=\"text-align: center\">\n <div style=\"font-size: 30px\">Our Portfolio</div>\n <div style=\"height: 3px;\n   width: 80px;\n   background-color: #2AA384;\n   margin: 25px auto 20px;\"></div>\n <div style=\"margin-bottom: 20px;\">Some of our previous work here</div>\n         </div>\n\n         <div style=\"clear: both; margin: 0 -10px;\">\n <div style=\"width: 33.33%; float: left;  padding: 0 10px; margin-bottom: 20px\">\n  <img src=\"https://fairsketch.com/media/proposal/portfolio-1.png\" style=\"    display: block; border: 0;\n       width: 100%;\n       max-width: 290px;\">\n </div>\n <div style=\"width: 33.33%; float: left; padding: 0 10px; margin-bottom: 20px\">\n  <img src=\"https://fairsketch.com/media/proposal/portfolio-2.png\" style=\"    display: block; border: 0;\n       width: 100%;\n       max-width: 290px;\">\n </div>\n <div style=\"width: 33.33%; float: left;  padding: 0 10px; margin-bottom: 20px\">\n  <img src=\"https://fairsketch.com/media/proposal/portfolio-3.png\" style=\"    display: block; border: 0;\n       width: 100%;\n       max-width: 290px;\">\n </div>\n         </div>\n         <div style=\"clear: both; margin: 0 -10px;\">\n <div style=\"width: 33.33%; float: left;  padding: 0 10px; margin-bottom: 20px\">\n  <img src=\"https://fairsketch.com/media/proposal/portfolio-4.png\" style=\"    display: block; border: 0;\n       width: 100%;\n       max-width: 290px;\">\n </div>\n <div style=\"width: 33.33%; float: left; padding: 0 10px; margin-bottom: 20px\">\n  <img src=\"https://fairsketch.com/media/proposal/portfolio-5.png\" style=\"    display: block; border: 0;\n       width: 100%;\n       max-width: 290px;\">\n </div>\n <div style=\"width: 33.33%; float: left;  padding: 0 10px; margin-bottom: 20px\">\n  <img src=\"https://fairsketch.com/media/proposal/portfolio-6.png\" style=\"    display: block; border: 0;\n       width: 100%;\n       max-width: 290px;\">\n </div>\n         </div>\n\n     </div>\n\n </td>\n  </tr>\n        </tbody>\n    </table>\n\n    <table style=\"margin-top: 0; margin-bottom: 40px\">\n        <tbody>\n  <tr>\n <td style=\"padding: 0;\">\n\n     <div style=\"\">\n         <div style=\"text-align: center\">\n <div style=\"font-size: 30px\">Contact Us</div>\n <div style=\"height: 3px;\n   width: 80px;\n   background-color: #2AA384;\n   margin: 25px auto 20px;\"></div>\n <div style=\"margin-bottom: 20px;\"></div>\n         </div>\n\n         <p style=\"text-align: center; margin-bottom: 20px;\">We are looking forward working with you. Please feel free to contact us.&nbsp;</p><p style=\"text-align: center; margin-bottom: 20px;\"><br></p>\n         <div style=\"text-align: center;\"><img src=\"https://fairsketch.com/media/proposal/bg-1.png\" style=\"width: 100%;\"></div></div>\n\n </td>\n  </tr>\n        </tbody>\n    </table>\n\n    {PROPOSAL_NOTE}\n\n</div>', 'AnWNfIHvVI', 0, '', 0),
(5, 514, '2024-04-15', '2024-04-30', 'Test', NULL, 'draft', 2, 6, 'before_tax', 0, 'percentage', '<div style=\"display: flex; flex-direction: column;\">\n\n <table style=\"margin-top: 0; margin-bottom: 40px\">\n <tbody>\n <tr>\n <td style=\"padding: 0;\">\n <div style=\"background-size: cover; background-position: center top; background-image: url(\'https://fairsketch.com/media/proposal/bg-2.png\'); padding: 140px 10px 100px;\">\n <div style=\"color:#FFFFFF;font-size:50px;line-height:62px !important;font-weight:600;letter-spacing:0px; text-align: center;\">App Development<br>Proposal</div>\n <div style=\"color: #FFFFFF;\n font-size: 15px;\n text-align: center;\n  padding-top: 30px;\">We develop amazing apps for your business. <br>Here is our best offer for you.</div>\n     </div>\n </td>\n  </tr>\n        </tbody>\n    </table>We are excited to get to work on your new mobile app, and we want to make sure you are satisfied with our proposal and have a full understanding of what to expect in this lengthy process. Creating a mobile app is exciting and our expert team is fully capable of giving you something unique that will help grow your business.<table style=\"margin-top: 0; margin-bottom: 40px\"><tbody>\n        </tbody>\n    </table>\n\n    <table style=\"margin-top: 0; margin-bottom: 40px\">\n        <tbody>\n  <tr>\n <td style=\"padding: 0;\">\n     <div style=\"text-align: center\">\n         <div style=\"font-size: 30px\">{PROPOSAL_ID}</div>\n         <div style=\"height: 3px;\n  width: 80px;\n  background-color: #2AA384;\n  margin: 25px auto 20px;\"></div>\n         <div style=\"margin-bottom: 20px;\"><div style=\"text-align: center;\">Proposal Date:&nbsp;<span style=\"text-align: start;\">{PROPOSAL_DATE}&nbsp;</span></div><div style=\"text-align: center;\"><span style=\"text-align: start;\">Expiry Date:&nbsp;&nbsp;{PROPOSAL_EXPIRY_DATE}</span></div><div style=\"text-align: left;\"><br></div></div>\n     </div>\n     <div style=\"clear: both;\">\n         <div style=\"width: 50%; float: left;  padding-right: 10px\">\n <img src=\"https://fairsketch.com/media/proposal/proposal-to.png\" style=\"width: 100%; margin-bottom: 15px\">\n <p><i>Proposal For</i></p>\n <br>\n {PROPOSAL_TO_INFO}\n         </div>\n\n         <div style=\"width: 50%; float: left; padding-left: 10px\">\n <img src=\"https://fairsketch.com/media/proposal/proposal-from.png\" style=\"width: 100%; margin-bottom: 15px\">\n <p><i>Proposal From</i></p>\n <br>\n {COMPANY_INFO}\n         </div>\n     </div>\n </td>\n  </tr>\n        </tbody>\n    </table>\n\n    <table style=\"margin-top: 0; margin-bottom: 40px\">\n        <tbody>\n  <tr>\n <td style=\"padding: 0;\">\n     <div style=\"margin-top: 20px\">\n         <div style=\"text-align: center\">\n <div style=\"font-size: 30px\">Our Best Offer</div>\n <div style=\"height: 3px; width: 80px; background-color: #2AA384; margin: 25px auto 20px;\"></div>\n <div style=\"margin-bottom: 20px;\">We would like to develop a mobile app for your business. <br>Please feel free to contact us if you have any questions.</div>\n         </div>\n\n         <p>\n {PROPOSAL_ITEMS}\n         </p>\n     </div>\n </td>\n  </tr>\n        </tbody>\n    </table>\n\n    <table style=\"margin-top: 0; margin-bottom: 60px\">\n        <tbody>\n  <tr>\n <td style=\"padding: 0;\">\n\n     <div style=\"\">\n         <div style=\"text-align: center\">\n <div style=\"font-size: 30px\">Our Objective</div>\n <div style=\"height: 3px;\n   width: 80px;\n   background-color: #2AA384;\n   margin: 25px auto 20px;\"></div>\n <div style=\"margin-bottom: 20px;\">We build professional apps for business. Customer satisfaction is our main goal.&nbsp;</div><div style=\"margin-bottom: 20px;\"><br></div>\n         </div>\n\n         <div style=\"clear: both;\">\n <div style=\"width: 50%; float: left;  padding-right: 10px\">\n  <img src=\"https://fairsketch.com/media/proposal/phone-picture.png\" style=\"    display: block; border: 0;\n       width: 100%;\n       max-width: 290px;\">\n </div>\n\n <div style=\"width: 50%; float: left; padding-left: 10px\">\n  <div style=\"font-size:18px;line-height:28px;font-weight:600;letter-spacing:0px;padding:0;padding-bottom:20px; padding-top: 80px;\">\n      We Provide High Quality and Cost Effective Services.\n  </div>\n  <div style=\"font-size:15px;line-height:28px;letter-spacing:0px;padding:0;padding-bottom:30px;\">Our app development process contains different levels of testing. We can confirm you the largest amount of device support guaranty.</div>\n  <div>Work with us.</div>\n  <div style=\"font-size: 15px; color:  #2AA384; padding-top: 5px;\">Be a part of our happy customers.</div><div style=\"font-size: 15px; color:  #2AA384; padding-top: 5px;\"><br></div><div style=\"font-size: 15px; color:  #2AA384; padding-top: 5px;\"><br></div><br></div>\n         </div>\n     </div>\n\n </td>\n  </tr>\n        </tbody>\n    </table>\n\n    <table style=\"margin-top: 0; margin-bottom: 40px\">\n        <tbody>\n  <tr>\n <td style=\"padding: 0;\">\n\n     <div style=\"\">\n         <div style=\"text-align: center\">\n <div style=\"font-size: 30px\">Our Portfolio</div>\n <div style=\"height: 3px;\n   width: 80px;\n   background-color: #2AA384;\n   margin: 25px auto 20px;\"></div>\n <div style=\"margin-bottom: 20px;\">Some of our previous work here</div>\n         </div>\n\n         <div style=\"clear: both; margin: 0 -10px;\">\n <div style=\"width: 33.33%; float: left;  padding: 0 10px; margin-bottom: 20px\">\n  <img src=\"https://fairsketch.com/media/proposal/portfolio-1.png\" style=\"    display: block; border: 0;\n       width: 100%;\n       max-width: 290px;\">\n </div>\n <div style=\"width: 33.33%; float: left; padding: 0 10px; margin-bottom: 20px\">\n  <img src=\"https://fairsketch.com/media/proposal/portfolio-2.png\" style=\"    display: block; border: 0;\n       width: 100%;\n       max-width: 290px;\">\n </div>\n <div style=\"width: 33.33%; float: left;  padding: 0 10px; margin-bottom: 20px\">\n  <img src=\"https://fairsketch.com/media/proposal/portfolio-3.png\" style=\"    display: block; border: 0;\n       width: 100%;\n       max-width: 290px;\">\n </div>\n         </div>\n         <div style=\"clear: both; margin: 0 -10px;\">\n <div style=\"width: 33.33%; float: left;  padding: 0 10px; margin-bottom: 20px\">\n  <img src=\"https://fairsketch.com/media/proposal/portfolio-4.png\" style=\"    display: block; border: 0;\n       width: 100%;\n       max-width: 290px;\">\n </div>\n <div style=\"width: 33.33%; float: left; padding: 0 10px; margin-bottom: 20px\">\n  <img src=\"https://fairsketch.com/media/proposal/portfolio-5.png\" style=\"    display: block; border: 0;\n       width: 100%;\n       max-width: 290px;\">\n </div>\n <div style=\"width: 33.33%; float: left;  padding: 0 10px; margin-bottom: 20px\">\n  <img src=\"https://fairsketch.com/media/proposal/portfolio-6.png\" style=\"    display: block; border: 0;\n       width: 100%;\n       max-width: 290px;\">\n </div>\n         </div>\n\n     </div>\n\n </td>\n  </tr>\n        </tbody>\n    </table>\n\n    <table style=\"margin-top: 0; margin-bottom: 40px\">\n        <tbody>\n  <tr>\n <td style=\"padding: 0;\">\n\n     <div style=\"\">\n         <div style=\"text-align: center\">\n <div style=\"font-size: 30px\">Contact Us</div>\n <div style=\"height: 3px;\n   width: 80px;\n   background-color: #2AA384;\n   margin: 25px auto 20px;\"></div>\n <div style=\"margin-bottom: 20px;\"></div>\n         </div>\n\n         <p style=\"text-align: center; margin-bottom: 20px;\">We are looking forward working with you. Please feel free to contact us.&nbsp;</p><p style=\"text-align: center; margin-bottom: 20px;\"><br></p>\n         <div style=\"text-align: center;\"><img src=\"https://fairsketch.com/media/proposal/bg-1.png\" style=\"width: 100%;\"></div></div>\n\n </td>\n  </tr>\n        </tbody>\n    </table>\n\n    {PROPOSAL_NOTE}\n\n</div>', 'PKeVcvWvxl', 0, '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `proposal_items`
--

CREATE TABLE `proposal_items` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `description` text DEFAULT NULL,
  `quantity` double NOT NULL,
  `unit_type` varchar(20) NOT NULL DEFAULT '',
  `rate` double NOT NULL,
  `total` double NOT NULL,
  `sort` int(11) NOT NULL DEFAULT 0,
  `proposal_id` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  `unit_sale` double NOT NULL,
  `unit_sale_total` double NOT NULL,
  `margin` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `proposal_items`
--

INSERT INTO `proposal_items` (`id`, `title`, `description`, `quantity`, `unit_type`, `rate`, `total`, `sort`, `proposal_id`, `deleted`, `unit_sale`, `unit_sale_total`, `margin`) VALUES
(7, 'Phone', 'Lenovo', 2, 'NA', 20000, 40000, 0, 2, 1, 23000, 0, 0),
(8, 'Phone', 'Lenovo', 2, 'NA', 20000, 42000, 1, 2, 0, 23000, 48300, 6300),
(9, 'Phone', 'Lenovo', 1, 'NA', 20000, 20000, 3, 2, 0, 25000, 25000, 5000),
(10, 'Phone', 'Lenovo', 1, 'NA', 20000, 20000, 5, 2, 0, 25000, 25000, 5000),
(11, 'Phone', 'Lenovo', 1, 'NA', 20000, 20000, 7, 2, 0, 4000, 4000, -16000),
(12, 'Phone', 'Lenovo', 1, 'NA', 20000, 20000, 9, 2, 0, 23000, 23000, 3000);

-- --------------------------------------------------------

--
-- Table structure for table `proposal_templates`
--

CREATE TABLE `proposal_templates` (
  `id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `template` mediumtext DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `proposal_templates`
--

INSERT INTO `proposal_templates` (`id`, `title`, `template`, `deleted`) VALUES
(1, 'Proposal template 1', '<div style=\"display: flex; flex-direction: column;\">\n\n <table style=\"margin-top: 0; margin-bottom: 40px\">\n <tbody>\n <tr>\n <td style=\"padding: 0;\">\n <div style=\"background-size: cover; background-position: center top; background-image: url(\'https://fairsketch.com/media/proposal/bg-2.png\'); padding: 140px 10px 100px;\">\n <div style=\"color:#FFFFFF;font-size:50px;line-height:62px !important;font-weight:600;letter-spacing:0px; text-align: center;\">App Development<br>Proposal</div>\n <div style=\"color: #FFFFFF;\n font-size: 15px;\n text-align: center;\n  padding-top: 30px;\">We develop amazing apps for your business. <br>Here is our best offer for you.</div>\n     </div>\n </td>\n  </tr>\n        </tbody>\n    </table>We are excited to get to work on your new mobile app, and we want to make sure you are satisfied with our proposal and have a full understanding of what to expect in this lengthy process. Creating a mobile app is exciting and our expert team is fully capable of giving you something unique that will help grow your business.<table style=\"margin-top: 0; margin-bottom: 40px\"><tbody>\n        </tbody>\n    </table>\n\n    <table style=\"margin-top: 0; margin-bottom: 40px\">\n        <tbody>\n  <tr>\n <td style=\"padding: 0;\">\n     <div style=\"text-align: center\">\n         <div style=\"font-size: 30px\">{PROPOSAL_ID}</div>\n         <div style=\"height: 3px;\n  width: 80px;\n  background-color: #2AA384;\n  margin: 25px auto 20px;\"></div>\n         <div style=\"margin-bottom: 20px;\"><div style=\"text-align: center;\">Proposal Date:&nbsp;<span style=\"text-align: start;\">{PROPOSAL_DATE}&nbsp;</span></div><div style=\"text-align: center;\"><span style=\"text-align: start;\">Expiry Date:&nbsp;&nbsp;{PROPOSAL_EXPIRY_DATE}</span></div><div style=\"text-align: left;\"><br></div></div>\n     </div>\n     <div style=\"clear: both;\">\n         <div style=\"width: 50%; float: left;  padding-right: 10px\">\n <img src=\"https://fairsketch.com/media/proposal/proposal-to.png\" style=\"width: 100%; margin-bottom: 15px\">\n <p><i>Proposal For</i></p>\n <br>\n {PROPOSAL_TO_INFO}\n         </div>\n\n         <div style=\"width: 50%; float: left; padding-left: 10px\">\n <img src=\"https://fairsketch.com/media/proposal/proposal-from.png\" style=\"width: 100%; margin-bottom: 15px\">\n <p><i>Proposal From</i></p>\n <br>\n {COMPANY_INFO}\n         </div>\n     </div>\n </td>\n  </tr>\n        </tbody>\n    </table>\n\n    <table style=\"margin-top: 0; margin-bottom: 40px\">\n        <tbody>\n  <tr>\n <td style=\"padding: 0;\">\n     <div style=\"margin-top: 20px\">\n         <div style=\"text-align: center\">\n <div style=\"font-size: 30px\">Our Best Offer</div>\n <div style=\"height: 3px; width: 80px; background-color: #2AA384; margin: 25px auto 20px;\"></div>\n <div style=\"margin-bottom: 20px;\">We would like to develop a mobile app for your business. <br>Please feel free to contact us if you have any questions.</div>\n         </div>\n\n         <p>\n {PROPOSAL_ITEMS}\n         </p>\n     </div>\n </td>\n  </tr>\n        </tbody>\n    </table>\n\n    <table style=\"margin-top: 0; margin-bottom: 60px\">\n        <tbody>\n  <tr>\n <td style=\"padding: 0;\">\n\n     <div style=\"\">\n         <div style=\"text-align: center\">\n <div style=\"font-size: 30px\">Our Objective</div>\n <div style=\"height: 3px;\n   width: 80px;\n   background-color: #2AA384;\n   margin: 25px auto 20px;\"></div>\n <div style=\"margin-bottom: 20px;\">We build professional apps for business. Customer satisfaction is our main goal.&nbsp;</div><div style=\"margin-bottom: 20px;\"><br></div>\n         </div>\n\n         <div style=\"clear: both;\">\n <div style=\"width: 50%; float: left;  padding-right: 10px\">\n  <img src=\"https://fairsketch.com/media/proposal/phone-picture.png\" style=\"    display: block; border: 0;\n       width: 100%;\n       max-width: 290px;\">\n </div>\n\n <div style=\"width: 50%; float: left; padding-left: 10px\">\n  <div style=\"font-size:18px;line-height:28px;font-weight:600;letter-spacing:0px;padding:0;padding-bottom:20px; padding-top: 80px;\">\n      We Provide High Quality and Cost Effective Services.\n  </div>\n  <div style=\"font-size:15px;line-height:28px;letter-spacing:0px;padding:0;padding-bottom:30px;\">Our app development process contains different levels of testing. We can confirm you the largest amount of device support guaranty.</div>\n  <div>Work with us.</div>\n  <div style=\"font-size: 15px; color:  #2AA384; padding-top: 5px;\">Be a part of our happy customers.</div><div style=\"font-size: 15px; color:  #2AA384; padding-top: 5px;\"><br></div><div style=\"font-size: 15px; color:  #2AA384; padding-top: 5px;\"><br></div><br></div>\n         </div>\n     </div>\n\n </td>\n  </tr>\n        </tbody>\n    </table>\n\n    <table style=\"margin-top: 0; margin-bottom: 40px\">\n        <tbody>\n  <tr>\n <td style=\"padding: 0;\">\n\n     <div style=\"\">\n         <div style=\"text-align: center\">\n <div style=\"font-size: 30px\">Our Portfolio</div>\n <div style=\"height: 3px;\n   width: 80px;\n   background-color: #2AA384;\n   margin: 25px auto 20px;\"></div>\n <div style=\"margin-bottom: 20px;\">Some of our previous work here</div>\n         </div>\n\n         <div style=\"clear: both; margin: 0 -10px;\">\n <div style=\"width: 33.33%; float: left;  padding: 0 10px; margin-bottom: 20px\">\n  <img src=\"https://fairsketch.com/media/proposal/portfolio-1.png\" style=\"    display: block; border: 0;\n       width: 100%;\n       max-width: 290px;\">\n </div>\n <div style=\"width: 33.33%; float: left; padding: 0 10px; margin-bottom: 20px\">\n  <img src=\"https://fairsketch.com/media/proposal/portfolio-2.png\" style=\"    display: block; border: 0;\n       width: 100%;\n       max-width: 290px;\">\n </div>\n <div style=\"width: 33.33%; float: left;  padding: 0 10px; margin-bottom: 20px\">\n  <img src=\"https://fairsketch.com/media/proposal/portfolio-3.png\" style=\"    display: block; border: 0;\n       width: 100%;\n       max-width: 290px;\">\n </div>\n         </div>\n         <div style=\"clear: both; margin: 0 -10px;\">\n <div style=\"width: 33.33%; float: left;  padding: 0 10px; margin-bottom: 20px\">\n  <img src=\"https://fairsketch.com/media/proposal/portfolio-4.png\" style=\"    display: block; border: 0;\n       width: 100%;\n       max-width: 290px;\">\n </div>\n <div style=\"width: 33.33%; float: left; padding: 0 10px; margin-bottom: 20px\">\n  <img src=\"https://fairsketch.com/media/proposal/portfolio-5.png\" style=\"    display: block; border: 0;\n       width: 100%;\n       max-width: 290px;\">\n </div>\n <div style=\"width: 33.33%; float: left;  padding: 0 10px; margin-bottom: 20px\">\n  <img src=\"https://fairsketch.com/media/proposal/portfolio-6.png\" style=\"    display: block; border: 0;\n       width: 100%;\n       max-width: 290px;\">\n </div>\n         </div>\n\n     </div>\n\n </td>\n  </tr>\n        </tbody>\n    </table>\n\n    <table style=\"margin-top: 0; margin-bottom: 40px\">\n        <tbody>\n  <tr>\n <td style=\"padding: 0;\">\n\n     <div style=\"\">\n         <div style=\"text-align: center\">\n <div style=\"font-size: 30px\">Contact Us</div>\n <div style=\"height: 3px;\n   width: 80px;\n   background-color: #2AA384;\n   margin: 25px auto 20px;\"></div>\n <div style=\"margin-bottom: 20px;\"></div>\n         </div>\n\n         <p style=\"text-align: center; margin-bottom: 20px;\">We are looking forward working with you. Please feel free to contact us.&nbsp;</p><p style=\"text-align: center; margin-bottom: 20px;\"><br></p>\n         <div style=\"text-align: center;\"><img src=\"https://fairsketch.com/media/proposal/bg-1.png\" style=\"width: 100%;\"></div></div>\n\n </td>\n  </tr>\n        </tbody>\n    </table>\n\n    {PROPOSAL_NOTE}\n\n</div>', 0);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `permissions` mediumtext DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `title`, `permissions`, `deleted`) VALUES
(1, 'Lead Manager', 'a:47:{s:5:\"leave\";s:0:\"\";s:14:\"leave_specific\";s:0:\"\";s:10:\"attendance\";s:0:\"\";s:19:\"attendance_specific\";s:0:\"\";s:7:\"invoice\";s:9:\"read_only\";s:8:\"estimate\";s:3:\"own\";s:8:\"proposal\";s:0:\"\";s:7:\"expense\";s:0:\"\";s:5:\"order\";s:0:\"\";s:6:\"client\";s:3:\"all\";s:4:\"lead\";s:3:\"own\";s:6:\"ticket\";s:3:\"all\";s:15:\"ticket_specific\";s:0:\"\";s:12:\"announcement\";s:0:\"\";s:23:\"help_and_knowledge_base\";s:0:\"\";s:23:\"can_manage_all_projects\";N;s:19:\"can_create_projects\";N;s:17:\"can_edit_projects\";N;s:19:\"can_delete_projects\";N;s:30:\"can_add_remove_project_members\";N;s:16:\"can_create_tasks\";N;s:14:\"can_edit_tasks\";N;s:16:\"can_delete_tasks\";N;s:20:\"can_comment_on_tasks\";N;s:24:\"show_assigned_tasks_only\";N;s:37:\"can_update_only_assigned_tasks_status\";N;s:21:\"can_create_milestones\";N;s:19:\"can_edit_milestones\";N;s:21:\"can_delete_milestones\";N;s:16:\"can_delete_files\";N;s:34:\"can_view_team_members_contact_info\";N;s:34:\"can_view_team_members_social_links\";N;s:29:\"team_member_update_permission\";s:0:\"\";s:38:\"team_member_update_permission_specific\";s:0:\"\";s:27:\"timesheet_manage_permission\";s:0:\"\";s:36:\"timesheet_manage_permission_specific\";s:0:\"\";s:21:\"disable_event_sharing\";N;s:22:\"hide_team_members_list\";N;s:28:\"can_delete_leave_application\";N;s:18:\"message_permission\";s:0:\"\";s:27:\"message_permission_specific\";s:0:\"\";s:26:\"job_info_manage_permission\";s:0:\"\";s:32:\"can_manage_all_kinds_of_settings\";N;s:36:\"can_manage_user_role_and_permissions\";s:0:\"\";s:34:\"can_add_or_invite_new_team_members\";N;s:19:\"timeline_permission\";s:2:\"no\";s:28:\"timeline_permission_specific\";s:0:\"\";}', 0),
(2, 'Developer', 'a:47:{s:5:\"leave\";s:0:\"\";s:14:\"leave_specific\";s:0:\"\";s:10:\"attendance\";s:0:\"\";s:19:\"attendance_specific\";s:0:\"\";s:7:\"invoice\";s:9:\"read_only\";s:8:\"estimate\";s:3:\"own\";s:8:\"proposal\";s:0:\"\";s:7:\"expense\";s:0:\"\";s:5:\"order\";s:0:\"\";s:6:\"client\";s:3:\"all\";s:4:\"lead\";s:3:\"own\";s:6:\"ticket\";s:3:\"all\";s:15:\"ticket_specific\";s:0:\"\";s:12:\"announcement\";s:0:\"\";s:23:\"help_and_knowledge_base\";s:0:\"\";s:23:\"can_manage_all_projects\";N;s:19:\"can_create_projects\";N;s:17:\"can_edit_projects\";N;s:19:\"can_delete_projects\";N;s:30:\"can_add_remove_project_members\";N;s:16:\"can_create_tasks\";N;s:14:\"can_edit_tasks\";N;s:16:\"can_delete_tasks\";N;s:20:\"can_comment_on_tasks\";N;s:24:\"show_assigned_tasks_only\";N;s:37:\"can_update_only_assigned_tasks_status\";N;s:21:\"can_create_milestones\";N;s:19:\"can_edit_milestones\";N;s:21:\"can_delete_milestones\";N;s:16:\"can_delete_files\";N;s:34:\"can_view_team_members_contact_info\";N;s:34:\"can_view_team_members_social_links\";N;s:29:\"team_member_update_permission\";s:0:\"\";s:38:\"team_member_update_permission_specific\";s:0:\"\";s:27:\"timesheet_manage_permission\";s:0:\"\";s:36:\"timesheet_manage_permission_specific\";s:0:\"\";s:21:\"disable_event_sharing\";N;s:22:\"hide_team_members_list\";N;s:28:\"can_delete_leave_application\";N;s:18:\"message_permission\";s:0:\"\";s:27:\"message_permission_specific\";s:0:\"\";s:26:\"job_info_manage_permission\";s:0:\"\";s:32:\"can_manage_all_kinds_of_settings\";N;s:36:\"can_manage_user_role_and_permissions\";s:0:\"\";s:34:\"can_add_or_invite_new_team_members\";N;s:19:\"timeline_permission\";s:2:\"no\";s:28:\"timeline_permission_specific\";s:0:\"\";}', 0);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `setting_name` varchar(100) NOT NULL,
  `setting_value` mediumtext NOT NULL,
  `type` varchar(20) NOT NULL DEFAULT 'app',
  `deleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`setting_name`, `setting_value`, `type`, `deleted`) VALUES
('accepted_file_formats', 'jpg,jpeg,png,doc,xlsx,txt,pdf,zip', 'app', 0),
('allow_partial_invoice_payment_from_clients', '1', 'app', 0),
('allowed_ip_addresses', '', 'app', 0),
('app_title', 'Ioweb3 - CRM', 'app', 0),
('client_can_pay_invoice_without_login', '1', 'app', 0),
('company_address', '16A, Second Floor, \r\nDowntown City Vista, \r\nKharadi, \r\nPune - 411014', 'app', 0),
('company_email', 'sales@ioweb3.io', 'app', 0),
('company_name', 'IOWEB3 Technologies Private Limited', 'app', 0),
('company_phone', '8668521368', 'app', 0),
('company_vat_number', '27AAGCI5819J1Z8', 'app', 0),
('company_website', 'ioweb3.io', 'app', 0),
('conversion_rate', 'a:0:{}', 'app', 0),
('currency_position', 'left', 'app', 0),
('currency_symbol', '$', 'app', 0),
('date_format', 'Y-m-d', 'app', 0),
('decimal_separator', '.', 'app', 0),
('default_currency', 'USD', 'app', 0),
('default_due_date_after_billing_date', '', 'app', 0),
('default_template', '1', 'app', 0),
('default_theme_color', 'F2F2F2', 'app', 0),
('email_protocol', '', 'app', 0),
('email_sent_from_address', 'accounts@ioweb3.io', 'app', 0),
('email_sent_from_name', 'Sudarshan', 'app', 0),
('email_smtp_host', '', 'app', 0),
('email_smtp_pass', '04b019b291eaf97020e35b9fc79df8eeb2f655132bdacfc1a40ef98da35cbd6c07061d12258f3b85b3ab8878a6d4231f5d00c34d8cb3aa0b0485da0d4f086546d7abbf46f9798b8b35b61508952df3cbf7145b5894b483795e4d8e5209a8ca', 'app', 0),
('email_smtp_port', '', 'app', 0),
('email_smtp_security_type', 'none', 'app', 0),
('email_smtp_user', '', 'app', 0),
('enable_rich_text_editor', '0', 'app', 0),
('favicon', 'a:1:{s:9:\"file_name\";s:30:\"_file62eabdce93ef4-favicon.png\";}', 'app', 0),
('first_day_of_week', '0', 'app', 0),
('initial_number_of_the_invoice', '26', 'app', 0),
('invoice_color', '', 'app', 0),
('invoice_footer', '<p><br></p>', 'app', 0),
('invoice_logo', 'a:1:{s:9:\"file_name\";s:35:\"_file62eabd466a91c-invoice-logo.png\";}', 'app', 0),
('invoice_prefix', '', 'app', 0),
('invoice_style', 'style_1', 'app', 0),
('item_purchase_code', 'KEY-CODE', 'app', 0),
('language', 'english', 'app', 0),
('module_announcement', '1', 'app', 0),
('module_attendance', '1', 'app', 0),
('module_chat', '1', 'app', 0),
('module_estimate', '1', 'app', 0),
('module_estimate_request', '1', 'app', 0),
('module_event', '1', 'app', 0),
('module_expense', '1', 'app', 0),
('module_gantt', '1', 'app', 0),
('module_help', '1', 'app', 0),
('module_invoice', '1', 'app', 0),
('module_knowledge_base', '1', 'app', 0),
('module_lead', '1', 'app', 0),
('module_leave', '1', 'app', 0),
('module_message', '1', 'app', 0),
('module_note', '1', 'app', 0),
('module_order', '1', 'app', 0),
('module_project_timesheet', '1', 'app', 0),
('module_proposal', '1', 'app', 0),
('module_ticket', '1', 'app', 0),
('module_timeline', '1', 'app', 0),
('module_todo', '1', 'app', 0),
('no_of_decimals', '2', 'app', 0),
('rows_per_page', '10', 'app', 0),
('rtl', '0', 'app', 0),
('scrollbar', 'jquery', 'app', 0),
('send_bcc_to', 'sudarshanpatil72@gmail.com', 'app', 0),
('send_invoice_due_after_reminder', '', 'app', 0),
('send_invoice_due_pre_reminder', '', 'app', 0),
('send_recurring_invoice_reminder_before_creation', '', 'app', 0),
('show_background_image_in_signin_page', 'no', 'app', 0),
('show_logo_in_signin_page', 'no', 'app', 0),
('show_theme_color_changer', 'yes', 'app', 0),
('signin_page_background', 'sigin-background-image.jpg', 'app', 0),
('site_logo', 'a:1:{s:9:\"file_name\";s:32:\"_file62eabdcb387b2-site-logo.png\";}', 'app', 0),
('task_point_range', '5', 'app', 0),
('time_format', 'small', 'app', 0),
('timezone', 'Asia/Kolkata', 'app', 0),
('user_1_dashboard', '', 'user', 0),
('user_1_personal_language', 'english', 'user', 0),
('user_2_dashboard', '', 'user', 0),
('user_502_dashboard', '', 'user', 0),
('user_506_dashboard', '', 'user', 0),
('user_507_dashboard', '', 'user', 0),
('user_508_dashboard', '', 'user', 0),
('users_can_input_only_total_hours_instead_of_period', '1', 'app', 0),
('users_can_start_multiple_timers_at_a_time', '', 'app', 0),
('weekends', '', 'app', 0);

-- --------------------------------------------------------

--
-- Table structure for table `social_links`
--

CREATE TABLE `social_links` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `facebook` text DEFAULT NULL,
  `twitter` text DEFAULT NULL,
  `linkedin` text DEFAULT NULL,
  `googleplus` text DEFAULT NULL,
  `digg` text DEFAULT NULL,
  `youtube` text DEFAULT NULL,
  `pinterest` text DEFAULT NULL,
  `instagram` text DEFAULT NULL,
  `github` text DEFAULT NULL,
  `tumblr` text DEFAULT NULL,
  `vine` text DEFAULT NULL,
  `whatsapp` text DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `stripe_ipn`
--

CREATE TABLE `stripe_ipn` (
  `id` int(11) NOT NULL,
  `payment_intent` text NOT NULL,
  `verification_code` text NOT NULL,
  `payment_verification_code` text NOT NULL,
  `invoice_id` int(11) NOT NULL,
  `contact_user_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `payment_method_id` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tasks`
--

CREATE TABLE `tasks` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `description` mediumtext DEFAULT NULL,
  `project_id` int(11) NOT NULL,
  `milestone_id` int(11) NOT NULL DEFAULT 0,
  `assigned_to` int(11) NOT NULL,
  `deadline` date DEFAULT NULL,
  `labels` text DEFAULT NULL,
  `points` tinyint(4) NOT NULL DEFAULT 1,
  `status` enum('to_do','in_progress','done') NOT NULL DEFAULT 'to_do',
  `status_id` int(11) NOT NULL,
  `start_date` date DEFAULT NULL,
  `collaborators` text NOT NULL,
  `sort` int(11) NOT NULL DEFAULT 0,
  `recurring` tinyint(1) NOT NULL DEFAULT 0,
  `repeat_every` int(11) NOT NULL DEFAULT 0,
  `repeat_type` enum('days','weeks','months','years') DEFAULT NULL,
  `no_of_cycles` int(11) NOT NULL DEFAULT 0,
  `recurring_task_id` int(11) NOT NULL DEFAULT 0,
  `no_of_cycles_completed` int(11) NOT NULL DEFAULT 0,
  `created_date` date NOT NULL,
  `blocking` text NOT NULL,
  `blocked_by` text NOT NULL,
  `parent_task_id` int(11) NOT NULL,
  `next_recurring_date` date DEFAULT NULL,
  `reminder_date` date NOT NULL,
  `ticket_id` int(11) NOT NULL,
  `status_changed_at` datetime DEFAULT NULL,
  `deleted` tinyint(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tasks`
--

INSERT INTO `tasks` (`id`, `title`, `description`, `project_id`, `milestone_id`, `assigned_to`, `deadline`, `labels`, `points`, `status`, `status_id`, `start_date`, `collaborators`, `sort`, `recurring`, `repeat_every`, `repeat_type`, `no_of_cycles`, `recurring_task_id`, `no_of_cycles_completed`, `created_date`, `blocking`, `blocked_by`, `parent_task_id`, `next_recurring_date`, `reminder_date`, `ticket_id`, `status_changed_at`, `deleted`) VALUES
(1, 'Database Structure', 'Design Database Structure for the project.', 2, 0, 506, '2023-02-02', '', 1, 'to_do', 3, '2023-01-31', '506,507', 10000000, 0, 0, '', 0, 0, 0, '2023-01-31', '', '', 0, NULL, '0000-00-00', 0, '2023-01-31 14:22:34', 0),
(2, 'skyi', 'iso 9001', 4, 0, 1, '2024-03-21', '', 2, 'to_do', 1, '2024-03-19', '', 10000000, 0, 0, '', 0, 0, 0, '2024-03-19', '', '', 0, NULL, '0000-00-00', 0, '2024-03-20 06:55:01', 0),
(3, 'ioWeb3 Website', 'Complete website along with following:\r\n1. Content\r\n2. Images\r\n3. Admin Panel for Blog Posting, Additional Pages on website\r\n4. Website Inquiries to be listed on Admin Dashboard as well as email notification\r\n5. Careers Page complete functionality', 4, 0, 1, '2024-04-23', '1', 3, 'to_do', 1, '2024-04-16', '1', 0, 0, 0, '', 0, 0, 0, '2024-04-15', '', '', 0, NULL, '0000-00-00', 0, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `task_status`
--

CREATE TABLE `task_status` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `key_name` varchar(100) NOT NULL,
  `color` varchar(7) NOT NULL,
  `sort` int(11) NOT NULL,
  `hide_from_kanban` tinyint(1) NOT NULL DEFAULT 0,
  `deleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `task_status`
--

INSERT INTO `task_status` (`id`, `title`, `key_name`, `color`, `sort`, `hide_from_kanban`, `deleted`) VALUES
(1, 'To Do', 'to_do', '#F9A52D', 0, 0, 0),
(2, 'In progress', 'in_progress', '#1672B9', 1, 0, 0),
(3, 'Done', 'done', '#00B393', 10, 0, 0),
(4, 'Input Required', '', '#aab7b7', 2, 0, 0),
(5, 'Blocked', '', '#e74c3c', 3, 0, 0),
(6, 'In-QA', '', '#f1c40f', 4, 0, 0),
(7, 'QA-Passed', '', '#29c2c2', 5, 0, 0),
(8, 'QA-Failed', '', '#34495e', 6, 0, 0),
(9, 'Ready for Deploy', '', '#83c340', 7, 0, 0),
(10, 'In Production Testing', '', '#f1c40f', 8, 0, 0),
(11, 'Production Testing Failed', '', '#e74c3c', 9, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `taxes`
--

CREATE TABLE `taxes` (
  `id` int(11) NOT NULL,
  `title` tinytext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `percentage` double NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `taxes`
--

INSERT INTO `taxes` (`id`, `title`, `percentage`, `deleted`) VALUES
(1, 'Tax (10%)', 10, 0),
(2, 'GST (18%)', 18, 0),
(3, 'CGST', 2.5, 0),
(4, 'SGST', 2.5, 0),
(5, 'TDS (2%)', 2, 0),
(6, 'GST (5%)', 5, 0);

-- --------------------------------------------------------

--
-- Table structure for table `team`
--

CREATE TABLE `team` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `members` mediumtext NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `team_member_job_info`
--

CREATE TABLE `team_member_job_info` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `date_of_hire` date DEFAULT NULL,
  `deleted` int(1) NOT NULL DEFAULT 0,
  `salary` double NOT NULL DEFAULT 0,
  `salary_term` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `team_member_job_info`
--

INSERT INTO `team_member_job_info` (`id`, `user_id`, `date_of_hire`, `deleted`, `salary`, `salary_term`) VALUES
(1, 2, '2022-02-01', 0, 25000, 'Monthly'),
(2, 502, '2022-03-11', 0, 45000, '5.4'),
(3, 506, '2022-10-01', 0, 20000, ''),
(4, 507, '2022-10-01', 0, 20000, 'Test'),
(5, 508, '2022-08-01', 0, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `tickets`
--

CREATE TABLE `tickets` (
  `id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL DEFAULT 0,
  `ticket_type_id` int(11) NOT NULL,
  `title` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `requested_by` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `status` enum('new','client_replied','open','closed') NOT NULL DEFAULT 'new',
  `last_activity_at` datetime DEFAULT NULL,
  `assigned_to` int(11) NOT NULL DEFAULT 0,
  `creator_name` varchar(100) NOT NULL,
  `creator_email` varchar(255) NOT NULL,
  `labels` text DEFAULT NULL,
  `task_id` int(11) NOT NULL,
  `closed_at` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ticket_comments`
--

CREATE TABLE `ticket_comments` (
  `id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `description` mediumtext NOT NULL,
  `ticket_id` int(11) NOT NULL,
  `files` longtext DEFAULT NULL,
  `is_note` tinyint(1) NOT NULL DEFAULT 0,
  `deleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ticket_templates`
--

CREATE TABLE `ticket_templates` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `description` mediumtext NOT NULL,
  `ticket_type_id` int(11) NOT NULL,
  `private` mediumtext NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ticket_types`
--

CREATE TABLE `ticket_types` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ticket_types`
--

INSERT INTO `ticket_types` (`id`, `title`, `deleted`) VALUES
(1, 'General Support', 0);

-- --------------------------------------------------------

--
-- Table structure for table `to_do`
--

CREATE TABLE `to_do` (
  `id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `title` text NOT NULL,
  `description` mediumtext DEFAULT NULL,
  `labels` text DEFAULT NULL,
  `status` enum('to_do','done') NOT NULL DEFAULT 'to_do',
  `start_date` date DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `user_type` enum('staff','client','lead') NOT NULL DEFAULT 'client',
  `is_admin` tinyint(1) NOT NULL DEFAULT 0,
  `role_id` int(11) NOT NULL DEFAULT 0,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `image` text DEFAULT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `message_checked_at` datetime DEFAULT NULL,
  `client_id` int(11) NOT NULL DEFAULT 0,
  `notification_checked_at` datetime DEFAULT NULL,
  `is_primary_contact` tinyint(1) NOT NULL DEFAULT 0,
  `job_title` varchar(100) NOT NULL DEFAULT 'Untitled',
  `disable_login` tinyint(1) NOT NULL DEFAULT 0,
  `note` mediumtext DEFAULT NULL,
  `address` text DEFAULT NULL,
  `alternative_address` text DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `alternative_phone` varchar(20) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `ssn` varchar(20) DEFAULT NULL,
  `gender` enum('male','female') DEFAULT NULL,
  `sticky_note` mediumtext DEFAULT NULL,
  `skype` text DEFAULT NULL,
  `enable_web_notification` tinyint(1) NOT NULL DEFAULT 1,
  `enable_email_notification` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` datetime DEFAULT NULL,
  `last_online` datetime DEFAULT NULL,
  `requested_account_removal` tinyint(1) NOT NULL DEFAULT 0,
  `deleted` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `user_type`, `is_admin`, `role_id`, `email`, `password`, `image`, `status`, `message_checked_at`, `client_id`, `notification_checked_at`, `is_primary_contact`, `job_title`, `disable_login`, `note`, `address`, `alternative_address`, `phone`, `alternative_phone`, `dob`, `ssn`, `gender`, `sticky_note`, `skype`, `enable_web_notification`, `enable_email_notification`, `created_at`, `last_online`, `requested_account_removal`, `deleted`) VALUES
(1, 'Sudarshan', 'P', 'staff', 1, 0, 'sudarshan@ioweb3.io', '$2y$10$VgT85g.4o0MlYFQMeNA3HuSo/QIsbb6ThFMASunh3CT2fFzSt/hxu', NULL, 'active', '2024-04-24 09:25:45', 0, '2024-04-24 09:25:47', 0, 'CTO', 0, NULL, 'sudarshan@ioweb3.io', '9, A, New Ajay, Besides Dominoz, \r\nSenapati Bapat Road,\r\nPune- 411016\r\nMaharashtra, India', '+918668521368', '', '1992-07-20', '', 'male', '', 'sudarshanpatil72@gmail.com', 1, 1, '2021-01-28 06:35:26', '2024-04-24 12:18:11', 0, 0),
(2, 'Ruchita', 'Gupta', 'staff', 0, 0, 'ruchita@gmail.com', '$2y$10$V0Ofa0y/21lVrbit36rnLOwnJGrKwRhzhz5xmU.K4aZj4gCFC0E5W', NULL, 'active', NULL, 0, NULL, 0, 'Lead Manager', 0, NULL, '', NULL, 'ruchita@gmail.com', NULL, NULL, NULL, 'female', NULL, NULL, 1, 1, '2022-02-04 10:50:40', '2022-02-04 11:24:55', 0, 1),
(3, 'A', 'A', 'lead', 0, 0, 'mshamaskar@gmail.com', NULL, NULL, 'active', NULL, 1, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(4, 'B', 'B', 'lead', 0, 0, 'akankshadhekane73@gmail.com', NULL, NULL, 'active', NULL, 2, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(5, 'C', 'C', 'lead', 0, 0, 'shankarkundgire@gmail.com', NULL, NULL, 'active', NULL, 3, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(6, 'A', 'A', 'lead', 0, 0, 'janhavilokhande100@gmail.com', NULL, NULL, 'active', NULL, 4, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(7, 'B', 'B', 'lead', 0, 0, 'sale@goelganga.com', NULL, NULL, 'active', NULL, 5, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(8, 'C', 'C', 'lead', 0, 0, 'dobriyanurdin@gmail.com', NULL, NULL, 'active', NULL, 6, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(9, 'A', 'A', 'lead', 0, 0, 'bilkispathan6@gmail.com', NULL, NULL, 'active', NULL, 7, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(10, 'B', 'B', 'lead', 0, 0, 'priyankawayse@gmail.com', NULL, NULL, 'active', NULL, 8, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(11, 'C', 'C', 'lead', 0, 0, 'akshaysingheul@gmail.com', NULL, NULL, 'active', NULL, 9, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(12, 'A', 'A', 'lead', 0, 0, 'jayshrilone@gmail.com', NULL, NULL, 'active', NULL, 10, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(13, 'B', 'B', 'lead', 0, 0, 'suryawanshi.sagar06@gmail.com', NULL, NULL, 'active', NULL, 11, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(14, 'C', 'C', 'lead', 0, 0, 'ranjitrandive90@gmail.com', NULL, NULL, 'active', NULL, 12, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(15, 'A', 'A', 'lead', 0, 0, 'ankitpandey484@gmail.com', NULL, NULL, 'active', NULL, 13, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(16, 'B', 'B', 'lead', 0, 0, 'Pintu@yahoo.com', NULL, NULL, 'active', NULL, 14, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(17, 'C', 'C', 'lead', 0, 0, '', NULL, NULL, 'active', NULL, 15, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(18, 'A', 'A', 'lead', 0, 0, '', NULL, NULL, 'active', NULL, 16, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(19, 'B', 'B', 'lead', 0, 0, 'harshal.lodha1234@gmail.com', NULL, NULL, 'active', NULL, 17, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(20, 'C', 'C', 'lead', 0, 0, 'bladesofblood96@gmail.com', NULL, NULL, 'active', NULL, 18, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(21, 'A', 'A', 'lead', 0, 0, 'cspuathex201@gmail.com', NULL, NULL, 'active', NULL, 19, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(22, 'B', 'B', 'lead', 0, 0, 'kumarmatta90@gmail.com', NULL, NULL, 'active', NULL, 20, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(23, 'C', 'C', 'lead', 0, 0, 'amitrj21@gmail.com', NULL, NULL, 'active', NULL, 21, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(24, 'A', 'A', 'lead', 0, 0, 'christopher_nm@yahoo.com', NULL, NULL, 'active', NULL, 22, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(25, 'B', 'B', 'lead', 0, 0, 'Ritikaghanshani@gmail.com', NULL, NULL, 'active', NULL, 23, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(26, 'C', 'C', 'lead', 0, 0, 'ashokakt10@gmail.com', NULL, NULL, 'active', NULL, 24, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 1),
(27, 'A', 'A', 'lead', 0, 0, '', NULL, NULL, 'active', NULL, 25, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(28, 'B', 'B', 'lead', 0, 0, '', NULL, NULL, 'active', NULL, 26, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(29, 'C', 'C', 'lead', 0, 0, '', NULL, NULL, 'active', NULL, 27, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(30, 'A', 'A', 'lead', 0, 0, '', NULL, NULL, 'active', NULL, 28, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(31, 'B', 'B', 'lead', 0, 0, '', NULL, NULL, 'active', NULL, 29, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(32, 'C', 'C', 'lead', 0, 0, 'ndjagtap@gmail.com', NULL, NULL, 'active', NULL, 30, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(33, 'A', 'A', 'lead', 0, 0, 'minalchaudhari2594@gmail.com', NULL, NULL, 'active', NULL, 31, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(34, 'B', 'B', 'lead', 0, 0, '12thcomd1467ganeshj@gmail.com', NULL, NULL, 'active', NULL, 32, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(35, 'C', 'C', 'lead', 0, 0, 'ojha.abhi17@gmail.com', NULL, NULL, 'active', NULL, 33, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(36, 'A', 'A', 'lead', 0, 0, 'sanketpathare1998@gmail.com', NULL, NULL, 'active', NULL, 34, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(37, 'B', 'B', 'lead', 0, 0, 'sushmakthorat@gmail.com', NULL, NULL, 'active', NULL, 35, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(38, 'C', 'C', 'lead', 0, 0, 'mangalalandikar@gmail.com', NULL, NULL, 'active', NULL, 36, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(39, 'A', 'A', 'lead', 0, 0, 'tulinag.nag@gmail.com', NULL, NULL, 'active', NULL, 37, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(40, 'B', 'B', 'lead', 0, 0, 'vaibhav8810@gmail.com', NULL, NULL, 'active', NULL, 38, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(41, 'C', 'C', 'lead', 0, 0, 'kaushik.ankit@gmail.com', NULL, NULL, 'active', NULL, 39, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(42, 'A', 'A', 'lead', 0, 0, 'rakesh0198@gmail.com', NULL, NULL, 'active', NULL, 40, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(43, 'B', 'B', 'lead', 0, 0, 'priyankamurdia@gmail.com', NULL, NULL, 'active', NULL, 41, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(44, 'C', 'C', 'lead', 0, 0, 'megha.kangokar@gmail.com', NULL, NULL, 'active', NULL, 42, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(45, 'A', 'A', 'lead', 0, 0, 'pawarm501@gmail.com', NULL, NULL, 'active', NULL, 43, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(46, 'B', 'B', 'lead', 0, 0, 'anna@gmail.com', NULL, NULL, 'active', NULL, 44, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(47, 'C', 'C', 'lead', 0, 0, 'vijaykumarverma7345@gmail.com', NULL, NULL, 'active', NULL, 45, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(48, 'A', 'A', 'lead', 0, 0, 'sumitdeswal9@gmail.com', NULL, NULL, 'active', NULL, 46, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(49, 'B', 'B', 'lead', 0, 0, 'kariachirag033@gmail.com', NULL, NULL, 'active', NULL, 47, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(50, 'C', 'C', 'lead', 0, 0, '', NULL, NULL, 'active', NULL, 48, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(51, 'A', 'A', 'lead', 0, 0, 'akshaydongar123@gmail.com', NULL, NULL, 'active', NULL, 49, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(52, 'B', 'B', 'lead', 0, 0, 'jaingitika1992@gmail.com', NULL, NULL, 'active', NULL, 50, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(53, 'C', 'C', 'lead', 0, 0, '', NULL, NULL, 'active', NULL, 51, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(54, 'A', 'A', 'lead', 0, 0, 'ngsh.patil@rediffmail.com', NULL, NULL, 'active', NULL, 52, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(55, 'B', 'B', 'lead', 0, 0, 'agolf2216@gmail.com', NULL, NULL, 'active', NULL, 53, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(56, 'C', 'C', 'lead', 0, 0, '', NULL, NULL, 'active', NULL, 54, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(57, 'A', 'A', 'lead', 0, 0, 'abcrani321@gmail.com', NULL, NULL, 'active', NULL, 55, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(58, 'B', 'B', 'lead', 0, 0, 'nit27071990@gmail.com', NULL, NULL, 'active', NULL, 56, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(59, 'C', 'C', 'lead', 0, 0, 'sikander.pathan11@gmail.com', NULL, NULL, 'active', NULL, 57, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(60, 'A', 'A', 'lead', 0, 0, 'jogendrakumar622@gmail.com', NULL, NULL, 'active', NULL, 58, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(61, 'B', 'B', 'lead', 0, 0, 'amulkachhwaha@gmail.com', NULL, NULL, 'active', NULL, 59, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(62, 'C', 'C', 'lead', 0, 0, 'nalinibpawar23559@gmail.com', NULL, NULL, 'active', NULL, 60, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(63, 'A', 'A', 'lead', 0, 0, 'shubhangidongare111@gmail.com', NULL, NULL, 'active', NULL, 61, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(64, 'B', 'B', 'lead', 0, 0, 'bhatesameer@gmail.com', NULL, NULL, 'active', NULL, 62, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(65, 'C', 'C', 'lead', 0, 0, 'dattatrayshelar30@gmail.com', NULL, NULL, 'active', NULL, 63, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(66, 'A', 'A', 'lead', 0, 0, 'sandeep9ahluwalia@rediffmail.com', NULL, NULL, 'active', NULL, 64, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(67, 'B', 'B', 'lead', 0, 0, 'owner_9921619423@indiatimes.com', NULL, NULL, 'active', NULL, 65, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(68, 'C', 'C', 'lead', 0, 0, 'manish.holey@gmail.com', NULL, NULL, 'active', NULL, 66, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(69, 'A', 'A', 'lead', 0, 0, '', NULL, NULL, 'active', NULL, 67, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(70, 'B', 'B', 'lead', 0, 0, 'vkumar38872@gmail.com', NULL, NULL, 'active', NULL, 68, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(71, 'C', 'C', 'lead', 0, 0, 'ramchandraninamar691@gmail.com', NULL, NULL, 'active', NULL, 69, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(72, 'A', 'A', 'lead', 0, 0, 'nitinchougule8788@gmail.com', NULL, NULL, 'active', NULL, 70, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(73, 'B', 'B', 'lead', 0, 0, 'na@na.com', NULL, NULL, 'active', NULL, 71, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(74, 'C', 'C', 'lead', 0, 0, 'paixempire@gmail.com', NULL, NULL, 'active', NULL, 72, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(75, 'A', 'A', 'lead', 0, 0, 'ypardeshi07@gmail.com', NULL, NULL, 'active', NULL, 73, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(76, 'B', 'B', 'lead', 0, 0, 'devensabadra@gmail.com', NULL, NULL, 'active', NULL, 74, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(77, 'C', 'C', 'lead', 0, 0, 'rajanikonde24@gmail.com', NULL, NULL, 'active', NULL, 75, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(78, 'A', 'A', 'lead', 0, 0, 'poojary905@gmail.com', NULL, NULL, 'active', NULL, 76, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(79, 'B', 'B', 'lead', 0, 0, 'Ashah@gmail.com', NULL, NULL, 'active', NULL, 77, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(80, 'C', 'C', 'lead', 0, 0, 'sumeetchauhan92@gmail.com', NULL, NULL, 'active', NULL, 78, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(81, 'A', 'A', 'lead', 0, 0, 'samtasinha23@gmail.com', NULL, NULL, 'active', NULL, 79, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(82, 'B', 'B', 'lead', 0, 0, 'addictiveforsuccess@gmail.com', NULL, NULL, 'active', NULL, 80, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(83, 'C', 'C', 'lead', 0, 0, 'mukeshgaba2019@gmail.com', NULL, NULL, 'active', NULL, 81, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(84, 'A', 'A', 'lead', 0, 0, 'dsdeepa77@gmail.com', NULL, NULL, 'active', NULL, 82, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(85, 'B', 'B', 'lead', 0, 0, '', NULL, NULL, 'active', NULL, 83, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(86, 'C', 'C', 'lead', 0, 0, 'swaliakhan50@gmail.com', NULL, NULL, 'active', NULL, 84, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(87, 'A', 'A', 'lead', 0, 0, '', NULL, NULL, 'active', NULL, 85, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(88, 'B', 'B', 'lead', 0, 0, 'sulendrapratap@gmail.com', NULL, NULL, 'active', NULL, 86, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(89, 'C', 'C', 'lead', 0, 0, 'shetesarika06@gmail.com', NULL, NULL, 'active', NULL, 87, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(90, 'A', 'A', 'lead', 0, 0, 'rohitkadlag77@gmail.com', NULL, NULL, 'active', NULL, 88, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(91, 'B', 'B', 'lead', 0, 0, 'suvarnakarle97@gmail.com', NULL, NULL, 'active', NULL, 89, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(92, 'C', 'C', 'lead', 0, 0, 'abhishek.tamrakar08@gmail.com', NULL, NULL, 'active', NULL, 90, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(93, 'A', 'A', 'lead', 0, 0, 'prithvigunjal@gmail.com', NULL, NULL, 'active', NULL, 91, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(94, 'B', 'B', 'lead', 0, 0, 'vishal.vishlove2001@gmail.com', NULL, NULL, 'active', NULL, 92, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(95, 'C', 'C', 'lead', 0, 0, 'sumaiya.afsh@gmail.com', NULL, NULL, 'active', NULL, 93, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(96, 'A', 'A', 'lead', 0, 0, 'pawalenarayan@gmail.com', NULL, NULL, 'active', NULL, 94, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(97, 'B', 'B', 'lead', 0, 0, 'pushpendragautam23@gmail.com', NULL, NULL, 'active', NULL, 95, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(98, 'C', 'C', 'lead', 0, 0, 'jyoti.pawar8412@gmail.com', NULL, NULL, 'active', NULL, 96, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(99, 'A', 'A', 'lead', 0, 0, 'akanshajain0081@gmail.com', NULL, NULL, 'active', NULL, 97, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(100, 'B', 'B', 'lead', 0, 0, 'Amol@gmail.com', NULL, NULL, 'active', NULL, 98, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(101, 'C', 'C', 'lead', 0, 0, 'pburud.79@gmail.com', NULL, NULL, 'active', NULL, 99, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(102, 'A', 'A', 'lead', 0, 0, 'madhurishah1993@gmail.com', NULL, NULL, 'active', NULL, 100, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(103, 'B', 'B', 'lead', 0, 0, 'jasmantsisodiya483@gmail.com', NULL, NULL, 'active', NULL, 101, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(104, 'C', 'C', 'lead', 0, 0, 'kanshajain0081@gmail.com', NULL, NULL, 'active', NULL, 102, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(105, 'A', 'A', 'lead', 0, 0, 'nkkishore24@gmail.com', NULL, NULL, 'active', NULL, 103, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(106, 'B', 'B', 'lead', 0, 0, 'gensis1603@gmail.com', NULL, NULL, 'active', NULL, 104, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(107, 'C', 'C', 'lead', 0, 0, 'amirsohailbashird786@gmail.com', NULL, NULL, 'active', NULL, 105, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(108, 'A', 'A', 'lead', 0, 0, 'abhiyanta.mahesh@gmail.com', NULL, NULL, 'active', NULL, 106, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(109, 'B', 'B', 'lead', 0, 0, 'abhi2032@gmail.com', NULL, NULL, 'active', NULL, 107, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(110, 'C', 'C', 'lead', 0, 0, 'rohitchhabria@gmail.com', NULL, NULL, 'active', NULL, 108, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(111, 'A', 'A', 'lead', 0, 0, 'ad14jul@gmail.com', NULL, NULL, 'active', NULL, 109, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(112, 'B', 'B', 'lead', 0, 0, 'yojanaborate1@gmail.com', NULL, NULL, 'active', NULL, 110, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(113, 'C', 'C', 'lead', 0, 0, 'vishal_kale@outlook.com', NULL, NULL, 'active', NULL, 111, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(114, 'A', 'A', 'lead', 0, 0, 'anin_78@yahoo.com', NULL, NULL, 'active', NULL, 112, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(115, 'B', 'B', 'lead', 0, 0, 'sanjayagarwal6871@gmail.com', NULL, NULL, 'active', NULL, 113, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(116, 'C', 'C', 'lead', 0, 0, '', NULL, NULL, 'active', NULL, 114, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(117, 'A', 'A', 'lead', 0, 0, 'patilgirish_4@yahoo.co.in', NULL, NULL, 'active', NULL, 115, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(118, 'B', 'B', 'lead', 0, 0, 'vpjksolutions@gmail.com', NULL, NULL, 'active', NULL, 116, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(119, 'C', 'C', 'lead', 0, 0, 'karandikar.priya@gmail.com', NULL, NULL, 'active', NULL, 117, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(120, 'A', 'A', 'lead', 0, 0, 'swapnilbotre1@gmail.com', NULL, NULL, 'active', NULL, 118, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(121, 'B', 'B', 'lead', 0, 0, 'ssp391@gmail.com', NULL, NULL, 'active', NULL, 119, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(122, 'C', 'C', 'lead', 0, 0, 'kulkarnidigvijay63@gmail.com', NULL, NULL, 'active', NULL, 120, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(123, 'A', 'A', 'lead', 0, 0, 'vijayacotton@yahoo.co.in', NULL, NULL, 'active', NULL, 121, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(124, 'B', 'B', 'lead', 0, 0, 'priyankasherkhane73@gmail.com', NULL, NULL, 'active', NULL, 122, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(125, 'C', 'C', 'lead', 0, 0, 'er.umesh251991@gmail.com', NULL, NULL, 'active', NULL, 123, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(126, 'A', 'A', 'lead', 0, 0, 'shiva.niwas70@gmail.com', NULL, NULL, 'active', NULL, 124, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(127, 'B', 'B', 'lead', 0, 0, 'mangeshpillai_632@yahoo.co.in', NULL, NULL, 'active', NULL, 125, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(128, 'C', 'C', 'lead', 0, 0, 'ajaymoorthy@gmail.com', NULL, NULL, 'active', NULL, 126, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(129, 'A', 'A', 'lead', 0, 0, 'aghosh1231@hotmail.com', NULL, NULL, 'active', NULL, 127, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(130, 'B', 'B', 'lead', 0, 0, 'amolchaudhary57@gmail.com', NULL, NULL, 'active', NULL, 128, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(131, 'C', 'C', 'lead', 0, 0, 'anilmule5831@gmail.com', NULL, NULL, 'active', NULL, 129, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(132, 'A', 'A', 'lead', 0, 0, 'puneet.guitarist@gmail.com', NULL, NULL, 'active', NULL, 130, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(133, 'B', 'B', 'lead', 0, 0, 'support@housing.com', NULL, NULL, 'active', NULL, 131, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(134, 'C', 'C', 'lead', 0, 0, 'shreyashnaikwadi8989@gmail.com', NULL, NULL, 'active', NULL, 132, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(135, 'A', 'A', 'lead', 0, 0, 'supriyaraut78@yahoo.com', NULL, NULL, 'active', NULL, 133, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(136, 'B', 'B', 'lead', 0, 0, 'dinkarkhande@gmail.com', NULL, NULL, 'active', NULL, 134, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(137, 'C', 'C', 'lead', 0, 0, 'mominsp237@gmail.com', NULL, NULL, 'active', NULL, 135, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(138, 'A', 'A', 'lead', 0, 0, '', NULL, NULL, 'active', NULL, 136, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(139, 'B', 'B', 'lead', 0, 0, 'shashanks@gmail.com', NULL, NULL, 'active', NULL, 137, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(140, 'C', 'C', 'lead', 0, 0, 'sunny242kar@gmail.com', NULL, NULL, 'active', NULL, 138, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(141, 'A', 'A', 'lead', 0, 0, 'bhagwankhedkar2018@gmail.com', NULL, NULL, 'active', NULL, 139, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(142, 'B', 'B', 'lead', 0, 0, 'prashantpathak_1976@yahoo.co.in', NULL, NULL, 'active', NULL, 140, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(143, 'C', 'C', 'lead', 0, 0, 'kakad.amit@gmail.com', NULL, NULL, 'active', NULL, 141, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(144, 'A', 'A', 'lead', 0, 0, 'chaand5865@gmail.com', NULL, NULL, 'active', NULL, 142, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(145, 'B', 'B', 'lead', 0, 0, 'ankitachandekar134@gmail.com', NULL, NULL, 'active', NULL, 143, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(146, 'C', 'C', 'lead', 0, 0, 'jainraj1379@gmail.com', NULL, NULL, 'active', NULL, 144, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(147, 'A', 'A', 'lead', 0, 0, 'nidhi@gmail.com', NULL, NULL, 'active', NULL, 145, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(148, 'B', 'B', 'lead', 0, 0, 'purtyamit2014@gmail.com', NULL, NULL, 'active', NULL, 146, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(149, 'C', 'C', 'lead', 0, 0, 'relkhanaik09india@gmail.com', NULL, NULL, 'active', NULL, 147, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(150, 'A', 'A', 'lead', 0, 0, 'piyarai08@gmail.com', NULL, NULL, 'active', NULL, 148, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(151, 'B', 'B', 'lead', 0, 0, 'prchowdhary@ymail.com', NULL, NULL, 'active', NULL, 149, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(152, 'C', 'C', 'lead', 0, 0, 'mayachoudhary82@gmail.com', NULL, NULL, 'active', NULL, 150, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(153, 'A', 'A', 'lead', 0, 0, 'akhileshrocks786@gmail.com', NULL, NULL, 'active', NULL, 151, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(154, 'B', 'B', 'lead', 0, 0, 'kirns.lokhande@gmail.com', NULL, NULL, 'active', NULL, 152, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(155, 'C', 'C', 'lead', 0, 0, 'saurav.c161297@gmail.com', NULL, NULL, 'active', NULL, 153, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(156, 'A', 'A', 'lead', 0, 0, 'Gne.manish@gmail.com', NULL, NULL, 'active', NULL, 154, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(157, 'B', 'B', 'lead', 0, 0, 'abhishekstxaviet@gmail.com', NULL, NULL, 'active', NULL, 155, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(158, 'C', 'C', 'lead', 0, 0, 'vpakhode@gmail.com', NULL, NULL, 'active', NULL, 156, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(159, 'A', 'A', 'lead', 0, 0, 'gutriputri@gmail.com', NULL, NULL, 'active', NULL, 157, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(160, 'B', 'B', 'lead', 0, 0, 'kotemahesh16@gmail.com', NULL, NULL, 'active', NULL, 158, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(161, 'C', 'C', 'lead', 0, 0, 'kvraop@yahoo.com', NULL, NULL, 'active', NULL, 159, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(162, 'A', 'A', 'lead', 0, 0, 'santoshjot07@gmail.com', NULL, NULL, 'active', NULL, 160, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(163, 'B', 'B', 'lead', 0, 0, 'bhawsar.mitul@gmail.com', NULL, NULL, 'active', NULL, 161, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(164, 'C', 'C', 'lead', 0, 0, 'neerajhaval@gmail.com', NULL, NULL, 'active', NULL, 162, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(165, 'A', 'A', 'lead', 0, 0, 'ashishbedre2125@gmail.com', NULL, NULL, 'active', NULL, 163, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(166, 'B', 'B', 'lead', 0, 0, 'ashoksonawane80@gmail.com', NULL, NULL, 'active', NULL, 164, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(167, 'C', 'C', 'lead', 0, 0, 'aad15itya@gmail.com', NULL, NULL, 'active', NULL, 165, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(168, 'A', 'A', 'lead', 0, 0, 'amolhegana90@hotmail.com', NULL, NULL, 'active', NULL, 166, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(169, 'B', 'B', 'lead', 0, 0, 'swapnil.dhole20@gmail.com', NULL, NULL, 'active', NULL, 167, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(170, 'C', 'C', 'lead', 0, 0, 'rahullawrence94@gmail.com', NULL, NULL, 'active', NULL, 168, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(171, 'A', 'A', 'lead', 0, 0, 'harshadusane@gmail.com', NULL, NULL, 'active', NULL, 169, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(172, 'B', 'B', 'lead', 0, 0, 'Trupti.bhide@gmail.com', NULL, NULL, 'active', NULL, 170, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(173, 'C', 'C', 'lead', 0, 0, 'swapnilkamarikar2015@gmail.com', NULL, NULL, 'active', NULL, 171, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(174, 'A', 'A', 'lead', 0, 0, 'hussain.abbas84786@yahoo.com', NULL, NULL, 'active', NULL, 172, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(175, 'B', 'B', 'lead', 0, 0, 'deepakarki.021@gmail.com', NULL, NULL, 'active', NULL, 173, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(176, 'C', 'C', 'lead', 0, 0, 'Jabeenjamal87@gmail.com', NULL, NULL, 'active', NULL, 174, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(177, 'A', 'A', 'lead', 0, 0, '', NULL, NULL, 'active', NULL, 175, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(178, 'B', 'B', 'lead', 0, 0, 'amitkalinge@gmail.com', NULL, NULL, 'active', NULL, 176, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(179, 'C', 'C', 'lead', 0, 0, 'venkateshwaraprop@gmail.com', NULL, NULL, 'active', NULL, 177, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(180, 'A', 'A', 'lead', 0, 0, 'chaudharil1970@gmail.com', NULL, NULL, 'active', NULL, 178, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 1),
(181, 'B', 'B', 'lead', 0, 0, 'sgsarkar7@gmail.com', NULL, NULL, 'active', NULL, 179, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(182, 'C', 'C', 'lead', 0, 0, 'krishkhadke22@gmail.com', NULL, NULL, 'active', NULL, 180, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(183, 'A', 'A', 'lead', 0, 0, '', NULL, NULL, 'active', NULL, 181, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(184, 'B', 'B', 'lead', 0, 0, 'anupam.katara@gmail.com', NULL, NULL, 'active', NULL, 182, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(185, 'C', 'C', 'lead', 0, 0, 'valkal.bairagi@gmail.com', NULL, NULL, 'active', NULL, 183, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(186, 'A', 'A', 'lead', 0, 0, 'priyankatiwary9851@gmail.com', NULL, NULL, 'active', NULL, 184, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(187, 'B', 'B', 'lead', 0, 0, 'juhi3kulkarni@gmail.com', NULL, NULL, 'active', NULL, 185, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(188, 'C', 'C', 'lead', 0, 0, 'satputeajidtutxnkya@gmail.com', NULL, NULL, 'active', NULL, 186, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(189, 'A', 'A', 'lead', 0, 0, 'abhijitnath695@gmail.com', NULL, NULL, 'active', NULL, 187, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(190, 'B', 'B', 'lead', 0, 0, 'vinodparashar30@gmail.com', NULL, NULL, 'active', NULL, 188, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(191, 'C', 'C', 'lead', 0, 0, 'abhijeetbansal3@gmail.com', NULL, NULL, 'active', NULL, 189, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(192, 'A', 'A', 'lead', 0, 0, 'parate194@gmail.com', NULL, NULL, 'active', NULL, 190, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(193, 'B', 'B', 'lead', 0, 0, 'K.mahedh1978@gmail.com', NULL, NULL, 'active', NULL, 191, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(194, 'C', 'C', 'lead', 0, 0, 'vijaykumarlanke76@gmail.com', NULL, NULL, 'active', NULL, 192, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(195, 'A', 'A', 'lead', 0, 0, 'vikasjoshi1502@gmail.com', NULL, NULL, 'active', NULL, 193, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(196, 'B', 'B', 'lead', 0, 0, 'sheetalpatil102@gmail.com', NULL, NULL, 'active', NULL, 194, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(197, 'C', 'C', 'lead', 0, 0, 'nileshengg28@gmail.com', NULL, NULL, 'active', NULL, 195, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(198, 'A', 'A', 'lead', 0, 0, '', NULL, NULL, 'active', NULL, 196, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(199, 'B', 'B', 'lead', 0, 0, '', NULL, NULL, 'active', NULL, 197, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(200, 'C', 'C', 'lead', 0, 0, 'ravivarma142@gmail.com', NULL, NULL, 'active', NULL, 198, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(201, 'A', 'A', 'lead', 0, 0, 'swapnilgst2017@gmail.com', NULL, NULL, 'active', NULL, 199, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(202, 'B', 'B', 'lead', 0, 0, 'nilimadeshpandep@gmail.com', NULL, NULL, 'active', NULL, 200, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(203, 'C', 'C', 'lead', 0, 0, 'rgchauhan2000@gmail.com', NULL, NULL, 'active', NULL, 201, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(204, 'A', 'A', 'lead', 0, 0, 'geminiamriin151@gmail.com', NULL, NULL, 'active', NULL, 202, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(205, 'B', 'B', 'lead', 0, 0, 'dillip.meher@gmail.com', NULL, NULL, 'active', NULL, 203, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(206, 'C', 'C', 'lead', 0, 0, 'ridhikajog@gmail.com', NULL, NULL, 'active', NULL, 204, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(207, 'A', 'A', 'lead', 0, 0, 'bhowmick.atish@gmail.com', NULL, NULL, 'active', NULL, 205, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(208, 'B', 'B', 'lead', 0, 0, 'thotat1990@gmail.com', NULL, NULL, 'active', NULL, 206, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(209, 'C', 'C', 'lead', 0, 0, 'hemali2986@gmail.com', NULL, NULL, 'active', NULL, 207, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(210, 'A', 'A', 'lead', 0, 0, 'aishwaryabhandari2248@gmail.com', NULL, NULL, 'active', NULL, 208, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(211, 'B', 'B', 'lead', 0, 0, 'rshi.kotw77@gmail.com', NULL, NULL, 'active', NULL, 209, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(212, 'C', 'C', 'lead', 0, 0, 'dattachavan441970@gmail.com', NULL, NULL, 'active', NULL, 210, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(213, 'A', 'A', 'lead', 0, 0, 'yogeshshivpuje123@gmail.com', NULL, NULL, 'active', NULL, 211, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(214, 'B', 'B', 'lead', 0, 0, 'goteiqbal1964@gmail.com', NULL, NULL, 'active', NULL, 212, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(215, 'C', 'C', 'lead', 0, 0, 'ashutoshchauhan013@gmail.com', NULL, NULL, 'active', NULL, 213, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(216, 'A', 'A', 'lead', 0, 0, 'nilimasonawane19@gmail.com', NULL, NULL, 'active', NULL, 214, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(217, 'B', 'B', 'lead', 0, 0, 'monalirajwade@gmail.com', NULL, NULL, 'active', NULL, 215, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(218, 'C', 'C', 'lead', 0, 0, '', NULL, NULL, 'active', NULL, 216, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(219, 'A', 'A', 'lead', 0, 0, 'shetsayali1289@gmail.com', NULL, NULL, 'active', NULL, 217, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(220, 'B', 'B', 'lead', 0, 0, 'rohidas288@gmail.com', NULL, NULL, 'active', NULL, 218, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(221, 'C', 'C', 'lead', 0, 0, 'trvnsalunke.2006@gmail.com', NULL, NULL, 'active', NULL, 219, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(222, 'A', 'A', 'lead', 0, 0, 'dkhude20@gmail.com', NULL, NULL, 'active', NULL, 220, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(223, 'B', 'B', 'lead', 0, 0, 'valmikjagdale15@gmail.com', NULL, NULL, 'active', NULL, 221, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(224, 'C', 'C', 'lead', 0, 0, 'shindesantosh077@gmail.com', NULL, NULL, 'active', NULL, 222, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(225, 'A', 'A', 'lead', 0, 0, 'sudeepchavan17@gmail.com', NULL, NULL, 'active', NULL, 223, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(226, 'B', 'B', 'lead', 0, 0, 'santoshsalunke10101@gmail.com', NULL, NULL, 'active', NULL, 224, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(227, 'C', 'C', 'lead', 0, 0, 'navkarnavkar9@gmail.com', NULL, NULL, 'active', NULL, 225, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(228, 'A', 'A', 'lead', 0, 0, 'aksh55mhalaskar@gmail.com', NULL, NULL, 'active', NULL, 226, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(229, 'B', 'B', 'lead', 0, 0, 'kdnjoshi@rediffmail.com', NULL, NULL, 'active', NULL, 227, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(230, 'C', 'C', 'lead', 0, 0, 'meenakshi.joshi1604@gmail.com', NULL, NULL, 'active', NULL, 228, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(231, 'A', 'A', 'lead', 0, 0, 'Sunnynagdevi@rediffmail.com', NULL, NULL, 'active', NULL, 229, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(232, 'B', 'B', 'lead', 0, 0, 'snehajitshukla@gmail.com', NULL, NULL, 'active', NULL, 230, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0);
INSERT INTO `users` (`id`, `first_name`, `last_name`, `user_type`, `is_admin`, `role_id`, `email`, `password`, `image`, `status`, `message_checked_at`, `client_id`, `notification_checked_at`, `is_primary_contact`, `job_title`, `disable_login`, `note`, `address`, `alternative_address`, `phone`, `alternative_phone`, `dob`, `ssn`, `gender`, `sticky_note`, `skype`, `enable_web_notification`, `enable_email_notification`, `created_at`, `last_online`, `requested_account_removal`, `deleted`) VALUES
(233, 'C', 'C', 'lead', 0, 0, 'bbhanushali@hotmail.com', NULL, NULL, 'active', NULL, 231, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(234, 'A', 'A', 'lead', 0, 0, 'prav2004n@rediffmail.com', NULL, NULL, 'active', NULL, 232, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(235, 'B', 'B', 'lead', 0, 0, 'kiransalve112@gmail.com', NULL, NULL, 'active', NULL, 233, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(236, 'C', 'C', 'lead', 0, 0, 'purvajagdale@gmail.com', NULL, NULL, 'active', NULL, 234, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(237, 'A', 'A', 'lead', 0, 0, 'shahanawaj.shaikh@squareyards.co.in', NULL, NULL, 'active', NULL, 235, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(238, 'B', 'B', 'lead', 0, 0, '220698muskanshaikh@gmail.com', NULL, NULL, 'active', NULL, 236, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(239, 'C', 'C', 'lead', 0, 0, 'anilt159@gmail.com', NULL, NULL, 'active', NULL, 237, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(240, 'A', 'A', 'lead', 0, 0, 'rks.kumar2006@gmail.com', NULL, NULL, 'active', NULL, 238, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(241, 'B', 'B', 'lead', 0, 0, 'sharadrastogi916@gmail.com', NULL, NULL, 'active', NULL, 239, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(242, 'C', 'C', 'lead', 0, 0, 'tyagraj@gmail.com', NULL, NULL, 'active', NULL, 240, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(243, 'A', 'A', 'lead', 0, 0, 'vkumar.rachita@gmail.com', NULL, NULL, 'active', NULL, 241, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(244, 'B', 'B', 'lead', 0, 0, '', NULL, NULL, 'active', NULL, 242, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(245, 'C', 'C', 'lead', 0, 0, 'abhaykolapkar123@gmail.com', NULL, NULL, 'active', NULL, 243, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(246, 'A', 'A', 'lead', 0, 0, 'maliramesh999@gmail.com', NULL, NULL, 'active', NULL, 244, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(247, 'B', 'B', 'lead', 0, 0, 'deepikakansara56@gmail.com', NULL, NULL, 'active', NULL, 245, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(248, 'C', 'C', 'lead', 0, 0, 'rushi.de@gmail.com', NULL, NULL, 'active', NULL, 246, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(249, 'A', 'A', 'lead', 0, 0, 'niting19@gmail.com', NULL, NULL, 'active', NULL, 247, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(250, 'B', 'B', 'lead', 0, 0, 'ganesh.dandgavhal@gmail.com', NULL, NULL, 'active', NULL, 248, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(251, 'C', 'C', 'lead', 0, 0, '', NULL, NULL, 'active', NULL, 249, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(252, 'A', 'A', 'lead', 0, 0, 'sudhir972000@rediffmail.com', NULL, NULL, 'active', NULL, 250, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(253, 'B', 'B', 'lead', 0, 0, 'bhavsarrajendra64@gmail.com', NULL, NULL, 'active', NULL, 251, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(254, 'C', 'C', 'lead', 0, 0, 'nitu.pathak@sutherlandglobal.com', NULL, NULL, 'active', NULL, 252, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(255, 'A', 'A', 'lead', 0, 0, 'flyingrealestate@gmail.com', NULL, NULL, 'active', NULL, 253, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(256, 'B', 'B', 'lead', 0, 0, 'prerana.26oct@gmail.com', NULL, NULL, 'active', NULL, 254, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(257, 'C', 'C', 'lead', 0, 0, 'amitr.kumar@edelweiss.in', NULL, NULL, 'active', NULL, 255, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(258, 'A', 'A', 'lead', 0, 0, 'singh4877sk161095@gmail.com', NULL, NULL, 'active', NULL, 256, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(259, 'B', 'B', 'lead', 0, 0, 'aaliaraashid77@gmail.com', NULL, NULL, 'active', NULL, 257, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(260, 'C', 'C', 'lead', 0, 0, 'chabukswarsoniya8@gmail.com', NULL, NULL, 'active', NULL, 258, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(261, 'A', 'A', 'lead', 0, 0, 'ritu.gour7@gmail.com', NULL, NULL, 'active', NULL, 259, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(262, 'B', 'B', 'lead', 0, 0, 'dipaliloans123@gmail.com', NULL, NULL, 'active', NULL, 260, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(263, 'C', 'C', 'lead', 0, 0, 'manoj@wordbox.in', NULL, NULL, 'active', NULL, 261, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(264, 'A', 'A', 'lead', 0, 0, 'mayurdalavi183@gmail.com', NULL, NULL, 'active', NULL, 262, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(265, 'B', 'B', 'lead', 0, 0, 'anyata.s@gmail.com', NULL, NULL, 'active', NULL, 263, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(266, 'C', 'C', 'lead', 0, 0, 'preetamsamtani@gmail.com', NULL, NULL, 'active', NULL, 264, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(267, 'A', 'A', 'lead', 0, 0, 'risane3222@heroulo.com', NULL, NULL, 'active', NULL, 265, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(268, 'B', 'B', 'lead', 0, 0, 'sridivya.piratla@gmail.com', NULL, NULL, 'active', NULL, 266, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(269, 'C', 'C', 'lead', 0, 0, 'dsanchaykumar@gmail.com', NULL, NULL, 'active', NULL, 267, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(270, 'A', 'A', 'lead', 0, 0, 'csavita431@gmail.com', NULL, NULL, 'active', NULL, 268, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(271, 'B', 'B', 'lead', 0, 0, 'adeshuttekar68@gmail.com', NULL, NULL, 'active', NULL, 269, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(272, 'C', 'C', 'lead', 0, 0, 'joy.tew96@gmail.com', NULL, NULL, 'active', NULL, 270, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(273, 'A', 'A', 'lead', 0, 0, 'rkanurudh@gmail.com', NULL, NULL, 'active', NULL, 271, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(274, 'B', 'B', 'lead', 0, 0, 'harsimransinghchhabra@gmail.com', NULL, NULL, 'active', NULL, 272, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(275, 'C', 'C', 'lead', 0, 0, 'saieekulkarni59@gmail.com', NULL, NULL, 'active', NULL, 273, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(276, 'A', 'A', 'lead', 0, 0, 'vishalgavali1587@gmail.com', NULL, NULL, 'active', NULL, 274, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(277, 'B', 'B', 'lead', 0, 0, 'ajinkyap1011@gmail.com', NULL, NULL, 'active', NULL, 275, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(278, 'C', 'C', 'lead', 0, 0, 'tamhaneranjana@gmail.com', NULL, NULL, 'active', NULL, 276, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(279, 'A', 'A', 'lead', 0, 0, 'mukta.zip@gmail.com', NULL, NULL, 'active', NULL, 277, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(280, 'B', 'B', 'lead', 0, 0, 'drmegha7769@gmail.com', NULL, NULL, 'active', NULL, 278, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(281, 'C', 'C', 'lead', 0, 0, 'namratabarethiya83@gmail.com', NULL, NULL, 'active', NULL, 279, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(282, 'A', 'A', 'lead', 0, 0, 'rahul.pandule@techniche-engg.com', NULL, NULL, 'active', NULL, 280, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(283, 'B', 'B', 'lead', 0, 0, 'neeta.shori@gmail.com', NULL, NULL, 'active', NULL, 281, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(284, 'C', 'C', 'lead', 0, 0, 'rkawde@gmail.com', NULL, NULL, 'active', NULL, 282, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 1),
(285, 'A', 'A', 'lead', 0, 0, 'Nidhi.eeti@gmail.com', NULL, NULL, 'active', NULL, 283, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(286, 'B', 'B', 'lead', 0, 0, 'laptop@myself.com', NULL, NULL, 'active', NULL, 284, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(287, 'C', 'C', 'lead', 0, 0, 'vinodpokharna06@gmail.com', NULL, NULL, 'active', NULL, 285, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(288, 'A', 'A', 'lead', 0, 0, 'avi_shelar2011@rediffmail.com', NULL, NULL, 'active', NULL, 286, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(289, 'B', 'B', 'lead', 0, 0, 'adipanadare141093@gmail.com', NULL, NULL, 'active', NULL, 287, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(290, 'C', 'C', 'lead', 0, 0, 'priyadaundmath@gmail.com', NULL, NULL, 'active', NULL, 288, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(291, 'A', 'A', 'lead', 0, 0, 'robbininbox@gmail.com', NULL, NULL, 'active', NULL, 289, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(292, 'B', 'B', 'lead', 0, 0, 'akshayrandhir.37@gmail.com', NULL, NULL, 'active', NULL, 290, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(293, 'C', 'C', 'lead', 0, 0, 'ashvini.shirbhate105@gmail.com', NULL, NULL, 'active', NULL, 291, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(294, 'A', 'A', 'lead', 0, 0, 'er.aksomani@yahoo.com', NULL, NULL, 'active', NULL, 292, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(295, 'B', 'B', 'lead', 0, 0, 'Jittujais19922@gmail.com', NULL, NULL, 'active', NULL, 293, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(296, 'C', 'C', 'lead', 0, 0, 'rohan_kakade@ymail.com', NULL, NULL, 'active', NULL, 294, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(297, 'A', 'A', 'lead', 0, 0, 'sshahid95031@gmail.com', NULL, NULL, 'active', NULL, 295, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(298, 'B', 'B', 'lead', 0, 0, 'tushard877@gmail.com', NULL, NULL, 'active', NULL, 296, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(299, 'C', 'C', 'lead', 0, 0, 'swapnilstalent3@gnmail.com', NULL, NULL, 'active', NULL, 297, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(300, 'A', 'A', 'lead', 0, 0, 'ramprakashp021@gmail.com', NULL, NULL, 'active', NULL, 298, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(301, 'B', 'B', 'lead', 0, 0, 'someshwarghungase@1234gmile.com', NULL, NULL, 'active', NULL, 299, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(302, 'C', 'C', 'lead', 0, 0, '', NULL, NULL, 'active', NULL, 300, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(303, 'A', 'A', 'lead', 0, 0, 'firozaahanger565@gmail.com', NULL, NULL, 'active', NULL, 301, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(304, 'B', 'B', 'lead', 0, 0, 'avadh.sapre@gmail.com', NULL, NULL, 'active', NULL, 302, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(305, 'C', 'C', 'lead', 0, 0, 'koranne.preeti@gmail.com', NULL, NULL, 'active', NULL, 303, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(306, 'A', 'A', 'lead', 0, 0, 'aajinkya.4321@gamil.com', NULL, NULL, 'active', NULL, 304, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(307, 'B', 'B', 'lead', 0, 0, 'aaradhyamodak13@gmail.com', NULL, NULL, 'active', NULL, 305, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(308, 'C', 'C', 'lead', 0, 0, 'mudraa.shah@gmail.com', NULL, NULL, 'active', NULL, 306, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(309, 'A', 'A', 'lead', 0, 0, 'sumit82@yahoo.com', NULL, NULL, 'active', NULL, 307, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(310, 'B', 'B', 'lead', 0, 0, 'pawarpraphul16@gmail.com', NULL, NULL, 'active', NULL, 308, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(311, 'C', 'C', 'lead', 0, 0, 'adityapurandare88@gmail.com', NULL, NULL, 'active', NULL, 309, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(312, 'A', 'A', 'lead', 0, 0, 'pchavan@yahoo.com', NULL, NULL, 'active', NULL, 310, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(313, 'B', 'B', 'lead', 0, 0, 'gandhi_manali@yahoo.com', NULL, NULL, 'active', NULL, 311, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(314, 'C', 'C', 'lead', 0, 0, 'kshtj.gupta@gmail.com', NULL, NULL, 'active', NULL, 312, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(315, 'A', 'A', 'lead', 0, 0, 'vivek.krishna20@gmail.com', NULL, NULL, 'active', NULL, 313, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(316, 'B', 'B', 'lead', 0, 0, 'therahul.17@gmail.com', NULL, NULL, 'active', NULL, 314, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(317, 'C', 'C', 'lead', 0, 0, 'vinodcpr04@gmail.com', NULL, NULL, 'active', NULL, 315, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(318, 'A', 'A', 'lead', 0, 0, 'surahbhai995@gmail.com', NULL, NULL, 'active', NULL, 316, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(319, 'B', 'B', 'lead', 0, 0, '28abhi89@gmail.com', NULL, NULL, 'active', NULL, 317, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(320, 'C', 'C', 'lead', 0, 0, 'mohinikurekar1454@gmail.com', NULL, NULL, 'active', NULL, 318, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(321, 'A', 'A', 'lead', 0, 0, 'hmorkya0@gmail.com', NULL, NULL, 'active', NULL, 319, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(322, 'B', 'B', 'lead', 0, 0, 'vjpathak.59@gmail.com', NULL, NULL, 'active', NULL, 320, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(323, 'C', 'C', 'lead', 0, 0, 'kishor.kanase555@gmail.com', NULL, NULL, 'active', NULL, 321, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(324, 'A', 'A', 'lead', 0, 0, 'srivastava.srivastav@gmail.com', NULL, NULL, 'active', NULL, 322, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(325, 'B', 'B', 'lead', 0, 0, 'rajgaurikanchan@gmail.com', NULL, NULL, 'active', NULL, 323, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(326, 'C', 'C', 'lead', 0, 0, 'amitakaradkhedkar@gmail.com', NULL, NULL, 'active', NULL, 324, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(327, 'A', 'A', 'lead', 0, 0, 'rahul.plsr@gmail.com', NULL, NULL, 'active', NULL, 325, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(328, 'B', 'B', 'lead', 0, 0, 'adonfoods@gmail.com', NULL, NULL, 'active', NULL, 326, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(329, 'C', 'C', 'lead', 0, 0, 's.agrawal11@gmail.com', NULL, NULL, 'active', NULL, 327, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(330, 'A', 'A', 'lead', 0, 0, 'prashantbattellu@gmail.com', NULL, NULL, 'active', NULL, 328, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(331, 'B', 'B', 'lead', 0, 0, 'swapnakulkarni97@gmail.com', NULL, NULL, 'active', NULL, 329, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(332, 'C', 'C', 'lead', 0, 0, 'avinashjondhale18991@gmail.com', NULL, NULL, 'active', NULL, 330, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(333, 'A', 'A', 'lead', 0, 0, 'reenahaldankar@gmail.com', NULL, NULL, 'active', NULL, 331, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(334, 'B', 'B', 'lead', 0, 0, 'ranj.tiwari91@gmail.com', NULL, NULL, 'active', NULL, 332, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(335, 'C', 'C', 'lead', 0, 0, 'diwakarmandal239@gmail.com', NULL, NULL, 'active', NULL, 333, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(336, 'A', 'A', 'lead', 0, 0, 'swapnilwaghmode.1@gmail.com', NULL, NULL, 'active', NULL, 334, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(337, 'B', 'B', 'lead', 0, 0, 'baby.uma24@gmail.com', NULL, NULL, 'active', NULL, 335, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(338, 'C', 'C', 'lead', 0, 0, 'rajiv1k1wadhwa@gmail.com', NULL, NULL, 'active', NULL, 336, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(339, 'A', 'A', 'lead', 0, 0, 'ashwini.musale2@gmail.com', NULL, NULL, 'active', NULL, 337, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(340, 'B', 'B', 'lead', 0, 0, 'manewaryogesh@gmail.com', NULL, NULL, 'active', NULL, 338, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(341, 'C', 'C', 'lead', 0, 0, 'nitinppawar007@gmail.com', NULL, NULL, 'active', NULL, 339, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(342, 'A', 'A', 'lead', 0, 0, 'gawadedg@gmail.com', NULL, NULL, 'active', NULL, 340, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(343, 'B', 'B', 'lead', 0, 0, 'sanjay.bobate1969@gmail.com', NULL, NULL, 'active', NULL, 341, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(344, 'C', 'C', 'lead', 0, 0, 'kiran2bluved@yahoo.com', NULL, NULL, 'active', NULL, 342, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(345, 'A', 'A', 'lead', 0, 0, 'sahilpardeshi02890@gmail.com', NULL, NULL, 'active', NULL, 343, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(346, 'B', 'B', 'lead', 0, 0, 'py627374@gmail.com', NULL, NULL, 'active', NULL, 344, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(347, 'C', 'C', 'lead', 0, 0, 'vikasthombe2000@gmail.com', NULL, NULL, 'active', NULL, 345, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(348, 'A', 'A', 'lead', 0, 0, 'adv.brindagurbuxani@gmail.com', NULL, NULL, 'active', NULL, 346, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(349, 'B', 'B', 'lead', 0, 0, 'pradipdhumal2500@gmail.com', NULL, NULL, 'active', NULL, 347, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(350, 'C', 'C', 'lead', 0, 0, 'raju92@gamil.com', NULL, NULL, 'active', NULL, 348, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(351, 'A', 'A', 'lead', 0, 0, 'braverroland@gmail.com', NULL, NULL, 'active', NULL, 349, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(352, 'B', 'B', 'lead', 0, 0, 'Sanjeevjagtap4@gmail.com', NULL, NULL, 'active', NULL, 350, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(353, 'C', 'C', 'lead', 0, 0, 'nivedita_dhariwal@yahoo.com', NULL, NULL, 'active', NULL, 351, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(354, 'A', 'A', 'lead', 0, 0, 'sourabh.infinite@gmail.com', NULL, NULL, 'active', NULL, 352, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(355, 'B', 'B', 'lead', 0, 0, 'prashantmtrivedi@gmail.com', NULL, NULL, 'active', NULL, 353, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(356, 'C', 'C', 'lead', 0, 0, 'ashok.bhojne8888@gmail.com', NULL, NULL, 'active', NULL, 354, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(357, 'A', 'A', 'lead', 0, 0, 'roshanlewisroshanrkl379@gmail.com', NULL, NULL, 'active', NULL, 355, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(358, 'B', 'B', 'lead', 0, 0, 'kulkarnishubham229@gmail.com', NULL, NULL, 'active', NULL, 356, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(359, 'C', 'C', 'lead', 0, 0, 'speakt0nilesh@rediffmail.com', NULL, NULL, 'active', NULL, 357, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(360, 'A', 'A', 'lead', 0, 0, 'sunny.kunjir84@gmail.com', NULL, NULL, 'active', NULL, 358, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(361, 'B', 'B', 'lead', 0, 0, 'nileshdevgirikar@gmail.com', NULL, NULL, 'active', NULL, 359, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(362, 'C', 'C', 'lead', 0, 0, 'ajitpalsing123.rajput@gmail.com', NULL, NULL, 'active', NULL, 360, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(363, 'A', 'A', 'lead', 0, 0, 'debus2k@hotmail.com', NULL, NULL, 'active', NULL, 361, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(364, 'B', 'B', 'lead', 0, 0, 'swapnil3111989@gmail.com', NULL, NULL, 'active', NULL, 362, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(365, 'C', 'C', 'lead', 0, 0, 'animeshrockstheworld@gmail.com', NULL, NULL, 'active', NULL, 363, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(366, 'A', 'A', 'lead', 0, 0, 'ujwala.maghade@gmail.com', NULL, NULL, 'active', NULL, 364, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(367, 'B', 'B', 'lead', 0, 0, 'nilesh.lande12@gmail.com', NULL, NULL, 'active', NULL, 365, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(368, 'C', 'C', 'lead', 0, 0, 'nik69deshmukh@gmail.com', NULL, NULL, 'active', NULL, 366, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(369, 'A', 'A', 'lead', 0, 0, 'deepakjoshi933@gmail.com', NULL, NULL, 'active', NULL, 367, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(370, 'B', 'B', 'lead', 0, 0, 'drshitala@gmail.com', NULL, NULL, 'active', NULL, 368, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(371, 'C', 'C', 'lead', 0, 0, 'patilpravin8579@gmail.com', NULL, NULL, 'active', NULL, 369, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(372, 'A', 'A', 'lead', 0, 0, 'priyasha.t27@gmail.com', NULL, NULL, 'active', NULL, 370, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(373, 'B', 'B', 'lead', 0, 0, 'wase.prashant19@gmail.com', NULL, NULL, 'active', NULL, 371, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(374, 'C', 'C', 'lead', 0, 0, 'ankittandon_ca@yahoo.co.in', NULL, NULL, 'active', NULL, 372, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(375, 'A', 'A', 'lead', 0, 0, 'atoletushr2311@gmail.com', NULL, NULL, 'active', NULL, 373, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(376, 'B', 'B', 'lead', 0, 0, 'sksah3925@gmail.com', NULL, NULL, 'active', NULL, 374, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(377, 'C', 'C', 'lead', 0, 0, 'pravaja23@rediffmail.com', NULL, NULL, 'active', NULL, 375, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(378, 'A', 'A', 'lead', 0, 0, 'khanjavanoor786@gmail.com', NULL, NULL, 'active', NULL, 376, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(379, 'B', 'B', 'lead', 0, 0, 'sanketpatharr1998@gmail.com', NULL, NULL, 'active', NULL, 377, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(380, 'C', 'C', 'lead', 0, 0, 'aniketdixit629@gmail.com', NULL, NULL, 'active', NULL, 378, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(381, 'A', 'A', 'lead', 0, 0, '', NULL, NULL, 'active', NULL, 379, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(382, 'B', 'B', 'lead', 0, 0, '', NULL, NULL, 'active', NULL, 380, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(383, 'C', 'C', 'lead', 0, 0, 'milind.a.sable@gmail.com', NULL, NULL, 'active', NULL, 381, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(384, 'A', 'A', 'lead', 0, 0, 'sbpropconsultant@gmail.com', NULL, NULL, 'active', NULL, 382, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(385, 'B', 'B', 'lead', 0, 0, 'rmrahulmehrotra@gmail.com', NULL, NULL, 'active', NULL, 383, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(386, 'C', 'C', 'lead', 0, 0, '', NULL, NULL, 'active', NULL, 384, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(387, 'A', 'A', 'lead', 0, 0, 'yashashreedhotre@gmail.com', NULL, NULL, 'active', NULL, 385, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(388, 'B', 'B', 'lead', 0, 0, 'ankitashaha07@gmail.com', NULL, NULL, 'active', NULL, 386, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(389, 'C', 'C', 'lead', 0, 0, 'narenramse@gmail.com', NULL, NULL, 'active', NULL, 387, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(390, 'A', 'A', 'lead', 0, 0, 'pillay.niranjan@gmail.com', NULL, NULL, 'active', NULL, 388, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(391, 'B', 'B', 'lead', 0, 0, 'navnathshinde550@gmail.com', NULL, NULL, 'active', NULL, 389, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(392, 'C', 'C', 'lead', 0, 0, 'ramkrishna.facebook@gmail.com', NULL, NULL, 'active', NULL, 390, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(393, 'A', 'A', 'lead', 0, 0, 'nitkale03@gmail.com', NULL, NULL, 'active', NULL, 391, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(394, 'B', 'B', 'lead', 0, 0, 'karangadve@gimel.cam', NULL, NULL, 'active', NULL, 392, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(395, 'C', 'C', 'lead', 0, 0, 'gaurav1221@gmail.com', NULL, NULL, 'active', NULL, 393, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(396, 'A', 'A', 'lead', 0, 0, 'onlysandeep21@gmail.com', NULL, NULL, 'active', NULL, 394, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(397, 'B', 'B', 'lead', 0, 0, 'satyawan.sawant@rediffmail.com', NULL, NULL, 'active', NULL, 395, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(398, 'C', 'C', 'lead', 0, 0, 'harbha87700@gmail.com', NULL, NULL, 'active', NULL, 396, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(399, 'A', 'A', 'lead', 0, 0, 'skbadir028@gmail.com', NULL, NULL, 'active', NULL, 397, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(400, 'B', 'B', 'lead', 0, 0, 'pankaj.kende@rediffmail.com', NULL, NULL, 'active', NULL, 398, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(401, 'C', 'C', 'lead', 0, 0, 'kumarguddu070191@gmail.com', NULL, NULL, 'active', NULL, 399, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(402, 'A', 'A', 'lead', 0, 0, 'sanjay768@yahoo.co.in', NULL, NULL, 'active', NULL, 400, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(403, 'B', 'B', 'lead', 0, 0, 'roshan8986@gmail.com', NULL, NULL, 'active', NULL, 401, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(404, 'C', 'C', 'lead', 0, 0, 'gauti.khandelwal57@gmail.com', NULL, NULL, 'active', NULL, 402, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(405, 'A', 'A', 'lead', 0, 0, 'rushki89@gmail.com', NULL, NULL, 'active', NULL, 403, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(406, 'B', 'B', 'lead', 0, 0, 'aps.bhat29@gmail.com', NULL, NULL, 'active', NULL, 404, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(407, 'C', 'C', 'lead', 0, 0, '4u.amardeep@gmail.com', NULL, NULL, 'active', NULL, 405, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(408, 'A', 'A', 'lead', 0, 0, 'sandip.gugaliya@gmail.com', NULL, NULL, 'active', NULL, 406, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(409, 'B', 'B', 'lead', 0, 0, 'rajivinhell@gmail.com', NULL, NULL, 'active', NULL, 407, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(410, 'C', 'C', 'lead', 0, 0, 'manthansurana9@gmail.com', NULL, NULL, 'active', NULL, 408, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(411, 'A', 'A', 'lead', 0, 0, 'kumardip.it@gmail.com', NULL, NULL, 'active', NULL, 409, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(412, 'B', 'B', 'lead', 0, 0, 'riteshofcl@gmaiil.com', NULL, NULL, 'active', NULL, 410, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(413, 'C', 'C', 'lead', 0, 0, 'Srjhotkar@gmail.com', NULL, NULL, 'active', NULL, 411, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(414, 'A', 'A', 'lead', 0, 0, 'harshankbansal91@gmail.com', NULL, NULL, 'active', NULL, 412, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(415, 'B', 'B', 'lead', 0, 0, 'rahulcomp47@gmail.com', NULL, NULL, 'active', NULL, 413, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(416, 'C', 'C', 'lead', 0, 0, 'siddhi.ninawe@gmail.com', NULL, NULL, 'active', NULL, 414, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(417, 'A', 'A', 'lead', 0, 0, 'shubhkadam95@gmail.com', NULL, NULL, 'active', NULL, 415, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(418, 'B', 'B', 'lead', 0, 0, 'Rupeshmallade12@gmail.com', NULL, NULL, 'active', NULL, 416, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(419, 'C', 'C', 'lead', 0, 0, 'abhijeetcontrollu1977@gmail.com', NULL, NULL, 'active', NULL, 417, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(420, 'A', 'A', 'lead', 0, 0, 'chinmaysray@gmail.com', NULL, NULL, 'active', NULL, 418, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(421, 'B', 'B', 'lead', 0, 0, 'deepali.gadekar29@gmail.com', NULL, NULL, 'active', NULL, 419, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(422, 'C', 'C', 'lead', 0, 0, 's.kedar89@gmail.com', NULL, NULL, 'active', NULL, 420, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(423, 'A', 'A', 'lead', 0, 0, 'richa.sharma171785@gmail.com', NULL, NULL, 'active', NULL, 421, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(424, 'B', 'B', 'lead', 0, 0, 'shahwaizsheikh123@gmail.com', NULL, NULL, 'active', NULL, 422, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(425, 'C', 'C', 'lead', 0, 0, 'pjmore498@gmail.com', NULL, NULL, 'active', NULL, 423, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(426, 'A', 'A', 'lead', 0, 0, 'wazik2409@gmail.com', NULL, NULL, 'active', NULL, 424, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(427, 'B', 'B', 'lead', 0, 0, 'amitdange15@gmail.com', NULL, NULL, 'active', NULL, 425, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(428, 'C', 'C', 'lead', 0, 0, 'sanammunjal@gmail.com', NULL, NULL, 'active', NULL, 426, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(429, 'A', 'A', 'lead', 0, 0, 'vishwaspateliya@gmail.com', NULL, NULL, 'active', NULL, 427, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(430, 'B', 'B', 'lead', 0, 0, 'mithenkadam@gmail.com', NULL, NULL, 'active', NULL, 428, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(431, 'C', 'C', 'lead', 0, 0, 'nishita.bora@gmail.com', NULL, NULL, 'active', NULL, 429, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(432, 'A', 'A', 'lead', 0, 0, 'mwadhavankar@gmail.com', NULL, NULL, 'active', NULL, 430, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(433, 'B', 'B', 'lead', 0, 0, 'sq.ft01@gmail.com', NULL, NULL, 'active', NULL, 431, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(434, 'C', 'C', 'lead', 0, 0, 'priyapatil62025@gmail.com', NULL, NULL, 'active', NULL, 432, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(435, 'A', 'A', 'lead', 0, 0, 'avkokare@gmail.com', NULL, NULL, 'active', NULL, 433, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(436, 'B', 'B', 'lead', 0, 0, 'turate.praful@gmail.com', NULL, NULL, 'active', NULL, 434, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(437, 'C', 'C', 'lead', 0, 0, 'nileshyennal@gmail.com', NULL, NULL, 'active', NULL, 435, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(438, 'A', 'A', 'lead', 0, 0, 'manojkadam507@gmail.com', NULL, NULL, 'active', NULL, 436, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(439, 'B', 'B', 'lead', 0, 0, 'rajendrashelar21@gmail.com', NULL, NULL, 'active', NULL, 437, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(440, 'C', 'C', 'lead', 0, 0, 'pratikshabhavan6@gmail.com', NULL, NULL, 'active', NULL, 438, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(441, 'A', 'A', 'lead', 0, 0, 'compuankit@gmail.com', NULL, NULL, 'active', NULL, 439, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(442, 'B', 'B', 'lead', 0, 0, 'lilakarr@gmail.com', NULL, NULL, 'active', NULL, 440, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(443, 'C', 'C', 'lead', 0, 0, 'kk9664935@gmail.com', NULL, NULL, 'active', NULL, 441, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(444, 'A', 'A', 'lead', 0, 0, 'cstriptibhardwaj@gmail.com', NULL, NULL, 'active', NULL, 442, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(445, 'B', 'B', 'lead', 0, 0, 'manojmmulye@rediffmail.com', NULL, NULL, 'active', NULL, 443, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(446, 'C', 'C', 'lead', 0, 0, 'asrao25@yahoo.com', NULL, NULL, 'active', NULL, 444, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(447, 'A', 'A', 'lead', 0, 0, 'jyoti_plastics2005@yahoo.co.in', NULL, NULL, 'active', NULL, 445, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(448, 'B', 'B', 'lead', 0, 0, 'manjushree.tanwar@gmail.com', NULL, NULL, 'active', NULL, 446, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(449, 'C', 'C', 'lead', 0, 0, 'qtabassum358@gmail.com', NULL, NULL, 'active', NULL, 447, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(450, 'A', 'A', 'lead', 0, 0, 'jyotiag2002@gmail.com', NULL, NULL, 'active', NULL, 448, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(451, 'B', 'B', 'lead', 0, 0, 'ajeemshaikh4@gmail.com', NULL, NULL, 'active', NULL, 449, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(452, 'C', 'C', 'lead', 0, 0, 'ashurokade1999@gmail.com', NULL, NULL, 'active', NULL, 450, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(453, 'A', 'A', 'lead', 0, 0, 'ruchika.sanghavi93@gmail.com', NULL, NULL, 'active', NULL, 451, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(454, 'B', 'B', 'lead', 0, 0, 'josetomsy@gmail.com', NULL, NULL, 'active', NULL, 452, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(455, 'C', 'C', 'lead', 0, 0, 'pampasarovar@gmail.com', NULL, NULL, 'active', NULL, 453, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(456, 'A', 'A', 'lead', 0, 0, 'manishalwr@gmail.com', NULL, NULL, 'active', NULL, 454, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(457, 'B', 'B', 'lead', 0, 0, 'shindebharat38.sb@gmail.com', NULL, NULL, 'active', NULL, 455, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(458, 'C', 'C', 'lead', 0, 0, 'nishaddeshmukh1@gmail.com', NULL, NULL, 'active', NULL, 456, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(459, 'A', 'A', 'lead', 0, 0, 'arpita.99k@gmail.com', NULL, NULL, 'active', NULL, 457, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(460, 'B', 'B', 'lead', 0, 0, 'kwagalgave@gmail.com', NULL, NULL, 'active', NULL, 458, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(461, 'C', 'C', 'lead', 0, 0, 'vragho@gmail.com', NULL, NULL, 'active', NULL, 459, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(462, 'A', 'A', 'lead', 0, 0, 'deshpande.atul2010@gmail.com', NULL, NULL, 'active', NULL, 460, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(463, 'B', 'B', 'lead', 0, 0, 'nagrajbaraki@gmail.com', NULL, NULL, 'active', NULL, 461, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0);
INSERT INTO `users` (`id`, `first_name`, `last_name`, `user_type`, `is_admin`, `role_id`, `email`, `password`, `image`, `status`, `message_checked_at`, `client_id`, `notification_checked_at`, `is_primary_contact`, `job_title`, `disable_login`, `note`, `address`, `alternative_address`, `phone`, `alternative_phone`, `dob`, `ssn`, `gender`, `sticky_note`, `skype`, `enable_web_notification`, `enable_email_notification`, `created_at`, `last_online`, `requested_account_removal`, `deleted`) VALUES
(464, 'C', 'C', 'lead', 0, 0, 'moni_rai311@yahoo.co.in', NULL, NULL, 'active', NULL, 462, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(465, 'A', 'A', 'lead', 0, 0, 'suniltapdiya@gmail.com', NULL, NULL, 'active', NULL, 463, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(466, 'B', 'B', 'lead', 0, 0, 'priyankapriyanka4966@gmail.com', NULL, NULL, 'active', NULL, 464, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(467, 'C', 'C', 'lead', 0, 0, 'surajdahale02@gmail.com', NULL, NULL, 'active', NULL, 465, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(468, 'A', 'A', 'lead', 0, 0, 'jhil79@gmail.com', NULL, NULL, 'active', NULL, 466, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(469, 'B', 'B', 'lead', 0, 0, 'aaba.deshpande11@gmail.com', NULL, NULL, 'active', NULL, 467, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(470, 'C', 'C', 'lead', 0, 0, 'kartik.dhumal@gmail.com', NULL, NULL, 'active', NULL, 468, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(471, 'A', 'A', 'lead', 0, 0, 'mukeshgairola71@gmail.com', NULL, NULL, 'active', NULL, 469, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(472, 'B', 'B', 'lead', 0, 0, 'ganesh.ramanjan24@gmail.com', NULL, NULL, 'active', NULL, 470, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(473, 'C', 'C', 'lead', 0, 0, 'bipin4icici@gmail.com', NULL, NULL, 'active', NULL, 471, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(474, 'A', 'A', 'lead', 0, 0, 'drshantanupachkore@gmail.com', NULL, NULL, 'active', NULL, 472, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(475, 'B', 'B', 'lead', 0, 0, 'kunalvikamsey@gmail.com', NULL, NULL, 'active', NULL, 473, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(476, 'C', 'C', 'lead', 0, 0, 'varunakaduke@gmail.com', NULL, NULL, 'active', NULL, 474, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(477, 'A', 'A', 'lead', 0, 0, 'rahulg.kalpins@gmail.com', NULL, NULL, 'active', NULL, 475, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(478, 'B', 'B', 'lead', 0, 0, 'pranavdatar62@gmail.com', NULL, NULL, 'active', NULL, 476, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(479, 'C', 'C', 'lead', 0, 0, 'vijayinorganic@gmail.com', NULL, NULL, 'active', NULL, 477, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(480, 'A', 'A', 'lead', 0, 0, 'preeti.kadam08@gmail.com', NULL, NULL, 'active', NULL, 478, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(481, 'B', 'B', 'lead', 0, 0, 'amritmoonka@gmail.com', NULL, NULL, 'active', NULL, 479, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(482, 'C', 'C', 'lead', 0, 0, 'fahim.mohammed27@gmail.com', NULL, NULL, 'active', NULL, 480, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(483, 'A', 'A', 'lead', 0, 0, 'ranjitwale@rediffmail.com', NULL, NULL, 'active', NULL, 481, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(484, 'B', 'B', 'lead', 0, 0, 'shuklashubhra@gmail.com', NULL, NULL, 'active', NULL, 482, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(485, 'C', 'C', 'lead', 0, 0, 'gurpreet.parekh@gmail.com', NULL, NULL, 'active', NULL, 483, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(486, 'A', 'A', 'lead', 0, 0, 'umeshy2009@gmail.com', NULL, NULL, 'active', NULL, 484, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(487, 'B', 'B', 'lead', 0, 0, 'alam.adil.12345@gmail.com', NULL, NULL, 'active', NULL, 485, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(488, 'C', 'C', 'lead', 0, 0, 'bhugoms@rediffmail.com', NULL, NULL, 'active', NULL, 486, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(489, 'A', 'A', 'lead', 0, 0, 'rameshjanardhanmore@gmail.com', NULL, NULL, 'active', NULL, 487, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(490, 'B', 'B', 'lead', 0, 0, 'abhilashpachupate@gmail.com', NULL, NULL, 'active', NULL, 488, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(491, 'C', 'C', 'lead', 0, 0, 'jyotihomes111@gmail.com', NULL, NULL, 'active', NULL, 489, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(492, 'A', 'A', 'lead', 0, 0, '', NULL, NULL, 'active', NULL, 490, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(493, 'B', 'B', 'lead', 0, 0, 'piyalmisra@yahoo.com', NULL, NULL, 'active', NULL, 491, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(494, 'C', 'C', 'lead', 0, 0, 'kartikkamble122@gmail.com', NULL, NULL, 'active', NULL, 492, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(495, 'A', 'A', 'lead', 0, 0, 'khushbubandwal101@gmail.com', NULL, NULL, 'active', NULL, 493, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(496, 'B', 'B', 'lead', 0, 0, 'rs027377@gmail.com', NULL, NULL, 'active', NULL, 494, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(497, 'C', 'C', 'lead', 0, 0, 'khanna.ashish1@gmail.com', NULL, NULL, 'active', NULL, 495, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(498, 'A', 'A', 'lead', 0, 0, 'brpatil1559@gmail.com', NULL, NULL, 'active', NULL, 496, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(499, 'B', 'B', 'lead', 0, 0, 'r.v.dhadphale@gmail.com', NULL, NULL, 'active', NULL, 497, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(500, 'C', 'C', 'lead', 0, 0, 'swamiasha44@gmai.com', NULL, NULL, 'active', NULL, 498, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(501, 'A', 'A', 'lead', 0, 0, 'adeshdabh@gmail.com', NULL, NULL, 'active', NULL, 499, NULL, 1, 'Untitled', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-02-26 08:45:50', NULL, 0, 0),
(502, 'Bharat', 'Raikwar', 'staff', 0, 0, 'bhuwaneshwarraikwar@gmail.com', '$2y$10$BAAxzNOUL1RaA1zjrsewWug9j0wdWmg9wFaGG1Pi87SWRShVrn/tS', NULL, 'active', NULL, 0, NULL, 0, 'devloper', 0, NULL, 'Bhopal', NULL, '9039801521', NULL, NULL, NULL, 'male', NULL, NULL, 1, 1, '2022-03-11 17:40:39', '2022-03-11 17:41:02', 0, 1),
(503, 'Sopan', 'k', 'client', 0, 0, 'bhosaleankita76@gmail.com', '$2y$10$4TQ8jGbbDApb7mhaDUysgOmHxLdotPPxWywn0lELwYerjfbahplXm', NULL, 'active', NULL, 500, NULL, 1, 'NA', 0, '', NULL, NULL, '8888888888', NULL, NULL, NULL, 'male', NULL, 'NA', 1, 1, '2022-07-10 06:04:40', NULL, 0, 1),
(504, 'Sopan', 'k', 'client', 0, 0, 'bhosaleankita16@gmail.com', '$2y$10$L54K4XzxN6t5ZChrXHkfg.wGsIwseQMwpSmS5uMVK3QmYO0lsAMe2', NULL, 'active', NULL, 500, NULL, 1, 'Lead', 0, '', NULL, NULL, '8888888888', NULL, NULL, NULL, 'male', NULL, 'NA', 1, 1, '2022-07-10 11:10:44', NULL, 0, 1),
(505, 'Sopan', 'k', 'client', 0, 0, 'bhosaleankita76@gmail.com', '$2y$10$QqKFibhJrHz6hPtpPoes8ekWPZ28jeYBWKamu7tmM6zGBu6tA3yTK', NULL, 'active', NULL, 501, NULL, 1, 'sopan@codexxa.in', 0, '', NULL, NULL, '8888888888', NULL, NULL, NULL, 'male', NULL, 'NA', 1, 1, '2022-07-13 11:41:52', NULL, 0, 1),
(506, 'Viren', 'S', 'staff', 0, 0, 'admin@ioweb3.io', '$2y$10$YhdKfOtCDg8Qhz1dLdn7HuaPiLc4KSUM.aAYMykqZ.CzUu5fX4jui', NULL, 'active', NULL, 0, NULL, 0, 'Admin', 0, NULL, 'admin@ioweb3.io', NULL, '9999999999', NULL, NULL, NULL, 'male', NULL, NULL, 1, 1, '2023-01-31 13:31:41', '2023-01-31 14:23:54', 0, 1),
(507, 'Viren', 'S', 'staff', 0, 0, 'viren@ioweb3.io', '$2y$10$UHwJxcFbQoBW64Kehu6Qq.aztgQP9F48VXmgMKEWNFPQPEzjTwtUe', NULL, 'active', NULL, 0, NULL, 0, 'Admin', 0, NULL, 'admin@ioweb3.io', NULL, '9999999999', NULL, NULL, NULL, 'male', NULL, NULL, 1, 1, '2023-01-31 13:35:36', '2023-01-31 14:16:34', 0, 1),
(508, 'Shubham', 'Ramtekkar', 'staff', 0, 1, 'shubham@ioweb3.io', '$2y$10$NJVTTokZBurfh30QPOZQTu0s92W5/Fr0CBQaTlm24HT2HfL3XIsbq', NULL, 'active', NULL, 0, NULL, 0, 'Head of Technology', 0, NULL, 'work.shubhamramtekkar@gmail.com', NULL, '7875834429', NULL, NULL, NULL, 'male', NULL, NULL, 1, 1, '2023-03-24 09:35:05', '2023-08-21 07:17:37', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `verification`
--

CREATE TABLE `verification` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `type` enum('invoice_payment','reset_password','verify_email','invitation') NOT NULL,
  `code` varchar(10) NOT NULL,
  `params` text NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `verification`
--

INSERT INTO `verification` (`id`, `created_at`, `type`, `code`, `params`, `deleted`) VALUES
(1, '2022-07-10 11:08:58', 'invitation', 'aCSjnkSECp', 'a:4:{s:5:\"email\";s:25:\"bhosaleankita16@gmail.com\";s:4:\"type\";s:6:\"client\";s:9:\"client_id\";s:3:\"500\";s:11:\"expire_time\";i:1657537738;}', 0),
(0, '2024-04-24 09:04:47', 'invitation', 'YGYsTQPjef', 'a:3:{s:5:\"email\";s:24:\"rutujamore1601@gmail.com\";s:4:\"type\";s:5:\"staff\";s:11:\"expire_time\";i:1714035887;}', 0),
(0, '2024-04-24 09:21:59', 'invitation', 'CHTTNWMpUq', 'a:3:{s:5:\"email\";s:17:\"shubham@ioweb3.io\";s:4:\"type\";s:5:\"staff\";s:11:\"expire_time\";i:1714036919;}', 0),
(0, '2024-04-24 09:28:22', 'invitation', 'toPvsGYZZq', 'a:3:{s:5:\"email\";s:16:\"rutuja@ioweb3.io\";s:4:\"type\";s:5:\"staff\";s:11:\"expire_time\";i:1714037302;}', 0),
(0, '2024-04-24 09:33:42', 'invitation', 'hLPuLTBVnB', 'a:3:{s:5:\"email\";s:16:\"rutuja@ioweb3.io\";s:4:\"type\";s:5:\"staff\";s:11:\"expire_time\";i:1714037622;}', 0),
(0, '2024-04-24 09:34:54', 'invitation', 'DFxNFQgJIU', 'a:3:{s:5:\"email\";s:16:\"rutuja@ioweb3.io\";s:4:\"type\";s:5:\"staff\";s:11:\"expire_time\";i:1714037694;}', 0),
(0, '2024-04-24 10:38:18', 'invitation', 'wkKxvEeCMT', 'a:3:{s:5:\"email\";s:19:\"sudarshan@ioweb3.io\";s:4:\"type\";s:5:\"staff\";s:11:\"expire_time\";i:1714041498;}', 0),
(0, '2024-04-24 10:52:33', 'invitation', 'AvhJAVyrIW', 'a:3:{s:5:\"email\";s:19:\"sudarshan@ioweb3.io\";s:4:\"type\";s:5:\"staff\";s:11:\"expire_time\";i:1714042353;}', 0),
(0, '2024-04-24 11:02:43', 'invitation', 'ifPrDPuxgB', 'a:3:{s:5:\"email\";s:19:\"sudarshan@ioweb3.io\";s:4:\"type\";s:5:\"staff\";s:11:\"expire_time\";i:1714042963;}', 0),
(0, '2024-04-24 11:17:31', 'invitation', 'FsEddIZKXS', 'a:3:{s:5:\"email\";s:19:\"sudarshan@ioweb3.io\";s:4:\"type\";s:5:\"staff\";s:11:\"expire_time\";i:1714043851;}', 0),
(0, '2024-04-24 11:22:15', 'invitation', 'JlkHqZTjvE', 'a:3:{s:5:\"email\";s:19:\"sudarshan@ioweb3.io\";s:4:\"type\";s:5:\"staff\";s:11:\"expire_time\";i:1714044135;}', 0),
(0, '2024-04-24 11:28:18', 'invitation', 'LbHmSJaWNI', 'a:3:{s:5:\"email\";s:19:\"sudarshan@ioweb3.io\";s:4:\"type\";s:5:\"staff\";s:11:\"expire_time\";i:1714044498;}', 0),
(0, '2024-04-24 11:56:48', 'invitation', 'adXHlhtnNe', 'a:3:{s:5:\"email\";s:15:\"viren@ioweb3.io\";s:4:\"type\";s:5:\"staff\";s:11:\"expire_time\";i:1714046208;}', 0),
(0, '2024-04-24 12:02:39', 'invitation', 'OgivtJXBGg', 'a:3:{s:5:\"email\";s:15:\"viren@ioweb3.io\";s:4:\"type\";s:5:\"staff\";s:11:\"expire_time\";i:1714046559;}', 0),
(0, '2024-04-24 12:04:37', 'invitation', 'mQmzccougj', 'a:3:{s:5:\"email\";s:15:\"viren@ioweb3.io\";s:4:\"type\";s:5:\"staff\";s:11:\"expire_time\";i:1714046677;}', 0),
(0, '2024-04-24 12:09:08', 'invitation', 'oqVRgVdTpA', 'a:3:{s:5:\"email\";s:19:\"sudarshan@ioweb3.io\";s:4:\"type\";s:5:\"staff\";s:11:\"expire_time\";i:1714046948;}', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activity_logs`
--
ALTER TABLE `activity_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `announcements`
--
ALTER TABLE `announcements`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`);

--
-- Indexes for table `attendance`
--
ALTER TABLE `attendance`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `checked_by` (`checked_by`);

--
-- Indexes for table `certification`
--
ALTER TABLE `certification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `checklist_items`
--
ALTER TABLE `checklist_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `checklist_template`
--
ALTER TABLE `checklist_template`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD KEY `ci_sessions_timestamp` (`timestamp`);

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `client_groups`
--
ALTER TABLE `client_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `custom_fields`
--
ALTER TABLE `custom_fields`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `custom_field_values`
--
ALTER TABLE `custom_field_values`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `custom_widgets`
--
ALTER TABLE `custom_widgets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dashboards`
--
ALTER TABLE `dashboards`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email_templates`
--
ALTER TABLE `email_templates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `estimates`
--
ALTER TABLE `estimates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `estimate_comments`
--
ALTER TABLE `estimate_comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `estimate_forms`
--
ALTER TABLE `estimate_forms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `estimate_items`
--
ALTER TABLE `estimate_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `estimate_requests`
--
ALTER TABLE `estimate_requests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`);

--
-- Indexes for table `expenses`
--
ALTER TABLE `expenses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `expense_categories`
--
ALTER TABLE `expense_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `file_category`
--
ALTER TABLE `file_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `general_files`
--
ALTER TABLE `general_files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `help_articles`
--
ALTER TABLE `help_articles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `help_categories`
--
ALTER TABLE `help_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoices`
--
ALTER TABLE `invoices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoice_items`
--
ALTER TABLE `invoice_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoice_payments`
--
ALTER TABLE `invoice_payments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `id_2` (`id`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `item_categories`
--
ALTER TABLE `item_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `labels`
--
ALTER TABLE `labels`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `leads`
--
ALTER TABLE `leads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lead_source`
--
ALTER TABLE `lead_source`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lead_status`
--
ALTER TABLE `lead_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `leave_applications`
--
ALTER TABLE `leave_applications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `leave_type_id` (`leave_type_id`),
  ADD KEY `user_id` (`applicant_id`),
  ADD KEY `checked_by` (`checked_by`);

--
-- Indexes for table `leave_types`
--
ALTER TABLE `leave_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `likes`
--
ALTER TABLE `likes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `message_from` (`from_user_id`),
  ADD KEY `message_to` (`to_user_id`);

--
-- Indexes for table `milestones`
--
ALTER TABLE `milestones`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notes`
--
ALTER TABLE `notes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `notification_settings`
--
ALTER TABLE `notification_settings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `event` (`event`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_items`
--
ALTER TABLE `order_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_status`
--
ALTER TABLE `order_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_methods`
--
ALTER TABLE `payment_methods`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `paypal_ipn`
--
ALTER TABLE `paypal_ipn`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pin_comments`
--
ALTER TABLE `pin_comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project_comments`
--
ALTER TABLE `project_comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project_files`
--
ALTER TABLE `project_files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project_members`
--
ALTER TABLE `project_members`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `certification`
--
ALTER TABLE `certification`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
