<?php

namespace App\Models;

class Certification_model extends Crud_model
{
    protected $table = null;

    function __construct()
    {
        $this->table = 'certification';
        parent::__construct($this->table);
    }
}
