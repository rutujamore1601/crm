<?php

namespace App\Controllers;

class Clients extends Security_Controller
{
    function __construct()
    {
        parent::__construct();

        //check permission to access this module
        $this->init_permission_checker("certification");
    }
}
